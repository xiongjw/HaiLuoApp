//
//  AppDelegate.h
//  AXGY
//
//  Created by xiongjw on 16/5/24.
//  Copyright © 2016年 xiongjw. All rights reserved.
//

#import "X_Button.h"

#import <objc/runtime.h>

static const char * const X_ButtonBlockKey = "X_ButtonBlock";
static const char * const X_ButtonDragBlockKey = "X_ButtonDragBlock";

@implementation X_Button

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _isDragging = NO;
        [self setExclusiveTouch:YES];
    }
    return self;
}

- (void)setClickBlock:(X_ButtonBlock)block {
    objc_setAssociatedObject(self, X_ButtonBlockKey, block, OBJC_ASSOCIATION_COPY);
    [self addTarget:self action:@selector(buttonWasPressed) forControlEvents:UIControlEventTouchUpInside];
    [self setExclusiveTouch:YES];
}

- (void)setDragBlock:(X_ButtonDragBlock)block {
    objc_setAssociatedObject(self, X_ButtonDragBlockKey, block, OBJC_ASSOCIATION_COPY);
    [self addTarget:self action:@selector(dragMoving:withEvent: )
   forControlEvents: UIControlEventTouchDragInside];
    [self addTarget:self action:@selector(dragEnded:withEvent: )
   forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
}

- (void)buttonWasPressed {
    if (!_isDragging) {
        X_ButtonBlock block = objc_getAssociatedObject(self, X_ButtonBlockKey);
        if(block)
            block();
        //block = nil;
    }
}


- (void)dragMoving:(UIButton *)btn withEvent:event {
    _isDragging = YES;
    
    CGPoint point = [[[event allTouches] anyObject] locationInView:self.superview];
    CGFloat x = point.x;
    CGFloat y = point.y;
    
    CGFloat btnx = btn.frame.size.width / 2;
    CGFloat btny = btn.frame.size.height / 2;
    
    if (x <= btnx) point.x = btnx;
    if (x >= self.superview.bounds.size.width - btnx)  point.x = self.superview.bounds.size.width - btnx;
    
    if (y <= btny) point.y = btny;
    if (y >= self.superview.bounds.size.height - btny) point.y = self.superview.bounds.size.height - btny;
    
    btn.center = point;
}

- (void)dragEnded:(UIButton *)btn withEvent:event {
    //_isDragging = NO;
    //还原为止
    
    CGRect superviewFrame = self.superview.frame;
    CGRect frame = self.frame;
    [UIView animateWithDuration:0.2f animations:^{
        if (self.frame.origin.x <= superviewFrame.size.width / 2 - frame.size.width / 2) {
            self.center = CGPointMake(frame.size.width / 2, self.center.y);
        }
        else {
            self.center = CGPointMake(superviewFrame.size.width - frame.size.width / 2, self.center.y);
        }
       
    } completion:^(BOOL finished) {
        _isDragging = NO;
    }];
}
@end
