//
//  AppDelegate.h
//  AXGY
//
//  Created by xiongjw on 16/5/24.
//  Copyright © 2016年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^completeBlock) (NSInteger buttonIndex);

@interface UIActionSheet (XBlock) <UIActionSheetDelegate>

- (void)showActionSheetViewWithCompleteBlock:(completeBlock) block;

@end
