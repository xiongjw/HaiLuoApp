//
//  X_HttpUtil.m
//  旅行真人译
//
//  Created by xiongyw on 14-10-31.
//  Copyright (c) 2014年 CC. All rights reserved.
//

#import "X_HttpUtil.h"
//#import "X_LoadingView.h"
#import "X_Toast.h"

#define AXErroMsg @"网络异常,请稍后再试."

static AFHTTPSessionManager* sessionManager;
static NSInteger networkActivityCount;

@implementation X_HttpUtil

+ (NSString *)wholeRequestURLStr:(NSString *)urlStr
{
    return FormatString(@"%@%@",serverPath,urlStr);
}

+ (void)getRequestWithIntactUrl:(NSString *)intactUrl completionBlock:(void (^)(NSDictionary *resultData))completion
{
    intactUrl = [intactUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:intactUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (completion) {
            completion(data);
        }
    } failure:nil];
}

+ (NSURLSessionDataTask*)getRequestWithIntactUrl:(NSString *)intactUrl
                                           param:(NSDictionary *)param
                                            view:(UIView *)view
                                 completionBlock:(void (^)(NSDictionary *resultData))completion
{
    [self didStartNetworking];
    MBProgressHUD *hud = [self hudForView:view];
    
    return [[self sessionManager] GET:intactUrl parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //
        [self didStopNetworking];
        [hud hide:NO];
        if (completion) {
            completion(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //
        [self didStopNetworking];
        [hud hide:NO];
        if (completion) {
            completion(nil);
        }
    }];
    
}


+ (NSURLSessionDataTask*)postRequestWithIntactUrl:(NSString *)intactUrl
                                            param:(NSDictionary *)param
                                             view:(UIView *)view
                                  completionBlock:(void (^)(NSDictionary *resultData))completion
{
    MBProgressHUD *hud = [self hudForView:view];
    [self didStartNetworking];
    
    return [[self sessionManager] POST:intactUrl parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self didStopNetworking];
        [hud hide:NO];
        if (completion) {
            completion(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self didStopNetworking];
        [hud hide:NO];
        if (completion) {
            completion(nil);
        }
    }];
}

+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                       successBlock:(void (^)(NSDictionary *resultData))success
{
    return [self apiRequest:urlStr param:param view:view showErrorMessage:YES successBlock:success failureBlock:NULL];
}

+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(NSDictionary *resultData))failure
{
    return [self apiRequest:urlStr param:param view:view showErrorMessage:YES successBlock:success failureBlock:failure];
}

+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                   showErrorMessage:(BOOL)showErrorMessage
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(NSDictionary *resultData))failure
{
    MBProgressHUD *hud = [self hudForView:view];
    [self didStartNetworking];
    return [[self sessionManager] POST:[self wholeRequestURLStr:urlStr] parameters:[self apiParam:param] progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //
        [self didStopNetworking];
        [hud hide:NO];
        
        if ([self checkResponseData:responseObject showErrorMessage:showErrorMessage urlStr:urlStr])
        {
            if (success) success(responseObject);
        }
        else
        {
            if (failure) failure(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //
        [self didStopNetworking];
        [hud hide:NO];
        //取消网络请求
        if (error.code == -999) {
            return;
        }
        else if (error.code == -1011)
        {
            if ([@"login" isEqualToString:urlStr]) {
                if(showErrorMessage) [X_Toast showErrorWithStatus:FormatString(@"%@,请重新登录！",@"登录失效")];
            }
            else {
                if(showErrorMessage) [ShareApp logoutSystem:FormatString(@"%@,请重新登录！",@"登录失效")];
            }
            return;
        }
        if (showErrorMessage) [X_Toast showErrorWithStatus:AXErroMsg];
        if (failure) failure(nil);
    }];
    
}

//带二进制数据参数的请求
+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                   showErrorMessage:(BOOL)showErrorMessage
          constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> formData))constructingBodyWithBlock
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(void))failure
{
    MBProgressHUD *hud = [self hudForView:view];
    [self didStartNetworking];
    
    return [[self sessionManager] POST:[self wholeRequestURLStr:urlStr] parameters:[self apiParam:param] constructingBodyWithBlock:constructingBodyWithBlock progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //
        [self didStopNetworking];
        [hud hide:NO];
        if ([self checkResponseData:responseObject showErrorMessage:showErrorMessage]) {
            if (success) success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //
        [self didStopNetworking];
        [hud hide:NO];
        
        [X_Toast showErrorWithStatus:AXErroMsg];
        if (failure) failure();
    }];
    
}


+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                   showErrorMessage:(BOOL)showErrorMessage
                         imageArray:(NSArray*)imageArray
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(void))failure
{
    return [self apiRequest:urlStr param:param view:view showErrorMessage:showErrorMessage constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (UIImage* img in imageArray) {
            UIImage* currentImg = img;
            //图片尺寸超过服务器规定的大小，则等比缩放
            if (MAX(img.size.width, img.size.height) > PickPhotoMaxPixelSize) {
                CGFloat zoomRate = PickPhotoMaxPixelSize / MAX(img.size.width, img.size.height);
                CGSize newSize = CGSizeMake(img.size.width * zoomRate, img.size.height * zoomRate);
                UIGraphicsBeginImageContext(newSize);
                [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
                currentImg = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }
            //大小超出
            CGFloat byteSize = UIImageJPEGRepresentation(currentImg, 1).length;
            if (byteSize > 512 * 1024) {
                CGFloat q = byteSize > 1024 * 1024 ? 0.5 : 0.8;
                currentImg = [UIImage imageWithData:UIImageJPEGRepresentation(currentImg, q)];
            }
            [formData appendPartWithFileData:UIImageJPEGRepresentation(currentImg, 1)
                                        name:@"file"
                                    fileName:@"photo.jpg"
                                    mimeType:@"image/jpg"];
        }
        /*
        [formData appendPartWithFormData:[NSJSONSerialization dataWithJSONObject:@{@"title":@"华夏爱信"} options:NSJSONWritingPrettyPrinted error:nil]
                                    name:@"title"];
         */
    } successBlock:success failureBlock:failure];
}

+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                              image:(UIImage *)image
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(void))failure
{
    return [self apiRequest:urlStr param:param view:view showErrorMessage:YES constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        UIImage* currentImg = image;
        //图片尺寸超过服务器规定的大小，则等比缩放
        if (MAX(image.size.width, image.size.height) > PickPhotoMaxPixelSize) {
            CGFloat zoomRate = PickPhotoMaxPixelSize / MAX(image.size.width, image.size.height);
            CGSize newSize = CGSizeMake(image.size.width * zoomRate, image.size.height * zoomRate);
            UIGraphicsBeginImageContext(newSize);
            [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
            currentImg = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        //大小超出
        CGFloat byteSize = UIImageJPEGRepresentation(currentImg, 1).length;
        if (byteSize > 512 * 1024) {
            CGFloat q = byteSize > 1024 * 1024 ? 0.5 : 0.8;
            currentImg = [UIImage imageWithData:UIImageJPEGRepresentation(currentImg, q)];
        }
        [formData appendPartWithFileData:UIImageJPEGRepresentation(currentImg, 1)
                                    name:@"file"
                                fileName:@"photo.jpg"
                                mimeType:@"image/jpg"];
        
    } successBlock:success failureBlock:failure];
}

+ (void)cancelAllRequest
{
    [sessionManager.operationQueue cancelAllOperations];
    networkActivityCount = 0;
    [self didStopNetworking];
}


+ (NSDictionary *)apiParam:(NSDictionary*)paramDic
{
    /*
    if (paramDic) {
        paramDic = @{@"common":[self getParamHeader], @"params":paramDic};
    }
    else {
        paramDic = @{@"common":[self getParamHeader]};
    }
     */
    return paramDic;
}

+ (NSDictionary *)getParamHeader {
    return @{
             @"v":@"1.0",
             @"sign":@"",
             @"appversion":[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
             //@"versioncode":[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"],
             //@"timestamp":FormatString(@"%lld",[X_DataDeal getCurrentDate]),
             //@"devicemodel":[UIDevice currentDevice].model,
             @"deviceType":@"1",
             @"deviceVersion":[[UIDevice currentDevice] systemVersion],
             //@"deviceNo":[self getDeviceNo]
             //@"userid":@([X_UserData sharedInstance].userId)
             };
}

#pragma mark - 私有方法
+ (AFHTTPSessionManager*)sessionManager
{
    if (!sessionManager) {
        NSURLSessionConfiguration *normalConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        normalConfiguration.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
        normalConfiguration.timeoutIntervalForRequest = 15;
        normalConfiguration.timeoutIntervalForResource = 32;
        
        //ssl证书安全模式
        // 1.初始化
        sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:nil sessionConfiguration:normalConfiguration];
        sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        // 2.设置证书模式
        /*
        NSString *cerPath = nil;
        
        cerPath = [[NSBundle mainBundle] pathForResource:@"restapi_new_20171122" ofType:@"cer"];

        NSData *cerData = [NSData dataWithContentsOfFile:cerPath];
        
        sessionManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate withPinnedCertificates:[[NSSet alloc] initWithObjects:cerData, nil]];
        // 客户端是否信任非法证书
        sessionManager.securityPolicy.allowInvalidCertificates = YES;
        // 是否在证书域字段中验证域名
        [sessionManager.securityPolicy setValidatesDomainName:NO];
         */
    }
    // 这个失效比较快
    if ([ISSDataStorage hasObject:@"ISSUserTokenKey"]) {
        [sessionManager.requestSerializer setValue:[ISSDataStorage getObject:@"ISSUserTokenKey"] forHTTPHeaderField:@"Access-Token"];
    }
    [sessionManager.requestSerializer setValue:[[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode] forHTTPHeaderField:@"language"];
    return sessionManager;
}


+ (MBProgressHUD*)hudForView:(UIView*)view
{
    MBProgressHUD *hud = nil;
    if (view) {
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.labelText = @"加载中...";
        //hud.dimBackground = YES;
        //hud.mode = MBProgressHUDModeCustomView;
        hud.mode = MBProgressHUDModeIndeterminate;
        //hud.color = [UIColor clearColor];
        //hud.layer.cornerRadius = 14;
        //hud.customView = [self loadAnimatingView];
        hud.removeFromSuperViewOnHide = YES;
    }
    return hud;
}


+ (id)loadAnimatingView
{
    UIImage *image = [UIImage imageNamed:@"loading1"];
    CGRect frame = CGRectMake(0, 0, image.size.width, image.size.width);
    UIView* loadingView = [[UIView alloc] initWithFrame:frame];
    if (loadingView) {
        // Initialization code
        loadingView.backgroundColor = [UIColor clearColor];
        
        UIImage *image = [UIImage imageNamed:@"loading1"];
        
        UIImageView *loading = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        loading.center = loadingView.center;
        loading.image = image;
        NSMutableArray *loadings = [NSMutableArray new];
        for (int i = 1; i <= 34; i++) {
            [loadings addObject:[UIImage imageNamed:FormatString(@"loading%d",i)]];
        }
        
        loading.animationImages = loadings;
        loading.animationDuration = loadings.count * 0.1;
        loading.animationRepeatCount = MAXFLOAT;
        [loadingView addSubview:loading];
        
        
        [loading startAnimating];
    }
    return loadingView;
}


+ (void)didStartNetworking
{
    networkActivityCount++;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = networkActivityCount > 0;
}
+ (void) didStopNetworking
{
    networkActivityCount = MAX(0, networkActivityCount - 1);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = networkActivityCount > 0;
}


+ (BOOL)checkResponseData:(NSDictionary *)resultData showErrorMessage:(BOOL)showErrorMessage
{
    if ([resultData[@"ok"] boolValue])
    {
        return YES;
    }
    else
    {
        if (showErrorMessage)
        {
            NSString *msg = resultData[@"mess"];
            if ([@"-200" isEqualToString:resultData[@"code"]])
            {
                [ShareApp logoutSystem:FormatString(@"%@,%@！",msg,LoadLanguagesKey(@"network_relogin"))];
                return NO;
            }
            if (msg == nil || msg.length == 0) {
                msg = LoadLanguagesKey(@"network_error");
            }
            [X_Toast showErrorWithStatus:msg];
        }
        return NO;
    }
}

+ (BOOL)checkResponseData:(NSDictionary *)resultData showErrorMessage:(BOOL)showErrorMessage urlStr:(NSString *)urlStr
{
    if ([@"login" isEqualToString:urlStr])
    {
        if ([resultData[@"ok"] boolValue]) {
            return YES;
        }
        else
        {
            if (showErrorMessage)
            {
                NSString *msg = resultData[@"mess"];
                if ([@"失败" isEqualToString:msg]) {
                    msg = LoadLanguagesKey(@"login_nameAndPwdWrong");
                }
                if ([@"-200" isEqualToString:resultData[@"code"]])
                {
                    [ShareApp logoutSystem:FormatString(@"%@,%@！",msg,LoadLanguagesKey(@"network_relogin"))];
                    return NO;
                }
                if (msg == nil || msg.length == 0) {
                    msg = LoadLanguagesKey(@"network_error");
                }
                [X_Toast showErrorWithStatus:msg];
            }
            return NO;
        }
    }
    else
    {
        return [self checkResponseData:resultData showErrorMessage:showErrorMessage];
    }
    
}

@end
