//
//  ISSNavUtil.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSNavUtil.h"

@implementation ISSNavUtil

+ (NSString *)getImageName:(NSInteger)imageType {
    if (imageType == 1) return @"nav_back";
    else if (imageType == 2) return @"nav_search";
    else if (imageType == 3) return @"nav_add";
    else if (imageType == 4) return @"nav_search_gray";
    else if (imageType == 5) return @"nav_refresh";
    else if (imageType == 6) return @"nav_back_gray";
    return @"";
}

+ (UIImage *)getImage:(NSInteger)imageType {
    return [UIImage imageNamed:[self getImageName:imageType]];
}

+ (UIImage *)getImageHL:(NSInteger)imageType {
    return [UIImage imageNamed:FormatString(@"%@HL",[self getImageName:imageType])];
}

/**                                      */
+ (X_Button *)getImageBtn:(NSInteger)btnType
               clickBlock:(X_ButtonBlock)block
{
    /*
    UIImage *image = nil;
    if (btnType == 4) {
        image = [[self getImage:2] getNewImageWithColor:[UIColor describeColor_61]];
    }
    else if (btnType == 6) {
        image = [[self getImage:1] getNewImageWithColor:[UIColor describeColor_61]];
    }
    else {
        image = [self getImage:btnType];
    }
     */
    UIImage *image = [self getImage:btnType];
    
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeCustom];
    //btn.frame = CGRectMake(0.f, 0, image.size.width, image.size.height);
    //btn.frame = CGRectMake(0, 0, image.size.width+10, 44);
    btn.frame = CGRectMake(0, 0, 44, 44);
    [btn setImage:image forState:UIControlStateNormal];
    //[btn setImage:[self getImageHL:btnType] forState:UIControlStateHighlighted];
    if (btnType == 1 || btnType == 6)btn.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    if (block) [btn setClickBlock:block];
    return btn;
}

+ (X_Button *)getTitleBtn:(NSString *)btnTitle
               titleColor:(UIColor *)titleColor
               clickBlock:(X_ButtonBlock)block
{
    return [self getTitleBtn:btnTitle titleColor:titleColor titleFont:[UIFont systemFontOfSize:16] clickBlock:block];
}

+ (X_Button *)getTitleBtn:(NSString *)btnTitle
               titleColor:(UIColor *)titleColor
                titleFont:(UIFont *)titleFont
               clickBlock:(X_ButtonBlock)block
{
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    [btn sizeToFit];
    [btn.titleLabel setFont:titleFont];
    if (block) [btn setClickBlock:block];
    return btn;
}

+ (UIBarButtonItem *)getBarItem:(id)delegate
                        btnType:(NSInteger)btnType
                    clickAction:(SEL)clickAction
{
    UIButton *btn = [self getImageBtn:btnType clickBlock:nil];
    [btn addTarget:delegate action:clickAction forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    return bar;
}

+ (UIBarButtonItem *)getBarItem:(NSInteger)btnType
                     clickBlock:(X_ButtonBlock)block
{
    X_Button *btn = [self getImageBtn:btnType clickBlock:block];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    return bar;
}

+ (UIBarButtonItem *)getBarItemTitle:(NSString*)btnTitle
                          clickBlock:(X_ButtonBlock)block
{
    return [self getBarItemTitle:btnTitle titleColor:[UIColor themeColor] clickBlock:block];
}

+ (UIBarButtonItem *)getBarItemTitle:(NSString*)btnTitle
                          titleColor:(UIColor *)titleColor
                          clickBlock:(X_ButtonBlock)block
{
    X_Button *btn = [self getTitleBtn:btnTitle titleColor:titleColor clickBlock:block];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    
    return bar;
}

+ (UIBarButtonItem *)getBarItemTitle:(NSString*)btnTitle
                          titleColor:(UIColor *)titleColor
                           titleFont:(UIFont *)titleFont
                          clickBlock:(X_ButtonBlock)block
{
    X_Button *btn = [self getTitleBtn:btnTitle titleColor:titleColor titleFont:titleFont clickBlock:block];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    return bar;
}

@end

