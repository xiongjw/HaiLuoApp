//
//  T_ValidCheck.h
//  PowerMail
//
//  Created by xiongyw on 13-11-20.
//  Copyright (c) 2013年 xiongyw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface X_ValidCheck : NSObject

+ (BOOL)isValidEmail:(NSString *)checkString;
+ (BOOL)isValidPlateNo:(NSString *)checkString;
+ (BOOL)isValidCellPhoneNo:(NSString *)checkString;
+ (BOOL)isValidPostCode:(NSString *)checkString;
+ (BOOL)isValidIDNO:(NSString *)checkString;
+ (BOOL)isValidName:(NSString *)checkString;
+ (BOOL)isValidIDName:(NSString *)checkString;
+ (BOOL)isValidYear:(NSString *)checkString;
+ (BOOL)isValidNumber:(NSString *)checkString;
+ (BOOL)isValidVIN:(NSString *)checkString;//新修改，车架号不包括I，O
+ (BOOL)isDate:(NSString *)checkString;

+ (BOOL)isValidLength:(NSString *)checkString min:(int)min max:(int) max;

@end
