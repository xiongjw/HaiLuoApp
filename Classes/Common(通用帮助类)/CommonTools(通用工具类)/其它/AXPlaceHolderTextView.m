//
//  AXPlaceHolderTextView.m
//  AXGY
//
//  Created by xiongjw on 2017/1/16.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import "AXPlaceHolderTextView.h"

#ifdef __IPHONE_6_0
# define IFLY_ALIGN_CENTER NSTextAlignmentCenter
#else
# define IFLY_ALIGN_CENTER UITextAlignmentCenter
#endif

#ifdef __IPHONE_6_0
# define IFLY_WORDWRAPPING NSLineBreakByWordWrapping
#else
# define IFLY_WORDWRAPPING UILineBreakModeWordWrap
#endif

@implementation AXPlaceHolderTextView

@synthesize placeholder = _placeholder;
@synthesize placeholderColor = _placeholderColor;
@synthesize placeHolderLabel = _placeHolderLabel;

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if __has_feature(objc_arc)
#else
    [_placeHolderLabel release]; _placeHolderLabel = nil;
    [_placeholderColor release]; _placeholderColor = nil;
    [_placeholder release]; _placeholder = nil;
    [super dealloc];
#endif
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Use Interface Builder User Defined Runtime Attributes to set
    // placeholder and placeholderColor in Interface Builder.
    if (!self.placeholder) {
        [self setPlaceholder:@""];
    }
    
    if (!self.placeholderColor) {
        [self setPlaceholderColor:UIColorFromRGB(0x999999)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setPlaceholder:@""];
        [self setPlaceholderColor:UIColorFromRGB(0x999999)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)textChanged:(NSNotification *)notification
{
    if(self.placeholder.length != 0)
    {
        if(self.text.length == 0)
        {
            [[self viewWithTag:999] setAlpha:1];
        }
        else
        {
            [[self viewWithTag:999] setAlpha:0];
        }
    }
    /*
    if ([AXPubfun isContainsEmoji:self.text]) {
        self.text = [self.text substringToIndex:self.text.length-2];
    }
     */
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textChanged:nil];
}

- (void)drawRect:(CGRect)rect
{
    if( [[self placeholder] length] > 0 )
    {
        if (_placeHolderLabel == nil )
        {
            CGRect plFrame = CGRectMake(5,8,self.bounds.size.width - 10,0);
            if (self.textContainer.lineFragmentPadding == 0 && self.textContainerInset.left == 0 && self.textContainerInset.top == 0) {
                plFrame = CGRectMake(0, 0, self.bounds.size.width, 0);
            }
            _placeHolderLabel = [[UILabel alloc] initWithFrame:plFrame];
            _placeHolderLabel.lineBreakMode = IFLY_WORDWRAPPING;
            _placeHolderLabel.numberOfLines = 0;
            _placeHolderLabel.font = self.font;
            _placeHolderLabel.backgroundColor = [UIColor clearColor];
            _placeHolderLabel.textColor = self.placeholderColor;
            _placeHolderLabel.alpha = 0;
            _placeHolderLabel.tag = 999;
            [self addSubview:_placeHolderLabel];
        }
        
        _placeHolderLabel.text = self.placeholder;
        [_placeHolderLabel sizeToFit];
        [self sendSubviewToBack:_placeHolderLabel];
    }
    
    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
    {
        [[self viewWithTag:999] setAlpha:1];
    }
    
    [super drawRect:rect];
}

@end
