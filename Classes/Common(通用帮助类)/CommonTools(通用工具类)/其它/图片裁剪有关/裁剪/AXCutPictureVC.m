//
//  AXCutPictureVC.m
//  AXGY
//
//  Created by xiongjw on 2017/4/10.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import "AXCutPictureVC.h"

#import "AXImageCropperView.h"
#import "UIImage+AXExtension.h"

@interface AXCutPictureVC ()

@property (nonatomic,strong) AXImageCropperView *cropperView;

@end

@implementation AXCutPictureVC

#pragma mark - Action

- (void)cancelBtnTapped
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//完成裁剪
- (void)saveBtnTapped
{
    if (_completion) {
        _completion([_cropperView getCroppedImage]);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//旋转
- (void)rotateBtnTapped
{
    [_cropperView actionRotate];
}

- (instancetype)initWithCropImage:(UIImage*)cropImage cropSize:(CGSize)cropSize
{
    if (self = [super init]) {
        
        //裁剪View
        _cropperView = [[AXImageCropperView alloc] initWithCropImage:cropImage cropSize:cropSize];
        [self.view addSubview:_cropperView];
        
        UIView *toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, Screen_Height - 44, Screen_Width, 44)];
        toolbar.backgroundColor = [UIColor colorWithWhite:0.12f alpha:1.0f];
        [self.view addSubview:toolbar];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame = CGRectMake(8, 0, 60, 44);
        cancelBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:[UIColor colorWithRed:0.09 green:0.49 blue:1 alpha:1] forState:UIControlStateNormal];
        [cancelBtn setTitleColor:[UIColor colorWithRed:0.11 green:0.17 blue:0.26 alpha:1] forState:UIControlStateHighlighted];
        cancelBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [cancelBtn addTarget:self action:@selector(cancelBtnTapped) forControlEvents:UIControlEventTouchUpInside];
        [toolbar addSubview:cancelBtn];
        
        UIButton *cropBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cropBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        cropBtn.frame = CGRectMake(Screen_Width - 68, 0, 60, 44);
        [cropBtn setTitle:@"完成" forState:UIControlStateNormal];
        [cropBtn setTitleColor:[UIColor colorWithRed:1 green:0.8 blue:0 alpha:1] forState:UIControlStateNormal];
        [cropBtn setTitleColor:[UIColor colorWithRed:0.26 green:0.23 blue:0.13 alpha:1] forState:UIControlStateHighlighted];
        cropBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [cropBtn addTarget:self action:@selector(saveBtnTapped) forControlEvents:UIControlEventTouchUpInside];
        [toolbar addSubview:cropBtn];
        
        //旋转按钮
        UIButton *rotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];//button的类型
        rotateBtn.frame = CGRectMake(0, 0 ,44, 44);//button的frame
        [rotateBtn setCenterX:toolbar.centerX];
        [rotateBtn setImage:[UIImage imageNamed:@"rotate"] forState:UIControlStateNormal];
        [rotateBtn addTarget:self action:@selector(rotateBtnTapped) forControlEvents:UIControlEventTouchUpInside];
        
        [toolbar addSubview:rotateBtn];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
