//
//  AXImageCropperView.h
//  AXGY
//
//  Created by xiongjw on 2017/4/10.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXImageCropperView : UIView

/**
 *  图片裁剪初始化
 *
 *  @param cropImage 需要裁剪的图片
 *  @param cropSize  裁剪框的size 目前裁剪框的宽度为屏幕宽度
 *
 *  @return <#return value description#>
 */
- (instancetype)initWithCropImage:(UIImage*)cropImage cropSize:(CGSize)cropSize;

- (UIImage*)getCroppedImage;//获取裁剪后的图片

- (void) actionRotate;//旋转

@end
