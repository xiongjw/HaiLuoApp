//
//  AXCutPictureVC.h
//  AXGY
//
//  Created by xiongjw on 2017/4/10.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXCutPictureVC : UIViewController

@property (nonatomic,copy) void (^completion)(UIImage *finishImage);

/**
 *  图片裁剪界面初始化
 *
 *  @param cropImage 需要裁剪的图片
 *  @param cropSize  裁剪框的size
 *
 *  @return <#return value description#>
 */
- (instancetype)initWithCropImage:(UIImage*)cropImage cropSize:(CGSize)cropSize;

@end
