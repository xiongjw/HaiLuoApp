//
//  X_ImageCropperVC.h
//  WHZHT
//
//  Created by xiongjw on 15/7/13.
//  Copyright (c) 2015年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@class X_ImageCropperVC;

@protocol X_ImageCropperDelegate <NSObject>

- (void)imageCropperDidFinished:(X_ImageCropperVC *)cropperVC editedImage:(UIImage *)editedImage;
- (void)imageCropperDidCancel:(X_ImageCropperVC *)cropperVC;
@end

@interface X_ImageCropperVC : UIViewController

@property (nonatomic, assign) id<X_ImageCropperDelegate> delegate;
@property (nonatomic, assign) CGRect cropFrame;

- (instancetype)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame limitScaleRatio:(NSInteger)limitRatio;
@end
