//
//  AXChoosePhotoHelper.m
//  AXGY
//
//  Created by xiongjw on 2017/9/6.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import "AXChoosePhotoHelper.h"

#import "TZImagePickerController.h"
#import "TZImageManager.h"

@interface AXChoosePhotoHelper () <TZImagePickerControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic, weak) id delegate;

@property (nonatomic, copy) AXChoosePhotoBlock choosePhotoBlock;
@property (nonatomic, copy) AXChooseMorePhotoBlock chooseMorePhotoBlock;

@property (nonatomic, strong) UIImagePickerController *imagePickerVc;

@end

@implementation AXChoosePhotoHelper

+ (AXChoosePhotoHelper *)sharedInstance
{
    static AXChoosePhotoHelper *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[AXChoosePhotoHelper alloc] init];
    });
    return instance;
}

// 单选不裁剪
- (void)choosePhoto:(id)delegate
              block:(AXChoosePhotoBlock)block
{
    _delegate = delegate;
    _choosePhotoBlock = block;
    [self pushTZImagePickerController:1 cutPhoto:NO cutRate:0];
}

// 单选裁剪
- (void)chooseCutPhoto:(id)delegate
               cutRate:(CGFloat)cutRate
                 block:(AXChoosePhotoBlock)block
{
    _delegate = delegate;
    _choosePhotoBlock = block;
    [self pushTZImagePickerController:1 cutPhoto:YES cutRate:cutRate];
}

// 多选
- (void)chooseMorePhoto:(id)delegate
               maxCount:(NSInteger)maxCount
                  block:(AXChooseMorePhotoBlock)block
{
    _delegate = delegate;
    _chooseMorePhotoBlock = block;
    [self pushTZImagePickerController:maxCount cutPhoto:NO cutRate:0];
}

- (void)pushTZImagePickerController:(NSInteger)maxCount
                           cutPhoto:(BOOL)cutPhoto
                            cutRate:(CGFloat)cutRate
{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:maxCount columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    // imagePickerVc.navigationBar.translucent = NO;
    
#pragma mark - 五类个性化设置，这些参数都可以不传，此时会走默认设置

    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    
    // imagePickerVc.photoWidth = 1000;
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
    // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
    // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];
    // imagePickerVc.navigationBar.translucent = NO;
    
    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo = NO;
    //imagePickerVc.allowPickingImage = self.allowPickingImageSwitch.isOn;
    //imagePickerVc.allowPickingOriginalPhoto = self.allowPickingOriginalPhotoSwitch.isOn;
    //imagePickerVc.allowPickingGif = self.allowPickingGifSwitch.isOn;
    //imagePickerVc.allowPickingMultipleVideo = self.allowPickingMuitlpleVideoSwitch.isOn; // 是否可以多选视频
    
    // 4. 照片排列按修改时间升序
    //imagePickerVc.sortAscendingByModificationDate = self.sortAscendingSwitch.isOn;
    
    // imagePickerVc.minImagesCount = 3;
    // imagePickerVc.alwaysEnableDoneBtn = YES;
    
    // imagePickerVc.minPhotoWidthSelectable = 3000;
    // imagePickerVc.minPhotoHeightSelectable = 2000;
    
    /// 5. Single selection mode, valid when maxImagesCount = 1
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = NO;
    
    if (cutPhoto)
    {
        imagePickerVc.allowCrop = YES;
        imagePickerVc.needCircleCrop = NO;
        // 设置竖屏下的裁剪尺寸
        CGFloat width = Screen_Width;
        CGFloat height = width*cutRate;
        CGFloat top = (Screen_Height - height) / 2;
        imagePickerVc.cropRect = CGRectMake(0, top, width, height);
        // 设置横屏下的裁剪尺寸
        // imagePickerVc.cropRectLandscape = CGRectMake((ScreenFrame_Height - widthHeight) / 2, 0, widthHeight, widthHeight);
        /*
         [imagePickerVc setCropViewSettingBlock:^(UIView *cropView) {
         cropView.layer.borderColor = [UIColor redColor].CGColor;
         cropView.layer.borderWidth = 2.0;
         }];*/
    }
    
    //imagePickerVc.allowPreview = NO;
    
    imagePickerVc.statusBarStyle = UIStatusBarStyleLightContent;
#pragma mark - 到这里为止
    
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [_delegate presentViewController:imagePickerVc animated:YES completion:nil];
}


#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker
{
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto
{
    if (_choosePhotoBlock) {
        _choosePhotoBlock(photos[0]);
    }
    else
    {
        if (_chooseMorePhotoBlock) {
            _chooseMorePhotoBlock(photos);
        }
    }
}

//
- (UIImagePickerController *)imagePickerVc {
    if (_imagePickerVc == nil) {
        _imagePickerVc = [[UIImagePickerController alloc] init];
        //_imagePickerVc.allowsEditing = YES;
        _imagePickerVc.delegate = self;
        _imagePickerVc.sourceType = UIImagePickerControllerSourceTypeCamera;
        _imagePickerVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    return _imagePickerVc;
}
- (void)pushImagePickerController:(id)delegate
                            block:(AXChoosePhotoBlock)block
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        _delegate = delegate;
        _choosePhotoBlock = block;
        [_delegate presentViewController:self.imagePickerVc animated:YES completion:nil];
    }
    else {
        NSLog(@"模拟器中无法打开照相机,请在真机中使用");
    }
}
/*
- (void)pushImagePickerController:(id)delegate
                          cutRate:(CGFloat)cutRate
                            block:(AXChoosePhotoBlock)block
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        _delegate = delegate;
        _choosePhotoBlock = block;
        [_delegate presentViewController:_imagePickerVc animated:YES completion:nil];
    }
    else {
        NSLog(@"模拟器中无法打开照相机,请在真机中使用");
    }
}
 */

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    //图片存入相册
    [[TZImageManager manager] savePhotoWithImage:image completion:nil];
    if (_choosePhotoBlock) {
        _choosePhotoBlock(image);
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
