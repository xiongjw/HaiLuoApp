//
//  ISSPickerModel.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSPickerModel : NSObject

@property (nonatomic,  copy) NSString *title;//非必填
@property (nonatomic,strong) NSArray *dataSoure;//数据选择器必要
@property (nonatomic,  copy) NSString *codeKey;//非必填
@property (nonatomic,  copy) NSString *nameKey;//非必填
@property (nonatomic,  copy) NSString *value;
@property (nonatomic,  copy) NSString *showValue;
@property (nonatomic)        NSInteger type;//必填
@property (nonatomic)        BOOL needMin;//时间选择器 是否有最小（以当前时间）
@property (nonatomic,  copy) NSString *needMinValue;
@property (nonatomic)        BOOL needMax;//时间选择器 是否有最大（以当前时间）
@property (nonatomic,  copy) NSString *needMaxValue;

- (instancetype)init;

@end
