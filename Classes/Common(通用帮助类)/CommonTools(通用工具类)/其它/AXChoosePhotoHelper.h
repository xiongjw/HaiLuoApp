//
//  AXChoosePhotoHelper.h
//  AXGY
//
//  Created by xiongjw on 2017/9/6.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^AXChoosePhotoBlock) (UIImage *photo);
typedef void (^AXChooseMorePhotoBlock) (NSArray<UIImage *> *photos);

@interface AXChoosePhotoHelper : NSObject

+ (AXChoosePhotoHelper *)sharedInstance;

// 单选不裁剪
- (void)choosePhoto:(id)delegate
              block:(AXChoosePhotoBlock)block;

// 单选裁剪
- (void)chooseCutPhoto:(id)delegate
               cutRate:(CGFloat)cutRate
                 block:(AXChoosePhotoBlock)block;

// 多选
- (void)chooseMorePhoto:(id)delegate
               maxCount:(NSInteger)maxCount
                  block:(AXChooseMorePhotoBlock)block;

// 调用相机,图片不裁剪
- (void)pushImagePickerController:(id)delegate
                            block:(AXChoosePhotoBlock)block;

// 调用相机,裁剪
/*
- (void)pushImagePickerController:(id)delegate
                          cutRate:(CGFloat)cutRate
                            block:(AXChoosePhotoBlock)block;
 */

@end
