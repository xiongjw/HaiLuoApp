//
//  AXMyTextView.m
//  AXGY
//
//  Created by xiongjw on 16/7/12.
//  Copyright © 2016年 xiongjw. All rights reserved.
//

#import "AXMyTextView.h"

//#define MAX_LIMIT_NUMS  255

@interface AXMyTextView ()
{
    bool _showLb;
    NSInteger _maxLimitNums;
}

@property (nonatomic, copy) AXTextViewDidEndEditingBlock textViewDidEndEditingBlock;
@property (nonatomic, copy) AXTextViewDidChangeBlock textViewDidChangeBlock;

@end

@implementation AXMyTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame showLabel:NO];
}

- (instancetype)initWithFrame:(CGRect)frame showLabel:(BOOL)showLb
{
    return [self initWithFrame:frame showLabel:showLb maxLimitNums:255];
}

- (instancetype)initWithFrame:(CGRect)frame showLabel:(BOOL)showLb maxLimitNums:(NSInteger)maxLimitNums
{
    return [self initWithFrame:frame showLabel:showLb maxLimitNums:maxLimitNums didEndEditingBlock:NULL];
}

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums
           didEndEditingBlock:(AXTextViewDidEndEditingBlock)didEndEditingBlock
{
    return [self initWithFrame:frame showLabel:showLb maxLimitNums:maxLimitNums didEndEditingBlock:didEndEditingBlock didChangeBlock:NULL];
}

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums
               didChangeBlock:(AXTextViewDidChangeBlock)didChangeBlock
{
    return [self initWithFrame:frame showLabel:showLb maxLimitNums:maxLimitNums didEndEditingBlock:NULL didChangeBlock:didChangeBlock];
}

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums
           didEndEditingBlock:(AXTextViewDidEndEditingBlock)didEndEditingBlock
               didChangeBlock:(AXTextViewDidChangeBlock)didChangeBlock
{
    self = [super initWithFrame:frame];
    if (self) {
        /*
         self.layer.borderWidth = 1;
         self.layer.borderColor = [UIColor borderAndLineColor].CGColor;
         
         self.layer.cornerRadius = 6;
         self.layer.masksToBounds = YES;
         */
        
        _showLb = showLb;
        _maxLimitNums = maxLimitNums;
        
        _textViewDidEndEditingBlock = didEndEditingBlock;
        _textViewDidChangeBlock = didChangeBlock;
        
        self.backgroundColor = [UIColor clearColor];
        
        if (_showLb) {
            _myTextView = [[AXPlaceHolderTextView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame) - 24)];
        }
        else
        {
            _myTextView = [[AXPlaceHolderTextView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))];
        }
        _myTextView.placeholder = @"请输入";
        _myTextView.textColor = UIColorFromRGB(0x333333);
        _myTextView.font = [UIFont systemFontOfSize:13];
        _myTextView.delegate = self;
        _myTextView.backgroundColor = [UIColor clearColor];
        [self addSubview:_myTextView];
        
        if (_showLb)
        {
            _showLabel = [UILabel oneLineLbWithX:CGRectGetWidth(frame) - 115
                                               y:CGRectGetHeight(frame) - 20
                                           width:100
                                        fontSize:11
                                           color:UIColorFromRGB(0x999999)
                                            text:FormatString(@"0/%zd",maxLimitNums)];
            _showLabel.textAlignment = NSTextAlignmentRight;
            _showLabel.attributedText = [self attributedTextWithRange:NSMakeRange(0, 1)];
            [self addSubview:_showLabel];
        }
        
    }
    return self;
}

#pragma mark -限制输入字数(最多不超过255个字)
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    //不支持系统表情的输入
    /*
    NSString *primaryLanguage = [[textView textInputMode] primaryLanguage];
    
    if (primaryLanguage == nil || [primaryLanguage isEqualToString:@"emoji"]) {
        return NO;
    }
     */
    
    UITextRange *selectedRange = [textView markedTextRange];
    
    //获取高亮部分
    
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
    
    //获取高亮部分内容
    //NSString * selectedtext = [textView textInRange:selectedRange];
    //如果有高亮且当前字数开始位置小于最大限制时允许输入
    if (selectedRange && pos)
    {
        NSInteger startOffset = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selectedRange.start];
        NSInteger endOffset = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selectedRange.end];
        
        NSRange offsetRange = NSMakeRange(startOffset, endOffset - startOffset);
        
        if (offsetRange.location < _maxLimitNums)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    NSString *comcatstr = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSInteger caninputlen = _maxLimitNums - comcatstr.length;
    if (caninputlen >= 0)
    {
        return YES;
    }
    else
    {
        NSInteger len = text.length + caninputlen;
        //防止当text.length + caninputlen < 0时，使得rg.length为一个非法最大正数出错
        NSRange rg = {0,MAX(len,0)};
        if (rg.length > 0)
        {
            NSString *s = @"";
            //判断是否只普通的字符或asc码(对于中文和表情返回NO)
            BOOL asc = [text canBeConvertedToEncoding:NSASCIIStringEncoding];
            if (asc)
            {
                s = [text substringWithRange:rg];//因为是ascii码直接取就可以了不会错
            }
            else
            {
                __block NSInteger idx =0;
                __block NSString  *trimString = @"";//截取出的字串
                
                //使用字符串遍历，这个方法能准确知道每个emoji是占一个unicode还是两个
                
                [text enumerateSubstringsInRange:NSMakeRange(0, [text length])
                                        options:NSStringEnumerationByComposedCharacterSequences
                                     usingBlock: ^(NSString* substring,NSRange substringRange,NSRange enclosingRange,BOOL* stop) {
                                         
                                         if (idx >= rg.length) {
                                             *stop = YES;//取出所需要就break，提高效率
                                             return ;
                                         }
                                         
                                         trimString = [trimString stringByAppendingString:substring];
                                         idx++;
                                     }];
                
                s = trimString;
                
            }
            
            //rang是指从当前光标处进行替换处理(注意如果执行此句后面返回的是YES会触发didchange事件)
            
            [textView setText:[textView.text stringByReplacingCharactersInRange:range withString:s]];
            
            //既然是超出部分截取了，哪一定是最大限制了。
            
            if (_showLb) {
                _showLabel.text = [NSString stringWithFormat:@"%d/%ld",0,(long)_maxLimitNums];
                _showLabel.attributedText = [self attributedTextWithRange:NSMakeRange(0, 1)];
            }
            
        }
        return NO;
    }
}

#pragma mark -显示当前可输入字数/总字数
- (void)textViewDidChange:(UITextView *)textView
{
    if (_textViewDidChangeBlock) {
        _textViewDidChangeBlock();
    }
    UITextRange *selectedRange = [textView markedTextRange];
    
    //获取高亮部分
    
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
    
    //如果在变化中是高亮部分在变，就不要计算字符了
    
    if (selectedRange && pos) {
        
        return;
    }
    
    NSString  *nsTextContent = textView.text;
    
    NSInteger existTextNum = nsTextContent.length;
    
    if (existTextNum > _maxLimitNums) {
        //截取到最大位置的字符(由于超出截部分在should时被处理了所在这里这了提高效率不再判断)
        
        NSString *s = [nsTextContent substringToIndex:_maxLimitNums];
        
        [textView setText:s];
        
    }
    
    //不让显示负数
    if (_showLb) {
        NSInteger nums = MIN(existTextNum, _maxLimitNums);
        _showLabel.text = [NSString stringWithFormat:@"%zd/%zd",nums,_maxLimitNums];
        _showLabel.attributedText = [self attributedTextWithRange:NSMakeRange(0, [FormatString(@"%zd",nums) length])];
    }
}

- (NSMutableAttributedString *)attributedTextWithRange:(NSRange)range
{
    return [ISSPubfun setLabelAttributed:_showLabel.text
                                   range:range
                                   color:UIColorFromRGB(0xf76252)
                                    font:_showLabel.font];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (_textViewDidEndEditingBlock) {
        _textViewDidEndEditingBlock();
    }
}

- (void)reSetLimitTextNum
{
    NSInteger nums = _myTextView.text.length;
    _showLabel.text = [NSString stringWithFormat:@"%zd/%zd",nums,_maxLimitNums];
    _showLabel.attributedText = [self attributedTextWithRange:NSMakeRange(0, [FormatString(@"%zd",nums) length])];
}

- (void)reSetMaxLimitTextNum:(NSInteger)maxLimitTextNum
{
    _maxLimitNums = maxLimitTextNum;
    
    if (_myTextView.text.length > maxLimitTextNum)
    {
        //_myTextView.text = [_myTextView.text substringToIndex:maxLimitTextNum];
    }
    NSInteger nums = _myTextView.text.length;
    _showLabel.text = [NSString stringWithFormat:@"%zd/%zd",nums,_maxLimitNums];
    _showLabel.attributedText = [self attributedTextWithRange:NSMakeRange(0, [FormatString(@"%zd",nums) length])];
}

@end
