//
//  X_PopPickerView.m
//  WHZHT
//
//  Created by xiongjw on 15/7/7.
//  Copyright (c) 2015年 xiongjw. All rights reserved.
//

#import "X_PopPickerView.h"

@interface X_PopPickerView () <UIPickerViewDelegate,UIPickerViewDataSource> {
    
    NSString *_codeKey; //这个只是一个code
    NSString *_nameKey;  //这个是显示的名字
    
    ISSPickerModel *_pickerModel;
    NSArray *_dataSource;
    
    UIView *_uiView;//动画上下使用
    
    //时间类型需要使用到
    UIDatePicker *_uiDatePicker;
    NSDateFormatter *_dateFormatter;
    
    UIPickerView *_uiPickerView;
    
    NSInteger _pickerTag;
}

@property (nonatomic, weak) id<X_PopPickerViewDelegate> delegate;

@end

@implementation X_PopPickerView

-(void)setPickerTag:(NSInteger)pickerTag
{
    _pickerTag = pickerTag;
}

- (void)showInView:(UIView *)view {
    [view addSubview:self];
    
    __block UIView *blockView = _uiView;
    __block CGRect rect = _uiView.frame;
    
    [UIView animateWithDuration:.45 animations:^{
        rect.origin.y -= CGRectGetHeight(_uiView.frame);
        blockView.frame = rect;
    }];
}

- (void)dismiss {
    __block UIView *blockView = _uiView;
    __block CGRect rect = _uiView.frame;
    
    WeakSelf(self)
    [UIView animateWithDuration:.3 animations:^{
        rect.origin.y += CGRectGetHeight(_uiView.frame);
        blockView.frame = rect;
    } completion:^(BOOL finished) {
        [weakself removeFromSuperview];
    }];
}

- (void)cancel {
    
    [self dismiss];
}

- (void)submit {
    
    NSInteger type = _pickerModel.type;
    NSDictionary *item = nil;
    if (type == TextField_DataRadio)
    {
        if (_dataSource.count == 0) {
            [self dismiss];
            return;
        }
        id obj = _dataSource[[_uiPickerView selectedRowInComponent:0]];
        if ([obj isKindOfClass:[NSString class]]) {
            item = @{_codeKey:obj,_nameKey:obj};
        }
        else
        {
            item = _dataSource[[_uiPickerView selectedRowInComponent:0]];
            if (![item[_codeKey] isKindOfClass:[NSString class]]) {
                //item = @{_nameKey:item[_nameKey],_codeKey:formatString(@"%lld",[item[_codeKey] longLongValue])};
                
                NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:item];
                [data setObject:FormatString(@"%lld",[item[_codeKey] longLongValue]) forKey:_codeKey];
                
                item = [NSDictionary dictionaryWithDictionary:data];
            }
        }
        
    }
    else if (type == TextField_OnlyDate ||
             type == TextField_DateTime ||
             type == TextField_OnlyTime ||
             type == TextField_CountDown)
    {
        NSString *value = [_dateFormatter stringFromDate:_uiDatePicker.date];
        item = @{_codeKey:value, _nameKey:value};
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(actionPopView:pickerTag:)]) {
        [_delegate actionPopView:item pickerTag:_pickerTag];
    }
    
    [self dismiss];
}

- (UIView *)topView
{
    UIToolbar* toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 44)];
    toolBar.tag = 98;
    toolBar.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:[[UIView alloc] init]];
    UIBarButtonItem* btnItemCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIBarButtonItem* btnItemDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(submit)];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:[[UIView alloc] init]];
    
    toolBar.items = @[left,btnItemCancel, flexibleSpace, btnItemDone,right];
    
    return toolBar;
}

//针对list<Map> code可能是longlong的情况
- (NSString *)getListMapStr:(id)obj {
    if ([obj isKindOfClass:[NSString class]]) {
        return obj;
    }
    NSString *result = FormatString(@"%lld",[obj longLongValue]);
    return result;
}

//针对可能是 list<String> 的情况
- (NSString *)getListStringStr:(id)obj
{
    if ([obj isKindOfClass:[NSString class]]) {
        return obj;
    }
    NSDictionary *item = obj;
    // 特殊情况
    if ([@"equipCode" isEqualToString:_codeKey]) {
        return FormatString(@"%@-%@",item[_nameKey],item[_codeKey]);
    }
    return item[_nameKey];
}

- (instancetype)initWithFactor:(id)delegate pickerModel:(ISSPickerModel *)pickerModel
{
    self = [super initWithFrame:ScreenFrame_ISS];
    if (self) {
        _delegate = delegate;
        _pickerModel = pickerModel;
        
        _pickerTag = 0;
        
        _codeKey = _pickerModel.codeKey ? _pickerModel.codeKey:@"code";//有些时候服务器返回的key 不一定是code
        _nameKey = _pickerModel.nameKey ? _pickerModel.nameKey:@"name";//有些时候服务器返回的key 不一定是name
        
        NSInteger type = _pickerModel.type;
        
        self.backgroundColor = [UIColor clearColor];
        _uiView = [[UIView alloc] initWithFrame:CGRectMake(0, Screen_Height, Screen_Width, 260)];
        _uiView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_uiView];
        
        [_uiView addSubview:[self topView]];
        
        if (type == TextField_DataRadio)
        {
            _dataSource = _pickerModel.dataSoure;
            
            NSInteger row = 0;
            
            if (_dataSource.count > 0)
            {
                
                if ([_pickerModel.value length] > 0) {
                    for (NSInteger i = 0; i < _dataSource.count; i++) {
                        id obj = _dataSource[i];
                        if ([obj isKindOfClass:[NSString class]])
                        {
                            if ([obj isEqualToString:_pickerModel.value]) {
                                row = i;
                                break;
                            }
                        }
                        else
                        {
                            NSDictionary *item = _dataSource[i];
                            if ([[self getListMapStr:item[_codeKey]] isEqualToString:_pickerModel.value]) {
                                row = i;
                                break;
                            }
                        }
                        
                    }
                }
            }
            
            _uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, Screen_Width, CGRectGetHeight(_uiView.frame)-44)];
            _uiPickerView.backgroundColor = [UIColor groupTableViewBackgroundColor];
            _uiPickerView.dataSource = self;
            _uiPickerView.delegate = self;
            [_uiPickerView selectRow:row inComponent:0 animated:NO];
            [_uiView addSubview:_uiPickerView];
            
        }
        else if (type == TextField_OnlyDate || type == TextField_DateTime || type == TextField_OnlyTime || type == TextField_CountDown) {
            
            _uiDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(ScreenFrame_ISS), CGRectGetHeight(_uiView.frame)-44)];
            _uiDatePicker.backgroundColor = [UIColor groupTableViewBackgroundColor];
            //[_uiDatePicker setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            
            _dateFormatter = [[NSDateFormatter alloc] init];
            
            if (type == TextField_OnlyDate) {
                [_dateFormatter setDateFormat:AXDateDayFormat];
                _uiDatePicker.datePickerMode = UIDatePickerModeDate;
            }
            else if (type == TextField_OnlyTime) {
                [_dateFormatter setDateFormat:@"HH:mm"];
                _uiDatePicker.datePickerMode = UIDatePickerModeTime;
            }
            else if (type == TextField_DateTime) {
                [_dateFormatter setDateFormat:AXDateMinuteFormat];
                _uiDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
            }
            else if (type == TextField_CountDown) {
                [_dateFormatter setDateFormat:@"HH:mm"];
                _uiDatePicker.datePickerMode = UIDatePickerModeCountDownTimer;
            }
            
            if (_pickerModel.needMin)
            {
                if (_pickerModel.needMinValue) {
                    _uiDatePicker.minimumDate = [_dateFormatter dateFromString:_pickerModel.needMinValue];
                }
                else
                {
                    _uiDatePicker.minimumDate = [NSDate date];
                    //_uiDatePicker.minimumDate = [[NSDate date] dateByAddingTimeInterval:24*60*60];
                }
            }
            if (_pickerModel.needMax) {
                
                if (_pickerModel.needMaxValue) {
                    _uiDatePicker.maximumDate = [_dateFormatter dateFromString:_pickerModel.needMaxValue];
                }
                else
                {
                    _uiDatePicker.maximumDate = [NSDate date];
                }
            }
            else {
                if (type == TextField_OnlyDate) {
                    _uiDatePicker.maximumDate = [_dateFormatter dateFromString:@"2037-12-31"];
                }
                else if (type == TextField_DateTime) {
                    _uiDatePicker.maximumDate = [_dateFormatter dateFromString:@"2037-12-31 23:59"];
                }
            }
            
            if ([_pickerModel.value length] > 0)
                _uiDatePicker.date = [_dateFormatter dateFromString:_pickerModel.value];
            
            [_uiView addSubview:_uiDatePicker];
        }
    }
    return self;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _dataSource.count;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self getListStringStr:_dataSource[row]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch.view == [self viewWithTag:98]) {
        return;
    }
    [self dismiss];
}

@end
