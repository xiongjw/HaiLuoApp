//
//  AXWebviewProgressLine.m
//  AXGY
//
//  Created by xiongjw on 2017/6/22.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import "AXWebviewProgressLine.h"

@implementation AXWebviewProgressLine

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.hidden = YES;
        self.backgroundColor = [UIColor colorWithRed:0.09 green:0.49 blue:1 alpha:1];
    }
    return self;
}

-(void)setLineColor:(UIColor *)lineColor
{
    _lineColor = lineColor;
    self.backgroundColor = lineColor;
}

-(void)startLoadingAnimation
{
    self.hidden = NO;
    self.width = 0.0;
    
    __weak UIView *weakSelf = self;
    [UIView animateWithDuration:0.4 animations:^{
        weakSelf.width = Screen_Width * 0.6;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 animations:^{
            weakSelf.width = Screen_Width * 0.8;
        }];
    }];    
}

-(void)endLoadingAnimation
{
    __weak UIView *weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.width = Screen_Width;
    } completion:^(BOOL finished) {
        weakSelf.hidden = YES;
    }];
}

@end
