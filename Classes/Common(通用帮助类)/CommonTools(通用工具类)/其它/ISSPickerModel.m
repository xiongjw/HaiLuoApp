//
//  ISSPickerModel.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSPickerModel.h"

@implementation ISSPickerModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.value = @"";
        self.showValue = @"";
        self.codeKey = @"code";
        self.nameKey = @"name";
    }
    return self;
}

@end
