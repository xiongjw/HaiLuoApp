//
//  AXPlaceHolderTextView.h
//  AXGY
//
//  Created by xiongjw on 2017/1/16.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXPlaceHolderTextView : UITextView

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, strong) UILabel *placeHolderLabel;
-(void)textChanged:(NSNotification*)notification;

@end
