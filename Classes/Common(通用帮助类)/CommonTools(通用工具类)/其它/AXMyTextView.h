//
//  AXMyTextView.h
//  AXGY
//
//  Created by xiongjw on 16/7/12.
//  Copyright © 2016年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AXPlaceHolderTextView.h"

typedef void (^AXTextViewDidEndEditingBlock) (void);
typedef void (^AXTextViewDidChangeBlock) (void);

@interface AXMyTextView : UIView <UITextViewDelegate>

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb;

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums;

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums
           didEndEditingBlock:(AXTextViewDidEndEditingBlock)didEndEditingBlock;

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums
               didChangeBlock:(AXTextViewDidChangeBlock)didChangeBlock;

- (instancetype)initWithFrame:(CGRect)frame
                    showLabel:(BOOL)showLb
                 maxLimitNums:(NSInteger)maxLimitNums
           didEndEditingBlock:(AXTextViewDidEndEditingBlock)didEndEditingBlock
               didChangeBlock:(AXTextViewDidChangeBlock)didChangeBlock;

@property (nonatomic,strong) AXPlaceHolderTextView *myTextView;
@property (nonatomic,strong) UILabel *showLabel;

//
- (void)reSetLimitTextNum;

//重置最大输入数
- (void)reSetMaxLimitTextNum:(NSInteger)maxLimitTextNum;

@end
