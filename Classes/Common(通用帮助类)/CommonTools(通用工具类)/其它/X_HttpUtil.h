//
//  X_HttpUtil.h
//  旅行真人译
//
//  Created by xiongyw on 14-10-31.
//  Copyright (c) 2014年 CC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFNetworking.h"

#import "MBProgressHUD.h"

@interface X_HttpUtil : NSObject

+ (void)getRequestWithIntactUrl:(NSString *)intactUrl completionBlock:(void (^)(NSDictionary *resultData))completion;
/**
 *  普通get请求
 */
+ (NSURLSessionDataTask*)getRequestWithIntactUrl:(NSString *)intactUrl
                                           param:(NSDictionary *)param
                                            view:(UIView *)view
                                 completionBlock:(void (^)(NSDictionary *resultData))completion;

/**
 *  普通post请求
 */
+ (NSURLSessionDataTask*)postRequestWithIntactUrl:(NSString *)intactUrl
                                            param:(NSDictionary *)param
                                             view:(UIView *)view
                                  completionBlock:(void (^)(NSDictionary *resultData))completion;


/**
 *  1.API请求,successBlock
 */
+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                       successBlock:(void (^)(NSDictionary *resultData))success;

/**
 *  2.API请求,successBlock,failureBlock
 */
+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(NSDictionary *resultData))failure;

/**
 *  3.API请求,showErrorMessage,successBlock,failureBlock
 */
+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                   showErrorMessage:(BOOL)showErrorMessage
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(NSDictionary *resultData))failure;


/**
 *  包含图片上传的API请求
 */
+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                   showErrorMessage:(BOOL)showErrorMessage
                         imageArray:(NSArray*)imageArray
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(void))failure;

+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                              image:(UIImage *)image
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(void))failure;


/**
 *  包含二进制数据参数的API请求
 */
+ (NSURLSessionDataTask*)apiRequest:(NSString *)urlStr
                              param:(NSDictionary *)param
                               view:(UIView *)view
                   showErrorMessage:(BOOL)showErrorMessage
          constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> formData))constructingBodyWithBlock
                       successBlock:(void (^)(NSDictionary *resultData))success
                       failureBlock:(void (^)(void))failure;



+ (void)cancelAllRequest;

+ (NSDictionary *)apiParam:(NSDictionary*)paramDic;

+ (MBProgressHUD*)hudForView:(UIView*)view;
+ (void)didStartNetworking;
+ (void)didStopNetworking;


@end
