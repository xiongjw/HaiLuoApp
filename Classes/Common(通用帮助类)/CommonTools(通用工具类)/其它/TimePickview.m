//
//  TimePickview.m
//

#import "TimePickview.h"
#define kZero 0
#define kFullWidth [UIScreen mainScreen].bounds.size.width
#define kFullHeight [UIScreen mainScreen].bounds.size.height

#define kDatePicY kFullHeight/3*2-64
#define kDatePicHeight kFullHeight/3

#define kDateTopBtnY kDatePicY - 30
#define kDateTopBtnHeight 30

#define kDateTopRightBtnWidth kDateTopLeftBtnWidth
#define kDateTopRightBtnX kFullWidth - 8 - kDateTopRightBtnWidth

#define kDateTopLeftbtnX 8
#define kDateTopLeftBtnWidth kFullWidth/10
@interface TimePickview()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSString *contentString;
    NSInteger commentRow;
}
@property (nonatomic,strong)UIDatePicker *dateP;
@property (nonatomic,strong)UIPickerView *pickerView;
@property (nonatomic,strong)UIButton *leftBtn;
@property (nonatomic,strong)UIButton *rightBtn;
@property (nonatomic,strong)UIView *topView;
@property (nonatomic,strong)UIView *groundV;

@property (nonatomic,assign)UIDatePickerMode type;

@property (strong, nonatomic) NSArray *array;

@end

@implementation TimePickview
- (instancetype)initWithFrame:(CGRect)frame type:(UIDatePickerMode)type{
    self = [super initWithFrame:frame];
    if (self) {
        self.type = type;
        [self addSubview:self.groundV];
        [self addSubview:self.dateP];
        [self addSubview:self.topView];
        [self addSubview:self.leftBtn];
        [self addSubview:self.rightBtn];
   

    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andArray:(NSArray *)array{
    self = [super initWithFrame:frame];
    if (self) {
        self.array = array;
        [self addSubview:self.pickerView];
        [self addSubview:self.topView];
    }
    return self;
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        UIPickerView *pickView=[[UIPickerView alloc] init];
        pickView.backgroundColor=[UIColor whiteColor];
        _pickerView=pickView;
        pickView.delegate=self;
        pickView.dataSource=self;
        pickView.frame=CGRectMake(0, kDatePicY, kFullWidth, kDatePicHeight);
        [self addSubview:pickView];
    }
    return _pickerView;
}

- (UIDatePicker *)dateP{
    if (!_dateP) {
        self.dateP = [[UIDatePicker alloc]initWithFrame:CGRectMake(kZero, kDatePicY, kFullWidth, kDatePicHeight)];
        self.dateP.backgroundColor = [UIColor whiteColor];
        
        self.dateP.datePickerMode = self.type;
        self.dateP.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"zh_CHS_CN"];
//        NSDate *maxDate = [NSDate date];
//        self.dateP.minimumDate = maxDate;
        
        [self.dateP addTarget:self action:@selector(handleDateP:) forControlEvents:UIControlEventValueChanged];
        
    }
    return _dateP;
}

- (UIView *)topView {
    if (!_topView) {
        self.topView = [[UIView alloc]initWithFrame:CGRectMake(kZero, kDateTopBtnY, kFullWidth, kDateTopBtnHeight)];
        self.topView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    }
    return _topView;
}
- (UIButton *)leftBtn{
    if (!_leftBtn) {
        self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftBtn.frame = CGRectMake(kDateTopLeftbtnX, kDateTopBtnY, kDateTopLeftBtnWidth, kDateTopBtnHeight);
        [self.leftBtn setTitle:@"取消" forState:UIControlStateNormal];
        self.leftBtn.titleLabel.font = [UIFont systemFontOfSize: 14];
        [self.leftBtn setTitleColor:[UIColor themeColor] forState:UIControlStateNormal];
        [self.leftBtn addTarget:self action:@selector(handleDateTopViewLeft) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftBtn;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn.frame = CGRectMake(kDateTopRightBtnX, kDateTopBtnY, kDateTopRightBtnWidth, kDateTopBtnHeight);
        [self.rightBtn setTitleColor:[UIColor themeColor] forState:UIControlStateNormal];
        [self.rightBtn setTitle:@"确定" forState:UIControlStateNormal];
        self.rightBtn.titleLabel.font = [UIFont systemFontOfSize: 14];
        [self.rightBtn addTarget:self action:@selector(handleDateTopViewRight) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}
-(void)handleDateTopViewLeft {
    [self end];
}

- (void)handleDateTopViewRight {
    [self.delegate determine:self.dateP.date];
    [self end];
}

- (UIView *)groundV {
    if (!_groundV) {
        self.groundV = [[UIView alloc]initWithFrame:self.bounds];
        self.groundV.backgroundColor = [UIColor blackColor];
        self.groundV.alpha = 0;
        //添加手势
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDateTopViewLeft)];
        [self.groundV setUserInteractionEnabled:YES];
        [self.groundV addGestureRecognizer:tap];
    }
    return _groundV;
}


- (void)setNowTime:(NSString *)dateStr{
    //    [self.pickerView selectRow:1 inComponent:0 animated:YES];
    [self.dateP setDate:[self dateFromString:dateStr] animated:YES];
    [UIView animateWithDuration:0.3 animations:^{
        _groundV.alpha = 0.3;
    }];
}
//设置显示动画
-(void)setAnimotion{
    _dateP.top = self.height;
    _leftBtn.top = self.height;
    _rightBtn.top = self.height;
    _topView.top = self.height;
    [UIView animateWithDuration:0.3 animations:^{
        _groundV.alpha = 0.3;
        _dateP.bottom = self.height;
        _leftBtn.bottom = kDatePicY;
        _rightBtn.bottom = kDatePicY;
        _topView.bottom = kDatePicY;
    }];

}

- (void)end{
    [UIView animateWithDuration:0.3 animations:^{
        _groundV.alpha = 0;
        _dateP.top = self.height;
        _leftBtn.top = self.height;
        _rightBtn.top = self.height;
        _topView.top = self.height;
    }completion:^(BOOL finished) {
        
        [self removeFromSuperview];
    }];

    
}

- (void)handleDateP :(NSDate *)date {
    
    [self.delegate changeTime:self.dateP.date];
}

- (void)handleDateTopViewCancel {
    [self end];
}
#pragma mark - 时间控件datePicker的数据传递
- (void)handleDateTopViewCertain {
    [self.delegate determine:self.dateP.date];
    [self end];
}
#pragma mark - 自定义数据传递
- (void)handleDateTopViewCertain1 {
    
    [self.pickerdelegate deterDate:contentString andComponent:commentRow];
    contentString = nil;
    [self end];
}



// NSDate --> NSString
- (NSString*)stringFromDate:(NSDate*)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    switch (self.type) {
        case UIDatePickerModeTime:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        case UIDatePickerModeDate:
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            break;
        case UIDatePickerModeDateAndTime:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            break;
        case UIDatePickerModeCountDownTimer:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        default:
            break;
    }
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    return destDateString;
    
}

//NSDate <-- NSString
- (NSDate*)dateFromString:(NSString*)dateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    switch (self.type) {
        case UIDatePickerModeTime:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        case UIDatePickerModeDate:
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            break;
        case UIDatePickerModeDateAndTime:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            break;
        case UIDatePickerModeCountDownTimer:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        default:
            break;
    }
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    
    return destDate;
}


#pragma mark piackView 数据源方法
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.array.count;
}

#pragma mark UIPickerViewdelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    contentString = self.array[row];
    commentRow = row;
    return self.array[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    contentString = self.array[row];
    commentRow = row;
    
    [self.pickerdelegate changeDate:self.array[row] andComponent:row];
}


@end
