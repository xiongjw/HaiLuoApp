//
//  ISSPubfun.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSPubfun.h"

@implementation ISSPubfun

+ (NSString *)getApplicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    basePath = [basePath stringByAppendingPathComponent:@"Private"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    BOOL success = [fileManager fileExistsAtPath:basePath isDirectory:&isDir];
    if (!success || !isDir) {
        NSError *err;
        success = [fileManager createDirectoryAtPath:basePath
                         withIntermediateDirectories:YES
                                          attributes:nil
                                               error:&err];
        if (!success) {
            NSLog(@"Unresolved error %@, %@", err, [err userInfo]);
            exit(-1);
        }
    }
    //NSLog(@"====%@",basePath);
    return basePath;
}


+ (NSString *)getFolder:(NSString *)folder
{
    NSString *path = [[self getApplicationDocumentsDirectory] stringByAppendingPathComponent:folder];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ( [fm fileExistsAtPath:path] == false ) {
        [fm createDirectoryAtPath:path
      withIntermediateDirectories:YES
                       attributes:nil
                            error:NULL];
    }
    return path;
}

+ (void)modifyNavigationController:(UINavigationController *)navController
{
    [self modifyNavigationBar:navController.navigationBar navColor:[UIColor themeColor]];
}

+ (void)modifyNavigationController:(UINavigationController *)navController navColor:(UIColor *)navColor
{
    [self modifyNavigationBar:navController.navigationBar navColor:navColor];
}

+ (void)modifyNavigationBar:(UINavigationBar *)navigationBar navColor:(UIColor *)navColor
{
    [navigationBar setBackgroundColor:[UIColor clearColor]];
    navigationBar.tintColor = [UIColor whiteColor];
    
    navigationBar.shadowImage = [[UIImage alloc] init];
    navigationBar.translucent = NO;
    
    UIColor *titleColor = [UIColor whiteColor];
    if (navColor == [UIColor whiteColor]) {
        titleColor = [UIColor describeColor_61];
    }
    navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                         titleColor,NSForegroundColorAttributeName,
                                         [UIFont boldSystemFontOfSize:18],NSFontAttributeName, nil];
    
    UIImage *image = [UIImage imageWithColor:navColor];
    [navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
}

+ (UIImage *)colorizeImage:(NSString *)baseImage
{
    UIImage *image = [UIImage imageNamed:baseImage];
    CGFloat scale = [UIScreen mainScreen].scale;
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/scale/2 - 1, image.size.width/scale/2 - 1, image.size.height/scale/2 - 1, image.size.width/scale/2 - 1)
                                 resizingMode:UIImageResizingModeStretch];
}

+ (CGFloat)getLabelHeight:(CGFloat)contentWidth font:(UIFont *)font text:(NSString *)text
{
    return [self getLabelSize:contentWidth fontType:font label:text].height;
}

+ (CGFloat)getLabelHeight:(UILabel *)label {
    return [self getLabelSize:CGRectGetWidth(label.frame) fontType:label.font label:label.text].height;
}

+ (CGSize)getLabelSize:(CGFloat)contentWidth fontType:(UIFont *)font label:(NSString *)text
{
    NSDictionary *attribute = @{NSFontAttributeName: font};
    CGSize retSize = [text boundingRectWithSize:CGSizeMake(contentWidth, MAXFLOAT)
                                        options:(NSStringDrawingTruncatesLastVisibleLine |
                                                 NSStringDrawingUsesLineFragmentOrigin |
                                                 NSStringDrawingUsesFontLeading)
                                     attributes:attribute
                                        context:nil].size;
    return retSize;
}

+ (CGSize)getLabelSize:(NSString *)value font:(UIFont *)font
{
    CGSize size = [value sizeWithAttributes:@{NSFontAttributeName : font}];
    return size;
}

+ (CGSize)getLabelSize:(UILabel *)label
{
    CGSize size = [label.text sizeWithAttributes:@{NSFontAttributeName : label.font}];
    return size;
}

+ (CGRect)getOneLineLabelRect:(UILabel *)textLabel
{
    CGRect rect = textLabel.frame;
    rect.size = [textLabel.text sizeWithAttributes:@{NSFontAttributeName:textLabel.font}];
    return rect;
}

+ (CGRect)getLabelRect:(UILabel *)textLabel
{
    CGRect rect = textLabel.frame;
    //rect.size.height = [self getLabelHeight:textLabel]+1;
    rect.size.height = [self getLabelHeight:textLabel];
    return rect;
}

+ (CGRect)getLimitLabelRect:(UILabel *)label limitLine:(NSInteger)line
{
    CGRect rect = [self getLabelRect:label];
    if (line == 1) {
        return rect;
    }
    NSMutableString *testString = [NSMutableString new];
    for (int i = 0; i < line - 1; i++) {
        [testString appendString:@"\r"];
    }
    CGSize testSize = [testString sizeWithAttributes:@{NSFontAttributeName : label.font}];
    if (rect.size.height > (testSize.height + 1)) {
        rect.size.height = testSize.height + 1;
    }
    
    return rect;
}

+ (NSMutableAttributedString *)setLabelShadow:(NSString *)value offset:(CGFloat)y
{
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:value];
    if (value.length == 0) {
        return attributed;
    }
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = RGBA(0, 0, 0, 8);
    shadow.shadowOffset = CGSizeMake(0, y);
    
    [attributed addAttributes:@{NSForegroundColorAttributeName :UIColorFromRGBA(0xffffff, 1) ,
                                NSShadowAttributeName : shadow
                                }
                        range:NSMakeRange(0, value.length)];
    return attributed;
}

+ (NSMutableAttributedString *)setLabelAttributed:(NSString *)value
                                            range:(NSRange)range
                                            color:(UIColor *)color
                                             font:(UIFont *)font
{
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:value];
    if (value.length == 0) {
        return attributed;
    }
    [attributed addAttribute:NSForegroundColorAttributeName value:color range:range];
    [attributed addAttribute:NSFontAttributeName value:font range:range];
    
    return attributed;
}

@end
