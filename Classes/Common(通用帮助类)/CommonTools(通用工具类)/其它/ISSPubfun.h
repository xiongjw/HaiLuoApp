//
//  ISSPubfun.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSPubfun : NSObject

//+ (NSString *)getServerBaseUrl;

//+ (void) appSystemInitialization;

+ (NSString *)getApplicationDocumentsDirectory;

+ (NSString *)getFolder:(NSString *)folder;

+ (void)modifyNavigationController:(UINavigationController *)navController;
+ (void)modifyNavigationController:(UINavigationController *)navController navColor:(UIColor *)navColor;
+ (void)modifyNavigationBar:(UINavigationBar *)navigationBar navColor:(UIColor *)navColor;

+ (UIImage *)colorizeImage:(NSString *)baseImage;

//
+ (CGFloat)getLabelHeight:(CGFloat)contentWidth font:(UIFont *)font text:(NSString *)text;
+ (CGFloat)getLabelHeight:(UILabel *)label;

+ (CGSize)getLabelSize:(NSString *)value font:(UIFont *)font;
+ (CGSize)getLabelSize:(UILabel *)label;

+ (CGRect)getLabelRect:(UILabel *)textLabel;

+ (CGRect)getOneLineLabelRect:(UILabel *)textLabel;

+ (CGRect)getLimitLabelRect:(UILabel *)label limitLine:(NSInteger)line;

+ (NSMutableAttributedString *)setLabelShadow:(NSString *)value offset:(CGFloat)y;

+ (NSMutableAttributedString *)setLabelAttributed:(NSString *)value
                                            range:(NSRange)range
                                            color:(UIColor *)color
                                             font:(UIFont *)font;



@end
