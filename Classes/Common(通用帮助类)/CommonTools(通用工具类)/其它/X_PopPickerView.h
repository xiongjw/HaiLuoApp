//
//  X_PopPickerView.h
//  WHZHT
//
//  Created by xiongjw on 15/7/7.
//  Copyright (c) 2015年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISSPickerModel.h"

typedef enum {
    TextField_Normal = 0, //普通文本
    TextField_DataRadio = 1, //数据单选
    TextField_OnlyDate = 2, //日期选择
    TextField_DateTime = 3, //日期选择，带时间
    TextField_OnlyTime = 4, //日期选择，只有时间
    TextField_CountDown = 5 //选择小时和分钟
} X_ViewType;

//typedef void (^PopPickerViewBlock) (NSDictionary *item ,NSInteger pickerTag);

@protocol X_PopPickerViewDelegate <NSObject>
- (void)actionPopView:(NSDictionary *)item pickerTag:(NSInteger)pickerTag;
@end

@interface X_PopPickerView : UIView

- (instancetype)initWithFactor:(id)delegate pickerModel:(ISSPickerModel *)pickerModel;

- (void)showInView:(UIView *)view;
//- (void)showInView:(UIView *)view block:(PopPickerViewBlock)block;

@property (nonatomic) NSInteger pickerTag;
@end
/*
NSDictionary *factor = @{
                         @"title": bean.title == nil ? @"" : @"",
                         @"dataSource": dataSoure == nil ? @[] : dataSoure,
                         @"codeKey": bean.codeKey == nil ? @"code" :bean.codeKey,
                         @"nameKey": bean.nameKey == nil ? @"name" :bean.nameKey,
                         @"value": [X_Pubfun checkObjNull:bean.value],
                         @"showValue":[X_Pubfun checkObjNull:bean.showValue],
                         @"type":[NSNumber numberWithInteger:bean.viewType],
                         @"needMin":bean.needMin ? @"Y": @"N",
                         @"needMax":bean.needMax ? @"Y": @"N"
                         };

X_PopPickerView *popView = [[X_PopPickerView alloc] initWithFactor:delegate factor:factor];
[popView showInView:view];
*/
