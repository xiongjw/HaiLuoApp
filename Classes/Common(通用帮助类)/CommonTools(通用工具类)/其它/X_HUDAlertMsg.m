//
//  T_HUDAlertMsg.m
//  PowerMail
//
//  Created by xiongyw on 13-12-16.
//  Copyright (c) 2013年 xiongyw. All rights reserved.
//

#import "X_HUDAlertMsg.h"

#import "MBProgressHUD.h"

#define X_MaxFontNum  15

@interface X_HUDAlertMsg () <MBProgressHUDDelegate> {
    
    MBProgressHUD *myHud;
}
@end

@implementation X_HUDAlertMsg

+ (void)showMsg:(NSString *)msg toView:(UIView *)view {
    [self showMsg:msg icon:nil toView:view];
}

+ (void)showMsg:(NSString *)msg
         toView:(UIView *)view
completionBlock:(void (^)(void))completion {
    
    NSTimeInterval delayTime = msg.length * 0.05 + 1.0;
    if (msg.length > 10) {
        delayTime = msg.length * 0.08 + 1.0;
    }
    [self showMsg:msg toView:view delay:delayTime completionBlock:completion];
}

+ (void)showMsg:(NSString *)msg
         toView:(UIView *)view
          delay:(NSTimeInterval)delay
completionBlock:(void (^)(void))completion {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    if ([msg length] > X_MaxFontNum) hud.detailsLabelText = msg;
    else hud.labelText = msg;
    hud.removeFromSuperViewOnHide = YES;
    [hud showAnimated:YES
  whileExecutingBlock:^{
      [NSThread sleepForTimeInterval:MIN(4, delay)];//停止当前线程
  } completionBlock:completion];
}

+ (void)showMsg:(NSString *)msg icon:(NSString *)icon toView:(UIView *)view
{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if ([msg length] > X_MaxFontNum) hud.detailsLabelText = msg;
    else hud.labelText = msg;
    if (icon) {
        // 设置图片
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", icon]]];
    }
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // n秒之后再消失
    [hud hide:YES afterDelay:MIN(4, msg.length * 0.05 + 1.0)];//1.2
}

+ (void)showError:(NSString *)error toView:(UIView *)view {
    [self showMsg:error icon:@"error-white.png" toView:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view {
    [self showMsg:success icon:@"success-white.png" toView:view];
}

@end
