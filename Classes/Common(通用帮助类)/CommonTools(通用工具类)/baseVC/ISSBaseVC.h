//
//  ISSBaseVC.h
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSBaseVC : UIViewController

@property (nonatomic) BOOL disablePopGesture;

@property (strong ,nonatomic) void (^didPopBlock)(void);
@property (strong ,nonatomic) void (^dismissBlock)(void);

- (void)setTitle:(NSString *)title showBackBtn:(BOOL)showBackBtn;
- (void)setTitle:(NSString *)title showBlackBackBtn:(BOOL)showBackBtn;
- (void)setTitle:(NSString *)title showBackBtn:(BOOL)showBackBtn dismissBlock:(void(^)(void))dismissBlock;

- (void)setTitle:(NSString *)title showBackBtn:(BOOL)showBackBtn didPopBlock:(void(^)(void))didPopBlock;

@end
