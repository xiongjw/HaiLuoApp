//
//  ISSMJRefreshTableVC.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSMJRefreshTableVC : ISSBaseVC

@property (nonatomic, strong) NSMutableArray *resultDataList;

@property (nonatomic, strong) UITableView *mTableView;

- (void)addHeaderAction;
- (void)addFooterAction;
- (void)addHeaderAndFooterAction;

- (void)requestDataReresh:(NSString *)direction;

- (void)headerBeginRefreshing;

- (void)endRefreshing;
//- (void)endRefreshing:(NSString *)direction list:(NSArray *)list;

//- (void)addResultDataToList:(NSDictionary *)data direction:(NSString *)direction;
- (void)addResultDataToList:(NSArray *)list direction:(NSString *)direction;

@end
