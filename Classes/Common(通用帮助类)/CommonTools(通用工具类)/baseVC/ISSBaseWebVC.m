//
//  ISSBaseWebVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseWebVC.h"

#import "AXWebviewProgressLine.h"

@interface ISSBaseWebVC () <UIWebViewDelegate>
{
    bool _isRequesting;
}

@property (nonatomic,strong) UIWebView  *myWebView;
@property (nonatomic,strong) AXWebviewProgressLine *progressLine;

@end

@implementation ISSBaseWebVC

#pragma mark -  user method

- (void)loadWebView
{
    if (_isString) {
        [self.myWebView loadHTMLString:_htmlString baseURL:nil];
    }
    else {
        if (_local) {
            //以utf8的编码格式加载html内容
            NSString *htmlString = [[NSString alloc] initWithContentsOfFile:_url encoding:NSUTF8StringEncoding error:nil];
            [self.myWebView loadHTMLString: htmlString baseURL: [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        }
        else {
            [_myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
        }
    }
}

- (void)createLeftBar
{
    UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeperator.width = -5;
    
    UIBarButtonItem *leftItem1 = [ISSNavUtil getBarItem:self btnType:1 clickAction:@selector(backAction)];
    WeakSelf(self)
    UIBarButtonItem *leftItem2 = [ISSNavUtil getBarItemTitle:@"关闭" titleColor:[UIColor whiteColor] clickBlock:^{
        [weakself closeAction];
    }];
    NSArray *buttonArray = [[NSArray alloc]
                            initWithObjects: negativeSeperator,leftItem1,leftItem2, nil];
    
    self.navigationItem.leftBarButtonItems = buttonArray;
}

- (void)backAction
{
    //首先需要判断能否返回
    if (_myWebView.canGoBack) {
        [_myWebView goBack];
        //增加关闭按钮
        if ([self.navigationItem.leftBarButtonItems count] < 3) [self createLeftBar];
        return;
    }
    if (![self checkPopViewController]) [self.navigationController popViewControllerAnimated:YES];
    else [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)closeAction
{
    if (![self checkPopViewController]) [self.navigationController popViewControllerAnimated:YES];
    else [self dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)checkPopViewController
{
    UIViewController *controller = [self presentingViewController];
    if (controller == nil) return NO;
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:self btnType:1 clickAction:@selector(backAction)];
    
    self.myWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    _myWebView.backgroundColor = ISSBackgroundColor;
    _myWebView.scalesPageToFit = YES;
    _myWebView.delegate = self;
    _myWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.view addSubview:self.myWebView];
    
    
    self.progressLine = [[AXWebviewProgressLine alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 2)];
    [self.view addSubview:self.progressLine];
    
    _isRequesting = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadWebView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([self.myWebView isLoading]) {
        [_myWebView stopLoading];
    }
}

- (void)dealloc
{
    _myWebView = nil;
}

#pragma mark –
#pragma mark UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    //判断请求是否相同,如果相同那么终止请求
    if ([request.URL.path isEqualToString:_url]) {
        return NO;
    }
    
    if( navigationType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSURL *requestURL =[request URL];
        //NSString *requestString = [[request URL] absoluteString];
        NSString *str = [requestURL scheme];
        
        if ([str isEqualToString:@"http"] || [str isEqualToString:@"https"]) {
            //return NO;
        }
    }
    else if( navigationType == UIWebViewNavigationTypeOther ) {
        
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.progressLine startLoadingAnimation];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.progressLine endLoadingAnimation];
    
    if (self.title.length == 0) {
        self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"网页加载失败...:%@,code:%zd",[error domain],error.code);
    [self.progressLine endLoadingAnimation];
}

@end
