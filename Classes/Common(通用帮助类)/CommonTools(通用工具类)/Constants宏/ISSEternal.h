//
//  ISSEternal.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#ifndef ISSEternal_h
#define ISSEternal_h

#import "AppDelegate.h"

//#define serverPath                  @"http://10.28.124.113:8099/conch/"        //南烨
//#define serverPath                  @"http://10.28.124.197:8099/conch/"        //马金涛
//#define serverPath                  @"http://10.28.124.195:8099/conch/"      //陈巍
//#define serverPath                  @"http://10.28.124.188:8099/conch/"      //周法
//#define serverPath                  @"http://10.28.124.116:8099/conch/"       // 田甘霖

//#define serverPath                  @"http://220.180.131.37:18000/conch/"         // 客户测试环境
#define serverPath                  @"http://220.180.131.37:18001/conch/"         // 客户生产环境

#define khServerPath                @"http://safe.conch.cn/"

#define LoadImageUrl(imageUrl)      [imageUrl hasPrefix:@"uploadDir"] ? FormatString(@"%@%@",khServerPath,imageUrl) : FormatString(@"%@%@",serverPath,imageUrl)

#define PickPhotoMaxPixelSize       750

//通用查询数量
#define PageSize                    @"15"

//获取屏幕 宽度、高度
#define ScreenFrame_ISS             [UIScreen mainScreen].bounds
#define Screen_Width                ([UIScreen mainScreen].bounds.size.width)
#define Screen_Height               ([UIScreen mainScreen].bounds.size.height)

#define Tabbar_Height               49

//校验字符串是否为null
#define CheckStringNull(value)  ((value == nil) ? @"" : value)

//格式化
#define FormatString(...)       [NSString stringWithFormat: __VA_ARGS__]

//-------------------打印日志-------------------------
//DEBUG  模式下打印日志,当前行
#ifdef Debug
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

//----------------------图片----------------------------
//读取本地图片
//#define LoadImageT(file,ext)        [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:file ofType:ext]]
//#define LoadImage(A)                [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:nil]]

//----------------------颜色类---------------------------
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGBA(rgb,a)      [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16))/255.0 green:((float)((rgb & 0xFF00) >> 8))/255.0 blue:((float)(rgb & 0xFF))/255.0 alpha:a]
#define UIColorFromRGB(rgb)         UIColorFromRGBA(rgb,1.0)

// 获取RGB颜色
#define RGBA(r,g,b,a)               [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b)                  RGBA(r,g,b,1.0f)
//背景色
#define ISSBackgroundColor          UIColorFromRGB(0xf2f1f7)
//清除背景色
#define ClearColor                  [UIColor clearColor]

//----------------------其他---------------------------- 
//字体大小定义
#define FontSIZE(F)                 [UIFont systemFontOfSize:F]
//方正黑体简体字体定义
#define FZFont(F)                   [UIFont fontWithName:@"FZHTJW--GB1-0" size:F]
//粗体字体大小定义
#define BoldFontSIZE(F)             [UIFont boldSystemFontOfSize:F]

//设置View的tag属性
#define ViewWithTag(_OBJECT, _TAG)  [_OBJECT viewWithTag : _TAG]

//G－C－D
#define GcdBack(block)              dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define GcdMain(block)              dispatch_async(dispatch_get_main_queue(),block)

#define WeakSelf(type)              __weak typeof(type) weak##type = type;
#define StrongSelf(type)            __strong typeof(type) strong##type = type;

//获取AppDelegate
#define ShareApp                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])


#define AXDateMonthFormat                 @"yyyy-MM"
#define AXDateDayFormat                   @"yyyy-MM-dd"
#define AXDateMinuteFormat                @"yyyy-MM-dd HH:mm"
#define AXDateSecondFormat                @"yyyy-MM-dd HH:mm:ss"

//通知
#define kDefaultCenter              [NSNotificationCenter defaultCenter]
#define kLoginOut                            @"kLoginOut"

#define LoadLanguagesKey(key)       NSLocalizedString(key, nil)

#endif /* ISSEternal_h */
