//
//  UIImageView+Create.m
//  WH_O2O_U
//
//  Created by xiongjw on 15/8/29.
//  Copyright (c) 2015年 xiongjw. All rights reserved.
//

#import "UIImageView+Create.h"

@implementation UIImageView (Create)

+ (UIImageView *)imageView:(CGRect)frame {
    return [self imageView:frame image:nil scale:NO];
}

+ (UIImageView *)imageView:(CGRect)frame scale:(BOOL)scale {
    return [self imageView:frame image:nil scale:scale];
}

+ (UIImageView *)imageView:(CGRect)frame image:(UIImage *)image {
    return [self imageView:frame image:image scale:NO];
}

+ (UIImageView *)imageView:(CGRect)frame image:(UIImage *)image scale:(BOOL)scale {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    if (image) {
        imageView.image = image;
    }
    if (scale) {
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
    }
    return imageView;
}
@end
