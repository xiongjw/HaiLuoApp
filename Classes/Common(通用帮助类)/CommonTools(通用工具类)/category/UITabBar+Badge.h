//
//  UITabBar+Badge.h
//  WH_O2O_U
//
//  Created by xiongjw on 15/10/19.
//  Copyright (c) 2015年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (Badge)

- (void)showBadgeOnItemIndex:(int)index;   //显示小红点

- (void)hideBadgeOnItemIndex:(int)index; //隐藏小红点

- (BOOL)hasPoint:(int)index;

@end
