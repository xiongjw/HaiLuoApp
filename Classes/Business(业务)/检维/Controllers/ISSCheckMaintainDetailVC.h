//
//  ISSCheckMaintainDetailVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSCheckMaintainDetailVC : ISSBaseTableVC

@property (nonatomic,  copy) NSString *equipId;

@property (nonatomic,strong) NSDictionary *scanInfo;

@end
