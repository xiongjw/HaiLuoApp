//
//  ISSCheckMaintainDetailVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCheckMaintainDetailVC.h"

#import "ISSCheckMaintainDetailCell.h"

#import "ISSCheckWebDetailVC.h"

@interface ISSCheckMaintainDetailVC ()

@end

@implementation ISSCheckMaintainDetailVC

- (void)dealWithScanInfo:(NSDictionary *)data
{
    self.tableData = @[
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_equipCode"),@"showValue":data[@"equipCode"]},
                           @{@"title":LoadLanguagesKey(@"check_detail_equipName"),@"showValue":data[@"name"]},
                           @{@"title":LoadLanguagesKey(@"check_detail_phone"),@"showValue":data[@"centerPhone"]}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look1"),@"itemId":data[@"attention"],@"imageName":@"checkDetail_icon1"}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look2"),@"itemId":data[@"hurtType"],@"imageName":@"checkDetail_icon2"}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look3"),@"itemId":data[@"accident"],@"imageName":@"checkDetail_icon3"}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look4"),@"itemId":data[@"cases"],@"imageName":@"checkDetail_icon1"}]
                       ,
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look5"),@"itemId":data[@"inspectionCode"],@"imageName":@"checkDetail_icon1"}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look6"),@"itemId":data[@"powerSystem"],@"imageName":@"checkDetail_icon1"}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look7"),@"itemId":data[@"examine"],@"imageName":@"checkDetail_icon1"}
                           ],
                       @[
                           @{@"title":LoadLanguagesKey(@"check_detail_look8"),@"itemId":data[@"isolationId"],@"imageName":@"checkDetail_icon1"}
                           ]
                       ];
    [self.mTableView reloadData];
}

- (void)requestDetail
{
    NSDictionary *data = @{
                           @"equipId":self.equipId
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"device/searchOneEquip" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSLog(@"resultData:%@",resultData);
        [weakself dealWithScanInfo:resultData[@"data"]];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:LoadLanguagesKey(@"check_detail_navTitle") showBackBtn:YES];
    self.view.backgroundColor = [UIColor themeColor];
    
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (self.equipId) {
        [self requestDetail];
    }
    else
    {
        [self dealWithScanInfo:self.scanInfo];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ISSCheckMaintainDetailCell *cell = (ISSCheckMaintainDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ISSCheckMaintainDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier type:indexPath.section];
    }
    [cell dealRoundCornerAndLine:indexPath.row
                    numberOfRows:[self.tableData[indexPath.section] count]
                       rowHeight:indexPath.section == 0 ? 50 : 40];
    [cell layoutCell:self.tableData[indexPath.section][indexPath.row]];
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section == 0 ? 50 : 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section >= 1)
    {
        NSDictionary *data = self.tableData[indexPath.section][indexPath.row];
        WeakSelf(self)
        [X_HttpUtil apiRequest:@"device/searchDescription" param:@{@"descId":data[@"itemId"]} view:self.view successBlock:^(NSDictionary *resultData) {
            /*
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"richText" ofType:@"txt"];
            NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding (kCFStringEncodingGB_18030_2000);
            NSString *htmlString = [[NSString alloc] initWithContentsOfFile:filePath encoding:enc error:nil];
            */
            // 展现富文本
            /*
            ISSBaseWebVC *vc = [[ISSBaseWebVC alloc] init];
            vc.title = data[@"title"];
            vc.local = YES;
            vc.isString = YES;
            vc.htmlString = [NSString noNullString:resultData[@"data"]];
            
            [weakself.navigationController pushViewController:vc animated:YES];
             */
            
            ISSCheckWebDetailVC *vc = [[ISSCheckWebDetailVC alloc] init];
            vc.title = data[@"title"];
            vc.htmlString = [NSString noNullString:resultData[@"data"]];
            [weakself.navigationController pushViewController:vc animated:YES];
        }];
    }
    
}

@end

