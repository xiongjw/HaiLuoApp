//
//  ISSCheckWebDetailVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCheckWebDetailVC.h"

@interface ISSCheckWebDetailVC ()

@end

@implementation ISSCheckWebDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:self.title showBackBtn:YES];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UILabel *descLb = [UILabel mutLineLbWithX:10 y:10 width:CGRectGetWidth(scrollView.frame) - 20 fontSize:12 color:[UIColor titleColor_26] text:@""];
    [scrollView addSubview:descLb];
    
    descLb.attributedText = [self attributedStringWithHTMLString:[NSString noNullString:self.htmlString]];
    descLb.frame = [ISSPubfun getLabelRect:descLb];
    
    [scrollView setContentSize:CGSizeMake(Screen_Width, CGRectGetMaxY(descLb.frame) + 10)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSAttributedString *)attributedStringWithHTMLString:(NSString *)htmlString
{
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
    return [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
}

@end
