//
//  ISSCheckWebDetailVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSCheckWebDetailVC : ISSBaseVC

@property (nonatomic,copy) NSString *htmlString;

@end
