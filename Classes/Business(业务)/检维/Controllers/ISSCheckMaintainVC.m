//
//  ISSCheckMaintainVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCheckMaintainVC.h"

#import "ISSBaseScanVC.h"
#import "ISSCheckMaintainDetailVC.h"
#import "ISSBaseSearchVC.h"
#import "ISSSearchResultListVC.h"

#import "ISSCheckTopView.h"

#import "ISSCheckMaintainListCell.h"

@interface ISSCheckMaintainVC ()

@end

@implementation ISSCheckMaintainVC

- (void)pushSearchResultListVC:(NSString *)searchText searchItem:(NSDictionary *)searchItem
{
    ISSSearchResultListVC *vc = [[ISSSearchResultListVC alloc] init];
    vc.searchType = ISSSearch_Check;
    vc.searchText = searchText;
    vc.searchItem = searchItem;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchAction
{
    WeakSelf(self)
    ISSBaseSearchVC *vc = [[ISSBaseSearchVC alloc] init];
    vc.cateList = @[
                    @{@"name":@"check",@"code":@"allCheck",@"cacheKey":@"Check_searchAll"}
                    ];
    vc.clickBlock = ^(NSDictionary *cateItem, NSString *searchText) {
        NSLog(@"cateItem:%@,searchText:%@",cateItem,searchText);
        [weakself pushSearchResultListVC:searchText searchItem:cateItem];
    };
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
    [self presentViewController:nav animated:NO completion:NULL];
}

- (void)scanAction
{
    // 扫描逻辑，获取id，调用接口获取详细资料，再拉取列表
    WeakSelf(self)
    ISSBaseScanVC *vc = [[ISSBaseScanVC alloc] init];
    vc.fromType = 1;
    vc.resultBlock = ^(NSDictionary *scanInfo) {
        
        // 刷新
        [weakself requestDataReresh:@"down"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //进入详情
            [weakself pushDetail:nil scanInfo:scanInfo];
        });
    };
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestDataReresh:(NSString *)direction
{
    // 分页lastId 取值有变
    NSDictionary *data = @{
                           @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"device/totalExamine" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSDictionary *data = resultData[@"data"];
        NSArray *list = data[@"list"];
        // 开始赋值
        UILabel *label = [self.view viewWithTag:7];
        label.text = FormatString(@"%lld",[data[@"total"] longLongValue]);
        
        [weakself addResultDataToList:list direction:direction];
    } failureBlock:^(NSDictionary *resultData) {
        [weakself endRefreshing];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ISSCheckTopView *topView = [[ISSCheckTopView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 110)];
    [self.view addSubview:topView];
    
    WeakSelf(self)
    X_Button *searchBtn = [ISSNavUtil getImageBtn:4 clickBlock:^{
        [weakself searchAction];
    }];
    searchBtn.frame = CGRectMake(Screen_Width - 70, 20, 70, 44);
    [topView addSubview:searchBtn];
    
    UILabel *leftLb1 = [UILabel oneLineLbWithX:15 y:CGRectGetMaxY(topView.frame) + 15 fontSize:14 color:[UIColor describeColor_9a] text:LoadLanguagesKey(@"check_main_equipTotalInfo")];
    [self.view addSubview:leftLb1];
    
    UIView *pointView = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(leftLb1.frame) + 20, 6, 6)];
    pointView.backgroundColor = [UIColor describeColor_9a];
    pointView.layer.cornerRadius = CGRectGetHeight(pointView.frame)/2;
    pointView.layer.masksToBounds = YES;
    [self.view addSubview:pointView];
    
    UILabel *leftLb2 = [UILabel oneLineLbWithX:CGRectGetMaxX(pointView.frame) + 15 y:0 fontSize:12 color:[UIColor describeColor_9a] text:LoadLanguagesKey(@"check_main_equipScan")];
    [leftLb2 setCenterY:pointView.centerY];
    [self.view addSubview:leftLb2];
    
    UILabel *rightLb1 = [UILabel oneLineLbWithX:0 y:0 fontSize:12 color:UIColorFromRGB(0x5e97f6) text:LoadLanguagesKey(@"check_main_equipInfo")];
    [rightLb1 setLeft:Screen_Width - 20 - CGRectGetWidth(rightLb1.frame)];
    [rightLb1 setCenterY:leftLb1.centerY];
    [self.view addSubview:rightLb1];
    
    UILabel *rightLb2 = [UILabel oneLineLbWithX:Screen_Width - (CGRectGetWidth(rightLb1.frame) + 40) y:0 width:CGRectGetWidth(rightLb1.frame) + 40 fontSize:40 color:UIColorFromRGB(0x5e97f6) text:@"0"];
    rightLb2.textAlignment = NSTextAlignmentCenter;
    [rightLb2 setCenterY:leftLb2.centerY];
    rightLb2.tag = 7;
    [self.view addSubview:rightLb2];
    
    X_Button *scanBtn = [X_Button buttonWithType:UIButtonTypeCustom];
    scanBtn.frame = CGRectMake(0, CGRectGetMaxY(leftLb2.frame) + 10, 130, 130);
    scanBtn.centerX = self.view.centerX;
    [scanBtn setTitle:LoadLanguagesKey(@"check_main_scan") forState:UIControlStateNormal];
    scanBtn.backgroundColor = UIColorFromRGB(0x2be449);
    [scanBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    scanBtn.titleLabel.font = [UIFont systemFontOfSize:24];
    
    scanBtn.layer.cornerRadius = CGRectGetHeight(scanBtn.frame)/2;
    //scanBtn.layer.masksToBounds = YES;
    scanBtn.layer.shadowOffset =  CGSizeMake(0, 8);
    scanBtn.layer.shadowOpacity = 0.3;
    scanBtn.layer.shadowColor =  UIColorFromRGB(0x2be449).CGColor;
    
    //[scanBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x2be449)] forState:UIControlStateNormal];
    [scanBtn addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scanBtn];
    
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mTableView.frame = CGRectMake(0, CGRectGetMaxY(scanBtn.frame) + 15, Screen_Width, Screen_Height - (CGRectGetMaxY(scanBtn.frame) + 15) - Tabbar_Height - 5);
    [self addHeaderAndFooterAction];
    
    [self headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ISSCheckMaintainListCell";
    ISSCheckMaintainListCell *cell = (ISSCheckMaintainListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ISSCheckMaintainListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell layoutCell:self.resultDataList[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = self.resultDataList[indexPath.row];
    
    [self pushDetail:data[@"id"] scanInfo:nil];
}

- (void)pushDetail:(NSString *)equipId scanInfo:(NSDictionary *)scanInfo
{
    ISSCheckMaintainDetailVC *vc = [[ISSCheckMaintainDetailVC alloc] init];
    if (equipId) vc.equipId = equipId;
    else vc.scanInfo = scanInfo;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
