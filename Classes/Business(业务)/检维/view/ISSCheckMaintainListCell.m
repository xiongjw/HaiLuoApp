//
//  ISSCheckMaintainListCell.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCheckMaintainListCell.h"

@implementation ISSCheckMaintainListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _bgContentView = [[UIView alloc] initWithFrame:CGRectMake(15, 10, Screen_Width - 30, 100)];
        _bgContentView.backgroundColor = [UIColor whiteColor];
        _bgContentView.layer.cornerRadius = 8;
        _bgContentView.layer.masksToBounds = YES;
        _bgContentView.layer.borderWidth = 0.5;
        _bgContentView.layer.borderColor = [UIColor borderColor].CGColor;
        [self.contentView addSubview:_bgContentView];
        
        _titleLb = [UILabel oneLineLbWithX:12 y:12 width:100 fontSize:14 color:[UIColor titleColor_26] text:@"设备名称"];
        [_bgContentView addSubview:_titleLb];
        
        NSString *dateText = @"2018年12月12日 12:59";
        CGSize dateSize = [dateText sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]}];
        _dateLb = [UILabel oneLineLbWithX:CGRectGetWidth(_bgContentView.frame) - 10 - (dateSize.width + 5) y:0 width:dateSize.width + 5 fontSize:12 color:[UIColor describeColor_61] text:dateText];
        [_dateLb setCenterY:_titleLb.centerY];
        _dateLb.textAlignment = NSTextAlignmentRight;
        [_bgContentView addSubview:_dateLb];
        
        [_titleLb setWidth:CGRectGetMinX(_dateLb.frame) - CGRectGetMinX(_titleLb.frame)];
        
        _numberLb = [UILabel mutLineLbWithX:12 y:CGRectGetMaxY(_titleLb.frame) + 10 width:CGRectGetWidth(_bgContentView.frame) - 24 fontSize:13 color:[UIColor describeColor_61] text:@""];
        [_bgContentView addSubview:_numberLb];
        
        _addressLb = [UILabel oneLineLbWithX:CGRectGetMinX(_numberLb.frame) y:0 width:CGRectGetWidth(_bgContentView.frame) - CGRectGetMinX(_numberLb.frame) - 10 fontSize:12 color:[UIColor lightGrayColor] text:@"作业地点"];
        [_addressLb setTop:CGRectGetHeight(_bgContentView.frame) - 8 - CGRectGetHeight(_addressLb.frame)];
        [_bgContentView addSubview:_addressLb];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = ISSBackgroundColor;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutCell:(NSDictionary *)data
{
    _titleLb.text = [NSString noNullString:data[@"name"]];
    _dateLb.text = [ISSDataDeal getDateStrFromResponseData:[data[@"showTime"] longLongValue]];
    _numberLb.text = FormatString(@"设备编号：%@",[NSString noNullString:data[@"equipCode"]]);
    _numberLb.frame = [ISSPubfun getLimitLabelRect:_numberLb limitLine:2];
    _addressLb.text = [NSString noNullString:data[@"location"]];
}

@end
