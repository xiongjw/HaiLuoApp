//
//  ISSCheckMaintainDetailCell.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCheckMaintainDetailCell.h"

@implementation ISSCheckMaintainDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier type:(NSInteger)type
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        if (type == 0)
        {
            _leftLb = [UILabel oneLineLbWithX:16 y:0 fontSize:15 color:[UIColor titleColor_26] text:@"中控室电话电话电话"];
            _leftLb.font = [UIFont boldSystemFontOfSize:15];
            [_leftLb setHeight:50];
            _leftLb.numberOfLines = 2;
            _leftLb.adjustsFontSizeToFitWidth = YES;
            [self.bgContentView addSubview:_leftLb];
            
            _rightLb = [UILabel oneLineLbWithX:CGRectGetMaxX(_leftLb.frame) + 5 y:0 width:CGRectGetWidth(self.bgContentView.frame) - 25 - (CGRectGetMaxX(_leftLb.frame) + 5) fontSize:15 color:[UIColor describeColor_66] text:@""];
            _rightLb.textAlignment = NSTextAlignmentRight;
            [_rightLb setCenterY:_leftLb.centerY];
            [self.bgContentView addSubview:_rightLb];
        }
        else
        {
            _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 0, 20, 20)];
            [_iconView setCenterY:20];
            [self.bgContentView addSubview:_iconView];
            
            _leftLb = [UILabel oneLineLbWithX:50 y:0 width:CGRectGetWidth(self.bgContentView.frame) - 50 - 10 fontSize:15 color:[UIColor describeColor_66] text:@""];
            [_leftLb setCenterY:20];
            [self.bgContentView addSubview:_leftLb];
        }
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutCell:(NSDictionary *)data
{
    _leftLb.text = data[@"title"];
    if (data[@"showValue"]) {
        _rightLb.text = data[@"showValue"];
    }
    if (data[@"imageName"]) {
        _iconView.image = [UIImage imageNamed:data[@"imageName"]];
    }
}

- (void)layoutJobDetailCell:(ISSJobDetailCellModel *)model
{
    _leftLb.text = model.title;
    _rightLb.text = model.showValue;
    _rightLb.textColor = model.textColor;
    
    _rightLb.numberOfLines = 0;
    _rightLb.frame = [ISSPubfun getLabelRect:_rightLb];
    
    CGRect rect = self.frame;
    rect.size.height = MAX(50, CGRectGetMaxY(_rightLb.frame) + CGRectGetMinY(_rightLb.frame));
    [super setFrame:rect];
}

@end
