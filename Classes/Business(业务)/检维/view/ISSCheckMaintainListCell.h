//
//  ISSCheckMaintainListCell.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSCheckMaintainListCell : UITableViewCell
{
    UIView *_bgContentView;
    
    UILabel *_titleLb;
    UILabel *_dateLb;
    UILabel *_numberLb;
    
    UILabel *_addressLb;
}

- (void)layoutCell:(NSDictionary *)data;

@end
