//
//  ISSCheckMaintainDetailCell.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISSBaseRoundCell.h"

#import "ISSJobDetailCellModel.h"

@interface ISSCheckMaintainDetailCell : ISSBaseRoundCell
{
    UILabel *_leftLb;
    //UILabel *_rightLb;
    
    UIImageView *_iconView;
}

@property (nonatomic,strong) UILabel *rightLb;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier type:(NSInteger)type;

- (void)layoutCell:(NSDictionary *)data;

- (void)layoutJobDetailCell:(ISSJobDetailCellModel *)model;

@end
