//
//  ISSCheckTopView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCheckTopView.h"

@implementation ISSCheckTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, Screen_Width, 44)];
        titleLb.text = LoadLanguagesKey(@"check_main_navTitle");
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.font = [UIFont boldSystemFontOfSize:18];
        titleLb.textColor = [UIColor describeColor_61];
        [self addSubview:titleLb];
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(titleLb.frame) + 8, 30, 30)];
        icon.layer.cornerRadius = CGRectGetHeight(icon.frame)/2;
        icon.layer.masksToBounds = YES;
        [self addSubview:icon];
        
        UIImage *placeHolder = nil;
        if ([@"男" isEqualToString:[ISSUserData sharedInstance].sex])
            placeHolder = [UIImage imageNamed:@"men_photo"];
        else placeHolder = [UIImage imageNamed:@"women_photo"];
        icon.image = placeHolder;
        
        UILabel *nameLb = [UILabel oneLineLbWithX:CGRectGetMaxX(icon.frame) + 15 y:CGRectGetMinY(icon.frame)-4 fontSize:14 color:[UIColor titleColor_26] text:[ISSUserData sharedInstance].realName];
        //[nameLb setCenterY:icon.centerY - CGRectGetHeight(icon.frame)/4];
        [self addSubview:nameLb];
        
        UILabel *descLb = [UILabel oneLineLbWithX:CGRectGetMinX(nameLb.frame) y:CGRectGetMaxY(nameLb.frame) + 4 fontSize:12 color:[UIColor describeColor_9a] text:[ISSUserData sharedInstance].orgName];
        //[descLb setCenterY:icon.centerY + CGRectGetHeight(icon.frame)/4];
        [self addSubview:descLb];
        
        NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
        [dateF setDateFormat:@"yyyy年MM月dd日"];
        
        UILabel *dateLb = [UILabel oneLineLbWithX:Screen_Width - 100 - 15 y:0 width:100 fontSize:12 color:UIColorFromRGB(0x5e97f6) text:[dateF stringFromDate:[NSDate date]]];
        dateLb.textAlignment = NSTextAlignmentRight;
        [dateLb setCenterY:icon.centerY];
        [self addSubview:dateLb];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - 1, Screen_Width, 1)];
        line.backgroundColor = [UIColor borderColor];
        [self addSubview:line];
    }
    return self;
}

- (void)loadViewData
{
    
}

@end
