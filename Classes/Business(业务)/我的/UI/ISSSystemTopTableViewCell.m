
//
//  ISSSystemTopTableViewCell.m
//  SmartBuildingSite
//
//  Created by XT Xiong on 2017/10/16.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSSystemTopTableViewCell.h"
#import "UIImageView+WebCache.h"
//#import "ISSLoginUserModel.h"
#import "Masonry.h"

@interface ISSSystemTopTableViewCell()
{
    UIImageView     * backgroudIV;

}
@end

@implementation ISSSystemTopTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        backgroudIV = [[UIImageView alloc]init];
        backgroudIV.image = [UIImage imageNamed:@"mine_top_bg"];
        [self.contentView addSubview:backgroudIV];
        [backgroudIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@227);
            make.left.equalTo(self.contentView.mas_left);
            make.right.equalTo(self.contentView.mas_right);
        }];
        
        _headIV = [[UIImageView alloc]init];
        _headIV.layer.cornerRadius = 35;
        _headIV.layer.masksToBounds = YES;
        _headIV.layer.borderColor = [UIColor whiteColor].CGColor;
        _headIV.layer.borderWidth = 2;
        [self.contentView addSubview:_headIV];
//        [self.contentView addConstraint:[headIV constraintCenterXInContainer]];
//        [self.contentView addConstraints:[headIV constraintsTopInContainer:54]];
        [_headIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@70);
            make.center.equalTo(self.contentView);
        }];
        
        _usernameLabel = [[UILabel alloc]init];
        _usernameLabel.font = FontSIZE(18);
        _usernameLabel.textColor = [UIColor whiteColor];
        _usernameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_usernameLabel];
        [_usernameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_headIV.mas_bottom).offset(12);
            make.centerX.equalTo(self.contentView);
        }];
        
        _signatureLabel = [[UILabel alloc]init];
        _signatureLabel.font = FontSIZE(12);
        _signatureLabel.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        _signatureLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_signatureLabel];
        [_signatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_usernameLabel.mas_bottom).offset(8);
            make.centerX.equalTo(self.contentView);
        }];
    }
    return self;
}

@end
