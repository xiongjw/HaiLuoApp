//
//  ISSUserInfoBaseCell.m
//  SmartBuildingSite
//
//  Created by WuLeilei on 2017/12/28.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSUserInfoBaseCell.h"

@implementation ISSUserInfoBaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 11, 22, 22)];
        _icon.image = [UIImage imageNamed:@"noEdite"];
        [self.contentView addSubview:_icon];
        
        _keyLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_icon.frame) + 10, 0, 100, 44)];
        _keyLabel.font = [UIFont systemFontOfSize:15];
        _keyLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:_keyLabel];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
