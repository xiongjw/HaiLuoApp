//
//  ISSSystemTableViewCell.m
//  SmartBuildingSite
//
//  Created by XT Xiong on 2017/10/26.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSSystemTableViewCell.h"

@interface ISSSystemTableViewCell()
{
    UIImageView     * logoIV;
    UILabel         * titleLabel;
    UIView          * bottomLine;
}
@end

@implementation ISSSystemTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        logoIV = [[UIImageView alloc]init];
        [self.contentView addSubview:logoIV];
        [logoIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView.mas_left).offset(24);
        }];
        
        titleLabel = [[UILabel alloc]init];
        titleLabel.font = FontSIZE(14);
        titleLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(logoIV.mas_right).offset(12);
        }];
        
        bottomLine = [[UIImageView alloc]init];
        bottomLine.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:bottomLine];
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.left.equalTo(self.contentView.mas_left);
            make.right.equalTo(self.contentView.mas_right);
        }];
        
    }
    return self;
}

- (void)conFigDataTitle:(NSString *)titleStr Image:(NSString *)imageStr HiddenLine:(BOOL )isHiddenLine
{
    logoIV.image = [UIImage imageNamed:imageStr];
    titleLabel.text = titleStr;
    bottomLine.hidden = isHiddenLine;
}

@end
