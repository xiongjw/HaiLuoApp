//
//  ISSChooseCompanyVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/24.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^chooseCompanyBlock)(NSDictionary *data);

@interface ISSChooseCompanyVC : ISSBaseTableVC

@property (nonatomic,copy) NSString *sonId;
@property (nonatomic,copy) chooseCompanyBlock chooseBlock;

@end
