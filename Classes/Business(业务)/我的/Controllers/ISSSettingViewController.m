//
//  ISSSettingViewController.m
//  SmartBuildingSite
//
//  Created by XT Xiong on 2017/11/16.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSSettingViewController.h"
#import "ISSSettingsNotificationCell.h"
//#import "ISSLoginUserModel.h"
//#import <StoreKit/StoreKit.h> // 评分库

@interface ISSSettingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic , strong) UITableView           * mainTableView;

@end

@implementation ISSSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

     [self setTitle:LoadLanguagesKey(@"mine_setting_navTitle") showBackBtn:YES];
    
    [self.mainTableView registerClass:[ISSSettingsNotificationCell class] forCellReuseIdentifier:@"ISSSettingsNotificationCell"];
    [self.view addSubview:self.mainTableView];
}


#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if (indexPath.row == 0) {
        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageList"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    } else if (indexPath.row == 1) {
        __weak typeof(self) weakSelf = self;
        ISSSettingsNotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSSettingsNotificationCell" forIndexPath:indexPath];
        cell.label.text = @"接收推送通知";
        cell.theSwitch.on = [self.class isCarRecongizeOn];
        cell.changeSwitch = ^(BOOL isOn) {
            [weakSelf.class setCarRecongizeOn:isOn];
        };
        return cell;
    }else
     */
    if (indexPath.row == 0)
    {
        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageList"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = LoadLanguagesKey(@"mine_setting_point");
        cell.textLabel.font = FontSIZE(14);
        cell.textLabel.textColor = [UIColor blackColor];
        UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(16, 54, Screen_Width - 16, 0.5)];
        lineView.backgroundColor = [UIColor grayColor];
        [cell addSubview:lineView];
        return cell;
    }
    else
    {
        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageList"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = LoadLanguagesKey(@"mine_setting_clearCache");
        cell.textLabel.font = FontSIZE(14);
        cell.textLabel.textColor = [UIColor blackColor];
        UILabel * caculateLabel = [[UILabel alloc]init];
        //caculateLabel.text = [NSString stringWithFormat:@"%.1fM",[self filePath]];
        caculateLabel.text = [self getSize];
        caculateLabel.textColor = [UIColor grayColor];
        caculateLabel.font = FontSIZE(14);
        [cell.contentView addSubview:caculateLabel];
        [caculateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.contentView);
            make.right.equalTo(cell.contentView.mas_right).offset(-16);
        }];

        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        NSString *urlStr = FormatString(@"itms-apps://itunes.apple.com/app/id%@",@"1385014290");//详情页
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
    else if(indexPath.row == 1)
    {
        //[self clearFile];
        [self clearCacheOnCompletion:^{
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
    }
}

#pragma mark - Setter && Getter

-(UITableView *)mainTableView
{
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64) style:UITableViewStylePlain];
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.backgroundColor = [UIColor clearColor];
        _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _mainTableView;
}

- (NSString *)getSize
{
    NSUInteger tmpSize = [[SDImageCache sharedImageCache] getSize];
    
    NSString *clearCacheName = @"";
    if (tmpSize >= 1024*1024*1024) clearCacheName = FormatString(@"%.2fG",tmpSize /(1024.f*1024.f*1024.f));
    else if (tmpSize >= 1024*1024) clearCacheName = FormatString(@"%.2fM",tmpSize /(1024.f*1024.f));
    else clearCacheName = FormatString(@"%.2fK",tmpSize / 1024.f);
    
    return clearCacheName;
}

- (void)clearCacheOnCompletion:(void (^)(void))completion
{
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
        if (completion) {
            completion();
        }
    }];
}
#pragma mark - caculateFileSize
//清理缓存
-(void)clearFile
{
    NSString * cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    
    NSArray * files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];
    
    NSLog(@"cachpath = %@", cachPath);
    
    for (NSString * p in files) {
        
        NSError * error = nil;
        
        NSString * path = [cachPath stringByAppendingPathComponent:p];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            
        }
    }
    
//    [self clearCachSuccess];
//    ALERT(@"清理完毕");
    [self.mainTableView reloadData];
}

//显示缓存大小
-(float)filePath
{
    NSString * cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    return [self folderSizeAtPath:cachPath];
}

//遍历文件夹获得文件夹大小
-(float)folderSizeAtPath:(NSString*) folderPath{
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if (![manager fileExistsAtPath:folderPath]) return 0;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    
    long long folderSize = 0;
    
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
        
    }
    return folderSize/(1024.0*1024.0);
}

-(unsigned long long)fileSizeAtPath:(NSString*) filePath{
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath]){
        
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
        
    }
    
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (NSString *)getCarRecongizeOnKey {
    return [NSString stringWithFormat:@"%@-CarRecongizeOn",[ISSDataStorage getObject:@"ISSUserName"]];
}

+ (BOOL)isCarRecongizeOn {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isOn = [userDefaults boolForKey:[self.class getCarRecongizeOnKey]];
    return isOn;
}

+ (void)setCarRecongizeOn:(BOOL)isOn {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
   [userDefaults setBool:isOn forKey:[self.class getCarRecongizeOnKey]];
    [userDefaults synchronize];
}

@end
