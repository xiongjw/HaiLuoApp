
//
//  ISSFreedBackViewController.m
//  SmartBuildingSite
//
//  Created by XT Xiong on 2017/11/16.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSFreedBackViewController.h"
#import "XTTextView.h"

@interface ISSFreedBackViewController ()<UITextViewDelegate>

@property(nonatomic,strong) XTTextView          * feedbackTF;
@property(nonatomic,strong) UIButton            * sureBtn;


@end

@implementation ISSFreedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self setTitle:LoadLanguagesKey(@"mine_feedback_navTitle") showBackBtn:YES];
    
    [self initView];
}

- (void)initView
{
    UIImageView * bgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 220)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    _feedbackTF = [[XTTextView alloc]init];
    _feedbackTF.frame = CGRectMake(16, 16, Screen_Width - 32, 220 - 32);
    _feedbackTF.font = FontSIZE(14);
    _feedbackTF.placeholder = LoadLanguagesKey(@"mine_feedback_need");
    _feedbackTF.placeholderTextColor = [UIColor grayColor];
    _feedbackTF.delegate = self;
    _feedbackTF.layer.borderColor = [UIColor grayColor].CGColor;
    _feedbackTF.layer.borderWidth = 0.5f;
    _feedbackTF.layer.masksToBounds = YES;
    [self.view addSubview:_feedbackTF];
    
    _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _sureBtn.frame = CGRectMake(16, 236, Screen_Width - 32, 48);
    [_sureBtn setTitle:LoadLanguagesKey(@"btn_commit") forState:UIControlStateNormal];
    _sureBtn.layer.cornerRadius = 5;
    _sureBtn.layer.masksToBounds = YES;
    //[_sureBtn setBackgroundColor:[UIColor themeColor]];
    [_sureBtn setBackgroundImage:[UIImage imageWithColor:[UIColor themeColor]] forState:UIControlStateNormal];
    [_sureBtn addTarget:self action:@selector(sureBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sureBtn];
}


- (void)sureBtnAction
{
    if (_feedbackTF.text.length == 0) {
        return;
    }
    [self.view endEditing:YES];
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"info/addFeedBack" param:@{@"content":_feedbackTF.text} view:self.view successBlock:^(NSDictionary *resultData) {
        //[weakself.view makeToast:@"谢谢您的宝贵意见"];
        [weakself.view makeToast:LoadLanguagesKey(@"mine_feedback_thanks") duration:2 position:CSToastPositionCenter title:nil image:nil style:nil completion:^(BOOL didTap) {
            [weakself.navigationController popViewControllerAnimated:YES];
        }];
        
    }];
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
