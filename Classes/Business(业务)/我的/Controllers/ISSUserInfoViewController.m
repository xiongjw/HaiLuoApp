//
//  ISSUserInfoViewController.m
//  SmartBuildingSite
//
//  Created by WuLeilei on 2017/12/28.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSUserInfoViewController.h"
#import "ISSUserInfoEditCell.h"
#import "ISSUserInfoReadCell.h"
#import "ISSUserInfoSexCell.h"
//#import "ISSLoginUserModel.h"
//#import "LoadingManager.h"

#import "ISSEditPwdVC.h"
#import "ISSBindPhoneVC.h"

#import "ISSChooseCompanyVC.h"

@interface ISSUserInfoViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate>
{
   // ISSTaskPeopleModel *_editModel;
}
@end

@implementation ISSUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"个人中心" showBackBtn:YES];
    
    [self.mTableView registerClass:[ISSUserInfoEditCell class] forCellReuseIdentifier:@"ISSUserInfoEditCell"];
    [self.mTableView registerClass:[ISSUserInfoReadCell class] forCellReuseIdentifier:@"ISSUserInfoReadCell"];
    [self.mTableView registerClass:[ISSUserInfoSexCell class] forCellReuseIdentifier:@"ISSUserInfoSexCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    v.backgroundColor = ISSBackgroundColor;
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (section) {
        case 0:
            count = 3;
            break;
            
        case 1:
            count = 2;
            break;
            
        case 2:
            count = 2;
            //count = 1;
            break;
            
        case 3:
            count = 1;
            break;
        case 4:
            count = 1;
            break;
        default:
            break;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *theCell;
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            ISSUserInfoReadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoReadCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_name");
            cell.textField.text = [ISSUserData sharedInstance].realName;
            cell.textField.tag = 1;
            theCell = cell;
        }
        else if (indexPath.row == 1)
        {
            ISSUserInfoReadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoReadCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_role");
            cell.textField.text = [ISSUserData sharedInstance].userRole;
            theCell = cell;
        }
        else if (indexPath.row == 2)
        {
            /*
            ISSUserInfoSexCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoSexCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if([[ISSUserData sharedInstance].sex isEqualToString:@"男"]){
                
                cell.sex = 0;
            }else{
                
                cell.sex = 1;
            }
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_sex");
            
            theCell = cell;
             */
            ISSUserInfoReadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoReadCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_sex");
            cell.textField.text = [ISSUserData sharedInstance].sex;
            theCell = cell;
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            ISSUserInfoReadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoReadCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_account");
            // cell.textField.text = _editModel.account;
            cell.textField.text = [ISSDataStorage getObject:@"ISSUserName"];
            theCell = cell;
        }
        else if(indexPath.row == 1)
        {
            ISSUserInfoEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoEditCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_pwd");
            cell.textField.placeholder = LoadLanguagesKey(@"mine_personal_pwd_reset");
            //cell.textField.text = self.changePassWord;
            cell.textField.text = @"******";
            cell.textField.enabled = NO;
            cell.icon.image = [UIImage imageNamed:@"personal_edit"];
            theCell = cell;
        }
    }
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            ISSUserInfoReadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoReadCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_phone");
            
            NSString *phoneNum = [ISSUserData sharedInstance].phoneNum;
            if (phoneNum.length >= 11) {
                cell.textField.text = FormatString(@"%@****%@",[phoneNum substringToIndex:3],[phoneNum substringFromIndex:7]);
            }
            else cell.textField.text =phoneNum;
            cell.textField.tag = 2;
            theCell = cell;
        }
        else if (indexPath.row == 1)
        {
            ISSUserInfoEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoEditCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_company");
            cell.textField.text = [ISSUserData sharedInstance].companyName;
            cell.textField.enabled = NO;
            cell.icon.image = [UIImage imageNamed:@"personal_edit"];
            theCell = cell;
        }
    }
    else if (indexPath.section == 3)
    {
        if (indexPath.row == 0) {
            ISSUserInfoReadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoReadCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_department");
            //cell.textField.text = _editModel.des;
            cell.textField.text = [ISSUserData sharedInstance].orgName;
            cell.textField.tag = 3;
            theCell = cell;
        }
    }
    else if (indexPath.section == 4)
    {
        if (indexPath.row == 0) {
            ISSUserInfoEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ISSUserInfoEditCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.keyLabel.text = LoadLanguagesKey(@"mine_personal_changeBindPhone");
            //[cell.keyLabel sizeToFit];
            [cell.keyLabel setWidth:200];
            cell.textField.hidden = YES;
            cell.icon.image = [UIImage imageNamed:@"personal_edit"];
            theCell = cell;
        }
    }
    return theCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 1)
    {
        if(indexPath.row==1)
        {
            ISSEditPwdVC *vc = [[ISSEditPwdVC alloc] init];
            [self presentViewController:vc animated:YES completion:NULL];
        }
    }
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 1)
        {
            ISSChooseCompanyVC *vc = [[ISSChooseCompanyVC alloc] init];
            vc.chooseBlock = ^(NSDictionary *data) {
                
                BOOL isNeedLocalRefresh = YES;
                if (isNeedLocalRefresh)
                {
                    // 本地刷新，直接改变值
                    [ISSUserData sharedInstance].companyId = data[@"id"];
                    [ISSUserData sharedInstance].companyName = data[@"orgName"];
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
                else
                {
                    // 调用接口，改了就改了，没改就算了
                    [[ISSUserData sharedInstance] queryUserInfo:nil successBlock:^{
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }];
                }
            };
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            [self presentViewController:nav animated:YES completion:NULL];
        }
    }
    else if (indexPath.section == 4)
    {
        ISSBindPhoneVC *vc = [[ISSBindPhoneVC alloc] init];
        vc.bindPhoneSuccessBlock = ^(NSString *bindPhone) {
            
            [ISSUserData sharedInstance].phoneNum = bindPhone;
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];
        };
        [self presentViewController:vc animated:YES completion:NULL];
    }
}

@end
