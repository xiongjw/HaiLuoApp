//
//  ISSPersonalVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSPersonalVC.h"
#import "ISSSystemTopTableViewCell.h"
#import "ISSSystemTableViewCell.h"

#import "ISSAboutUsViewController.h"
#import "ISSFreedBackViewController.h"
#import "ISSSettingViewController.h"
//#import "ISSLoginUserModel.h"
#import "ISSUserInfoViewController.h"

@interface ISSPersonalVC ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic , strong) UITableView           * mainTableView;

@end

@implementation ISSPersonalVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.mainTableView];
    
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.mainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 227;
    }else if(indexPath.row == 2){
        return 10;
    }else if(indexPath.row == 6){
        return 10;
    }else{
        return 48;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        //顶部
        ISSSystemTopTableViewCell * cell = [[ISSSystemTopTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"HomeTop"];
        cell.usernameLabel.text = [ISSUserData sharedInstance].realName;
        cell.signatureLabel.text = [ISSUserData sharedInstance].companyName;
        if([[ISSUserData sharedInstance].sex isEqualToString:@"男"])
        {
            cell.headIV.image = [UIImage imageNamed:@"men_photo"];
        }
        else
        {
             cell.headIV.image = [UIImage imageNamed:@"women_photo"];
        }
        return cell;
    }else if(indexPath.row == 1){
        ISSSystemTableViewCell * cell = [[ISSSystemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"usercenter"];
        [cell conFigDataTitle:LoadLanguagesKey(@"mine_personal_navTitle") Image:@"mine_person_norma" HiddenLine:YES];
        return cell;
    }else if (indexPath.row == 3){
        ISSSystemTableViewCell * cell = [[ISSSystemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeMessage"];
        [cell conFigDataTitle:LoadLanguagesKey(@"mine_aboutUs_navTitle") Image:@"mine_about"  HiddenLine:NO];
        return cell;
    }else if (indexPath.row == 4){
        ISSSystemTableViewCell * cell = [[ISSSystemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeMessage"];
        [cell conFigDataTitle:LoadLanguagesKey(@"mine_feedback_navTitle") Image:@"mine_idea"  HiddenLine:NO];
        return cell;
    }else if (indexPath.row == 5){
        ISSSystemTableViewCell * cell = [[ISSSystemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeMessage"];
        [cell conFigDataTitle:LoadLanguagesKey(@"mine_setting_navTitle") Image:@"mine_setting"  HiddenLine:YES];
        return cell;
    } else if (indexPath.row == 7){
        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeMessage"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UILabel * signOutLabel = [[UILabel alloc]init];
        signOutLabel.font = FontSIZE(16);
        signOutLabel.textColor = [UIColor themeColor];
        signOutLabel.text = LoadLanguagesKey(@"mine_logout");
        signOutLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:signOutLabel];
        [signOutLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.contentView);
            make.centerX.equalTo(cell.contentView);
        }];
        return cell;
    }else if (indexPath.row == 8){
        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeMessage"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UILabel * vLabel = [[UILabel alloc]init];
        vLabel.font = FontSIZE(10);
        vLabel.textColor = [UIColor grayColor];
        vLabel.text = [NSString stringWithFormat:@"V%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
                       // [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
        vLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:vLabel];
        [vLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(cell.contentView);
            make.top.equalTo(cell.contentView.mas_top).offset(16);
            make.height.equalTo(@20);
        }];
        UILabel * nLabel = [[UILabel alloc]init];
        nLabel.font = FontSIZE(10);
        nLabel.textColor = [UIColor grayColor];
        nLabel.text = LoadLanguagesKey(@"info_main_title");
        nLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:nLabel];
        [nLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(cell.contentView);
            make.top.equalTo(vLabel.mas_bottom).offset(-1);
            make.height.equalTo(@20);
        }];
        return cell;
    }else{
        UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeMessage"];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1)
    {
        ISSUserInfoViewController * userCenterVC = [[ISSUserInfoViewController alloc]initWithStyle:UITableViewStylePlain];
        userCenterVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:userCenterVC animated:YES];
    }
    else if (indexPath.row == 3)
    {
        ISSAboutUsViewController * aboutUsVC = [[ISSAboutUsViewController alloc]init];
        aboutUsVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:aboutUsVC animated:YES];
    }
    else if (indexPath.row == 4)
    {
        ISSFreedBackViewController * freedBackVC = [[ISSFreedBackViewController alloc]init];
        freedBackVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:freedBackVC animated:YES];
    }
    else if (indexPath.row == 5)
    {
        ISSSettingViewController * settingVC = [[ISSSettingViewController alloc]init];
        settingVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:settingVC animated:YES];
    }
    else if (indexPath.row == 7)
    {
        [ShareApp setWindowRootVC:2];
    }
}


-(UITableView *)mainTableView
{
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, -64, Screen_Width, Screen_Height-49) style:UITableViewStylePlain];
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.backgroundColor = [UIColor clearColor];
        _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _mainTableView;
}
@end
