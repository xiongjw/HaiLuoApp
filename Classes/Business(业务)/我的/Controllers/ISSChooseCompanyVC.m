//
//  ISSChooseCompanyVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/24.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSChooseCompanyVC.h"

@interface ISSChooseCompanyVC ()

@end

@implementation ISSChooseCompanyVC

- (void)queryList
{
    NSString *companyId = [ISSUserData sharedInstance].companyId;
    if (companyId.length > 0) {
        [self requestList:companyId];
    }
    else
    {
        WeakSelf(self)
        [[ISSUserData sharedInstance] queryUserInfo:self.view successBlock:^{
            NSString *companyId = [ISSUserData sharedInstance].companyId;
            [weakself requestList:companyId];
        }];
    }
}

- (void)requestList:(NSString *)companyId
{
    //查询子公司
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"organization/getSubCompanyList" param:@{@"id":companyId} view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSArray *list = resultData[@"data"];
        if (list.count > 0)
        {
            [weakself.resultDataList addObjectsFromArray:list];
            [weakself.mTableView reloadData];
        }
        else
        {
            [weakself showAlertControllerWithMessage:LoadLanguagesKey(@"mine_personal_company_noSonCompany") handler:^(UIAlertAction *action) {
                [weakself dismissViewControllerAnimated:YES completion:NULL];
            }];
        }
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = LoadLanguagesKey(@"levelSelected_navTitle");
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        
        if (!weakself.sonId) [weakself dismissViewControllerAnimated:YES completion:NULL];
        else [weakself.navigationController popViewControllerAnimated:YES];
    }];
    
    self.mTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 8)];
    
    if (self.sonId) [self requestList:self.sonId];
    else [self queryList];
    
    if (!self.sonId)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedAction:) name:@"selectedCompanyNote" object:nil];
    }
}

- (void)selectedAction:(NSNotification *)note
{
    [self successBack:note.object];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = self.resultDataList[indexPath.row];
    cell.textLabel.text = data[@"orgName"];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    if ([data[@"hasChild"] intValue] == 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate withTitle: colorType
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *data = self.resultDataList[indexPath.row];
    
    if ([data[@"hasChild"] intValue] == 1)
    {
        ISSChooseCompanyVC *vc = [[ISSChooseCompanyVC alloc] init];
        vc.sonId = data[@"id"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        // 切换公司
        WeakSelf(self)
        [X_HttpUtil apiRequest:@"organization/updateCurrentUserFactoryId" param:@{@"factoryId":data[@"id"],@"companyName":data[@"orgName"]} view:self.view successBlock:^(NSDictionary *resultData) {
            
            [weakself chooseSuccess:data];
        }];
    }
    
    
}

- (void)chooseSuccess:(NSDictionary *)data
{
    if (self.navigationController.viewControllers.count > 1)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedCompanyNote" object:data];
        [self.navigationController popViewControllerAnimated:NO];
    }
    else
    {
        [self successBack:data];
    }
}

- (void)successBack:(NSDictionary *)data
{
    if (_chooseBlock) {
        _chooseBlock(data);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
