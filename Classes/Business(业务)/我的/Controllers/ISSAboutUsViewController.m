//
//  ISSAboutUsViewController.m
//  SmartBuildingSite
//
//  Created by XT Xiong on 2017/11/16.
//  Copyright © 2017年 iSoftStone. All rights reserved.
//

#import "ISSAboutUsViewController.h"

@interface ISSAboutUsViewController ()

@end

@implementation ISSAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setTitle:LoadLanguagesKey(@"mine_aboutUs_navTitle") showBackBtn:YES];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self initView];
}


- (void)initView
{
    /*
    UIImageView * imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"launch"];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(90);
    }];

    UILabel * nameLabel = [[UILabel alloc]init];
    nameLabel.text =LoadLanguagesKey(@"info_main_title");
    nameLabel.font = FontSIZE(18);
    nameLabel.textColor = [UIColor blackColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:nameLabel];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(imageView.mas_top).offset(16);
    }];
     */
    UIImage *image = [UIImage imageNamed:@"login_logo"];
    //确定位置
    UIImageView *logoView = [[UIImageView alloc] initWithImage:image];
    [logoView setCenterX:self.view.centerX];
    [logoView setTop:100];
    [self.view addSubview:logoView];
    
    UILabel * versionLabel = [[UILabel alloc]init];
    versionLabel.text = [NSString stringWithFormat:@"V %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    versionLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
    versionLabel.textColor = [UIColor blackColor];
    versionLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:versionLabel];
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(logoView.mas_bottom).offset(20);
    }];

    UILabel * copyrightLabel = [[UILabel alloc]init];
    copyrightLabel.text = [NSString stringWithFormat:@"%@ %@ %@",@"© 2018",LoadLanguagesKey(@"info_main_title"),@"All Rights Reserved."];
    copyrightLabel.font = FontSIZE(12);
    copyrightLabel.textColor = [UIColor grayColor];
    copyrightLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:copyrightLabel];
    [copyrightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom).offset(-24);
    }];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
