//
//  ISSAddNewSelectCell.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/14.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSAddNewSelectCell : UITableViewCell
@property(nonatomic,strong) UILabel *inputLabel;
//@property(nonatomic,strong)UIButton *selectButton;
@property(nonatomic,strong) UIImageView *arrowView;
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withTitle:(NSString *)titleStr;
@end
