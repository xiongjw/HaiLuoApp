//
//  ISSPhotoCell.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSPhotoCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *imageView;

@end
