//
//  ISSAddNewTimeCell.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/14.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSAddNewTimeCell : UITableViewCell

@property (nonatomic, strong) UILabel           * timeLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withTitle:(NSString *)titleStr;

@end
