//
//  ISSAddNewTimeCell.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/14.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSAddNewTimeCell.h"

@interface ISSAddNewTimeCell() {
    
    UILabel           * titleLabel;
}
@end

@implementation ISSAddNewTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withTitle:(NSString *)titleStr{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 50)];
        titleLabel.text = titleStr;
        titleLabel.textColor = UIColorFromRGB(0x262626);
        titleLabel.font = BoldFontSIZE(15);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:titleLabel];
        titleLabel.numberOfLines = 2;
        /*
        titleLabel.preferredMaxLayoutWidth = (100);
        [titleLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(20);
            make.centerY.equalTo(self.contentView);
            
        }];
         */
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.textColor = UIColorFromRGB(0x9a9a9a);
        _timeLabel.font = BoldFontSIZE(15);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-20);
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(titleLabel.mas_right).offset(15);
            make.height.equalTo(@20);
        }];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
