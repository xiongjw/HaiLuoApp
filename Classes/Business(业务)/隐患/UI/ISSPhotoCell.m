//
//  ISSPhotoCell.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//
#import "ISSPhotoCell.h"

@implementation ISSPhotoCell

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
//    self.layer.borderWidth = 1;
//    self.layer.borderColor = UIColorFromRGB(0x9a9a9a).CGColor;
    _imageView = [[UIImageView alloc]init];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    [self.contentView addSubview:_imageView];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).offset(0);
//        make.right.equalTo(self.mas_right).offset(0);
//        make.top.equalTo(self.mas_top).offset(0);
//        make.top.equalTo(self.mas_bottom).offset(0);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self);
        make.width.height.equalTo(@70);
    }];
}

@end
