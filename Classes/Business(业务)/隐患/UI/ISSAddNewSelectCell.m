//
//  ISSAddNewSelectCell.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/14.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSAddNewSelectCell.h"

@interface ISSAddNewSelectCell() {
    
    UILabel           * titleLabel;
}
@end

@implementation ISSAddNewSelectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withTitle:(NSString *)titleStr{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        titleLabel = [[UILabel alloc]init];
        titleLabel.text = titleStr;
        titleLabel.textColor = UIColorFromRGB(0x262626);
        titleLabel.font = BoldFontSIZE(15);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:titleLabel];
        titleLabel.preferredMaxLayoutWidth = (100);
        [titleLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(20);
            make.centerY.equalTo(self.contentView);
        }];
        /*
        _selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_selectButton setImage:[UIImage imageNamed:@"arrow_up"] forState:UIControlStateNormal];
        [_selectButton setBackgroundColor:UIColorFromRGB(0x2c68ec)];
        [self.contentView addSubview:_selectButton];
        [_selectButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-20);
            make.centerY.equalTo(self.contentView);
            make.width.equalTo(@30);
            make.height.equalTo(@30);
        }];
        _selectButton.layer.cornerRadius = 15;
         */
        _arrowView = [[UIImageView alloc] init];
        _arrowView.backgroundColor = UIColorFromRGB(0x2c68ec);
        _arrowView.layer.cornerRadius = 15;
        _arrowView.image = [UIImage imageNamed:@"arrow_up"];
        _arrowView.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_arrowView];
        [_arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-20);
            make.centerY.equalTo(self.contentView);
            make.width.equalTo(@30);
            make.height.equalTo(@30);
        }];
        
        _inputLabel = [[UILabel alloc]init];
        _inputLabel.textColor = UIColorFromRGB(0x9a9a9a);
        _inputLabel.textAlignment = NSTextAlignmentRight;
        _inputLabel.font = BoldFontSIZE(15);
        [self.contentView addSubview:_inputLabel];
        [_inputLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleLabel.mas_right).offset(15);
            make.right.equalTo(_arrowView.mas_left).offset(-8);
            make.centerY.equalTo(self.contentView);
            make.height.equalTo(@20);
        }];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
