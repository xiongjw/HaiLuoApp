//
//  ISSAddNewAnonymousCell.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSAddNewAnonymousCell : UITableViewCell

@property(nonatomic , strong)UIButton * leftButton;
@property(nonatomic , strong)UIButton * rightButton;
@property(nonatomic,strong)UILabel         * titleLabel;

@end
