//
//  ISSAddNewAnonymousCell.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSAddNewAnonymousCell.h"

@interface ISSAddNewAnonymousCell()
{
    
    UIView          * line;
}
@end

@implementation ISSAddNewAnonymousCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLabel = [[UILabel alloc]init];
        self.titleLabel.font = BoldFontSIZE(15);
        self.titleLabel.textColor = UIColorFromRGB(0x262626);
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(20);
            make.top.equalTo(self.contentView.mas_top).offset(0);
            make.height.equalTo(@50);
            make.width.lessThanOrEqualTo(@100);
        }];
        self.titleLabel.numberOfLines = 2;
        
        _rightButton = [[UIButton alloc] init];
        _rightButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_rightButton setTitleColor:UIColorFromRGB(0x9a9a9a) forState:UIControlStateNormal];
        [_rightButton setImage:[UIImage imageNamed:@"add_cell_ratio"] forState:UIControlStateNormal];
        [_rightButton setImage:[UIImage imageNamed:@"add_cell_ratioseltct"] forState:UIControlStateSelected];
        _rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
        _rightButton.titleLabel.font = FontSIZE(14);
        [self.contentView addSubview:_rightButton];
        [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.right.equalTo(self.contentView.mas_right).offset(-30);
            make.centerY.equalTo(self.contentView);
            make.width.equalTo(@80);
            make.height.equalTo(@30);
        }];
        
        _leftButton = [[UIButton alloc] init];
        _leftButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_leftButton setTitleColor:UIColorFromRGB(0x9a9a9a) forState:UIControlStateNormal];
        [_leftButton setImage:[UIImage imageNamed:@"add_cell_ratio"] forState:UIControlStateNormal];
        [_leftButton setImage:[UIImage imageNamed:@"add_cell_ratioseltct"] forState:UIControlStateSelected];
        _leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
        _leftButton.titleLabel.font = FontSIZE(14);
        [self.contentView addSubview:_leftButton];
        [_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.right.equalTo(self.rightButton.mas_left).offset(-20);
            make.centerY.equalTo(self.contentView);
            make.width.equalTo(@80);
            make.height.equalTo(@30);
        }];
        
        line = [[UIView alloc]init];
        line.backgroundColor = [UIColor colorWithRed:233/255.f green:233/255.f blue:233/255.f alpha:1];
        [self.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.contentView);
            make.trailing.equalTo(self.contentView);
            make.height.equalTo(@0.5);
            make.bottom.equalTo(self.contentView.mas_bottom);
        }];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
