//
//  AddNewMessageCell.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XTTextView.h"
@interface AddNewMessageInputCell : UITableViewCell{
    
    
}
@property(nonatomic,strong)UITextField          * textField;
@property(nonatomic,strong)UITextView           * textView;
@property(nonatomic,strong)UITextView           * textPlaceHolder;
@property(nonatomic,strong)UIView               * bgView;
@property(nonatomic,strong)UILabel              * bgLabel;
@property(nonatomic,strong)UILabel              * inputLabel;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withTitle:(NSString *)titleStr PlaceHolder:(NSString *)placeHolder IsShow:(BOOL)isShow ImageName:(NSString *)imageName IsText:(BOOL)isText;

@end

