//
//  AddNewMessageInputCell.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "AddNewMessageInputCell.h"
@interface AddNewMessageInputCell()<UITextFieldDelegate, UITextViewDelegate>
{
    UILabel         * titleLabel;
    
    UIView          * line;
    UIImageView     * inputImage;
    UIView          * textLeftView;
    BOOL              whetherText;
}
@end

@implementation AddNewMessageInputCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withTitle:(NSString *)titleStr PlaceHolder:(NSString *)placeHolder IsShow:(BOOL)isShow ImageName:(NSString *)imageName IsText:(BOOL)isText{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        titleLabel = [[UILabel alloc]init];
        titleLabel.text = titleStr;
        titleLabel.textColor = UIColorFromRGB(0x262626);
        titleLabel.font = BoldFontSIZE(15);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:titleLabel];
        //        titleLabel.preferredMaxLayoutWidth = (100);
        //        [titleLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(20);
            make.top.equalTo(self.contentView.mas_top).offset(0);
            make.height.equalTo(@50);
            make.width.lessThanOrEqualTo(@100);
        }];
        titleLabel.numberOfLines = 2;
        
        inputImage = [[UIImageView alloc]init];
        inputImage.image = [UIImage imageNamed:imageName];
        [self.contentView addSubview:inputImage];
        [inputImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-20);
            make.top.equalTo(self.contentView.mas_top).offset(10);
            make.width.height.equalTo(@30);
            
        }];
        inputImage.layer.cornerRadius = 15;
        inputImage.clipsToBounds = YES;
        
        _inputLabel = [[UILabel alloc]init];
        _inputLabel.textColor = UIColorFromRGB(0x9a9a9a);
        _inputLabel.textAlignment = NSTextAlignmentRight;
        _inputLabel.font = BoldFontSIZE(15);
        [self.contentView addSubview:_inputLabel];
        [_inputLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleLabel.mas_right).offset(15);
            make.right.equalTo(inputImage.mas_left).offset(-8);
            make.top.equalTo(self.contentView).offset(15);
            //            make.height.equalTo(@20);
        }];
        _inputLabel.numberOfLines = 4;
        
        if (isShow) {
            _bgView = [[UIView alloc]init];
            _bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
            [self.contentView addSubview:_bgView];
            [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.contentView.mas_left).offset(10);
                make.right.equalTo(self.contentView.mas_right).offset(-10);
                make.top.equalTo(self.contentView.mas_top).offset(2);
                make.bottom.equalTo(self.contentView.mas_bottom).offset(-2);
            }];
            _bgView.layer.cornerRadius = 5;
            _bgView.clipsToBounds = YES;
            
            _bgLabel = [[UILabel alloc]init];
            _bgLabel.text = titleStr;
            _bgLabel.textColor = [UIColor whiteColor];
            _bgLabel.font = BoldFontSIZE(15);
            _bgLabel.textAlignment = NSTextAlignmentLeft;
            [_bgView addSubview:_bgLabel];
            _bgLabel.hidden = YES;
            //            _bgLabel.preferredMaxLayoutWidth = (100);
            //            [_bgLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
            [_bgLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_bgView.mas_left).offset(10);
                make.top.equalTo(_bgView.mas_top).offset(10);
                make.height.equalTo(@20);
                make.width.lessThanOrEqualTo(@100);
            }];
            
            if (isText) {
                
                _textPlaceHolder =[[UITextView alloc]init];
                _textPlaceHolder.backgroundColor= UIColorFromRGB(0x4c84ff);
                _textPlaceHolder.text = placeHolder;
                _textPlaceHolder.font = [UIFont systemFontOfSize:15];
                _textPlaceHolder.textColor = [UIColor whiteColor];
                _textPlaceHolder.editable = NO;
                [_bgView addSubview:_textPlaceHolder];
                [_textPlaceHolder mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(_bgLabel.mas_right).offset(20);
                    make.right.equalTo(_bgView.mas_right).offset(-5);
                    make.top.equalTo(_bgView.mas_top).offset(5);
                    make.bottom.equalTo(_bgView.mas_bottom).offset(-5);
                }];
                _textPlaceHolder.layer.cornerRadius = 5;
                _textPlaceHolder.clipsToBounds = YES;
                
                _textView = [[UITextView alloc]init];
                _textView.backgroundColor = [UIColor clearColor];
                _textView.textColor = [UIColor whiteColor];
                _textView.text = @"";
                _textView.font = FontSIZE(14);
                _textView.hidden = YES;
                _textView.delegate = self;
                _textView.returnKeyType = UIReturnKeyDone;
                [_bgView addSubview:_textView];
                [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(_bgLabel.mas_right).offset(20);
                    make.right.equalTo(_bgView.mas_right).offset(-5);
                    make.top.equalTo(_bgView.mas_top).offset(5);
                    make.bottom.equalTo(_bgView.mas_bottom).offset(-5);
                }];
                _textView.layer.cornerRadius = 5;
                _textView.clipsToBounds = YES;
            }else{
                textLeftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
                textLeftView.backgroundColor = [UIColor clearColor];
                
                _textField = [[UITextField alloc]init];
                _textField.backgroundColor = [UIColor clearColor];
                _textField.tintColor = [UIColor whiteColor];
                [_textField setValue:UIColorFromRGBA(0xffffff, 0.5) forKeyPath:@"_placeholderLabel.textColor"];
                _textField.textColor = [UIColor whiteColor];
                _textField.placeholder = placeHolder;
                _textField.leftView = textLeftView;
                _textField.leftViewMode = UITextFieldViewModeAlways;
                _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                [_textField setAccessibilityLabel:@"textField"];
                _textField.returnKeyType = UIReturnKeyDone;
                _textField.delegate = self;
                _textField.font = FontSIZE(14);
                [_bgView addSubview:_textField];
                [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(_bgLabel.mas_right).offset(20);
                    make.right.equalTo(_bgView.mas_right).offset(-5);
                    make.top.equalTo(_bgView.mas_top).offset(5);
                    make.bottom.equalTo(_bgView.mas_bottom).offset(-5);
                }];
                _textField.layer.cornerRadius = 5;
                _textField.clipsToBounds = YES;
            }
            _bgView.hidden = YES;
        }
        
        line = [[UIView alloc]init];
        line.backgroundColor = [UIColor colorWithRed:233/255.f green:233/255.f blue:233/255.f alpha:1];
        [self.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.contentView);
            make.trailing.equalTo(self.contentView);
            make.height.equalTo(@0.5);
            make.bottom.equalTo(self.contentView.mas_bottom);
        }];
        
        
        
    }
    return self;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    _bgView.hidden = NO;
    if (whetherText) {
        _textView.hidden = NO;
    }else{
        _textField.backgroundColor = UIColorFromRGB(0x4c84ff);
    }
    _bgLabel.hidden = NO;
    //    _inputLabel.hidden = YES;
    _bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
    _bgView.layer.shadowOffset =  CGSizeMake(0, 6);
    _bgView.layer.shadowOpacity = 0.3;
    _bgView.layer.shadowColor =  UIColorFromRGB(0x1c63ff).CGColor;
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    _bgView.layer.shadowOffset =  CGSizeMake(0, 0);
    _bgView.layer.shadowOpacity = 0;
    _bgView.layer.shadowColor =  [UIColor clearColor].CGColor;
    
    _bgView.hidden = YES;
    if (whetherText) {
        _textView.hidden = YES;
    }else{
        _textField.backgroundColor = [UIColor clearColor];
    }
    _bgLabel.hidden = YES;
    _inputLabel.hidden = NO;
    _inputLabel.text = _textView.text;
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    };
    if(![text isEqualToString:@""])
    {
        [_textPlaceHolder setHidden:YES];
    }
    if([text isEqualToString:@""]&&range.length==1&&range.location==0){
        [_textPlaceHolder setHidden:NO];
    }
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _bgView.hidden = NO;
    if (whetherText) {
        _textView.hidden = NO;
    }else{
        _textField.backgroundColor = UIColorFromRGB(0x4c84ff);
    }
    _bgLabel.hidden = NO;
    //    _inputLabel.hidden = YES;
    _bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
    _bgView.layer.shadowOffset =  CGSizeMake(0, 6);
    _bgView.layer.shadowOpacity = 0.3;
    _bgView.layer.shadowColor =  UIColorFromRGB(0x1c63ff).CGColor;
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    _bgView.layer.shadowOffset =  CGSizeMake(0, 0);
    _bgView.layer.shadowOpacity = 0;
    _bgView.layer.shadowColor =  [UIColor clearColor].CGColor;
    
    _bgView.hidden = YES;
    if (whetherText) {
        _textView.hidden = YES;
    }else{
        _textField.backgroundColor = [UIColor clearColor];
    }
    _bgLabel.hidden = YES;
    _inputLabel.hidden = NO;
    _inputLabel.text = _textField.text;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _inputLabel.text = _textField.text;
    return [textField resignFirstResponder];
}

@end

