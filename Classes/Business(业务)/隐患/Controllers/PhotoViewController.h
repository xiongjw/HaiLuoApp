//
//  PhotoViewController.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SubmitPhotoSuccessBlock)(NSString *photoUrls);

@interface PhotoViewController : ISSBaseVC

@property (nonatomic,  copy) SubmitPhotoSuccessBlock submitPhotoSuccessBlock;

@end
