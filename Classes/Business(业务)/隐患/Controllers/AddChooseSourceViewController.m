//
//  AddChooseSourceViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/25.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "AddChooseSourceViewController.h"

@interface AddChooseSourceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView * tableView;

@end

@implementation AddChooseSourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    if(self.selectRow==2){
         self.navigationItem.title = LoadLanguagesKey(@"add_hiddenTrouble_findUnit");
        
    }else if (self.selectRow==3){
         self.navigationItem.title = LoadLanguagesKey(@"info_detail_breakCompany");
        
    }else{
        
         self.navigationItem.title = LoadLanguagesKey(@"info_detail_dangerSource");
    }
    
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        [weakself dismissViewControllerAnimated:YES completion:NULL];
    }];
    [self createUI];
}
-(void)createUI{
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 108) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableView];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;//最小数，相当于0
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * identifier = @"SourceCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
     cell.textLabel.text =self.dataArray[indexPath.row][@"NAME_"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_chooseSourceBlock) {
        _chooseSourceBlock(_dataArray[indexPath.row]);
    }
     [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
