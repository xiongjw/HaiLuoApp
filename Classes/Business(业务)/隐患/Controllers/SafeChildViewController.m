//
//  SafeChildViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "SafeChildViewController.h"
#import "ISSMessageSegView.h"
#import "ListViewTableViewCell.h"
#import "MessageCellModel.h"
#import "IssSafeListModel.h"
#import "ListCellViewController.h"

#import "ISSSafeMainVC.h"
#import "ISSFMDB.h"

@interface SafeChildViewController ()
<UITableViewDelegate,UITableViewDataSource>
{
    ISSMessageSegView *_segView;
    NSInteger _currentSegIndex;
}
@property (nonatomic,strong) NSMutableArray *resultDataList;
@property (nonatomic,strong) NSArray *sonList;
@end

@implementation SafeChildViewController

- (void)resfreshList
{
    UITableView *mTableView = [self.scrollView viewWithTag:99 + _currentSegIndex];
    [mTableView.mj_header beginRefreshing];
}
- (void)dealScrollFinish
{
    NSMutableArray *list = self.resultDataList[_currentSegIndex];
    if (list.count == 0)
    {
        UITableView *mTableView = [self.scrollView viewWithTag:99 + _currentSegIndex];
        [mTableView.mj_header beginRefreshing];
    }
}

- (void)dealClickTopBtn:(NSInteger)index
{
    _currentSegIndex = index;
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = _currentSegIndex * Screen_Width;
    
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    [self dealScrollFinish];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}
//通知
- (void)addDanger:(NSNotification *)noti{
    
    [self requestDataReresh:@"down"];
}
-(void)addBreak:(NSNotification *)noti{
    
    [self requestDataReresh:@"down"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.resultDataList = [NSMutableArray new];
    _currentSegIndex = 0;
    
    self.sonList = [self getUpdateList:nil];
    CGFloat segViewHeight = 44;
    WeakSelf(self)
    _segView = [[ISSMessageSegView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, segViewHeight) segList:self.sonList clickBlock:^(NSInteger index) {
        [weakself dealClickTopBtn:index];
    }];
    [self.view addSubview:_segView];
    if(self.sonList.count == 1){
        segViewHeight = 1;
        [_segView removeFromSuperview];
    }

    CGRect frame = CGRectMake(0, segViewHeight, Screen_Width, Screen_Height - 64 - self.topHeight  - segViewHeight - Tabbar_Height - 5);
    self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];
    for (int i = 0; i < self.sonList.count; i++)
    {
        [self.resultDataList addObject:[NSMutableArray new]];
        
        UITableView *mTableView = [[UITableView alloc] initWithFrame:CGRectMake(i*Screen_Width, 0, Screen_Width, frame.size.height) style:UITableViewStylePlain];
        mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        mTableView.tag = 99 + i;
        mTableView.dataSource = self;
        mTableView.delegate = self;
        
        [mTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        mTableView.backgroundColor = [UIColor clearColor];
        mTableView.backgroundView = nil;
        [self.scrollView addSubview:mTableView];
        
        mTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
        mTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
        mTableView.mj_footer.hidden = YES;
    }
    [self.scrollView setContentSize:CGSizeMake(Screen_Width *self.sonList.count, frame.size.height)];
    
    UITableView *mTableView = [self.scrollView viewWithTag:99 + _currentSegIndex];
    [mTableView.mj_header beginRefreshing];
}

- (void)headerRereshing
{
    [self performSelector:@selector(requestDataReresh:) withObject:@"down"];
}

- (void)footerRereshing {
    [self performSelector:@selector(requestDataReresh:) withObject:@"up"];
}
-(void)requestDataReresh:(NSString *)direction
{
    NSArray *topArr = @[@"yh",@"wz",@"wd"];
    __block NSMutableArray *currentList = self.resultDataList[_currentSegIndex];
    
    NSDictionary *dic = @{@"type":topArr[self.mainType],
                          @"item":self.sonList[_currentSegIndex][@"code"],
                          @"lastId":[@"down" isEqualToString:direction] ? @"" : ((IssSafeListModel *)[currentList lastObject]).id};
    NSLog(@"dic === %@",dic);
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"safety/getMyYHs" param:dic view:nil successBlock:^(NSDictionary *resultData) {
        
        UITableView *mTableView = [weakself.scrollView viewWithTag:99 + _currentSegIndex];
        if (mTableView.mj_header.isRefreshing) [mTableView.mj_header endRefreshing];
        if (mTableView.mj_footer.isRefreshing) [mTableView.mj_footer endRefreshing];
        
        NSDictionary *data = resultData[@"data"];
        // 处理数字显示
        if (weakself.mainType == 0 || weakself.mainType == 2)
        {
            NSDictionary *total = data[@"total"];
            [_segView updateSegView:[weakself getUpdateList:total]];
        }
        
        // 处理列表显示
        NSArray *list = [IssSafeListModel arrayOfModelsFromDictionaries:data[@"list"] error:nil];
        if ([@"down" isEqualToString:direction]) {
            
            if (mTableView.mj_footer.hidden) {
                mTableView.mj_footer.hidden = NO;
                [mTableView.mj_footer resetNoMoreData];
            }
            [currentList removeAllObjects];
        }
        if (list.count < [PageSize integerValue])
        {   mTableView.mj_footer.hidden = YES;
            [mTableView.mj_footer endRefreshingWithNoMoreData];
        }
        if (list.count > 0)
        {
            [currentList addObjectsFromArray:list];
        }
        [mTableView reloadData];
    } failureBlock:^(NSDictionary *resultData) {
        
        UITableView *mTableView = [weakself.scrollView viewWithTag:99 + _currentSegIndex];
        if (mTableView.mj_header.isRefreshing) [mTableView.mj_header endRefreshing];
        if (mTableView.mj_footer.isRefreshing) [mTableView.mj_footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 130;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultDataList[_currentSegIndex] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    IssSafeListModel *model = _resultDataList[_currentSegIndex][indexPath.row];
    ListViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ListViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.layer.cornerRadius = 8;
    }
    
    if (self.mainType == 0)
        [cell configListDataWithSafeModel:model type:@"yh"];
    else if (self.mainType == 1)
        [cell configListDataWithSafeModel:model type:@"wz"];
    else
        [cell configListDataWithSafeModel:model];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 判断有没有查看权限
    if (![ISSUserData sharedInstance].operation_privilege_yh_detailView) {
        [self.view makeToast:LoadLanguagesKey(@"operation_privilege_yh_detailView")];
        return;
    }
    IssSafeListModel *model = self.resultDataList[_currentSegIndex][indexPath.row];
    
    ListViewTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell.noReadFlag.hidden) {
        cell.noReadFlag.hidden = YES;
        
        if (self.mainType == 0)
            [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:@"1"];
        else if (self.mainType == 1)
            [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:@"2"];
        else
        {
            if ([@"yh" isEqualToString:model.yhlxbm])
                [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:@"1"];
            else
                [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:@"2"];
        }
        // 重新统计
        ISSSafeMainVC *vc = (ISSSafeMainVC *)self.parentViewController;
        [vc configNoReadData:YES];
        
        // 让其它页面也刷新
        [[NSNotificationCenter defaultCenter] postNotificationName:@"readPushMsgSuccessNote" object:nil];
    }
    
    ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
    listCellVC.hidesBottomBarWhenPushed = YES;
    listCellVC.itemId = model.id;
    listCellVC.comeType = self.mainType;
    //listCellVC.itemType = model.yhlxbm;
    if (self.mainType == 0)
        listCellVC.itemType = @"yh";
    else if (self.mainType == 1)
        listCellVC.itemType = @"wz";
    else
        listCellVC.itemType = model.yhlxbm;
    listCellVC.safeModel = model;
   [self.navigationController pushViewController:listCellVC animated:YES];
}
#pragma mark - UIScrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        _currentSegIndex = page;
        
        [_segView dealMoveLineFinish:_currentSegIndex];
        
        [self dealScrollFinish];
    }
}

- (NSArray *)getUpdateList:(NSDictionary *)total
{
    NSArray *list = nil;
    if (self.mainType == 0)
    {
        list = @[
                 @{@"code":@"all",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_all"),total ? [total[@"YH"] intValue] : 0)},
                 @{@"code":@"wzg",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_weizhenggai"),total ? [total[@"YHWZG"] intValue] : 0)},
                 @{@"code":@"yq",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_yuqi"),total ? [total[@"YHYQ"] intValue] : 0)},
                 @{@"code":@"wsp",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_weishenpi"),total ? [total[@"YHWSP"] intValue] : 0)},
                 @{@"code":@"wfs",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_weifasong"),total ? [total[@"YHWFS"] intValue] : 0)},
                 @{@"code":@"wys",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_weiyanshou"),total ? [total[@"YHWYS"] intValue] : 0)},
                 @{@"code":@"wxa",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_weixiaoan"),total ? [total[@"YHWXA"] intValue] : 0)},
                 @{@"code":@"yzg",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_yh_yizhenggai"),total ? [total[@"YHYZG"] intValue] : 0)},
                 ];
    }
    else if (self.mainType == 1)
    {
        list = @[@{@"code":@"",@"name":@""}];
    }
    else if (self.mainType == 2)
    {
        /*
        list = @[
                 @{@"code":@"all",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_mine"),total ? [total[@"WD"] intValue] : 0)},
                 @{@"code":@"yh",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_myHiddenTrouble"),total ? [total[@"WDYH"] intValue] : 0)},
                 @{@"code":@"wz",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_myBreakRule"),total ? [total[@"WDWZ"] intValue] : 0)}
                 ];
         */
        /*
        list = @[
                 @{@"code":@"fb",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_fabu"),total ? [total[@"WDFB"] intValue] : 0)},
                 @{@"code":@"zg",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_zhenggai"),total ? [total[@"WDZG"] intValue] : 0)},
                 @{@"code":@"yq",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_yuqi"),total ? [total[@"WDYQ"] intValue] : 0)},
                 @{@"code":@"xa",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_xiaoan"),total ? [total[@"WDXA"] intValue] : 0)}
                 ];
         */
        
        list = @[
                 @{@"code":@"fb",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_fabu"),total ? [total[@"WDFB"] intValue] : 0)},
                 @{@"code":@"sp",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_shenpi"),total ? [total[@"WDSP"] intValue] : 0)},
                 @{@"code":@"zg",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_zhenggai"),total ? [total[@"WDZG"] intValue] : 0)},
                 @{@"code":@"ys",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_yanshou"),total ? [total[@"WDYS"] intValue] : 0)},
                 @{@"code":@"xa",@"name":FormatString(@"%@ %d",LoadLanguagesKey(@"safe_main_my_xiaoan"),total ? [total[@"WDXA"] intValue] : 0)}
                 ];
    }
    return list;
}
@end
