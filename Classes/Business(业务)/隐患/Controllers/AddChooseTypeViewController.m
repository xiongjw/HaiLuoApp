//
//  AddChooseTypeViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/25.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "AddChooseTypeViewController.h"

@interface AddChooseTypeViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation AddChooseTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if([@"wz" isEqualToString:self.type])
    {
          self.navigationItem.title = LoadLanguagesKey(@"info_detail_breakClass");
    }
    else
    {
         self.navigationItem.title = LoadLanguagesKey(@"info_detail_dangerClass");
    }
  
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        if (weakself.level == 1) [weakself dismissViewControllerAnimated:YES completion:NULL];
        else [weakself.navigationController popViewControllerAnimated:YES];
    }];
    
    if (self.level == 1)
    {
        if ([@"yh" isEqualToString:self.type])
        {
            for (NSDictionary *data in self.typeList)
            {
                if (!data[@"PCODE_"])
                {
                    if (![@"8a88fee1570c782301570d037d7a0074" isEqualToString:data[@"CODE_"]])
                    {
                        [self.resultDataList addObject:data];
                    }
                }
            }
        }
        else
        {
            for (NSDictionary *data in self.typeList)
            {
                if (!data[@"PCODE_"])
                {
                    if ([@"8a88fee1570c782301570d037d7a0074" isEqualToString:data[@"CODE_"]])
                    {
                        [self.resultDataList addObject:data];
                        break;
                    }
                }
            }
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedAction:) name:@"levelSelectedNote" object:nil];
    }
    else
    {
        for (NSDictionary *data in self.typeList)
        {
            if (data[@"PCODE_"])
            {
                if ([data[@"PCODE_"] isEqualToString:self.pCode]) {
                    [self.resultDataList addObject:data];
                }
            }
        }
    }
}

- (void)selectedAction:(NSNotification *)note
{
    if (_chooseTypeBlock) {
        _chooseTypeBlock(note.object);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.resultDataList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (self.level == 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.resultDataList[indexPath.row][@"NAME_"];
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *data = self.resultDataList[indexPath.row];
    if (self.level == 1)
    {
        AddChooseTypeViewController *vc = [[AddChooseTypeViewController alloc] init];
        vc.level = self.level + 1;
        vc.pCode = data[@"CODE_"];
        vc.typeList = self.typeList;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"levelSelectedNote" object:data];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

@end
