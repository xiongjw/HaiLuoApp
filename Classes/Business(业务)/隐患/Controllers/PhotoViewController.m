//
//  PhotoViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "PhotoViewController.h"
#import "ISSPhotoCell.h"
#import "ZLPhoto.h"
#import "ISSUploadDeal.h"

@interface PhotoViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource,ZLPhotoPickerBrowserViewControllerDataSource,ZLPhotoPickerBrowserViewControllerDelegate>{
    
    UIButton * openCameraButton;
    UIButton * albumButton;
    UIButton * cancelButton;
}
@property (nonatomic, strong)UICollectionView * collection;
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableString *imageArr;
@property (nonatomic, strong) UIView * addPhotoView;
@property (nonatomic, strong) UIView * blackView;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createUI];
    [self createAddPhotoView];
}

- (void)createUI{
    [self setTitle:LoadLanguagesKey(@"add_photoChoose_navTitle") showBlackBackBtn:YES];
 //   WeakSelf(self)
//    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
//        [weakself dismissViewControllerAnimated:YES completion:NULL];
//    }];
    UIView * bgview = [[UIView alloc]init];
    bgview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgview];
    [bgview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.height.equalTo(@200);
    }];
    
    UIView * lineview = [[UIView alloc]init];
    lineview.backgroundColor = UIColorFromRGB(0xd9d9d9);
    [bgview addSubview:lineview];
    [lineview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgview.mas_left).offset(0);
        make.right.equalTo(bgview.mas_right).offset(0);
        make.top.equalTo(bgview.mas_top).offset(50);
        make.height.equalTo(@0.5);
    }];
    
    UILabel * tipLabel = [[UILabel alloc]init];
    tipLabel.text = LoadLanguagesKey(@"job_detail_camera_alert");
    tipLabel.font = FontSIZE(13);
    tipLabel.textColor = UIColorFromRGB(0x9a9a9a);
    tipLabel.numberOfLines = 2;
    [bgview addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(25);
        make.right.equalTo(self.view.mas_right).offset(-25);
        make.top.equalTo(lineview.mas_bottom).offset(15);
        make.height.equalTo(@32);
    }];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(70, 70);
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.minimumLineSpacing = 5;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _collection = [[UICollectionView alloc]initWithFrame:CGRectMake(17, 110, Screen_Width - 34, 90) collectionViewLayout:flowLayout];
    _collection.delegate = self;
    _collection.dataSource = self;
    _collection.backgroundColor = [UIColor whiteColor];
    [_collection registerClass:[ISSPhotoCell class] forCellWithReuseIdentifier:@"ISSPhotoCell"];
    [bgview addSubview:_collection];
    [_collection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(tipLabel.mas_left).offset(0);
        make.right.equalTo(bgview.mas_right).offset(-25);
        make.top.equalTo(tipLabel.mas_bottom).offset(20);
        make.height.equalTo(@70);
    }];
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)];
    longPress.minimumPressDuration = 1.0;
    [_collection addGestureRecognizer:longPress];
    
    UIButton * sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setBackgroundColor:[UIColor blueColor]];
    [sureButton setTitle:LoadLanguagesKey(@"btn_confirm") forState:UIControlStateNormal];
    [sureButton setTintColor:[UIColor whiteColor]];
    [sureButton addTarget:self action:@selector(clickSure) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureButton];
    sureButton.layer.cornerRadius = 25;
    [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.height.equalTo(@50);
        make.top.equalTo(bgview.mas_bottom).offset(150);
    }];
}

- (void)createAddPhotoView{
    
    _blackView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    _blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _blackView.alpha = 0.0f;
    [self.view addSubview:_blackView];
    self.addPhotoView.frame = CGRectMake(0, Screen_Height, Screen_Width, 220);
    [openCameraButton addTarget:self action:@selector(openCamera) forControlEvents:UIControlEventTouchUpInside];
    [albumButton addTarget:self action:@selector(openLocalPhoto) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.addPhotoView];
}

#pragma mark 获取多图数据源
-(void)getimagearrdate{
    if (_assets) {
        
        
        NSMutableString *datelist = [[NSMutableString alloc]init];
        
        for (int i=0; i<_assets.count; i++) {
            
            ZLPhotoAssets *asset = self.assets[i];
            NSData *data;
            if ([asset isKindOfClass:[ZLPhotoAssets class]]) {
                data = UIImageJPEGRepresentation([asset originImage], 0.4);
            }
            else if ([asset isKindOfClass:[ZLCamera class]]){
                
                data = UIImageJPEGRepresentation([asset thumbImage], 0.4);
            }
            
            //            data = [GTMBase64 encodeData:data];
            
            NSString *imagestr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            if (i==0) {
                [datelist appendString:imagestr];
            } else {
                [datelist appendString:[NSString stringWithFormat:@",%@",imagestr]];
            }
            
            _imageArr = datelist;
        }
    }
    if (!_imageArr) {
        _imageArr = [@"" copy];
    }
}
#pragma mark - <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.assets.count<3) {
        return self.assets.count+1;
    }
    else{
        return self.assets.count;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(70, 70);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, (Screen_Width - 210)/4);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ISSPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ISSPhotoCell" forIndexPath:indexPath];
    
    if (indexPath.row < self.assets.count) {
        
        // 判断类型来获取Image
        ZLPhotoAssets *asset = self.assets[indexPath.item];
        if ([asset isKindOfClass:[ZLPhotoAssets class]]) {
            cell.imageView.image = [asset aspectRatioImage];
        }else if ([asset isKindOfClass:[NSString class]]){
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:(NSString *)asset] placeholderImage:[UIImage imageNamed:@"pc_circle_placeholder"]];
        }else if([asset isKindOfClass:[UIImage class]]){
            cell.imageView.image = (UIImage *)asset;
        }else if ([asset isKindOfClass:[ZLCamera class]]){
            cell.imageView.image = [asset thumbImage];
        }
        
    }
    else{
        cell.imageView.image = [UIImage imageNamed:@"message_add_photo"];
    }
    return cell;
    
}

#pragma mark - <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row<self.assets.count) {
        // 图片游览器
        ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
        
        // 数据源/delegate
        // 动画方式
        /*
         *
         UIViewAnimationAnimationStatusZoom = 0, // 放大缩小
         UIViewAnimationAnimationStatusFade , // 淡入淡出
         UIViewAnimationAnimationStatusRotate // 旋转
         pickerBrowser.status = UIViewAnimationAnimationStatusFade;
         */
        pickerBrowser.delegate = self;
        pickerBrowser.dataSource = self;
        // 是否可以删除照片
        pickerBrowser.editing = YES;
        // 当前分页的值
        //         pickerBrowser.currentPage = indexPath.row;
        // 传入组
        pickerBrowser.currentIndexPath = indexPath;
        // 展示控制器
        [pickerBrowser showPickerVc:self];
    } else {
        [self showAddPhotoView];
    }
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDataSource>
- (NSInteger)numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser{
    return self.assets.count;
}

- (NSInteger)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    return self.assets.count;
}

- (ZLPhotoPickerBrowserPhoto *)photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath{
    id imageObj = [self.assets objectAtIndex:indexPath.item];
    ZLPhotoPickerBrowserPhoto *photo = [ZLPhotoPickerBrowserPhoto photoAnyImageObjWith:imageObj];
    // 包装下imageObj 成 ZLPhotoPickerBrowserPhoto 传给数据源
    ISSPhotoCell *cell = (ISSPhotoCell *)[self.collection cellForItemAtIndexPath:indexPath];
    // 缩略图
    if ([imageObj isKindOfClass:[ZLPhotoAssets class]]) {
        photo.asset = imageObj;
    }
    photo.toView = cell.imageView;
    photo.thumbImage = cell.imageView.image;
    return photo;
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDelegate>
#pragma mark 返回自定义View
//- (ZLPhotoPickerCustomToolBarView *)photoBrowserShowToolBarViewWithphotoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser{
//    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
////    [customBtn setTitle:@"实现代理自定义ToolBar" forState:UIControlStateNormal];
//    customBtn.frame = CGRectMake(10, 0, 200, 44);
//    return (ZLPhotoPickerCustomToolBarView *)customBtn;
//}

- (void)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser removePhotoAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row > [self.assets count]) return;
    [self.assets removeObjectAtIndex:indexPath.row];
    [self.collection reloadData];
}


- (void)openCamera{
    [self disAddPhotoView];
    ZLCameraViewController *cameraVc = [[ZLCameraViewController alloc] init];
    // 拍照最多个数
    if (self.assets.count < 3) {
        
        cameraVc.maxCount= 3-self.assets.count;
    }
    
    __weak typeof(self) weakSelf = self;
    cameraVc.callback = ^(NSArray *cameras){
        if (weakSelf.assets==nil) {
            weakSelf.assets = [cameras mutableCopy];
        } else {
            [weakSelf.assets addObjectsFromArray:cameras];
        }
        
        [weakSelf.collection reloadData];
    };
    [cameraVc showPickerVc:self];
}

- (void)openLocalPhoto{
    [self disAddPhotoView];
    ZLPhotoPickerViewController *pickerVc = [[ZLPhotoPickerViewController alloc] init];
    // 最多能选3张图片
    if (self.assets.count < 3) {
        
        pickerVc.maxCount = 3-self.assets.count;
    }
    pickerVc.status = PickerViewShowStatusCameraRoll;
    [pickerVc showPickerVc:self];
    
    __weak typeof(self) weakSelf = self;
    pickerVc.callBack = ^(NSArray *assets){
        if (weakSelf.assets==nil) {
            weakSelf.assets = [assets mutableCopy];
        } else {
            [weakSelf.assets addObjectsFromArray:assets];
        }
        
        
        [weakSelf.collection reloadData];
    };
}

//弹起添加图片
- (void)showAddPhotoView {
    __unsafe_unretained typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.35f delay:0 usingSpringWithDamping:0.9f initialSpringVelocity:0.7f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
        
        weakSelf.blackView.alpha = 1.0;
        _addPhotoView.frame = CGRectMake(0, Screen_Height - 220-88, Screen_Width, 220);
    } completion:NULL];
}

//关闭添加图片
- (void)disAddPhotoView{
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.35f delay:0 usingSpringWithDamping:0.9f initialSpringVelocity:0.7f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
        
        weakSelf.blackView.alpha = 0.0;
        _addPhotoView.frame = CGRectMake(0, Screen_Height, Screen_Width, 220);
        
    } completion:^(BOOL finished) {
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:_blackView];
    if (!CGRectContainsPoint([self.addPhotoView frame], point))
    {
        [self disAddPhotoView];
    }
}

- (void)cancel{
    
    [self disAddPhotoView];
}

- (void)clickSure{

    WeakSelf(self)
    NSMutableArray *imageList = [NSMutableArray new];
    
    for (id obj in self.assets)
    {
        if ([obj isKindOfClass:[ZLCamera class]])
        {
            ZLCamera *zlCamera = obj;
            [imageList addObject:zlCamera.photoImage];
        }
        else if ([obj isKindOfClass:[ZLPhotoAssets class]])
        {
            ZLPhotoAssets *zlPhotoAssets = obj;
            [imageList addObject:zlPhotoAssets.originImage];
        }
    }

    [[ISSUploadDeal sharedInstance] uploadImages:imageList view:self.view.window successBlock:^(NSString *photoUrls) {
        
        if (_submitPhotoSuccessBlock) {
            _submitPhotoSuccessBlock(photoUrls);
        }
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress{
    
    if (self.assets.count > 0) {
        CGPoint pointTouch = [longPress locationInView:self.collection];
        NSIndexPath *indexPath = [self.collection indexPathForItemAtPoint:pointTouch];
        //NSLog(@"%@",indexPath);
        //NSLog(@"%lu", (unsigned long)self.assets.count);
        //NSLog(@"%ld",indexPath.item);
        if (self.assets.count >= indexPath.item+1 && indexPath != nil) {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:LoadLanguagesKey(@"add_photoChoose_deleteAlert") preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_confirm") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.assets removeObjectAtIndex:indexPath.item];
                [_collection reloadData];
            }];
            
            UIAlertAction * cancel = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
}

- (UIView *)addPhotoView{
    
    if (!_addPhotoView) {
        _addPhotoView = [[UIView alloc]init];
        _addPhotoView.backgroundColor = [UIColor whiteColor];
        UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake((Screen_Width - 100)/2, 20, 100, 20)];
        titleLabel.text = LoadLanguagesKey(@"add_photoChoose_addPhoto");
        titleLabel.font = FontSIZE(15);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [_addPhotoView addSubview:titleLabel];
        UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 60, Screen_Width, 0.5)];
        lineView.backgroundColor = UIColorFromRGB(0x909090);
        [_addPhotoView addSubview:lineView];
        
        openCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        openCameraButton.frame = CGRectMake(60, 75, 66, 57);
        [openCameraButton setImage:[UIImage imageNamed:@"job_detail_takeCamera"] forState:UIControlStateNormal];
        [openCameraButton setTitle:LoadLanguagesKey(@"add_photoChoose_camera") forState:UIControlStateNormal];
        openCameraButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [openCameraButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        CGSize imageSize = openCameraButton.imageView.frame.size;
        CGSize titleSize = openCameraButton.titleLabel.frame.size;
        CGFloat totalHeight = (imageSize.height + titleSize.height);
        openCameraButton.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height), 20, 20, 20);
        openCameraButton.titleEdgeInsets = UIEdgeInsetsMake(0, - imageSize.width, - (totalHeight - titleSize.height), 0);
        [_addPhotoView addSubview:openCameraButton];
        
        albumButton = [UIButton buttonWithType:UIButtonTypeCustom];
        albumButton.frame = CGRectMake(Screen_Width-120, 75, 66, 57);
        [albumButton setImage:[UIImage imageNamed:@"message_photo_choose"] forState:UIControlStateNormal];
        [albumButton setTitle:LoadLanguagesKey(@"add_photoChoose_PhotoLibrary") forState:UIControlStateNormal];
        albumButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [albumButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        CGSize albumButtonimageSize = albumButton.imageView.frame.size;
        CGSize albumButtontitleSize = albumButton.titleLabel.frame.size;
        CGFloat albumButtontotalHeight = (albumButtonimageSize.height + albumButtontitleSize.height);
        albumButton.imageEdgeInsets = UIEdgeInsetsMake(- (albumButtontotalHeight - albumButtonimageSize.height), 20,20, 20);
        albumButton.titleEdgeInsets = UIEdgeInsetsMake(0, - albumButtonimageSize.width, - (albumButtontotalHeight - albumButtontitleSize.height), 0);
        [_addPhotoView addSubview:albumButton];
        
        cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelButton.frame = CGRectMake(17, 150, Screen_Width - 34, 50);
        [cancelButton setBackgroundColor: UIColorFromRGB(0xacacac)];
        [cancelButton setTitle:LoadLanguagesKey(@"btn_cancel") forState:UIControlStateNormal];
        [cancelButton setTintColor:[UIColor whiteColor]];
        [_addPhotoView addSubview:cancelButton];
        cancelButton.layer.cornerRadius = 8;
        cancelButton.clipsToBounds = YES;
    }
    return _addPhotoView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

