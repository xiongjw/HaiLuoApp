//
//  ISSDataDicUtil.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/5.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSDataDicUtil : NSObject

@property (nonatomic,strong) NSArray *yhjbList;//隐患级别
@property (nonatomic,strong) NSArray *sourceList;//隐患来源
@property (nonatomic,strong) NSArray *typeList;//隐患/违章类型

+ (ISSDataDicUtil *)sharedInstance;

- (void)searchParam;

- (NSString *)getNameByCode:(NSString *)code type:(NSInteger)type;
- (NSString *)getYHJB:(NSString *)code;

@end
