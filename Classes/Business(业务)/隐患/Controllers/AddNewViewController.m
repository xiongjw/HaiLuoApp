//
//  AddNewViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//
#import "AddNewViewController.h"
#import "AddNewMessageInputCell.h"
#import "ISSAddNewTimeCell.h"
#import "ISSAddNewSelectCell.h"
#import "ISSAddNewAnonymousCell.h"
#import "TimePickview.h"
#import "PhotoViewController.h"
#import "ISSChoosePersonVC.h"
#import "IssMessageAddModel.h"
#import "AddChooseTypeViewController.h"
#import "AddChooseSourceViewController.h"
#import "ISSChooseDepartmentVC.h"
@interface AddNewViewController ()<UITableViewDelegate,UITableViewDataSource,DatePikerViewDelegate,UITextViewDelegate>{
    
    BOOL         _dangerLevelFlag;//隐患级别，YES：一般 NO:重大
    BOOL         _isChangeFlag;//是否已整改，YES：是 NO:否
    BOOL         _isClickDangerDescribe;//是否点击隐患描述文本框
    
    BOOL         _isAnonymousFlag;//匿名举报，YES：是 NO:否
    BOOL         _isClickBreakDescribe;//是否点击隐患描述文本框
    
    NSIndexPath  *currentIndex;
    NSIndexPath  *selectIndex;
    NSString     *photoString;//隐患/违章照片
    NSString     *changePhotoString;//整改照片
    
    BOOL        _isHideChangeCell;//隐藏是否已整改，那一行
    BOOL        _isAddChangePhotoCell;//添加整改后的图片，那一行
    
}
@property(nonatomic,strong) UITableView                   * addMessageTable;
@property(nonatomic,strong) UIView                        * footerView;
@property(nonatomic,strong) TimePickview                  * timeV;
@property(nonatomic,strong) NSMutableDictionary           * timeDict;
@property(nonatomic,strong) IssMessageAddModel *addModel;
@property(nonatomic,strong) NSMutableArray *photoArr;
//查询列表的arr
@property(nonatomic,strong) NSArray *sourceList;//隐患来源
@property(nonatomic,strong) NSArray *typeList;
@end

@implementation AddNewViewController

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _addModel = [[IssMessageAddModel alloc]init];
    _timeDict = [[NSMutableDictionary alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [self createData];
    [self createUI];
    
}
-(void)createData
{
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"safety/searchParam" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSDictionary *data = resultData[@"data"];
        //发现单位和违章单位
        weakself.sourceList = data[@"YHLY"];
        weakself.typeList = data[@"YHLX"];
        
        NSArray *yhjbList = data[@"YHJB"];
        for (NSDictionary *item in yhjbList) {
            NSLog(@"%@_%@",item[@"CODE_"],item[@"NAME_"]);
        }
    }];
    
}

- (void)createUI
{
    // 初始化赋值
    // 发现日期
    NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
    [dateF setDateFormat:AXDateMinuteFormat];
    [_timeDict setObject:[dateF stringFromDate:[NSDate date]] forKey:@"startTime"];
    //[_timeDict setObject:[dateF stringFromDate:[NSDate date]] forKey:@"endTime"];
    
    // 发现单位/隐患单位、违章单位，先默认，可以修改
    _addModel.idForFindCompany = [ISSUserData sharedInstance].orgId;
    _addModel.findCompanyString = [ISSUserData sharedInstance].orgName;
    
    _addModel.idFordangerCompany = [ISSUserData sharedInstance].orgId;
    _addModel.breakCompanyString = [ISSUserData sharedInstance].orgName;
    
    
    // 排查人员
    _addModel.verbPerson = [ISSUserData sharedInstance].realName;
    _addModel.personId = [ISSUserData sharedInstance].userId;
    
    _dangerLevelFlag = YES;
    _addModel.dangerRank = @"8a88fee1570c782301570ce8c271004e";//默认一般隐患，8a88fee1570c782301570ce90c23004f（重大隐患）
    
    _isClickDangerDescribe = NO;
    
    _isChangeFlag = NO;
    _addModel.isChange = @"0";//默认未整改
    
    _isAnonymousFlag = YES;
    _addModel.isHiddleReport = @"1";//默认匿名举报
    
    _isClickBreakDescribe = NO;
    
    currentIndex = [NSIndexPath indexPathForRow:999 inSection:999];
    
    
    
    if(self.topTag == 10000) [self setTitle:LoadLanguagesKey(@"add_hiddenTrouble_input") showBackBtn:YES];
    else [self setTitle:LoadLanguagesKey(@"add_breakRule_input") showBackBtn:YES];
    
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        [weakself dismissViewControllerAnimated:YES completion:NULL];
    }];
    self.addMessageTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-64) style:UITableViewStyleGrouped];
    self.addMessageTable.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.addMessageTable.delegate = self;
    self.addMessageTable.dataSource = self;
    self.addMessageTable.bounces = YES;
    self.addMessageTable.estimatedSectionHeaderHeight = 0;
    self.addMessageTable.estimatedSectionFooterHeight = 0;
    self.addMessageTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.addMessageTable];
    self.addMessageTable.tableFooterView = self.footerView;
}
#pragma mark - tableView的代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(self.topTag == 10000) return 13;
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 11)
    {
        // 一般隐患，未整改
        if (_dangerLevelFlag && _isChangeFlag)  return 2;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 7)
    {
        // 违章描述
        if (_isClickBreakDescribe && self.topTag != 10000) return 100;
        //return 50;
    }
    else if (indexPath.section == 11)
    {
        if (_isHideChangeCell) return CGFLOAT_MIN;
    }
    // 隐患描述
    else if(indexPath.section == 12)
    {
        if (_isClickDangerDescribe) return 100;
        //return 50;
    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section == 11)
    {
        if (_isHideChangeCell) return CGFLOAT_MIN;
    }
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //  隐患描述/违章描述
    if (indexPath.section == 0)
    {
        AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell0"];
        if(addInfoCell==nil){
            
            if(self.topTag == 10000){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell0" withTitle:LoadLanguagesKey(@"add_hiddenTrouble_name") PlaceHolder:LoadLanguagesKey(@"add_hiddenTrouble_inputName") IsShow:YES ImageName:@"add_cell_edit" IsText:NO];
            }else{
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell0" withTitle:LoadLanguagesKey(@"add_breakRule_name") PlaceHolder:LoadLanguagesKey(@"add_breakRule_inputName") IsShow:YES ImageName:@"add_cell_edit" IsText:NO];
            }
        }
        addInfoCell.textField.tag = _addModel.titleTag;
        addInfoCell.inputLabel.text = _addModel.titleString;
        return addInfoCell;
    }
    // 发现日期
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            ISSAddNewTimeCell * timeCell = [tableView dequeueReusableCellWithIdentifier:@"TimeCell"];
            if(timeCell==nil){
                
                timeCell = [[ISSAddNewTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TimeCell" withTitle:LoadLanguagesKey(@"add_hiddenTrouble_findDate")];
            }
            timeCell.timeLabel.text = [_timeDict objectForKey:@"startTime"];
            return timeCell;
        }
        else if (indexPath.row == 1)
        {
            ISSAddNewTimeCell * timeCell = [tableView dequeueReusableCellWithIdentifier:@"TimeCell"];
            if(timeCell==nil){
                
                timeCell = [[ISSAddNewTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TimeCell" withTitle:LoadLanguagesKey(@"add_hiddenTrouble_registerDate")];
            }
            timeCell.timeLabel.text = [_timeDict objectForKey:@"endTime"];
            return timeCell;
        }
        return nil;
    }
    // 发现单位
    else if (indexPath.section == 2)
    {
        ISSAddNewSelectCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell"];
        
        if (cell == nil) {
            cell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell" withTitle:LoadLanguagesKey(@"add_hiddenTrouble_findUnit")];
        }
        cell.inputLabel.text = _addModel.findCompanyString;
        return cell;
    }
    // 隐患单位/违章单位
    else if (indexPath.section == 3)
    {
        ISSAddNewSelectCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell"];
        if(cell==nil){
            
            if(self.topTag == 10000){
                
                cell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell" withTitle:LoadLanguagesKey(@"info_detail_dangerCompany")];
            }else{
                
                cell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell" withTitle:LoadLanguagesKey(@"info_detail_breakCompany" )];
            }
        }
        cell.inputLabel.text = _addModel.breakCompanyString;
        return cell;
    }
    // 排查人员
    else if (indexPath.section == 4)
    {
        ISSAddNewSelectCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell"];
        if(addInfoCell==nil){
            
            addInfoCell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell" withTitle:LoadLanguagesKey(@"info_detail_findPeople")];
        }
        addInfoCell.inputLabel.text = _addModel.verbPerson;
        
        addInfoCell.selectionStyle = UITableViewCellSelectionStyleNone;
        //addInfoCell.selectButton.hidden = YES;
        if (self.topTag == 10000) {
            addInfoCell.selectionStyle = UITableViewCellSelectionStyleGray;
            //addInfoCell.selectButton.hidden = NO;
        }
        return addInfoCell;
    }
    // 隐患级别/违章类型
    else if (indexPath.section == 5)
    {
        if(self.topTag == 10000)
        {
            ISSAddNewAnonymousCell * cell = [[ISSAddNewAnonymousCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"AddAnonymousCell"];
            [cell.leftButton addTarget:self action:@selector(leftRankAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.rightButton addTarget:self action:@selector(rightRankAction) forControlEvents:UIControlEventTouchUpInside];
            cell.titleLabel.text =LoadLanguagesKey(@"info_detail_dangerRank");
            
            [cell.leftButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_sordinary") forState:UIControlStateNormal];
            [cell.rightButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_serious") forState:UIControlStateNormal];
            
            cell.leftButton.selected = _dangerLevelFlag;
            cell.rightButton.selected = !_dangerLevelFlag;
            return cell;
        }
        else
        {
            ISSAddNewSelectCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell"];
            
            if (cell == nil) {
                cell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell" withTitle:LoadLanguagesKey(@"info_detail_breakClass")];
            }
            cell.inputLabel.text = _addModel.breakTypeString;
            return cell;
        }
    }
    // 隐患地点/违章地点
    else if (indexPath.section == 6)
    {
        if(self.topTag==10000){
            
            AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell6"];
            if(addInfoCell==nil){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell6" withTitle:LoadLanguagesKey(@"info_detail_dangerPlace") PlaceHolder:LoadLanguagesKey(@"add_hiddenTrouble_inputAddress") IsShow:YES ImageName:@"add_cell_edit" IsText:NO];
            }
            addInfoCell.textField.tag = _addModel.breakPlaceTag;
            addInfoCell.inputLabel.text = _addModel.breakPlaceString;
            return addInfoCell;
        }
        else
        {
            AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell6"];
            if(addInfoCell==nil){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell6" withTitle:LoadLanguagesKey(@"info_detail_breakPlace") PlaceHolder:LoadLanguagesKey(@"add_breakRule_inputAddress") IsShow:YES ImageName:@"add_cell_edit" IsText:NO];
            }
            addInfoCell.textField.tag = _addModel.breakPlaceTag;
            addInfoCell.inputLabel.text = _addModel.breakPlaceString;
            return addInfoCell;
        }
        
    }
    // 隐患部位/违章描述
    else if (indexPath.section == 7)
    {
        
        AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell7"];
        if(addInfoCell==nil){
            if(self.topTag == 10000){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell7" withTitle:LoadLanguagesKey(@"info_detail_dangerPart") PlaceHolder:LoadLanguagesKey(@"add_hiddenTrouble_inpuPart") IsShow:YES ImageName:@"add_cell_edit" IsText:NO];
                addInfoCell.textField.tag = _addModel.dangerPartTag;
                addInfoCell.inputLabel.text = _addModel.dangerPart;
            }else{
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell7" withTitle:LoadLanguagesKey(@"info_detail_breakDescribe") PlaceHolder:LoadLanguagesKey(@"add_breakRule_inputDescription") IsShow:YES ImageName:@"add_cell_edit" IsText:YES];
                addInfoCell.textView.delegate = self;
                addInfoCell.textView.textAlignment = NSTextAlignmentLeft;
                if (_isClickBreakDescribe) {
                    _isClickBreakDescribe = YES;
                    
                    AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.bgView.hidden = NO;
                    cell.textView.hidden = NO;
                    cell.bgLabel.hidden = NO;
                }
                addInfoCell.textView.tag = _addModel.breakDescribeTag;
                addInfoCell.inputLabel.text = _addModel.breakDescribe;
            }
        }
        return addInfoCell;
    }
    // 隐患照片/违章照片
    else if (indexPath.section == 8)
    {
        
        if(self.topTag == 10000){
            
            AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell8"];
            if(addInfoCell==nil){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell8" withTitle:LoadLanguagesKey(@"add_hiddenTrouble_photo") PlaceHolder:@"" IsShow:NO ImageName:@"add_cell_photo" IsText:NO];
            }
            
            if (photoString != nil && photoString.length > 0) {
                addInfoCell.inputLabel.text = LoadLanguagesKey(@"add_hiddenTrouble_photoSelected");
            }
            return addInfoCell;
        }else{
            
            AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell8"];
            if(addInfoCell==nil){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell8" withTitle:LoadLanguagesKey(@"info_detail_changePhoto_wz") PlaceHolder:@"" IsShow:NO ImageName:@"add_cell_photo" IsText:NO];
            }
            if (photoString != nil && photoString.length > 0) {
                addInfoCell.inputLabel.text = LoadLanguagesKey(@"add_hiddenTrouble_photoSelected");
            }
            return addInfoCell;
        }
    }
    // 隐患来源/匿名举报
    else if (indexPath.section == 9)
    {
        
        if(self.topTag == 10000){
            
            ISSAddNewSelectCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell"];
            
            if (cell == nil) {
                cell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell" withTitle: LoadLanguagesKey(@"info_detail_dangerSource")];
            }
            cell.inputLabel.text = _addModel.dangerSource;
            return cell;
        }else{
            ISSAddNewAnonymousCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddAnonymousCell"];
            
            cell = [[ISSAddNewAnonymousCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"AddAnonymousCell"];
            [cell.leftButton addTarget:self action:@selector(leftButtonAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.rightButton addTarget:self action:@selector(rightButtonAction) forControlEvents:UIControlEventTouchUpInside];
            cell.titleLabel.text = LoadLanguagesKey(@"add_breakRule_report");
            
            [cell.leftButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_yes") forState:UIControlStateNormal];
            [cell.rightButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_no") forState:UIControlStateNormal];

            cell.leftButton.selected = _isAnonymousFlag;
            cell.rightButton.selected = !_isAnonymousFlag;
            
            return cell;
        }
    }
    // 隐患类型
    else if (indexPath.section == 10)
    {
        ISSAddNewSelectCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell1"];
        
        if (cell == nil) {
            cell = [[ISSAddNewSelectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCell1" withTitle:LoadLanguagesKey(@"info_detail_dangerClass")];
        }
        cell.inputLabel.text = _addModel.dangerType;
        return cell;
        
    }
    // 是否已整改
    else if (indexPath.section == 11)
    {
        if (indexPath.row == 0)
        {
            if (_isHideChangeCell)
            {
                static NSString *CellIdentifier = @"hideChangeCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                return cell;
            }
            else
            {
                ISSAddNewAnonymousCell * cell = [[ISSAddNewAnonymousCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"AddAnonymousCell"];
                [cell.leftButton addTarget:self action:@selector(leftChangeAction) forControlEvents:UIControlEventTouchUpInside];
                [cell.rightButton addTarget:self action:@selector(rightChangeAction) forControlEvents:UIControlEventTouchUpInside];
                cell.titleLabel.text = LoadLanguagesKey(@"info_detail_isChange");
                
                [cell.leftButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_yes") forState:UIControlStateNormal];
                [cell.rightButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_no") forState:UIControlStateNormal];
                
                cell.leftButton.selected = _isChangeFlag;
                cell.rightButton.selected = !_isChangeFlag;
                
                return cell;
            }
        }
        else
        {
            // 整改后的图片
            AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell11"];
            if(addInfoCell==nil){
                
                addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell11" withTitle:LoadLanguagesKey(@"add_hiddenTrouble_changePhoto") PlaceHolder:@"" IsShow:NO ImageName:@"add_cell_photo" IsText:NO];
            }
            if (changePhotoString != nil && changePhotoString.length > 0) {
                addInfoCell.inputLabel.text = LoadLanguagesKey(@"add_hiddenTrouble_photoSelected");
            }
            return addInfoCell;
        }
        
    }
    // 隐患描述
    else
    {
        AddNewMessageInputCell *addInfoCell = [tableView dequeueReusableCellWithIdentifier:@"AddInputCell12"];
        addInfoCell = [[AddNewMessageInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddInputCell12" withTitle:LoadLanguagesKey(@"info_detail_dangerDescribe") PlaceHolder:LoadLanguagesKey(@"add_hiddenTrouble_inputDescription") IsShow:YES ImageName:@"add_cell_edit" IsText:YES];
        addInfoCell.textView.delegate = self;
        addInfoCell.textView.textAlignment = NSTextAlignmentLeft;
        if (_isClickDangerDescribe) {
            _isClickDangerDescribe = YES;
            
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.bgView.hidden = NO;
            cell.textView.hidden = NO;
            cell.bgLabel.hidden = NO;
        }
        addInfoCell.textView.tag = _addModel.dangerDescribeTag;
        addInfoCell.inputLabel.text = _addModel.dangerDescribe;
        return addInfoCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WeakSelf(self)
    if(self.topTag == 10000){
        
        _isClickDangerDescribe = NO;
        if (indexPath.section == 0  || indexPath.section == 6|| indexPath.section == 7)
        {
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.bgView.hidden = NO;
            cell.textField.hidden = NO;
            [cell.textField becomeFirstResponder];
            cell.bgLabel.hidden = NO;
        }
        else if (indexPath.section == 1)
        {
            if (indexPath.row == 0)
            {
                selectIndex = indexPath;
                [self startTime];
            }
            else
            {
                selectIndex = indexPath;
                [self endTime];
            }
        }
        else if (indexPath.section == 4)
        {
            ISSChoosePersonVC *vc = [[ISSChoosePersonVC alloc] init];
            vc.title = LoadLanguagesKey(@"info_detail_findPeople");
            vc.chooseBlock = ^(NSDictionary *data) {
                _addModel.verbPerson= data[@"realName"];
                _addModel.personId = data[@"id"];
                [weakself.addMessageTable reloadRowsAtIndexPaths:@[indexPath]
                                                withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            [self presentViewController:nav animated:YES completion:NULL];
        }
        else if (indexPath.section == 8)
        {
            PhotoViewController * photo = [[PhotoViewController alloc]init];
            photo.submitPhotoSuccessBlock = ^(NSString *photoUrls) {
                photoString = photoUrls;
                [weakself.addMessageTable reloadRowsAtIndexPaths:@[indexPath]
                                                withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            [self.navigationController pushViewController:photo animated:YES];
        }
        else if ( indexPath.section == 2 || indexPath.section == 3 ||indexPath.section == 9||indexPath.section == 10)
        {
            currentIndex = indexPath;
            if( indexPath.section == 2 || indexPath.section == 3)
            {
                ISSAddNewSelectCell * cell = [_addMessageTable cellForRowAtIndexPath:currentIndex];
                ISSChooseDepartmentVC *vc = [[ISSChooseDepartmentVC alloc] init];
                vc.chooseBlock = ^(NSDictionary *data) {
                    
                    if(indexPath.section==2){
                        
                        _addModel.findCompanyString = data[@"ORG_NAME_"];
                        _addModel.idForFindCompany = data[@"ID_"];
                        cell.inputLabel.text =  data[@"ORG_NAME_"];
                    }else{
                        
                        _addModel.breakCompanyString = data[@"ORG_NAME_"];
                        _addModel.idFordangerCompany = data[@"ID_"];
                        cell.inputLabel.text =  data[@"ORG_NAME_"];
                    }
                    
                };
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:nav animated:YES completion:NULL];
                });
            }
            else if (indexPath.section == 10)
            {
                [self showCategoryList];
            }
            else
            {
                [self showCategoryList];
            }
        }
        else if (indexPath.section == 11)
        {
            if (indexPath.row == 1)
            {
                PhotoViewController * photo = [[PhotoViewController alloc]init];
                photo.submitPhotoSuccessBlock = ^(NSString *photoUrls) {
                    changePhotoString = photoUrls;
                    [weakself.addMessageTable reloadRowsAtIndexPaths:@[indexPath]
                                                    withRowAnimation:UITableViewRowAnimationAutomatic];
                };
                [self.navigationController pushViewController:photo animated:YES];
            }
        }
        else if (indexPath.section == 12)
        {
            _isClickDangerDescribe = YES;
            [_addMessageTable reloadData];
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.bgView.hidden = NO;
            cell.textView.hidden = NO;
            [cell.textView becomeFirstResponder];
            cell.bgLabel.hidden = NO;
        }
    }
    else
    {
        _isClickBreakDescribe = NO;
        
        if (indexPath.section == 0 || indexPath.section == 6)
        {
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.bgView.hidden = NO;
            cell.textField.hidden = NO;
            [cell.textField becomeFirstResponder];
            cell.bgLabel.hidden = NO;
        }
        else if (indexPath.section == 1)
        {
            if (indexPath.row == 0)
            {
                selectIndex = indexPath;
                [self startTime];
            }
            else
            {
                selectIndex = indexPath;
                [self endTime];
            }
        }
        // 违章 排查人员不能修改
        else if (indexPath.section == 4)
        {
            /*
            ISSChoosePersonVC *vc = [[ISSChoosePersonVC alloc] init];
            vc.title = LoadLanguagesKey(@"info_detail_findPeople");
            vc.chooseBlock = ^(NSDictionary *data) {
                _addModel.verbPerson= data[@"realName"];
                _addModel.personId = data[@"id"];
                [weakself.addMessageTable reloadRowsAtIndexPaths:@[indexPath]
                                                withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            [self presentViewController:nav animated:YES completion:NULL];
             */
        }
        else if ( indexPath.section == 2 || indexPath.section == 3 ||indexPath.section == 5)
        {
            currentIndex = indexPath;
            if( indexPath.section == 2 || indexPath.section == 3)
            {
                ISSAddNewSelectCell * cell = [_addMessageTable cellForRowAtIndexPath:currentIndex];
                ISSChooseDepartmentVC *vc = [[ISSChooseDepartmentVC alloc] init];
                vc.chooseBlock = ^(NSDictionary *data) {
                    if(indexPath.section==2){
                        
                        _addModel.findCompanyString = data[@"ORG_NAME_"];
                        _addModel.idForFindCompany = data[@"ID_"];
                        cell.inputLabel.text =  data[@"ORG_NAME_"];
                    }else{
                        
                        _addModel.breakCompanyString = data[@"ORG_NAME_"];
                        _addModel.idFordangerCompany = data[@"ID_"];
                        cell.inputLabel.text =  data[@"ORG_NAME_"];
                    }
                    
                };
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:nav animated:YES completion:NULL];
                });
            }
            else
            {
                [self showCategoryList];
            }
            
        }
        else if (indexPath.section == 7)
        {
            _isClickBreakDescribe = YES;
            [_addMessageTable reloadData];
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.bgView.hidden = NO;
            cell.textView.hidden = NO;
            [cell.textView becomeFirstResponder];
            cell.bgLabel.hidden = NO;
            
        }
        else if (indexPath.section == 8)
        {
            PhotoViewController * photo = [[PhotoViewController alloc]init];
            photo.submitPhotoSuccessBlock = ^(NSString *photoUrls) {
                photoString = photoUrls;
                [weakself.addMessageTable reloadRowsAtIndexPaths:@[indexPath]
                                                withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            [self.navigationController pushViewController:photo animated:YES];
        }
    }
    
    if (currentIndex.section == 7 &&self.topTag==10001)
    {
        _isClickBreakDescribe = NO;
        currentIndex = indexPath;
        //  [_addMessageTable reloadData];
        NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:7];
        AddNewMessageInputCell * cell = [tableView cellForRowAtIndexPath:index];
        cell.bgView.hidden = YES;
        cell.textView.hidden = YES;
        cell.bgLabel.hidden = YES;
    }
    else if (currentIndex.section == 12)
    {
        _isClickDangerDescribe = NO;
        currentIndex = indexPath;
        //  [_addMessageTable reloadData];
        NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:12];
        AddNewMessageInputCell * cell = [tableView cellForRowAtIndexPath:index];
        cell.bgView.hidden = YES;
        cell.textView.hidden = YES;
        cell.bgLabel.hidden = YES;
    }
    
    currentIndex = indexPath;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.topTag == 10000){
        
        if (indexPath.section == 0 || indexPath.section == 6|| indexPath.section == 7) {
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            //        cell.bgView.hidden = YES;
            //        cell.textField.hidden = YES;
            //        cell.bgLabel.hidden = YES;
            cell.inputLabel.text = cell.textField.text;
            [cell.textField resignFirstResponder];
        }else if (indexPath.section == 12){
            AddNewMessageInputCell * cell = [tableView cellForRowAtIndexPath:indexPath];
            [cell.textField resignFirstResponder];
        }
    }else{
        
        if (indexPath.section == 6) {
            AddNewMessageInputCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            //        cell.bgView.hidden = YES;
            //        cell.textField.hidden = YES;
            //        cell.bgLabel.hidden = YES;
            cell.inputLabel.text = cell.textField.text;
            [cell.textField resignFirstResponder];
        }else if (indexPath.section == 7){
            AddNewMessageInputCell * cell = [tableView cellForRowAtIndexPath:indexPath];
            [cell.textField resignFirstResponder];
        }
    }
    
    
}

//类型选择
-(void)showCategoryList{
    WeakSelf(self)
    if(self.topTag ==10000){
        
        if (currentIndex.section == 9)
        {
            ISSAddNewSelectCell * cell = [_addMessageTable cellForRowAtIndexPath:currentIndex];
            AddChooseSourceViewController*source = [[AddChooseSourceViewController alloc]init];
            source.chooseSourceBlock = ^(NSDictionary *typeDic) {
                _addModel.dangerSource= typeDic[@"NAME_"];
                _addModel.dangerCode = typeDic[@"CODE_"];
                cell.inputLabel.text = typeDic[@"NAME_"];
            };
            source.dataArray = weakself.sourceList;
            source.selectRow = currentIndex.section;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:source];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:nav animated:YES completion:NULL];
            });
        }
        else if (currentIndex.section == 10)
        {
            __block ISSAddNewSelectCell * cell = [_addMessageTable cellForRowAtIndexPath:currentIndex];
            AddChooseTypeViewController *type = [[AddChooseTypeViewController alloc]init];
            type.chooseTypeBlock = ^(NSDictionary *typeDic) {
                _addModel.dangerType= typeDic[@"NAME_"];
                _addModel.dangerTypeCode = typeDic[@"CODE_"];
                cell.inputLabel.text = typeDic[@"NAME_"];
            };
            type.type = @"yh";
            type.typeList = weakself.typeList;
            type.level = 1;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:type];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:nav animated:YES completion:NULL];
            });
        }
    }
    else
    {
        if (currentIndex.section == 5){
            
            __block ISSAddNewSelectCell *cell = [_addMessageTable cellForRowAtIndexPath:currentIndex];
            
            AddChooseTypeViewController *type = [[AddChooseTypeViewController alloc] init];
            type.chooseTypeBlock = ^(NSDictionary *typeDic) {
                _addModel.breakTypeString= typeDic[@"NAME_"];
                _addModel.breakTypeCode = typeDic[@"CODE_"];
                cell.inputLabel.text = typeDic[@"NAME_"];
            };
            type.type = @"wz";
            type.typeList = weakself.typeList;
            type.level = 1;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:type];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:nav animated:YES completion:NULL];
            });
        }
    }
}
//通知
- (void)textFieldTextChanged:(NSNotification *)notification {
    UITextField *textField = notification.object;
    NSString *text = textField.text;
    if (textField.tag == _addModel.titleTag) {
        _addModel.titleString = text;
    }
    else if (textField.tag == _addModel.breakPlaceTag) {
        _addModel.breakPlaceString = text;
    }else if (textField.tag == _addModel.dangerPartTag) {
        _addModel.dangerPart = text;
    }
}
#pragma mark textViewdelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if(self.topTag == 10000){
        _isClickDangerDescribe = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:12];
            AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
            cell.bgView.hidden = NO;
            
            cell.textView.hidden = NO;
            
            cell.bgLabel.hidden = NO;
            //    _inputLabel.hidden = YES;
            cell.bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
            cell.bgView.layer.shadowOffset =  CGSizeMake(0, 6);
            cell.bgView.layer.shadowOpacity = 0.3;
            cell.bgView.layer.shadowColor =  UIColorFromRGB(0x1c63ff).CGColor;
            [cell.textView becomeFirstResponder];
            //        [_addMessageTable reloadData];
        });
    }else{
        
        _isClickBreakDescribe = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:7];
            AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
            cell.bgView.hidden = NO;
            
            cell.textView.hidden = NO;
            
            cell.bgLabel.hidden = NO;
            //    _inputLabel.hidden = YES;
            cell.bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
            cell.bgView.layer.shadowOffset =  CGSizeMake(0, 6);
            cell.bgView.layer.shadowOpacity = 0.3;
            cell.bgView.layer.shadowColor =  UIColorFromRGB(0x1c63ff).CGColor;
            [cell.textView becomeFirstResponder];
            //        [_addMessageTable reloadData];
        });
    }
    
    
    
    
    return YES;
}
- (void)textViewTextChanged:(NSNotification *)notification {
    UITextView *textView = notification.object;
    NSString *text = textView.text;
    if (textView.tag == _addModel.breakDescribeTag) {
        _addModel.breakDescribe = text;
    }else{
        
        _addModel.dangerDescribe = text;
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    if(self.topTag==10000)
    {
        
        NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:12];
        AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
        cell.bgView.layer.shadowOffset =  CGSizeMake(0, 0);
        cell.bgView.layer.shadowOpacity = 0;
        cell.bgView.layer.shadowColor =  [UIColor clearColor].CGColor;
        
        cell.bgView.hidden = YES;
        
        cell.textView.hidden = YES;
        
        cell.bgLabel.hidden = YES;
        cell.inputLabel.hidden = NO;
        cell.inputLabel.text = cell.textView.text;
        
        [cell.textView resignFirstResponder];
        
        _isClickDangerDescribe = NO;
    }
    else
    {
        
        NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:7];
        AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
        cell.bgView.layer.shadowOffset =  CGSizeMake(0, 0);
        cell.bgView.layer.shadowOpacity = 0;
        cell.bgView.layer.shadowColor =  [UIColor clearColor].CGColor;
        
        cell.bgView.hidden = YES;
        
        cell.textView.hidden = YES;
        
        cell.bgLabel.hidden = YES;
        cell.inputLabel.hidden = NO;
        cell.inputLabel.text = cell.textView.text;
        
        [cell.textView resignFirstResponder];
        _isClickBreakDescribe = NO;
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]){
        
        if(self.topTag == 10000){
            _isClickDangerDescribe = NO;
            NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:12];
            AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
            cell.bgView.hidden = YES;
            cell.textView.hidden = YES;
            cell.bgLabel.hidden = YES;
            [textView resignFirstResponder];
        }else{
            _isClickBreakDescribe = NO;
            NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:7];
            AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
            cell.bgView.hidden = YES;
            cell.textView.hidden = YES;
            cell.bgLabel.hidden = YES;
            [textView resignFirstResponder];
        }
        //  [_addMessageTable reloadData];
        return NO;
    };
    
    if(self.topTag==10000){
        
        NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:12];
        AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
        if(![text isEqualToString:@""])
        {
            [cell.textPlaceHolder setHidden:YES];
            cell.textView.backgroundColor = UIColorFromRGB(0x4c84ff);
        }
        if([text isEqualToString:@""]&&range.length==1&&range.location==0){
            [cell.textPlaceHolder setHidden:NO];
            cell.textView.backgroundColor = [UIColor clearColor];
        }
    }else{
        
        NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:7];
        AddNewMessageInputCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
        if(![text isEqualToString:@""])
        {
            [cell.textPlaceHolder setHidden:YES];
            cell.textView.backgroundColor = UIColorFromRGB(0x4c84ff);
        }
        if([text isEqualToString:@""]&&range.length==1&&range.location==0){
            [cell.textPlaceHolder setHidden:NO];
            cell.textView.backgroundColor = [UIColor clearColor];
        }
    }
    
    return YES;
}

#pragma mark Picktimedelegate
//当时间改变时触发
- (void)changeTime:(NSDate *)date{
    
    ISSAddNewTimeCell * cell = [_addMessageTable cellForRowAtIndexPath:selectIndex];
    cell.timeLabel.text = [self.timeV stringFromDate:date];
    if (selectIndex.row == 0) {
        [_timeDict setObject:cell.timeLabel.text forKey:@"startTime"];
    }else{
        [_timeDict setObject:cell.timeLabel.text forKey:@"endTime"];
    }
    NSLog(@"%@",date);
}

//确定时间
- (void)determine:(NSDate *)date {
    ISSAddNewTimeCell * cell = [_addMessageTable cellForRowAtIndexPath:selectIndex];
    cell.timeLabel.text = [self.timeV stringFromDate:date];
    if (selectIndex.row == 0) {
        [_timeDict setObject:cell.timeLabel.text forKey:@"startTime"];
    }else{
        [_timeDict setObject:cell.timeLabel.text forKey:@"endTime"];
    }
}

#pragma mark - Button Action

//隐患级别选择
-(void)leftRankAction
{
    if (_dangerLevelFlag) {
        return;
    }
    NSIndexPath * index5 = [NSIndexPath indexPathForRow:0 inSection:5];
    ISSAddNewAnonymousCell * cell5 = [_addMessageTable cellForRowAtIndexPath:index5];
    
    cell5.leftButton.selected = YES;
    cell5.rightButton.selected = NO;
    
    _dangerLevelFlag = YES;
    _addModel.dangerRank = @"8a88fee1570c782301570ce8c271004e";
    
    _isHideChangeCell = NO;
    [self.addMessageTable reloadSections:[NSIndexSet indexSetWithIndex:11] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)rightRankAction
{
    if (!_dangerLevelFlag) {
        return;
    }
    NSIndexPath * index5 = [NSIndexPath indexPathForRow:0 inSection:5];
    ISSAddNewAnonymousCell * cell5 = [_addMessageTable cellForRowAtIndexPath:index5];
    
    cell5.leftButton.selected = NO;
    cell5.rightButton.selected = YES;
    _dangerLevelFlag = NO;
    _addModel.dangerRank = @"8a88fee1570c782301570ce90c23004f";
    
    // 是否已整改去掉，默认未整改
    _isHideChangeCell = YES;
    [self.addMessageTable reloadSections:[NSIndexSet indexSetWithIndex:11] withRowAnimation:UITableViewRowAnimationNone];
}

//是否整改
-(void)leftChangeAction
{
    if (_dangerLevelFlag)
    {
        if (_isChangeFlag) {
            return;
        }
        NSIndexPath * index11 = [NSIndexPath indexPathForRow:0 inSection:11];
        ISSAddNewAnonymousCell * cell11 = [_addMessageTable cellForRowAtIndexPath:index11];
        
        cell11.leftButton.selected = YES;
        cell11.rightButton.selected = NO;
        
        _isChangeFlag = YES;
        _addModel.isChange = @"1";
        
        [self.addMessageTable reloadSections:[NSIndexSet indexSetWithIndex:11] withRowAnimation:UITableViewRowAnimationNone];
    }
}
-(void)rightChangeAction
{
    if (_dangerLevelFlag)
    {
        if (!_isChangeFlag) {
            return;
        }
        NSIndexPath * index11 = [NSIndexPath indexPathForRow:0 inSection:11];
        ISSAddNewAnonymousCell * cell11 = [_addMessageTable cellForRowAtIndexPath:index11];
        
        cell11.leftButton.selected = NO;
        cell11.rightButton.selected = YES;
        
        _isChangeFlag = NO;
        _addModel.isChange = @"0";
        
        [self.addMessageTable reloadSections:[NSIndexSet indexSetWithIndex:11] withRowAnimation:UITableViewRowAnimationNone];
    }
}

//匿名举报
- (void)leftButtonAction
{
    if (_isAnonymousFlag) {
        return;
    }
    NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:9];
    ISSAddNewAnonymousCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
    
    cell.leftButton.selected = YES;
    cell.rightButton.selected = NO;
    _isAnonymousFlag = YES;
    _addModel.isHiddleReport = @"1";
}

- (void)rightButtonAction
{
    if (!_isAnonymousFlag) {
        return;
    }
    NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:9];
    ISSAddNewAnonymousCell * cell = [_addMessageTable cellForRowAtIndexPath:index];
    
    cell.leftButton.selected = NO;
    cell.rightButton.selected = YES;
    _isAnonymousFlag = NO;
    _addModel.isHiddleReport = @"0";
}

- (BOOL)yhCheckUp{
    
    if (([_addModel.titleString isEqualToString:@""] || _addModel.titleString == nil) ||
        ([_addModel.idForFindCompany isEqualToString:@""] || _addModel.idForFindCompany == nil) ||
        ([_addModel.findCompanyString isEqualToString:@""] || _addModel.findCompanyString == nil)||
        ([_addModel.idFordangerCompany isEqualToString:@""] ||_addModel.idFordangerCompany == nil)||
        ([_addModel.breakCompanyString isEqualToString:@""] || _addModel.breakCompanyString == nil)||
        ([_addModel.dangerSource isEqualToString:@""]||_addModel.dangerSource == nil)||
        ([_addModel.verbPerson isEqualToString:@""] || _addModel.verbPerson == nil) ||
        ([[_timeDict objectForKey:@"startTime"] isEqualToString:@""] || [_timeDict objectForKey:@"startTime"] == nil)||
        //([[_timeDict objectForKey:@"endTime"] isEqualToString:@""] || [_timeDict objectForKey:@"endTime"] == nil)||
        ([_addModel.dangerType isEqualToString:@""] || _addModel.dangerType == nil) ||
        ([_addModel.dangerRank isEqualToString:@""] || _addModel.dangerRank == nil)||
        ([_addModel.breakPlaceString isEqualToString:@""] || _addModel.breakPlaceString == nil)||
        ([_addModel.dangerPart isEqualToString:@""] || _addModel.dangerPart == nil) ||
        ([_addModel.isChange isEqualToString:@""] || _addModel.isChange == nil)||
        ([photoString isEqualToString:@""] || photoString == nil) ||
        ([_addModel.dangerDescribe isEqualToString:@""] || _addModel.dangerDescribe == nil))
    {
        [self.view makeToast:LoadLanguagesKey(@"job_add_alertOver")];
        return NO;
    }
    if (_dangerLevelFlag && [@"1" isEqualToString:_addModel.isChange])
    {
        //一般隐患 ，已整改
        if (changePhotoString == nil || changePhotoString.length == 0) {
            [self.view makeToast:LoadLanguagesKey(@"job_add_alertOver")];
            return NO;
        }
    }
    return YES;
}

- (BOOL)wzCheckUp{
    
    if (([_addModel.titleString isEqualToString:@""] || _addModel.titleString == nil) ||
        ([_addModel.findCompanyString isEqualToString:@""] || _addModel.findCompanyString == nil) ||
        ([_addModel.breakCompanyString isEqualToString:@""] || _addModel.breakCompanyString == nil)||
        ([_addModel.verbPerson isEqualToString:@""] || _addModel.verbPerson == nil) ||
        ([[_timeDict objectForKey:@"startTime"]  isEqualToString:@""] || [_timeDict objectForKey:@"startTime"]  == nil)||
        //([[_timeDict objectForKey:@"endTime"] isEqualToString:@""] ||[_timeDict objectForKey:@"endTime"] == nil)||
        ([_addModel.breakTypeString isEqualToString:@""] || _addModel.breakTypeString == nil)||
        ([_addModel.breakPlaceString isEqualToString:@""] || _addModel.breakPlaceString == nil)||
        ([_addModel.breakDescribe isEqualToString:@""] || _addModel.breakDescribe == nil) ||
        ([_addModel.isHiddleReport isEqualToString:@""] || _addModel.isHiddleReport == nil)||
        ([photoString isEqualToString:@""] || photoString == nil))
    {
        [self.view makeToast:@"请填写完整"];
        return NO;
    }
    return YES;
}

- (void)clickSureButton{
    WeakSelf(self)
    if(self.topTag ==10000){
        
        
        if ([self yhCheckUp]) {
            
            NSDictionary *normalParam = @{
                                          @"gsId": [ISSUserData sharedInstance].orgId,
                                          @"gsmc": [ISSUserData sharedInstance].orgName,
                                          @"yhmc": _addModel.titleString,
                                          @"jcdwid": _addModel.idForFindCompany,
                                          @"jcdwmc": _addModel.findCompanyString,
                                          @"sjdwid": _addModel.idFordangerCompany,
                                          @"sjdwmc": _addModel.breakCompanyString,
                                          @"yhly": _addModel.dangerCode,
                                          @"fxrmc": _addModel.verbPerson,
                                          @"fxrId":_addModel.personId,
                                          @"fxrq":  @([ISSDataDeal getTimeStamp:[_timeDict objectForKey:@"startTime"]]),
                                          //@"cjsj":  @([ISSDataDeal getTimeStamp:[_timeDict objectForKey:@"endTime"]]),
                                          @"yhlx": _addModel.dangerTypeCode,
                                          @"yhjb": _addModel.dangerRank,
                                          @"yhlbb":@"1",
                                          @"yhdd": _addModel.breakPlaceString,
                                          @"yhbw": _addModel.dangerPart,
                                          //@"sfxczg": _addModel.isChange,
                                          @"yhms":  _addModel.dangerDescribe,
                                          @"tplj":photoString
                                          };
            NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:normalParam];
            if (_dangerLevelFlag)
            {
                [param setObject:_addModel.isChange forKey:@"sfxczg"];
                
                if ([@"1" isEqualToString:_addModel.isChange]) {
                    [param setObject:changePhotoString forKey:@"zgtp"];
                }
            }
            else [param setObject:@"0" forKey:@"sfxczg"];
            
            [X_HttpUtil apiRequest:@"safety/addHiddenDanger" param:[param copy] view:self.view successBlock:^(NSDictionary *resultData) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"addDangerMessage" object:@"addHiddenDanger"];
                [weakself dismissViewControllerAnimated:YES completion:NULL];
            }failureBlock:^(NSDictionary *resultData) {
                
            }];
        }
        
    }else{
        if ([self wzCheckUp])
        {
            NSDictionary *param = @{
                                    @"gsId": [ISSUserData sharedInstance].orgId,
                                    @"gsmc": [ISSUserData sharedInstance].orgName,
                                    @"yhmc": _addModel.titleString,
                                    @"jcdwid": _addModel.idForFindCompany,
                                    @"jcdwmc": _addModel.findCompanyString,
                                    @"sjdwid": _addModel.idFordangerCompany,
                                    @"sjdwmc": _addModel.breakCompanyString,
                                    @"fxrmc": _addModel.verbPerson,
                                    @"fxrId":_addModel.personId,
                                    @"fxrq": @([ISSDataDeal getTimeStamp:[_timeDict objectForKey:@"startTime"]]),
                                    //@"cjsj":  @([ISSDataDeal getTimeStamp:[_timeDict objectForKey:@"endTime"]]),
                                    @"yhlx": _addModel.breakTypeCode,
                                    @"yhdd": _addModel.breakPlaceString,
                                    @"yhms": _addModel.breakDescribe,
                                    @"tplj": photoString,
                                    @"nm": _addModel.isHiddleReport
                                    };
            
            
            [X_HttpUtil apiRequest:@"safety/addBreakRule" param:param view:self.view showErrorMessage:YES successBlock:^(NSDictionary *resultData) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"addBreakMessage" object:@"addBreakDanger"];
                [weakself dismissViewControllerAnimated:YES completion:NULL];
            } failureBlock:NULL];
        }
    }
}

- (void)startTime
{
    NSIndexPath * index1 = [NSIndexPath indexPathForRow:0 inSection:1];
    ISSAddNewTimeCell * cell1 = [_addMessageTable cellForRowAtIndexPath:index1];
    [self.view addSubview:self.timeV];
    [self.timeV setAnimotion];
    //if (cell1.timeLabel.text.length != 0 && [cell1.timeLabel.text isEqualToString:LoadLanguagesKey(@"add_hiddenTrouble_chooseFindDate")]) {
    if (cell1.timeLabel.text.length > 0) {
        [self.timeV setNowTime:cell1.timeLabel.text];
        
        _addModel.findTimeString =  cell1.textLabel.text;
    }
}

- (void)endTime
{
    NSIndexPath * index1 = [NSIndexPath indexPathForRow:1 inSection:1];
    ISSAddNewTimeCell * cell1 = [_addMessageTable cellForRowAtIndexPath:index1];
    [self.view addSubview:self.timeV];
    [self.timeV setAnimotion];
    //if (cell1.timeLabel.text.length != 0 && [cell1.timeLabel.text isEqualToString:LoadLanguagesKey(@"add_hiddenTrouble_chooseRegisterDate")]) {
    if (cell1.timeLabel.text.length > 0) {
        [self.timeV setNowTime:cell1.timeLabel.text];
        _addModel.registerTimeString = cell1.textLabel.text;
    }
}

#pragma mark - lazyLoad

- (UIView *)footerView{
    
    if (!_footerView) {
        _footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 100)];
        _footerView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        UIButton * sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        sureButton.frame = CGRectMake(Screen_Width/4-20, 30, Screen_Width/2+40, 40);
        [sureButton setTitle:LoadLanguagesKey(@"add_hiddenTrouble_ok") forState:UIControlStateNormal];
        sureButton.backgroundColor = UIColorFromRGB(0x1c63ff);
        [sureButton setTintColor:[UIColor whiteColor]];
        [sureButton addTarget:self action:@selector(clickSureButton) forControlEvents:UIControlEventTouchUpInside];
        sureButton.layer.cornerRadius = 20;
        sureButton.clipsToBounds = YES;
        [_footerView addSubview:sureButton];
    }
    return _footerView;
}

- (TimePickview *)timeV{
    if (!_timeV) {
        self.timeV = [[TimePickview alloc]initWithFrame:self.view.bounds type:UIDatePickerModeDateAndTime];
        self.timeV.delegate = self;
    }
    return _timeV;
}
-(NSMutableArray *)photoArr{
    
    if(_photoArr == nil){
        
        _photoArr = [[NSMutableArray alloc]init];
    }
    return _photoArr;
}
@end

