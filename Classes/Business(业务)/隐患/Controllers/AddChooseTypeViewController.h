//
//  AddChooseTypeViewController.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/25.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^chooseTypeBlock)(NSDictionary *data);

/**
 隐患类型、违章类型
 */
@interface AddChooseTypeViewController : ISSBaseTableVC

@property (nonatomic,  copy) chooseTypeBlock chooseTypeBlock;
@property (nonatomic,strong) NSArray *typeList;//类型
@property (nonatomic,strong) NSString *type;// yh 还是 wz

@property (nonatomic,assign) int level;
@property (nonatomic,  copy) NSString *pCode;
@end
