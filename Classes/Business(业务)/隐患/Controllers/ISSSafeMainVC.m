//
//  ISSSafeMainVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSSafeMainVC.h"
#import "SafeChildViewController.h"
#import "AddNewViewController.h"
#import "UIView+LPWView.h"
#import "ISSBaseSearchVC.h"
#import "ISSSearchResultListVC.h"

#import "ISSFMDB.h"

@interface ISSSafeMainVC (){
    
    NSInteger _topIndex;
    NSDictionary *topDic;
    NSMutableArray * yhArr;
    NSMutableArray * wdArr;
}
@property (nonatomic, strong) UIButton * hiddenDangerButton;
@property (nonatomic, strong) UILabel *dangerNoReadPag;
@property (nonatomic, strong) UIButton * breakRuleButton;
@property (nonatomic, strong) UILabel *breakNoReadPag;
@property (nonatomic, strong) UIButton * mineButton;
@property (nonatomic, strong) UIView   * contentView;
@property (nonatomic, strong) UIView   * buttonView;
@property(nonatomic,strong)AddNewViewController *addVC;
@property(nonatomic,strong)UIButton * addButton;

@end

@implementation ISSSafeMainVC

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 要是放在didload执行，必须延迟执行，否则滑动会出现问题
    if (_topIndex == -1) {
        [self dealClickTopBtn:0];
    }
    
}
- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//通知
- (void)addDanger:(NSNotification *)noti
{
    //列表刷新
    SafeChildViewController *vc = self.childViewControllers[0];
    [vc resfreshList];
}
- (void)addBreak:(NSNotification *)noti
{
    //列表刷新
    SafeChildViewController *vc = self.childViewControllers[1];
    [vc resfreshList];
}
- (void)popCurrentTopIndex:(NSNotification *)noti
{
    //列表刷新
    SafeChildViewController *vc = self.childViewControllers[[noti.object intValue]];
    [vc resfreshList];
}

- (void)insertPushMsgSuccess
{
    [self configNoReadData:YES];
    
    SafeChildViewController *vc = self.childViewControllers[_topIndex];
    [vc resfreshList];
}

- (void)readPushMsgSuccess
{
    [self configNoReadData:YES];
    
    SafeChildViewController *vc = self.childViewControllers[_topIndex];
    [vc resfreshList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popCurrentTopIndex:) name:@"popCurrentTopIndex" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addDanger:) name:@"addDangerMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addBreak:) name:@"addBreakMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(insertPushMsgSuccess) name:@"insertPushMsgSuccessNote" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(readPushMsgSuccess) name:@"readPushMsgSuccessNote" object:nil];
    
    topDic = [[NSDictionary alloc]init];
    yhArr = [[NSMutableArray alloc]init];
    wdArr = [[NSMutableArray alloc]init];
    _topIndex = -1;
    [self createUI];
    
    // 统计未读
    [self configNoReadData:NO];
}

- (void)configNoReadData:(BOOL)updateTabBar
{
    self.dangerNoReadPag.hidden = NO;
    self.breakNoReadPag.hidden = NO;
    
    NSInteger num1 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"1"];
    NSInteger num2 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"2"];
    if (num1 == 0) self.dangerNoReadPag.hidden = YES;
    else self.dangerNoReadPag.text = FormatString(@"%ld",(long)num1);
    
    if (num2 == 0) self.breakNoReadPag.hidden = YES;
    else self.breakNoReadPag.text = FormatString(@"%ld",(long)num2);
    
    if (updateTabBar) [ShareApp setTabBarBadgeValue:num1 + num2 selectedIndex:1];
}

- (void)createUI{
    
    [self setTitle:LoadLanguagesKey(@"tabbar_security") showBackBtn:NO];
    [self createNavBarButton];
    [self createButtonView];
}

- (void)createNavBarButton
{
    if ([ISSUserData sharedInstance].operation_privilege_yh_add)
    {
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = 0;
        
        WeakSelf(self)
        self.addButton = [ISSNavUtil getImageBtn:3 clickBlock:^{
            [weakself add];
        }];
        UIBarButtonItem *rightItem1 = [[UIBarButtonItem alloc] initWithCustomView:self.addButton];
        UIBarButtonItem *rightItem2 = [ISSNavUtil getBarItem:self btnType:2 clickAction:@selector(search)];
        
        NSArray *buttonArray = [[NSArray alloc]
                                initWithObjects:rightItem1,negativeSeperator,rightItem2, nil];
        
        self.navigationItem.rightBarButtonItems = buttonArray;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItem:self btnType:2 clickAction:@selector(search)];
    }
}

- (void)createButtonView{
    
    _buttonView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 46)];
    _buttonView.backgroundColor = [UIColor themeColor];
    [self.view addSubview:_buttonView];
    
    CGFloat leftPadding = 25;
    CGFloat width = (Screen_Width - 25*4)/5;
    CGFloat height = 34;
    CGFloat topPadding = (CGRectGetHeight(_buttonView.frame) - height)/2 + 1;
    self.hiddenDangerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.hiddenDangerButton.frame = CGRectMake(leftPadding, topPadding, width*2, height);
    [self.hiddenDangerButton setTitle:LoadLanguagesKey(@"info_main_hiddenTroubleInfo") forState:UIControlStateNormal];
    self.hiddenDangerButton.titleLabel.numberOfLines = 0;
    self.hiddenDangerButton.titleLabel.font = [UIFont systemFontOfSize:14];
    self.hiddenDangerButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.hiddenDangerButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x16246F)] forState:UIControlStateNormal];
    [self.hiddenDangerButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x8953c9)] forState:UIControlStateSelected];
    [self.hiddenDangerButton addTarget:self action:@selector(clickHiddenDangerButton:) forControlEvents:UIControlEventTouchUpInside];
    self.hiddenDangerButton.layer.cornerRadius = 15;
    self.hiddenDangerButton.clipsToBounds = YES;
    self.hiddenDangerButton.tag = 10000;
    [_buttonView addSubview:self.hiddenDangerButton];
    
    self.dangerNoReadPag = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 18)];
    self.dangerNoReadPag.textColor= [UIColor whiteColor];
    self.dangerNoReadPag.backgroundColor = [UIColor redColor];
    self.dangerNoReadPag.font = FontSIZE(15);
    self.dangerNoReadPag.layer.cornerRadius = 5;
    self.dangerNoReadPag.layer.masksToBounds = YES;
    self.dangerNoReadPag.layer.borderWidth = 1;
    self.dangerNoReadPag.layer.borderColor = [UIColor whiteColor].CGColor;
    self.dangerNoReadPag.textAlignment = NSTextAlignmentCenter;
    self.dangerNoReadPag.center = CGPointMake(CGRectGetMaxX(self.hiddenDangerButton.frame), CGRectGetMinY(self.hiddenDangerButton.frame) + 2);
    [_buttonView addSubview:self.dangerNoReadPag];
    
    self.breakRuleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.breakRuleButton.frame = CGRectMake(CGRectGetMaxX(self.hiddenDangerButton.frame) + leftPadding, topPadding, width*2, height);
    [self.breakRuleButton setTitle:LoadLanguagesKey(@"info_main_breakRuleInfo") forState:UIControlStateNormal];
    self.breakRuleButton.titleLabel.numberOfLines = 0;
    self.breakRuleButton.titleLabel.font = [UIFont systemFontOfSize:14];
    self.breakRuleButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.breakRuleButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x16246F)] forState:UIControlStateNormal];
    [self.breakRuleButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xe544b0)] forState:UIControlStateSelected];
    [self.breakRuleButton addTarget:self action:@selector(clickBreakRuleButton:) forControlEvents:UIControlEventTouchUpInside];
    self.breakRuleButton.layer.cornerRadius = 15;
    self.breakRuleButton.clipsToBounds = YES;
    self.breakRuleButton.tag = 10001;
    [_buttonView addSubview:self.breakRuleButton];
    
    self.breakNoReadPag = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 18)];
    self.breakNoReadPag.textColor= [UIColor whiteColor];
    self.breakNoReadPag.backgroundColor = [UIColor redColor];
    self.breakNoReadPag.font = FontSIZE(15);
    self.breakNoReadPag.layer.cornerRadius = 5;
    self.breakNoReadPag.layer.masksToBounds = YES;
    self.breakNoReadPag.layer.borderWidth = 1;
    self.breakNoReadPag.layer.borderColor = [UIColor whiteColor].CGColor;
    self.breakNoReadPag.textAlignment = NSTextAlignmentCenter;
    self.breakNoReadPag.center = CGPointMake(CGRectGetMaxX(self.breakRuleButton.frame), CGRectGetMinY(self.breakRuleButton.frame) + 2);
    [_buttonView addSubview:self.breakNoReadPag];
    
    self.mineButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.mineButton.frame = CGRectMake(CGRectGetMaxX(self.breakRuleButton.frame) + leftPadding, topPadding, width, height);
    [self.mineButton setTitle:LoadLanguagesKey(@"safe_main_mine") forState:UIControlStateNormal];
    self.mineButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.mineButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x16246F)] forState:UIControlStateNormal];
    [self.mineButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
    [self.mineButton addTarget:self action:@selector(clickMineButton:) forControlEvents:UIControlEventTouchUpInside];
    self.mineButton.layer.cornerRadius = 15;
    self.mineButton.clipsToBounds = YES;
    self.mineButton.tag = 10002;
    [_buttonView addSubview:self.mineButton];
    self.hiddenDangerButton.selected = YES;
}

- (void)addChildView:(NSInteger)tag
{
    SafeChildViewController *vc = [[SafeChildViewController alloc] init];
    vc.topHeight = _buttonView.height;
    vc.mainType = tag - 9997;
    vc.view.frame = CGRectMake(0, CGRectGetMaxY(_buttonView.frame), Screen_Width, vc.scrollView.height);
    vc.view.tag = tag;
    [self.view addSubview:vc.view];
    [self addChildViewController:vc];
    
}

- (void)dealClickTopBtn:(NSInteger)index
{
    if (index == _topIndex) {
        return;
    }
    UIView *v1 = [self.view viewWithTag:9997];
    UIView *v2 = [self.view viewWithTag:9998];
    UIView *v3 = [self.view viewWithTag:9999];
    if (index == 0)
    {
        if (v1) v1.hidden = NO;
        else
        {
            [self addChildView:9997];
        }
        if (v2) v2.hidden = YES;
        if (v3) v3.hidden = YES;
    }
    else if (index == 1)
    {
        if (v1) v1.hidden = YES;
        if (v2) v2.hidden = NO;
        else
        {
            [self addChildView:9998];
        }
        if (v3) v3.hidden = YES;
    }
    else
    {
        if (v1) v1.hidden = YES;
        if (v2) v2.hidden = YES;
        if (v3) v3.hidden = NO;
        else
        {
            [self addChildView:9999];
        }
    }
    
    _topIndex = index;
}

#pragma mark - ButtonAction
//搜索

- (void)pushSearchResultListVC:(NSString *)searchText searchItem:(NSDictionary *)searchItem
{
    ISSSearchResultListVC *vc = [[ISSSearchResultListVC alloc] init];
    vc.searchType = ISSSearch_Safe;
    vc.searchText = searchText;
    vc.searchItem = searchItem;
    vc.mainType = _topIndex;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)search
{
    WeakSelf(self)
    ISSBaseSearchVC *vc = [[ISSBaseSearchVC alloc] init];
    vc.cateList = @[
                    @{@"name":LoadLanguagesKey(@"safe_main_all"),@"code":@"all",@"cacheKey":@"Safe_searchAllInfo"},
                    @{@"name":LoadLanguagesKey(@"info_main_hiddenTroubleInfo"),@"code":@"yh",@"cacheKey":@"Safe_searchDangerInfo"},
                    @{@"name":LoadLanguagesKey(@"info_main_breakRuleInfo"),@"code":@"wz",@"cacheKey":@"Safe_searchBreakInfo"},
                    @{@"name":LoadLanguagesKey(@"safe_main_mine"),@"code":@"wd",@"cacheKey":@"Safe_searchMineInfo"},
                    ];
    vc.clickBlock = ^(NSDictionary *cateItem, NSString *searchText) {
        NSLog(@"cateItem:%@,searchText:%@",cateItem,searchText);
        [weakself pushSearchResultListVC:searchText searchItem:cateItem];
    };
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
    [self presentViewController:nav animated:NO completion:NULL];
}
//加
- (void)add{
    
    self.addVC = [[AddNewViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.addVC];
    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
    if(self.hiddenDangerButton.selected ){
        
        self.addVC.topTag = 10000;
    }
    [self presentViewController:nav animated:YES completion:NULL];
    //[self.navigationController pushViewController:addVC animated:YES];
}

//点击隐患信息
- (void)clickHiddenDangerButton:(UIButton *)btn
{
    self.hiddenDangerButton.selected = YES;
    self.breakRuleButton.selected = NO;
    self.mineButton.selected = NO;
    self.addVC.topTag = btn.tag;
    if (self.addButton) self.addButton.hidden = NO;
    [self dealClickTopBtn:btn.tag - 10000];
}

//点击违章信息
- (void)clickBreakRuleButton:(UIButton *)btn
{
    self.hiddenDangerButton.selected = NO;
    self.breakRuleButton.selected = YES;
    self.mineButton.selected = NO;
    if (self.addButton) self.addButton.hidden = NO;
    [self dealClickTopBtn:btn.tag - 10000];
}

//点击我的
- (void)clickMineButton:(UIButton *)btn
{
    self.hiddenDangerButton.selected = NO;
    self.breakRuleButton.selected = NO;
    self.mineButton.selected = YES;
    [self.mineButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    if (self.addButton) self.addButton.hidden = YES;
    [self dealClickTopBtn:btn.tag - 10000];
}

@end

