//
//  ISSChooseDepartmentVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/28.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSChooseDepartmentVC.h"

@interface ISSChooseDepartmentVC ()

@end

@implementation ISSChooseDepartmentVC

- (void)queryList
{
    WeakSelf(self)
    if (!self.orgId) {
        //查询公司
        [X_HttpUtil apiRequest:@"safety/searchParam" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
            
            [weakself.resultDataList addObjectsFromArray:resultData[@"data"][@"ORG"]];
            [weakself.mTableView reloadData];
        }];
    }
    else
    {
        // 查询部门
        [X_HttpUtil apiRequest:@"safety/searchDepartments" param:@{@"orgId":self.orgId} view:self.view successBlock:^(NSDictionary *resultData) {
            [weakself.resultDataList addObjectsFromArray:resultData[@"data"]];
            [weakself.mTableView reloadData];
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = LoadLanguagesKey(@"levelSelected_navTitle");
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        
        if (!weakself.orgId) [weakself dismissViewControllerAnimated:YES completion:NULL];
        else [weakself.navigationController popViewControllerAnimated:YES];
    }];
    
    self.mTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 8)];
    
    [self queryList];
    
    if (!self.orgId)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedAction:) name:@"selectedDepartmentNote" object:nil];
    }
}

- (void)selectedAction:(NSNotification *)note
{
    if (_chooseBlock) {
        _chooseBlock(note.object);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = self.resultDataList[indexPath.row];
    cell.textLabel.text = data[@"ORG_NAME_"];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (!self.orgId) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        if ([data[@"HASCHILD"] intValue] == 1) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate withTitle: colorType
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *data = self.resultDataList[indexPath.row];
    if (!self.orgId)
    {
        ISSChooseDepartmentVC *vc = [[ISSChooseDepartmentVC alloc] init];
        vc.orgId = data[@"ID_"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        if ([data[@"HASCHILD"] intValue] == 1)
        {
            ISSChooseDepartmentVC *vc = [[ISSChooseDepartmentVC alloc] init];
            vc.orgId = data[@"ID_"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedDepartmentNote" object:data];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    
}

@end
