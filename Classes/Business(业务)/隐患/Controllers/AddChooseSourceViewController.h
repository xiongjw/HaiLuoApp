//
//  AddChooseSourceViewController.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/25.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 隐患来源
 */
typedef void (^chooseSourceBlock)(NSDictionary *data);

@interface AddChooseSourceViewController : UIViewController
@property (nonatomic,copy) chooseSourceBlock chooseSourceBlock;
@property (nonatomic ,strong) NSArray * dataArray;
@property(nonatomic,assign) NSInteger selectRow;
@end
