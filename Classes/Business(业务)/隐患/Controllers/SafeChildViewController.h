//
//  SafeChildViewController.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SafeChildViewController : ISSBaseVC

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic) CGFloat topHeight;
@property (nonatomic,assign) NSInteger mainType;

- (void)resfreshList;
@end
