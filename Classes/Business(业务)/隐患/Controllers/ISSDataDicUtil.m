//
//  ISSDataDicUtil.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/5.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSDataDicUtil.h"

@implementation ISSDataDicUtil

+ (ISSDataDicUtil *)sharedInstance
{
    static ISSDataDicUtil *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[ISSDataDicUtil alloc] init];
    });
    return instance;
}

- (void)searchParam
{
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"safety/searchParam" param:@{} view:nil showErrorMessage:NO successBlock:^(NSDictionary *resultData) {
        
        //发现单位和违章单位
        NSDictionary *data = resultData[@"data"];
        weakself.yhjbList = data[@"YHJB"];
        weakself.sourceList = data[@"YHLY"];
        weakself.typeList = data[@"YHLX"];
        
    } failureBlock:NULL];
}

- (NSString *)getNameByCode:(NSString *)code type:(NSInteger)type
{
    if (!self.sourceList) {
        return code;
    }
    NSString *result = code;
    //隐患来源
    if (type == 0)
    {
        for (NSDictionary *data in self.sourceList)
        {
            if ([code isEqualToString:data[@"CODE_"]]) {
                result = data[@"NAME_"];
                break;
            }
        }
    }
    // 隐患类型
    else if (type == 1)
    {
        if (!self.typeList) {
            return code;
        }
        /*
        NSMutableArray *firstList = [NSMutableArray new];
        for (NSDictionary *data in self.typeList)
        {
            // 第一级
            if (!data[@"PCODE_"])
            {
                // 人的不安全行为的子类 属于 违章类型，只展现子类
                if (![@"8a88fee1570c782301570d037d7a0074" isEqualToString:data[@"CODE_"]])
                {
                    [firstList addObject:data];
                }
            }
        }
        
        for (NSDictionary *firstData in firstList)
        {
            for (NSDictionary *item in self.typeList)
            {
                if (item[@"PCODE_"])
                {
                    if ([item[@"PCODE_"] isEqualToString:firstData[@"CODE_"]]) {
                        result = item[@"NAME_"];
                        NSLog(@"result:%@",result);
                        break;
                    }
                }
            }
        }
         */
        
        for (NSDictionary *data in self.typeList)
        {
            if (!data[@"PCODE_"]) {
                continue;
            }
            if ([code isEqualToString:data[@"CODE_"]]) {
                result = data[@"NAME_"];
                break;
            }
        }
    }
    // 违章类型
    else if (type == 2)
    {
        if (!self.typeList) {
            return code;
        }
        /*
        NSMutableArray *firstList = [NSMutableArray new];
        for (NSDictionary *data in self.typeList)
        {
            // 第一级
            if (!data[@"PCODE_"])
            {
                // 人的不安全行为的子类 属于 违章类型，只展现子类
                if ([@"8a88fee1570c782301570d037d7a0074" isEqualToString:data[@"CODE_"]])
                {
                    [firstList addObject:data];
                    break;
                }
            }
        }
        
        for (NSDictionary *firstData in firstList)
        {
            for (NSDictionary *item in self.typeList)
            {
                if (item[@"PCODE_"])
                {
                    if ([item[@"PCODE_"] isEqualToString:firstData[@"CODE_"]]) {
                        result = item[@"NAME_"];
                        break;
                    }
                }
            }
        }
         */
        for (NSDictionary *data in self.typeList)
        {
            if (!data[@"PCODE_"]) {
                continue;
            }
            if ([code isEqualToString:data[@"CODE_"]]) {
                result = data[@"NAME_"];
                break;
            }
        }
    }
    return result;
}

- (NSString *)getYHJB:(NSString *)code
{
    if (!self.yhjbList) {
        return code;
    }
    NSString *result = @"";
    for (NSDictionary *data in self.yhjbList)
    {
        if ([code isEqualToString:data[@"CODE_"]]) {
            result = data[@"NAME_"];
            break;
        }
    }
    return result;
}

@end
