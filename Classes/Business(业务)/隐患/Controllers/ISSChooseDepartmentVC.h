//
//  ISSChooseDepartmentVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/28.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^chooseDepartmentBlock)(NSDictionary *data);

@interface ISSChooseDepartmentVC : ISSBaseTableVC

@property (nonatomic,copy) NSString *orgId;
@property (nonatomic,copy) chooseDepartmentBlock chooseBlock;

@end
