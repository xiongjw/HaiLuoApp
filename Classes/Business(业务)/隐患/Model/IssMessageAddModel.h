//
//  IssMessageAddModel.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IssMessageAddModel : ISSBaseModel
@property(nonatomic,copy)NSString *titleString;
@property(nonatomic,assign)NSInteger titleTag;

@property(nonatomic,copy)NSString *findTimeString;

@property(nonatomic,copy)NSString *registerTimeString;

@property(nonatomic,copy)NSString *findCompanyString;

@property(nonatomic,copy)NSString *breakCompanyString;

@property(nonatomic,copy)NSString *verbPerson;

@property(nonatomic,copy)NSString *breakTypeString;

@property(nonatomic,copy)NSString *breakPlaceString;
@property(nonatomic,assign)NSInteger breakPlaceTag;

@property(nonatomic,copy)NSString *breakDescribe;
@property(nonatomic,assign)NSInteger breakDescribeTag;

@property(nonatomic,copy)NSArray *photoArr;

@property(nonatomic,copy)NSString *isHiddleReport;

@property(nonatomic,copy)NSString *dangerPart;
@property(nonatomic,assign)NSInteger dangerPartTag;

@property(nonatomic,copy)NSString *dangerSource;

@property(nonatomic,copy)NSString * dangerRank;

@property(nonatomic,copy)NSString* isChange;

@property(nonatomic,copy)NSString *dangerType;

@property(nonatomic,copy)NSString *dangerChange;

@property(nonatomic,copy)NSString *dangerDescribe;
@property(nonatomic,assign)NSInteger dangerDescribeTag;


//新增字段
//发现单位名称
@property(nonatomic,copy)NSString *idForFindCompany;
@property(nonatomic,copy)NSString *idFordangerCompany;


//上传需要不一样的字段
@property(nonatomic,copy)NSString *dangerCode;
@property(nonatomic,copy)NSString *personId;
@property(nonatomic,copy)NSString *dangerTypeCode;
@property(nonatomic,copy)NSString *breakTypeCode;

@end
