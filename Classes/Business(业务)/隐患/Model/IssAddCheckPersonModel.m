//
//  IssAddCheckPersonModel.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "IssAddCheckPersonModel.h"

@implementation IssAddCheckPersonModel
+ (instancetype)shareInstance {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}

+ (NSString *)getCategoryName:(NSString *)pid {
    NSString *name = @"";
    if (pid.length > 0) {
        for (IssAddCheckPersonModel *model in [IssAddCheckPersonModel shareInstance].dataList) {
            if ([model.value isEqualToString:pid]) {
                name = model.content;
                break;
            }
        }
    }
    return name;
}

@end
