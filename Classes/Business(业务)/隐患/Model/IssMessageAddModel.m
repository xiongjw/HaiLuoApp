//
//  IssMessageAddModel.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "IssMessageAddModel.h"

@implementation IssMessageAddModel
- (instancetype)init {
    if (self = [super init]) {
        _titleTag = 0;
        _breakPlaceTag = 3;
        _breakDescribeTag = 4;
        _dangerPartTag = 5;
    }
    return self;
}
@end
