//
//  IssAddCheckPersonModel.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IssAddCheckPersonModel : ISSBaseModel
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *content;

@property (nonatomic, strong) NSMutableArray *dataList;

+ (instancetype)shareInstance;
+ (NSString *)getCategoryName:(NSString *)pid;
@end
