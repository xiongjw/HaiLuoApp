//
//  ISSInfoTopView.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISSFMDB.h"

@interface ISSInfoTopView : UIView

@property(nonatomic,  strong)UIButton *locationBtn;

@property(nonatomic , strong)UIButton * dangerMessageButton;
@property(nonatomic , strong)UIButton * braekMessageButton;
@property(nonatomic , strong)UIButton * safeMessageButton;
@property(nonatomic , strong)UIButton * changeButton;

//dangerInfo
@property(nonatomic,  strong)UILabel *dangerNoReadPag;
@property(nonatomic,  strong)UILabel *dangerTotalPag;
@property(nonatomic,  strong)UILabel *noChangePag;
@property(nonatomic,  strong)UILabel *noChangeLabel;
//breakInfo
@property(nonatomic,  strong)UILabel *breakNoReadPag;
@property(nonatomic,  strong)UILabel *breakTotalPag;
//safeInfo
@property(nonatomic,  strong)UILabel *safeNoReadPag;
@property(nonatomic,  strong)UILabel *safeTotalPag;


- (void)configNoReadData:(BOOL)updateTabBar;

- (void)configTopConfigWithData:(NSDictionary *)dic;

// 天气
- (void)configWeatherWithData:(NSDictionary *)weatherData;
// 空气质量
- (void)configAqiWithData:(NSDictionary *)apiData weatherData:(NSDictionary *)weatherData;

- (void)configOldData:(ISSWeatherFMDBUtil *)model;

@end

