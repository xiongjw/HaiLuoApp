//
//  ListViewTableViewCell.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageCellModel.h"
#import "IssSafeListModel.h"
#import "MessageCellInfoModel.h"
#import "BraekCellInfoModel.h"
#import "SafeCellInfoModel.h"
@interface ListViewTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel       * noReadFlag;
@property (nonatomic, strong) UIImageView   * hiddenDangerImg;
@property (nonatomic, strong) UILabel       * titleLabel;
@property (nonatomic, strong) UIImageView   * hintImg;
@property (nonatomic, strong) UILabel       * timelabel;
@property (nonatomic, strong) UIImageView   * contentImg;
@property (nonatomic, strong) UILabel       * contentLabel;
@property (nonatomic, strong) UILabel       * addressLabel;

-(void)configListDataWithModel:(MessageCellModel *)cellModel;
-(void)configListDataWithSafeModel:(IssSafeListModel *)cellModel;

- (void)configListDataWithSafeModel:(IssSafeListModel *)cellModel type:(NSString *)type;
@end

