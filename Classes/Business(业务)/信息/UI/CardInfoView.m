//
//  CardInfoView.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "CardInfoView.h"

#import "ISSFMDB.h"

@implementation CardInfoView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initView];
        [self setFrameForView];
        
    }
    return self;
}

- (void)initView
{
    self.titleLb = [[UILabel alloc]init];
    self.titleLb.backgroundColor = [UIColor clearColor];
    self.titleLb.textColor = [UIColor whiteColor];
    self.titleLb.font = FontSIZE(25);
    [self addSubview:self.titleLb];
    
    self.totalValueLb = [[UILabel alloc]init];
    self.totalValueLb.backgroundColor = [UIColor clearColor];
    self.totalValueLb.text = @"0";
    self.totalValueLb.adjustsFontSizeToFitWidth = YES;
    self.totalValueLb.textColor = [UIColor whiteColor];
    self.totalValueLb.font = FontSIZE(50);
    [self addSubview:self.totalValueLb];
    
    self.totalKeyLb = [[UILabel alloc]init];
    self.totalKeyLb.backgroundColor = [UIColor clearColor];
    self.totalKeyLb.text = LoadLanguagesKey(@"info_card_item");
    self.totalKeyLb.textColor = [UIColor whiteColor];
    self.totalKeyLb.font = FontSIZE(15);
    [self addSubview:self.totalKeyLb];
    
    /*
    self.noReadKeyLb = [[UILabel alloc]init];
    self.noReadKeyLb.backgroundColor = [UIColor clearColor];
    self.noReadKeyLb.text = LoadLanguagesKey(@"info_card_notRead");
    self.noReadKeyLb.textColor = [UIColor whiteColor];
    self.noReadKeyLb.font = FontSIZE(15);
    [self addSubview:self.noReadKeyLb];
    
    self.noReadValueLb = [[UILabel alloc]init];
    self.noReadValueLb.backgroundColor = [UIColor clearColor];
    self.noReadValueLb.text = @"0";
    self.noReadValueLb.textColor = [UIColor whiteColor];
    self.noReadValueLb.font = FontSIZE(15);
    [self addSubview:self.noReadValueLb];
     */
    self.noReadBtn = [X_Button buttonWithType:UIButtonTypeCustom];
    _noReadBtn.titleLabel.font = FontSIZE(15);
    [_noReadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _noReadBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self addSubview:_noReadBtn];
    
    self.eyeImageView = [[UIImageView alloc]init];
    [self addSubview:self.eyeImageView];
    self.eyeImageView.image = [UIImage imageNamed:@"car_noRead"];
    
    /*
    self.noChangeView = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.noChangeView];
    
    self.noChangeLabel = [[UILabel alloc]init];
    self.noChangeLabel.backgroundColor = [UIColor clearColor];
    self.noChangeLabel.text =  LoadLanguagesKey(@"info_card_notAlter");
    self.noChangeLabel.textColor = [UIColor whiteColor];
    self.noChangeLabel.font = FontSIZE(12);
    [self.noChangeView addSubview:self.noChangeLabel];
    
    self.noChangePageLabel = [[UILabel alloc]init];
    self.noChangePageLabel.backgroundColor = [UIColor clearColor];
    self.noChangePageLabel.text = @"0";
    self.noChangePageLabel.textColor = [UIColor whiteColor];
    self.noChangePageLabel.font = FontSIZE(12);
    [self.noChangeView addSubview:self.noChangePageLabel];
    
    self.overTimeView = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.overTimeView];
    
    self.overTimeLabel = [[UILabel alloc]init];
    self.overTimeLabel.backgroundColor = [UIColor clearColor];
    self.overTimeLabel.text = LoadLanguagesKey(@"info_card_overdue");
    self.overTimeLabel.textColor = [UIColor whiteColor];
    self.overTimeLabel.font = FontSIZE(12);
    [self.overTimeView addSubview:self.overTimeLabel];
    
    self.overTimePageLabel = [[UILabel alloc]init];
    self.overTimePageLabel.backgroundColor = [UIColor clearColor];
    self.overTimePageLabel.text = @"0";
    self.overTimePageLabel.textColor = [UIColor whiteColor];
    self.overTimePageLabel.font = FontSIZE(12);
    [self.overTimeView addSubview:self.overTimePageLabel];
    
    self.safeView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.safeView.backgroundColor = UIColorFromRGB(0xEB5452);
    [self addSubview:self.safeView];
    
    self.safeLabel = [[UILabel alloc]init];
    self.safeLabel.backgroundColor = [UIColor clearColor];
    self.safeLabel.text = LoadLanguagesKey(@"info_card_punishment");
    self.safeLabel.textColor = [UIColor whiteColor];
    self.safeLabel.font = FontSIZE(12);
    [self.safeView addSubview:self.safeLabel];
    
    self.safePageLabel = [[UILabel alloc]init];
    self.safePageLabel.backgroundColor = [UIColor clearColor];
    self.safePageLabel.text = @"0";
    self.safePageLabel.textColor = [UIColor whiteColor];
    self.safePageLabel.font = FontSIZE(12);
    [self.safeView addSubview:self.safePageLabel];
     */
}

- (void)setFrameForView
{
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.mas_top).offset(20);
        make.height.equalTo(@30);
        make.width.mas_lessThanOrEqualTo(@200);
    }];
    
    [self.totalValueLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.titleLb.mas_bottom).offset(20);
        make.height.equalTo(@40);
        make.width.mas_lessThanOrEqualTo(@100);
    }];
    
    [self.totalKeyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.totalValueLb.mas_right).offset(10);
        make.bottom.equalTo(self.totalValueLb.mas_bottom);
        make.height.equalTo(@20);
        make.width.mas_lessThanOrEqualTo(@50);
    }];
    
    /*
    [self.noReadValueLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.bottom.equalTo(self.totalValueLb.mas_bottom);
        make.height.equalTo(@20);
        make.width.mas_lessThanOrEqualTo(@50);
    }];
    
    [self.noReadKeyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.noReadValueLb.mas_left).offset(-5);
        make.bottom.equalTo(self.totalValueLb.mas_bottom);
        make.height.equalTo(@20);
        make.width.mas_lessThanOrEqualTo(@100);
    }];
     */
    [self.noReadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.bottom.equalTo(self.totalValueLb.mas_bottom);
        make.height.equalTo(@30);
        make.width.mas_lessThanOrEqualTo(@100);
    }];
    
    [self.eyeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.mas_right).offset(-30);
        make.bottom.equalTo(self.noReadBtn.mas_top).offset(-10);
        //make.left.equalTo(self.noReadKeyLb.mas_left);
    }];
    
    /*
    [self.noChangeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.height.equalTo(@20);
        make.width.equalTo(@80);
    }];
    
    [self.noChangeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.noChangeView.mas_left).offset(10);
        make.top.equalTo(self.noChangeView.mas_top);
        make.bottom.equalTo(self.noChangeView.mas_bottom);
        make.width.mas_lessThanOrEqualTo(@80);
    }];
    
    [self.noChangePageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.noChangeView.mas_right).offset(-5);
        make.top.equalTo(self.noChangeView.mas_top);
        make.bottom.equalTo(self.noChangeView.mas_bottom);
        make.width.mas_lessThanOrEqualTo(@50);
    }];
    
    [self.overTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.noChangeView.mas_right).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.height.equalTo(@20);
        make.width.equalTo(@80);
    }];
    
    [self.overTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.overTimeView.mas_left).offset(10);
        make.top.equalTo(self.overTimeView.mas_top);
        make.bottom.equalTo(self.overTimeView.mas_bottom);
        make.width.mas_lessThanOrEqualTo(@80);
    }];
    
    [self.overTimePageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.overTimeView.mas_right).offset(-5);
        make.top.equalTo(self.overTimeView.mas_top);
        make.bottom.equalTo(self.overTimeView.mas_bottom);
        make.width.mas_lessThanOrEqualTo(@50);
    }];
    
    [self.safeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.overTimeView.mas_right).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.height.equalTo(@20);
        make.width.equalTo(@80);
    }];
    
    [self.safeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.safeView.mas_left).offset(10);
        make.top.equalTo(self.safeView.mas_top);
        make.bottom.equalTo(self.safeView.mas_bottom);
        make.width.mas_lessThanOrEqualTo(@80);
    }];
    
    [self.safePageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.safeView.mas_right).offset(-5);
        make.top.equalTo(self.safeView.mas_top);
        make.bottom.equalTo(self.overTimeView.mas_bottom);
        make.width.mas_lessThanOrEqualTo(@50);
    }];
     */
}

-(void)configDataWithCardViewTag:(NSInteger)cardViewTag withCardInfoDic:(NSDictionary *)infoDic{
    
    if(cardViewTag == 0)
    {
        self.titleLb.text = LoadLanguagesKey(@"info_main_hiddenTroubleInfo");
        self.totalValueLb.text = [NSString stringWithFormat:@"%d",[infoDic[@"yh"][@"total"]intValue] ];
        [self.noReadBtn setTitle:FormatString(@"%@ %ld",LoadLanguagesKey(@"info_card_notRead"),[[ISSFMDBUtil sharedInstance] queryTypeMsg:@"1"]) forState:UIControlStateNormal];
        /*
        self.noChangeView.backgroundColor = [UIColor colorWithRed:127/255.0f green:111/255.0f blue:218/255.0f alpha:1];
        self.overTimeView.backgroundColor = [UIColor colorWithRed:127/255.0f green:111/255.0f blue:218/255.0f alpha:1];
        self.safeView.hidden = YES;
        [self.noChangeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(10);
            make.bottom.equalTo(self.mas_bottom).offset(-20);
            make.height.equalTo(@20);
            make.width.equalTo(@120);
        }];
        [self.safeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.overTimeView.mas_right).offset(10);
            make.bottom.equalTo(self.mas_bottom).offset(-20);
            make.height.equalTo(@20);
            make.width.equalTo(@120);
        }];
        
        self.overTimePageLabel.text = [NSString stringWithFormat:@"%d",[infoDic[@"yh"][@"yq"]intValue]];
        self.noChangePageLabel.text = [NSString stringWithFormat:@"%d",[infoDic[@"yh"][@"wzg"]intValue]];
        */
    }
    else if (cardViewTag == 1)
    {
        self.titleLb.text = LoadLanguagesKey(@"info_main_breakRuleInfo");
        self.totalValueLb.text = [NSString stringWithFormat:@"%d",[infoDic[@"wz"][@"total"]intValue]];
        [self.noReadBtn setTitle:FormatString(@"未读 %ld",[[ISSFMDBUtil sharedInstance] queryTypeMsg:@"2"]) forState:UIControlStateNormal];
        /*
        self.noChangeView.backgroundColor = UIColorFromRGB(0xD64A73);
        self.overTimeView.backgroundColor = UIColorFromRGB(0xEB5452);
        self.noChangeView.hidden = YES;
        self.overTimeView.hidden = YES;
        self.safeView.hidden = YES;
         */
    }
    else
    {
        self.titleLb.text = LoadLanguagesKey(@"info_main_safeInfo");
        self.totalValueLb.text = [NSString stringWithFormat:@"%d",[infoDic[@"aq"][@"total"]intValue]];
        [self.noReadBtn setTitle:FormatString(@"未读 %ld",[[ISSFMDBUtil sharedInstance] queryTypeMsg:@"3"]) forState:UIControlStateNormal];
        
        /*
        self.safeView.hidden = NO;
        self.noChangeLabel.text = LoadLanguagesKey(@"info_card_notice");
        self.overTimeLabel.text = LoadLanguagesKey(@"info_card_reward");
        self.noChangeView.backgroundColor = UIColorFromRGB(0x79D8A5);
        self.overTimeView.backgroundColor = UIColorFromRGB(0x79D8A5);
        self.safeView.backgroundColor = UIColorFromRGB(0x79D8A5);
        self.noChangePageLabel.text = [NSString stringWithFormat:@"%d",[infoDic[@"aqgg"][@"total"]intValue]];
        self.overTimePageLabel.text = [NSString stringWithFormat:@"%d",[infoDic[@"jl"][@"total"]intValue]];
        self.safePageLabel.text = [NSString stringWithFormat:@"%d",[infoDic[@"cf"][@"total"]intValue]];
         */
    }
    
}
@end

