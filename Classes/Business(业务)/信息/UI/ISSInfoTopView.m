//
//  ISSInfoTopView.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSInfoTopView.h"
#import "UIView+LPWView.h"

#import "ISSFMDB.h"

@interface ISSInfoTopView()
{
    
    UILabel         * temperatureLabel;
    UIImageView     * dampIV;
    UILabel         * dampLabel;
    UIImageView     * airIV;
    UILabel         * airLabel;
    UILabel         * weatherLabel;
    
}
@end
@implementation ISSInfoTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initView];
        [self initButtonInfoMessage];
        
    }
    return self;
}
-(void)initView{
    
    self.locationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    self.locationBtn.frame = CGRectMake(17, 8, 120, 30);
    [self.locationBtn setTitle:@" 正在获取位置    " forState:UIControlStateNormal];
    [self.locationBtn setTitleColor:UIColorFromRGB(0x3A4B9D) forState:UIControlStateNormal];
    [self.locationBtn setImage:[UIImage imageNamed:@"info_location"] forState:UIControlStateNormal];
    self.locationBtn.titleLabel.font = FontSIZE(14);
    self.locationBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.locationBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.locationBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self addSubview:self.locationBtn];
    
    _changeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_changeButton setImage:[UIImage imageNamed:@"message_top_pull"] forState:UIControlStateNormal];
    [_changeButton setImage:[UIImage imageNamed:@"message_top_poll"] forState:UIControlStateSelected];
    [self addSubview:_changeButton];
    
    temperatureLabel = [[UILabel alloc]init];
    temperatureLabel.textColor = UIColorFromRGB(0x3A4B9D);
    temperatureLabel.font = FontSIZE(50);
    //temperatureLabel.text = @"26°";
    [self addSubview:temperatureLabel];
    
    weatherLabel = [[UILabel alloc]init];
    weatherLabel.textColor = UIColorFromRGB(0x3A4B9D);
    weatherLabel.font = FontSIZE(15);
    //weatherLabel.text = @"雨";
    [self addSubview:weatherLabel];
    
    dampIV = [[UIImageView alloc]init];
    dampIV.hidden = YES;
    dampIV.image = [UIImage imageNamed:@"message_top_humidity"];
    [self addSubview:dampIV];
    
    dampLabel = [[UILabel alloc]init];
    dampLabel.textColor = UIColorFromRGB(0x3A4B9D);
    dampLabel.font = FontSIZE(14);
    //dampLabel.text = @"51%";
    [self addSubview:dampLabel];
    
    airIV= [[UIImageView alloc]init];
    airIV.hidden = YES;
    airIV.image = [UIImage imageNamed:@"message_top_air"];
    [self addSubview:airIV];
    
    airLabel = [[UILabel alloc]init];
    airLabel.textColor = UIColorFromRGB(0x3A4B9D);
    airLabel.font = FontSIZE(14);
    //airLabel.text = @"西南风2级";
    [self addSubview:airLabel];
    
    _dangerMessageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dangerMessageButton.backgroundColor = [UIColor colorWithRed:147/255.0f green:65/255.0f blue:225/255.0f alpha:1];
    //    [_dangerMessageButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:147/255.0f green:65/255.0f blue:225/255.0f alpha:1]] forState:UIControlStateNormal];
    [_dangerMessageButton setTitle:LoadLanguagesKey(@"info_main_hiddenTroubleInfo") forState:UIControlStateNormal];
    [_dangerMessageButton setTitleColor:[UIColor colorWithWhite:1 alpha:0.8] forState:UIControlStateNormal];
    _dangerMessageButton.titleLabel.font = FontSIZE(16);
    _dangerMessageButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _dangerMessageButton.layer.cornerRadius = 15;
    // _dangerMessageButton.layer.masksToBounds = YES;
    _dangerMessageButton.titleLabel.numberOfLines = 2;
    [self addSubview:_dangerMessageButton];
    
    NSString *language = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    bool isEn = [language hasPrefix:@"en"];
    if (isEn) {
        _dangerMessageButton.titleLabel.font = FontSIZE(14);
    }
    
    _braekMessageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_braekMessageButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:255/255.0f green:54/255.0f blue:103/255.0f alpha:1]] forState:UIControlStateNormal];
    _braekMessageButton.backgroundColor = [UIColor colorWithRed:255/255.0f green:54/255.0f blue:103/255.0f alpha:1];
    [_braekMessageButton setTitle:LoadLanguagesKey(@"info_main_breakRuleInfo") forState:UIControlStateNormal];
    [_braekMessageButton setTitleColor:[UIColor colorWithWhite:1 alpha:0.8] forState:UIControlStateNormal];
    _braekMessageButton.titleLabel.font = FontSIZE(16);
    _braekMessageButton.layer.cornerRadius = 15;
    _braekMessageButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    //  _braekMessageButton.clipsToBounds = YES;
    _braekMessageButton.titleLabel.numberOfLines = 2;
    [self addSubview:_braekMessageButton];
    
    _safeMessageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_safeMessageButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:47/255.0f green:226/255.0f blue:145/255.0f alpha:1]] forState:UIControlStateNormal];
    _safeMessageButton.backgroundColor = [UIColor colorWithRed:47/255.0f green:226/255.0f blue:145/255.0f alpha:1];
    [_safeMessageButton setTitle:LoadLanguagesKey(@"info_main_safeInfo") forState:UIControlStateNormal];
    _safeMessageButton.titleLabel.font = FontSIZE(16);
    _safeMessageButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _safeMessageButton.layer.cornerRadius = 15;
    //_safeMessageButton.clipsToBounds = YES;
    _safeMessageButton.titleLabel.numberOfLines = 2;
    [self addSubview:_safeMessageButton];
    
    [self.locationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(8);
        make.left.equalTo(self.mas_left).offset(17);
        make.width.equalTo(@120);
        make.height.equalTo(@20);
    }];
    [_changeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-17);
        make.centerY.equalTo(self.locationBtn);
        make.width.height.equalTo(@30);
    }];
    
    temperatureLabel.preferredMaxLayoutWidth = (100);
    [temperatureLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [temperatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.locationBtn.mas_left);
        make.height.equalTo(@50);
        make.top.equalTo(self.locationBtn.mas_bottom).offset(8);
    }];
    
    [weatherLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(temperatureLabel.mas_right).offset(5);
        make.height.equalTo(@20);
        make.top.equalTo(temperatureLabel.mas_top);
    }];
    
    [dampIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weatherLabel.mas_left).offset(10);
        make.bottom.equalTo(temperatureLabel.mas_bottom);
        make.width.equalTo(@10);
        make.height.equalTo(@13);
    }];
    
    dampLabel.preferredMaxLayoutWidth = (60);
    [dampLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [dampLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(dampIV.mas_right).offset(5);
        make.centerY.equalTo(dampIV);
        make.height.equalTo(@20);
    }];
    
    [airIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(dampLabel.mas_right).offset(20);
        make.centerY.equalTo(dampLabel);
        make.height.equalTo(@14);
        make.width.equalTo(@13);
    }];
    
    [airLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(airIV.mas_right).offset(5);
        make.centerY.equalTo(airIV);
        make.height.equalTo(@20);
    }];
    
    [_dangerMessageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(17);
        make.top.equalTo(temperatureLabel.mas_bottom).offset(25);
        make.height.equalTo(@130);
        make.width.equalTo(@[_braekMessageButton,_safeMessageButton]);
    }];
    _dangerMessageButton.titleEdgeInsets = UIEdgeInsetsMake(_dangerMessageButton.size.height, 0, -60, 0);
    [_dangerMessageButton layoutIfNeeded];
    
    
    [_braekMessageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dangerMessageButton.mas_right).offset(20);
        make.centerY.equalTo(_dangerMessageButton);
        make.height.equalTo(@130);
    }];
    _braekMessageButton.titleEdgeInsets = UIEdgeInsetsMake(_braekMessageButton.size.height, 0, -60, 0);
    [_braekMessageButton layoutIfNeeded];
    
    [_safeMessageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-17);
        make.left.equalTo(_braekMessageButton.mas_right).offset(20);
        make.centerY.equalTo(_braekMessageButton);
        make.height.equalTo(@130);
        
    }];
    _safeMessageButton.titleEdgeInsets = UIEdgeInsetsMake(_safeMessageButton.size.height, 0, -60, 0);
    [_safeMessageButton layoutIfNeeded];
}

-(void)initButtonInfoMessage{
    
    self.dangerNoReadPag = [[UILabel alloc]init];
    self.dangerNoReadPag.textColor= [UIColor whiteColor];
    self.dangerNoReadPag.backgroundColor = [UIColor redColor];
    self.dangerNoReadPag.text = @"0";
    self.dangerNoReadPag.hidden = YES;
    self.dangerNoReadPag.font =FontSIZE(15);
    self.dangerNoReadPag.layer.cornerRadius = 5;
    self.dangerNoReadPag.layer.masksToBounds = YES;
    self.dangerNoReadPag.layer.borderWidth = 1;
    self.dangerNoReadPag.layer.borderColor = [UIColor whiteColor].CGColor;
    self.dangerNoReadPag.textAlignment = NSTextAlignmentCenter;
    [self.dangerMessageButton addSubview:self.dangerNoReadPag];
    [self.dangerMessageButton bringSubviewToFront:self.dangerNoReadPag];
    
    self.dangerTotalPag = [[UILabel alloc]init];
    self.dangerTotalPag.textColor= [UIColor whiteColor];
    self.dangerTotalPag.textAlignment = NSTextAlignmentCenter;
    self.dangerTotalPag.backgroundColor = [UIColor clearColor];
    self.dangerTotalPag.text = @"0";
    self.dangerTotalPag.font = FontSIZE(40);
    self.dangerTotalPag.adjustsFontSizeToFitWidth = YES;
    [self.dangerMessageButton  addSubview:self.dangerTotalPag];
    
    NSString *language = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    bool isEn = [language hasPrefix:@"en"];
    
    self.noChangeLabel = [[UILabel alloc]init];
    self.noChangeLabel.backgroundColor = [UIColor clearColor];
    self.noChangeLabel.text = LoadLanguagesKey(@"info_card_notAlter");
    self.noChangeLabel.adjustsFontSizeToFitWidth = YES;
    self.noChangeLabel.font = FontSIZE(12);
    self.noChangeLabel.textColor = [UIColor whiteColor];
    [self.dangerMessageButton addSubview:self.noChangeLabel];
    
    self.noChangePag = [[UILabel alloc]init];
    self.noChangePag.backgroundColor = [UIColor clearColor];
    self.noChangePag.textColor = [UIColor whiteColor];
    self.noChangePag.adjustsFontSizeToFitWidth = YES;
    self.noChangePag.text = @"0";
    self.noChangePag.font = FontSIZE(20);
    [self.dangerMessageButton addSubview:self.noChangePag];
    
    if (isEn) {
        self.noChangeLabel.font = FontSIZE(11);
        self.noChangePag.font = FontSIZE(16);
    }
    
    self.breakNoReadPag = [[UILabel alloc]init];
    self.breakNoReadPag.textColor= [UIColor whiteColor];
    self.breakNoReadPag.backgroundColor = [UIColor redColor];
    self.breakNoReadPag.textAlignment = NSTextAlignmentCenter;
    self.breakNoReadPag.text = @"0";
    self.breakNoReadPag.hidden = YES;
    self.breakNoReadPag.layer.cornerRadius = 5;
    self.breakNoReadPag.layer.masksToBounds = YES;
    self.breakNoReadPag.layer.borderWidth = 1;
    self.breakNoReadPag.layer.borderColor = [UIColor whiteColor].CGColor;
    self.breakNoReadPag.font = FontSIZE(15);
    [self.braekMessageButton addSubview:self.breakNoReadPag];
    
    self.breakTotalPag = [[UILabel alloc]init];
    self.breakTotalPag.backgroundColor = [UIColor clearColor];
    self.breakTotalPag.textColor = [UIColor whiteColor];
    self.breakTotalPag.text = @"0";
    self.breakTotalPag.adjustsFontSizeToFitWidth = YES;
    self.breakTotalPag.font = FontSIZE(40);
    self.breakTotalPag.textAlignment = NSTextAlignmentCenter;
    [self.braekMessageButton addSubview:self.breakTotalPag];
    
    self.safeNoReadPag = [[UILabel alloc]init];
    self.safeNoReadPag.textColor= [UIColor whiteColor];
    self.safeNoReadPag.backgroundColor = [UIColor redColor];
    self.safeNoReadPag.textAlignment = NSTextAlignmentCenter;
    self.safeNoReadPag.text = @"0";
    self.safeNoReadPag.hidden = YES;
    self.safeNoReadPag.layer.cornerRadius = 5;
    self.safeNoReadPag.layer.masksToBounds = YES;
    self.safeNoReadPag.layer.borderWidth = 1;
    self.safeNoReadPag.layer.borderColor = [UIColor whiteColor].CGColor;
    self.safeNoReadPag.font = FontSIZE(15);
    [self.safeMessageButton addSubview:self.safeNoReadPag];
    
    self.safeTotalPag = [[UILabel alloc]init];
    self.safeTotalPag.backgroundColor = [UIColor clearColor];
    self.safeTotalPag.textColor = [UIColor whiteColor];
    self.safeTotalPag.text = @"0";
    self.safeTotalPag.adjustsFontSizeToFitWidth = YES;
    self.safeTotalPag.font = FontSIZE(40);
    self.safeTotalPag.textAlignment = NSTextAlignmentCenter;
    [self.safeMessageButton addSubview:self.safeTotalPag];
    
    [self.dangerNoReadPag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@25);
        make.height.equalTo(@18);
        make.left.equalTo(self.dangerMessageButton.mas_right).offset(-13);
        make.top.equalTo(self.dangerMessageButton.mas_top).offset(-9);
    }];
    
    [self.dangerTotalPag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dangerMessageButton.mas_left).offset(10);
        make.right.equalTo(self.dangerMessageButton.mas_right).offset(-10);
        make.top.equalTo(self.dangerMessageButton.mas_top).offset(10);
        make.height.equalTo(@30);
    }];
    
    self.noChangeLabel.numberOfLines = 2;
    [self.noChangeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if (isEn) make.left.equalTo(self.dangerMessageButton.mas_left).offset(2);
        else make.left.equalTo(self.dangerMessageButton.mas_left).offset(10);
        
        make.top.equalTo(self.dangerTotalPag.mas_bottom).offset(20);
        make.height.equalTo(@20);
        //make.width.equalTo(@50);
    }];
    
    [self.noChangePag mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if (isEn) make.left.equalTo(self.noChangeLabel.mas_right).offset(2);
        else make.left.equalTo(self.noChangeLabel.mas_right).offset(10);
        
        make.bottom.equalTo(self.noChangeLabel.mas_bottom);
        make.height.equalTo(@20);
        //make.right.equalTo(self.dangerMessageButton.mas_right).offset(-10);
    }];
    
    [self.breakNoReadPag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@25);
        make.height.equalTo(@18);
        make.left.equalTo(self.braekMessageButton.mas_right).offset(-13);
        make.top.equalTo(self.braekMessageButton.mas_top).offset(-9);
    }];
    
    [self.breakTotalPag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.braekMessageButton.mas_left).offset(10);
        make.right.equalTo(self.braekMessageButton.mas_right).offset(-10);
        make.top.equalTo(self.braekMessageButton.mas_top).offset(10);
        make.height.equalTo(@30);
    }];
    
    [self.safeNoReadPag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@25);
        make.height.equalTo(@18);
        make.left.equalTo(self.safeMessageButton.mas_right).offset(-13);
        make.top.equalTo(self.safeMessageButton.mas_top).offset(-9);
    }];
    
    [self.safeTotalPag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.safeMessageButton.mas_left).offset(10);
        make.right.equalTo(self.safeMessageButton.mas_right).offset(-10);
        make.top.equalTo(self.safeMessageButton.mas_top).offset(10);
        make.height.equalTo(@30);
    }];
}

- (void)configNoReadData:(BOOL)updateTabBar
{
    self.dangerNoReadPag.hidden = NO;
    self.breakNoReadPag.hidden = NO;
    self.safeNoReadPag.hidden = NO;
    
    NSInteger num1 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"1"];
    NSInteger num2 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"2"];
    NSInteger num3 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"3"];
    if (num1 == 0) self.dangerNoReadPag.hidden = YES;
    else self.dangerNoReadPag.text = FormatString(@"%ld",(long)num1);
    
    if (num2 == 0) self.breakNoReadPag.hidden = YES;
    else self.breakNoReadPag.text = FormatString(@"%ld",(long)num2);
    
    if (num3 == 0) self.safeNoReadPag.hidden = YES;
    else self.safeNoReadPag.text = FormatString(@"%ld",(long)num3);
    
    if (updateTabBar) [ShareApp setTabBarBadgeValue:num1 + num2 + num3 selectedIndex:0];
}

-(void)configTopConfigWithData:(NSDictionary *)dic
{
    self.dangerTotalPag.text = [NSString stringWithFormat:@"%d", [dic[@"yh"][@"total"] intValue]] ;
    self.noChangePag.text = [NSString stringWithFormat:@"%d",[dic[@"yh"][@"wzg"] intValue]];
    self.breakTotalPag.text = [NSString stringWithFormat:@"%d",[dic[@"wz"][@"total"] intValue]];
    self.safeTotalPag.text = [NSString stringWithFormat:@"%d", [dic[@"aq"][@"total"]intValue]];
}

// 天气
- (void)configWeatherWithData:(NSDictionary *)data
{
    temperatureLabel.hidden = NO;
    weatherLabel.hidden = NO;
    dampLabel.hidden = NO;
    dampIV.hidden = NO;
    NSString *temp = [NSString noNullString:data[@"temp"]];
    if (temp.length > 0) {
        temperatureLabel.text = FormatString(@"%@°",[NSString noNullString:data[@"temp"]]);
        weatherLabel.text = [NSString noNullString:data[@"weather"]];
        dampLabel.text = [NSString noNullString:data[@"humidity"]];
    }
    else
    {
        temperatureLabel.hidden = YES;
        weatherLabel.hidden = YES;
        dampLabel.hidden = YES;
        dampIV.hidden = YES;
    }
}

// 空气质量
- (void)configAqiWithData:(NSDictionary *)apiData weatherData:(NSDictionary *)weatherData
{
    airIV.hidden = NO;
    airLabel.hidden = NO;
    NSString *wd = [NSString noNullString:weatherData[@"wd"]];
    if (wd.length > 0) {
        airLabel.text = FormatString(@"%@%@",[NSString noNullString:weatherData[@"wd"]],[NSString noNullString:apiData[@"level"]]);
    }
    else
    {
        airIV.hidden = YES;
        airLabel.hidden = YES;
    }
}

- (void)configOldData:(ISSWeatherFMDBUtil *)model
{
    temperatureLabel.hidden = NO;
    weatherLabel.hidden = NO;
    dampLabel.hidden = NO;
    dampIV.hidden = NO;
    airIV.hidden = NO;
    airLabel.hidden = NO;
    
    temperatureLabel.text = FormatString(@"%@°",model.temp);
    weatherLabel.text = model.weather;
    dampLabel.text = model.humidity;
    airLabel.text = FormatString(@"%@%@",model.wd,model.level);
}

@end

