//
//  CardInfoView.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CardInfoView : UIView
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)UILabel *totalValueLb;
@property(nonatomic,strong)UILabel *totalKeyLb;
@property(nonatomic,strong)UIImageView *eyeImageView;
/*
@property(nonatomic,strong)UILabel *noReadKeyLb;
@property(nonatomic,strong)UILabel *noReadValueLb;
 */
@property(nonatomic,strong)X_Button *noReadBtn;

/*
@property(nonatomic,strong)UIButton *noChangeView;
@property(nonatomic,strong)UILabel *noChangeLabel;
@property(nonatomic,strong)UILabel *noChangePageLabel;

@property(nonatomic,strong)UIButton *overTimeView;
@property(nonatomic,strong)UILabel *overTimeLabel;
@property(nonatomic,strong)UILabel *overTimePageLabel;

@property(nonatomic,strong)UIButton *safeView;
@property(nonatomic,strong)UILabel *safeLabel;
@property(nonatomic,strong)UILabel *safePageLabel;
*/

-(void)configDataWithCardViewTag:(NSInteger)cardViewTag withCardInfoDic:(NSDictionary *)infoDic;
@end
