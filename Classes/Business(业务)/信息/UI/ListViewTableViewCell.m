//
//  ListViewTableViewCell.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ListViewTableViewCell.h"

#import "UIImageView+Create.h"

#import "ISSFMDB.h"

@implementation ListViewTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self ininView];
    }
    return self;
}
-(void)setFrame:(CGRect)frame{
    frame.origin.x += 15;
    frame.size.width -= 2*15;
    frame.origin.y +=10;
    frame.size.height -=10;
    [super setFrame:frame];
}
-(void)ininView{
    
    self.noReadFlag = [[UILabel alloc]initWithFrame:CGRectMake(-4,-4,10,10)];
    self.noReadFlag.backgroundColor = [UIColor redColor];
    self.noReadFlag.layer.cornerRadius = 5;
    self.noReadFlag.layer.masksToBounds = YES;
    //self.noReadFlag.hidden = YES;
    [self.contentView addSubview:self.noReadFlag];
    
    //    CAShapeLayer *mask = [CAShapeLayer new];
    //    mask.path = [UIBezierPath bezierPathWithOvalInRect:view.bounds].CGPath;
    //    view.layer.mask = mask;
    self.hiddenDangerImg = [[UIImageView alloc]initWithFrame:CGRectMake(5, 8, 30, 30)];
    self.hiddenDangerImg.image = [UIImage imageNamed:@"message_list_danger"];
    [self.contentView addSubview: self.hiddenDangerImg];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = LoadLanguagesKey(@"info_main_hiddenTroubleInfo");
    self.titleLabel.textColor = UIColorFromRGB(0x262626);
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@20);
        make.centerY.equalTo(self.hiddenDangerImg);
        make.left.equalTo(self.hiddenDangerImg.mas_right).mas_offset(0);
        make.width.mas_lessThanOrEqualTo(@140);
    }];
    
    
    self.hintImg = [[UIImageView alloc]init];
    self.hintImg.image = [UIImage imageNamed:@"message_list_remind"];
    [self.contentView addSubview:self.hintImg];
    [self.hintImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(8);
        make.centerY.equalTo(self.titleLabel);
        make.width.height.equalTo(@20);
    }];
    
    self.timelabel = [[UILabel alloc]init];
    self.timelabel.text = @"2018年4月10日 11:20";
    self.timelabel.textColor = UIColorFromRGB(0x616161);
    self.timelabel.font = [UIFont systemFontOfSize:13];
    self.timelabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.timelabel];
    self.timelabel.preferredMaxLayoutWidth = (200);
    [self.timelabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self.timelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.hintImg);
        make.height.equalTo(@20);
    }];
    
    self.contentImg = [UIImageView imageView:CGRectMake(CGRectGetMinX(self.hiddenDangerImg.frame) + 10, CGRectGetMaxY(self.hiddenDangerImg.frame) + 4, 100, 50) scale:YES];
    self.contentImg.layer.cornerRadius = 5;
    [self.contentView addSubview:self.contentImg];
    
    self.contentLabel = [UILabel mutLineLbWithX:CGRectGetMaxX(self.contentImg.frame) + 8 y:CGRectGetMinY(self.contentImg.frame) width:self.contentView.frame.size.width - 17 - (CGRectGetMaxX(self.contentImg.frame) + 8) fontSize:14 color:[UIColor describeColor_61] text:@""];
    [self.contentView addSubview:self.contentLabel];
    
    self.addressLabel = [UILabel oneLineLbWithX:CGRectGetMinX(self.contentImg.frame) y:CGRectGetMaxY(self.contentImg.frame) + 6 width:Screen_Width - 30 - 30 fontSize:12 color:[UIColor describeColor_9a] text:@""];
    [self.contentView addSubview:self.addressLabel];
    /*
    self.addressLabel = [[UILabel alloc]init];
    self.addressLabel.textColor = UIColorFromRGB(0x9a9a9a);
    self.addressLabel.font = [UIFont systemFontOfSize:12];
    self.addressLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.addressLabel];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentImg.mas_left);
        make.top.equalTo(self.contentImg.mas_bottom).offset(8);
        make.bottom.equalTo(self.contentView).offset(-8);
        make.height.equalTo(@16);
    }];
     */
}
//首页模块列表 + 首页模块搜索
- (void)configListDataWithModel:(MessageCellModel *)cellModel
{
    self.contentImg.hidden = NO;
    [self.contentLabel setLeft:CGRectGetMaxX(self.contentImg.frame) + 8];
    [self.contentLabel setWidth:Screen_Width - 30 - 15 - (CGRectGetMaxX(self.contentImg.frame) + 8)];
    if(cellModel.imgs == nil)
    {
        self.contentImg.hidden = YES;
        [self.contentLabel setLeft:CGRectGetMinX(self.hiddenDangerImg.frame) + 10];
        [self.contentLabel setWidth:Screen_Width - 30 - 15 - (CGRectGetMinX(self.hiddenDangerImg.frame) + 10)];
    }
    else
    {
        //取第一张图片
        NSArray *arr = [cellModel.imgs componentsSeparatedByString:@","];
        [self.contentImg  sd_setImageWithURL:[NSURL URLWithString:[ISSDataDeal getFirstImageUrl:arr]] placeholderImage:[UIImage imageNamed:@"placeHolder_image"]];
    }
    self.noReadFlag.hidden = YES;
    self.hintImg.hidden = YES;
    if([@"yh" isEqualToString:cellModel.type])
    {
        self.noReadFlag.hidden = ![[ISSFMDBUtil sharedInstance] isExistMsgById:cellModel.id type:@"1"];
        self.titleLabel.text = LoadLanguagesKey(@"info_main_hiddenTroubleInfo");
        self.hiddenDangerImg.image = [UIImage imageNamed:@"message_list_danger"];
        if([@"8a88fee1570c782301570ce90c23004f" isEqualToString:cellModel.yhjb])//ZDYH
        {
            self.hintImg.hidden = NO;
        }
    }
    else if ([@"wz" isEqualToString:cellModel.type])
    {
        self.noReadFlag.hidden = ![[ISSFMDBUtil sharedInstance] isExistMsgById:cellModel.id type:@"2"];
        self.titleLabel.text = LoadLanguagesKey(@"info_main_breakRuleInfo");
        self.hiddenDangerImg.image = [UIImage imageNamed:@"message_list_breakrule"];
    }
    else if ([@"aq" isEqualToString:cellModel.type])
    {
        self.noReadFlag.hidden = ![[ISSFMDBUtil sharedInstance] isExistMsgById:cellModel.id type:@"3"];
        self.titleLabel.text = LoadLanguagesKey(@"info_main_safeInfo");
        self.hiddenDangerImg.image = [UIImage imageNamed:@"message_list_safe"];
    }
    self.timelabel.text = [ISSDataDeal getDateStrFromResponseData:[cellModel.createTime longLongValue]];
    self.contentLabel.text = [NSString noNullString:cellModel.content];
    self.contentLabel.frame = [ISSPubfun getLimitLabelRect:self.contentLabel limitLine:2];
    self.addressLabel.text = [NSString noNullString:cellModel.location] ;
}
//隐患模块列表 + 隐患模块搜索
- (void)configListDataWithSafeModel:(IssSafeListModel *)cellModel
{
    [self configListDataWithSafeModel:cellModel type:cellModel.yhlxbm];
}

- (void)configListDataWithSafeModel:(IssSafeListModel *)cellModel type:(NSString *)type
{
    self.noReadFlag.hidden = YES;
    self.hintImg.hidden = YES;
    if([@"yh" isEqualToString:type])
    {
        self.noReadFlag.hidden = ![[ISSFMDBUtil sharedInstance] isExistMsgById:cellModel.id type:@"1"];
        self.titleLabel.text = LoadLanguagesKey(@"info_main_hiddenTroubleInfo");
        self.hiddenDangerImg.image = [UIImage imageNamed:@"message_list_danger"];
        if([@"8a88fee1570c782301570ce90c23004f" isEqualToString:cellModel.yhjb])//ZDYH
        {
            self.hintImg.hidden = NO;
        }
    }
    else if ([@"wz" isEqualToString:type])
    {
        self.noReadFlag.hidden = ![[ISSFMDBUtil sharedInstance] isExistMsgById:cellModel.id type:@"2"];
        self.titleLabel.text = LoadLanguagesKey(@"info_main_breakRuleInfo");
        self.hiddenDangerImg.image = [UIImage imageNamed:@"message_list_breakrule"];
    }
    
    self.contentImg.hidden = NO;
    [self.contentLabel setLeft:CGRectGetMaxX(self.contentImg.frame) + 8];
    [self.contentLabel setWidth:Screen_Width - 30 - 15 - (CGRectGetMaxX(self.contentImg.frame) + 8)];
    if(cellModel.tplj == nil)
    {
        self.contentImg.hidden = YES;
        [self.contentLabel setLeft:CGRectGetMinX(self.hiddenDangerImg.frame) + 10];
        [self.contentLabel setWidth:Screen_Width - 30 - 15 - (CGRectGetMinX(self.hiddenDangerImg.frame) + 10)];
    }
    else
    {
        //取第一张图片
        NSArray *arr = [cellModel.tplj componentsSeparatedByString:@","];
        [self.contentImg  sd_setImageWithURL:[NSURL URLWithString:[ISSDataDeal getFirstImageUrl:arr]] placeholderImage:[UIImage imageNamed:@"placeHolder_image"]];
    }
    self.timelabel.text = [ISSDataDeal getDateStrFromResponseData:[cellModel.fxrq longLongValue]];
    self.contentLabel.text = [NSString noNullString:cellModel.yhms];
    self.contentLabel.frame = [ISSPubfun getLimitLabelRect:self.contentLabel limitLine:2];
    self.addressLabel.text = [NSString noNullString:cellModel.yhdd];
}

@end

