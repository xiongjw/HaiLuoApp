//
//  ISSInfoDetailBreakView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSInfoDetailBreakView.h"

#import "ISSDataDicUtil.h"

@implementation ISSInfoDetailBreakView

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    if (self) {
        
        self.backgroundColor = ISSBackgroundColor;
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, Screen_Width - 20, 0)];
        contentView.backgroundColor = [UIColor whiteColor];
        contentView.layer.cornerRadius = 5;
        contentView.layer.masksToBounds = YES;
        [self addSubview:contentView];
        
        CGFloat allWidth = Screen_Width - 20;
        NSArray *dataList = @[
                              @{
                                  @"icon":@"men_photo",
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_findPeople"),@"value":[self getPersonName:data],@"value2":[NSString noNullString:data[@"gsmc"]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_findTime"),@"value":[ISSDataDeal getDateStrFromResponseData:[data[@"fxrq"] longLongValue]]}
                                          ]
                                  },
                              @{
                                  @"icon":@"message_list_breakrule",
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_registerTime"),@"value":[ISSDataDeal getDateStrFromResponseData:[data[@"cjsj"] longLongValue]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_breakCompany" ),@"value":[NSString noNullString:data[@"sjdwmc"]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_breakPlace"),@"value":[NSString noNullString:data[@"yhdd"]]}
                                          ]
                                  },
                              @{
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_breakClass"),@"value":[[ISSDataDicUtil sharedInstance] getNameByCode:[NSString noNullString:data[@"yhlx"]] type:2]}
                                          ]
                                  },
                              @{
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"add_breakRule_description"),@"value":[NSString noNullString:data[@"yhms"]]}
                                          ]
                                  }
                              ];
        
        UIImageView *icon = nil;
        UILabel *titleLb = nil;
        UILabel *valueLb = nil;
        UIView *lineView = nil;
        
        NSDictionary *itemData = nil;
        NSArray *rowList = nil;
        
        CGFloat posY = 0;
        
        NSString *language = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
        bool isEn = [language hasPrefix:@"en"];
        CGFloat titleWidth = isEn ? 100 : 80;
        CGFloat titleHeight = isEn ? 40 : 34;
        
        for (int i = 0; i < dataList.count; i++)
        {
            itemData = dataList[i];
            rowList = itemData[@"list"];
            if (itemData[@"icon"])
            {
                if (i == 1) icon = [[UIImageView alloc] initWithFrame:CGRectMake(15-(titleHeight - 24)/2, posY, titleHeight, titleHeight)];
                else icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, posY + (titleHeight - 24)/2, 24, 24)];
                
                icon.image = [UIImage imageNamed:itemData[@"icon"]];
                [contentView addSubview:icon];
            }
            
            for (int j = 0; j < rowList.count; j++)
            {
                titleLb = [[UILabel alloc] initWithFrame:CGRectMake(60, posY, titleWidth, titleHeight)];
                titleLb.textColor = [UIColor titleColor_26];
                titleLb.font = [UIFont systemFontOfSize:13];
                titleLb.numberOfLines = 2;
                titleLb.text = rowList[j][@"title"];
                if (i > 0) {
                    titleLb.font = [UIFont boldSystemFontOfSize:13];
                }
                [contentView addSubview:titleLb];
                
                if (i == 0 && j == 0)
                {
                    // 1行2个value的模式
                    valueLb = [UILabel oneLineLbWithX:CGRectGetMaxX(titleLb.frame) y:0 fontSize:13 color:[UIColor describeColor_9a] text:rowList[j][@"value"]];
                    [valueLb setCenterY:titleLb.centerY];
                    [contentView addSubview:valueLb];
                    
                    UILabel *otherLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(valueLb.frame) + 15, posY, allWidth - 10 - (CGRectGetMaxX(valueLb.frame) + 15), titleHeight)];
                    otherLb.textColor = [UIColor describeColor_9a];
                    otherLb.font = [UIFont systemFontOfSize:13];
                    otherLb.numberOfLines = 2;
                    otherLb.text = rowList[j][@"value2"];
                    otherLb.textAlignment = NSTextAlignmentRight;
                    [contentView addSubview:otherLb];
                    
                    posY = CGRectGetMaxY(valueLb.frame);
                }
                // 违章描述
                else if (i == 3)
                {
                    valueLb = [UILabel mutLineLbWithX:CGRectGetMinX(titleLb.frame) y:CGRectGetMaxY(titleLb.frame) width:allWidth - 10 - CGRectGetMinX(titleLb.frame) fontSize:13 color:[UIColor describeColor_9a] text:rowList[j][@"value"]];
                    [contentView addSubview:valueLb];
                    
                    posY = CGRectGetMaxY(valueLb.frame) + 15;
                }
                else
                {
                    // 1行1个value的模式
                    valueLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLb.frame), posY, allWidth - 10 - CGRectGetMaxX(titleLb.frame), titleHeight)];
                    valueLb.textColor = [UIColor describeColor_9a];
                    valueLb.font = [UIFont systemFontOfSize:13];
                    valueLb.numberOfLines = 2;
                    valueLb.text = rowList[j][@"value"];
                    [contentView addSubview:valueLb];
                    
                    posY = CGRectGetMaxY(valueLb.frame);
                }
                
                if (i < dataList.count - 1 && j == rowList.count - 1)
                {
                    lineView = [[UIView alloc] initWithFrame:CGRectMake(15, posY, allWidth - 30, 1)];
                    lineView.backgroundColor = [UIColor sepLineColor];
                    [contentView addSubview:lineView];
                    posY = CGRectGetMaxY(lineView.frame);
                }
            }
            
        }
        
        [contentView setHeight:posY];
        [self setHeight:CGRectGetMaxY(contentView.frame) + 10];
    }
    return self;
}

- (NSString *)getPersonName:(NSDictionary *)data
{
    NSInteger nm = [[NSString noNullString:data[@"nm"]] integerValue];
    if (nm == 1) return @"匿名";
    else return [NSString noNullString:data[@"fxrmc"]];
}

@end
