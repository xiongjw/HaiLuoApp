//
//  ISSInfoDetailSafeView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSInfoDetailSafeView.h"

@implementation ISSInfoDetailSafeView

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    if (self) {
        
        self.backgroundColor = ISSBackgroundColor;
        
        UILabel *titleLb = [UILabel oneLineLbWithX:0 y:15 width:Screen_Width fontSize:16 color:[UIColor titleColor_26] text:[NSString noNullString:data[@"title"]]];
        titleLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLb];
        
        UILabel *dateLb = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(titleLb.frame) + 10 width:Screen_Width fontSize:16 color:[UIColor titleColor_26] text:[ISSDataDeal getDateStrFromResponseData:[data[@"createTime"] longLongValue]]];
        dateLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:dateLb];
        
        UIView *descView = [[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(dateLb.frame) + 15, Screen_Width - 10, 0)];
        descView.layer.cornerRadius = 5;
        descView.layer.masksToBounds = YES;
        descView.backgroundColor = [UIColor whiteColor];
        [self addSubview:descView];
        
        UILabel *descLb = [UILabel mutLineLbWithX:10 y:10 width:CGRectGetWidth(descView.frame) - 20 fontSize:12 color:[UIColor titleColor_26] text:@""];
        [descView addSubview:descLb];
        
        descLb.attributedText = [self attributedStringWithHTMLString:[NSString noNullString:data[@"content"]]];
        descLb.frame = [ISSPubfun getLabelRect:descLb];
        
        [descView setHeight:CGRectGetMaxY(descLb.frame) + 10];
        
        [self setHeight:CGRectGetMaxY(descView.frame) + 10];
    }
    return self;
}

- (NSAttributedString *)attributedStringWithHTMLString:(NSString *)htmlString
{
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
    return [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
}

@end
