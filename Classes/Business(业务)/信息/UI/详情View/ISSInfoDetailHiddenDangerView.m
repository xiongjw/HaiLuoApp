//
//  ISSInfoDetailHiddenDangerView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSInfoDetailHiddenDangerView.h"

#import "ISSDataDicUtil.h"

@implementation ISSInfoDetailHiddenDangerView

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    if (self) {
        
        self.backgroundColor = ISSBackgroundColor;
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, Screen_Width - 20, 0)];
        contentView.backgroundColor = [UIColor whiteColor];
        contentView.layer.cornerRadius = 5;
        contentView.layer.masksToBounds = YES;
        [self addSubview:contentView];
        
        CGFloat allWidth = Screen_Width - 20;
        NSArray *dataList = @[
                              @{
                                  @"icon":@"men_photo",
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_findPeople"),@"value":[NSString noNullString:data[@"fxrmc"]],@"value2":[NSString noNullString:data[@"fxrCompany"]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_findTime"),@"value":[ISSDataDeal getDateStrFromResponseData:[data[@"fxsj"] longLongValue]]}
                                          ]
                                  },
                              @{
                                  @"icon":@"message_list_danger",
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerCompany"),@"value":[NSString noNullString:data[@"findByUnit"]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerPlace"),@"value":[NSString noNullString:data[@"dangerSite"]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerPart"),@"value":[NSString noNullString:data[@"dangerPart"]]}
                                          ]
                                  },
                              @{
                                  @"icon":@"message_list_remind_big",
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerRank"),@"value":[[ISSDataDicUtil sharedInstance] getYHJB:[NSString noNullString:data[@"dangerLevel"]]]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerClass"),@"value":[[ISSDataDicUtil sharedInstance] getNameByCode:[NSString noNullString:data[@"dangerType"]] type:1]},
                                          @{@"title":LoadLanguagesKey(@"info_detail_timelimit"),@"value":[self getZGQX:data]}
                                          ]
                                  },
                              @{
                                  @"icon":[self getIsChangeIcon:data],//要判断
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_isChange"),@"value":[self getIsChangeText:data]}
                                          ]
                                  },
                              @{
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerDescribe"),@"value":[NSString noNullString:data[@"yhms"]]}
                                          ]
                                  },
                              @{
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_dangerSource"),@"value":[[ISSDataDicUtil sharedInstance] getNameByCode:[NSString noNullString:data[@"yhly"]] type:0]}
                                          ]
                                  },
                              @{
                                  @"list":@[
                                          @{@"title":LoadLanguagesKey(@"info_detail_changePhoto_yh"),@"value":@""}
                                          ]
                                  },
                              ];
        
        UIImageView *icon = nil;
        UILabel *titleLb = nil;
        UILabel *valueLb = nil;
        UIView *lineView = nil;
        
        NSDictionary *itemData = nil;
        NSArray *rowList = nil;
        
        CGFloat posY = 0;
        
        NSString *language = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
        bool isEn = [language hasPrefix:@"en"];
        CGFloat titleWidth = isEn ? 100 : 80;
        CGFloat titleHeight = isEn ? 40 : 34;
        
        for (int i = 0; i < dataList.count; i++)
        {
            itemData = dataList[i];
            rowList = itemData[@"list"];
            if (itemData[@"icon"])
            {
                if (i == 1) icon = [[UIImageView alloc] initWithFrame:CGRectMake(15-(titleHeight - 24)/2, posY, titleHeight, titleHeight)];
                else icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, posY + (titleHeight - 24)/2, 24, 24)];
                
                icon.image = [UIImage imageNamed:itemData[@"icon"]];
                [contentView addSubview:icon];
            }
            
            for (int j = 0; j < rowList.count; j++)
            {
                titleLb = [[UILabel alloc] initWithFrame:CGRectMake(60, posY, titleWidth, titleHeight)];
                titleLb.textColor = [UIColor titleColor_26];
                titleLb.font = [UIFont systemFontOfSize:13];
                titleLb.numberOfLines = 2;
                titleLb.text = rowList[j][@"title"];
                if (i > 0) {
                    titleLb.font = [UIFont boldSystemFontOfSize:13];
                }
                [contentView addSubview:titleLb];
                
                if (i == 0 && j == 0)
                {
                    // 1行2个value的模式
                    valueLb = [UILabel oneLineLbWithX:CGRectGetMaxX(titleLb.frame) y:0 fontSize:13 color:[UIColor describeColor_9a] text:rowList[j][@"value"]];
                    [valueLb setCenterY:titleLb.centerY];
                    [contentView addSubview:valueLb];
                    
                    UILabel *otherLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(valueLb.frame) + 15, posY, allWidth - 10 - (CGRectGetMaxX(valueLb.frame) + 15), titleHeight)];
                    otherLb.textColor = [UIColor describeColor_9a];
                    otherLb.font = [UIFont systemFontOfSize:13];
                    otherLb.numberOfLines = 2;
                    otherLb.text = rowList[j][@"value2"];
                    otherLb.textAlignment = NSTextAlignmentRight;
                    [contentView addSubview:otherLb];
                    
                    posY = CGRectGetMaxY(valueLb.frame);
                }
                // 隐患描述
                else if (i == 4)
                {
                    valueLb = [UILabel mutLineLbWithX:CGRectGetMinX(titleLb.frame) y:CGRectGetMaxY(titleLb.frame) width:allWidth - 10 - CGRectGetMinX(titleLb.frame) fontSize:13 color:[UIColor describeColor_9a] text:rowList[j][@"value"]];
                    [contentView addSubview:valueLb];
                    
                    posY = CGRectGetMaxY(valueLb.frame) + 15;
                }
                else
                {
                    // 1行1个value的模式
                    valueLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLb.frame), posY, allWidth - 10 - CGRectGetMaxX(titleLb.frame), titleHeight)];
                    valueLb.textColor = [UIColor describeColor_9a];
                    valueLb.font = [UIFont systemFontOfSize:13];
                    valueLb.numberOfLines = 2;
                    valueLb.text = rowList[j][@"value"];
                    [contentView addSubview:valueLb];
                    
                    posY = CGRectGetMaxY(valueLb.frame);
                }
                
                if (i < dataList.count - 1 && j == rowList.count - 1)
                {
                    lineView = [[UIView alloc] initWithFrame:CGRectMake(15, posY, allWidth - 30, 1)];
                    lineView.backgroundColor = [UIColor sepLineColor];
                    [contentView addSubview:lineView];
                    
                    posY = CGRectGetMaxY(lineView.frame);
                }
            }
            
        }
        
        [contentView setHeight:posY];
        [self setHeight:CGRectGetMaxY(contentView.frame) + 10];
    }
    return self;
}

// 整改期限
- (NSString *)getZGQX:(NSDictionary *)data
{
    NSString *zgqx = [NSString noNullString:data[@"zgqx"]];
    if (zgqx.length > 0) {
        return  [ISSDataDeal getDateStrFromResponseData:[zgqx longLongValue] format:@"yyyy-MM-dd"];
    }
    return @"";
}

- (NSString *)getIsChangeIcon:(NSDictionary *)data
{
    NSString *yhzgzt = [NSString noNullString:data[@"yhzgzt"]];
    if (yhzgzt.length == 0)
    {
        return @"info_detail_notChange";
    }
    else
    {
        if ([@"0" isEqualToString:yhzgzt])
        {
            return @"info_detail_notChange";
        }
        else if ([@"1" isEqualToString:yhzgzt])
        {
            return @"info_detail_notChange";
        }
        else
        {
            return @"job_detail_scansuccess";
        }
        
    }
}

- (NSString *)getIsChangeText:(NSDictionary *)data
{
    NSString *yhzgzt = [NSString noNullString:data[@"yhzgzt"]];
    if (yhzgzt.length == 0)
    {
        return LoadLanguagesKey(@"info_detail_changeState_undo");
    }
    else
    {
        if ([@"0" isEqualToString:yhzgzt])
        {
            return LoadLanguagesKey(@"info_detail_changeState_undo");
        }
        else if ([@"1" isEqualToString:yhzgzt])
        {
            return LoadLanguagesKey(@"info_detail_changeState_doing");
        }
        else
        {
            return LoadLanguagesKey(@"info_detail_changeState_done");
        }
        
    }
}

@end
