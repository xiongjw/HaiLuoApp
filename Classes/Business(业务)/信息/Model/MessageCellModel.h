//
//  MessageCellModel.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageCellModel : ISSBaseModel
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *location;
@property(nonatomic,copy)NSString *createTime;
@property(nonatomic,copy)NSString *imgs;
@property(nonatomic,copy)NSString *mId;
@property(nonatomic,copy)NSString *yhjb;
@end
