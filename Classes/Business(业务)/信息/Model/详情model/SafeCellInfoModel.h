//
//  SafeCellInfoModel.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/27.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SafeCellInfoModel : ISSBaseModel
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *createId;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *createTime;
@end
