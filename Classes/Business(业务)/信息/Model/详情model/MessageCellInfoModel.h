//
//  MessageCellInfoModel.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageCellInfoModel : ISSBaseModel
@property(nonatomic,copy)NSString *id;  //主键
@property(nonatomic,copy)NSString *gsId;//公司id
@property(nonatomic,copy)NSString *gsmc;//公司名字
@property(nonatomic,copy)NSString *jcdwId;//检查单位编号
@property(nonatomic,copy)NSString *jcdwmc;//检查单位名称
@property(nonatomic,copy)NSString *fxrmc;//发现人名称
@property(nonatomic,copy)NSString *sjdwId;//受检单位编号
@property(nonatomic,copy)NSString *sjdwmc;//受检单位名称
@property(nonatomic,copy)NSString *fxrq;//发现日期
@property(nonatomic,copy)NSString *yhly;//隐患来源
@property(nonatomic,copy)NSString *cjdwId;//创建单位Id
@property(nonatomic,copy)NSString *cjdwmc;//创建单位名称
@property(nonatomic,copy)NSString *cjsj;//创建时间
@property(nonatomic,copy)NSString *cjrId;//创建人Id
@property(nonatomic,copy)NSString *cjrmc;//创建人名称
@property(nonatomic,copy)NSString *xgsj;//修改时间
@property(nonatomic,copy)NSString *xgrId;//修改人Id
@property(nonatomic,copy)NSString *xgrmc;//修改人名称
@property(nonatomic,copy)NSString *yhms;//隐患描述
@property(nonatomic,copy)NSString *yhlx;//隐患类型ID
@property(nonatomic,copy)NSString *yhlbb;//隐患类别Id
@property(nonatomic,copy)NSString *yhjb;//隐患级别Id
@property(nonatomic,copy)NSString *knzchg;//可能造成后果ID
@property(nonatomic,copy)NSString *yhdd;//隐患地点
@property(nonatomic,copy)NSString *yhbw;//隐患部位
@property(nonatomic,copy)NSString *sfxczg;//是否现场整改(0:否，1：是)
@property(nonatomic,copy)NSString *jlje;//建立金额;
@property(nonatomic,copy)NSString *zgyjlx;//整改意见类型Id
@property(nonatomic,copy)NSString *zgyjms;//整改意见描述
@property(nonatomic,copy)NSString *yhbh;//隐患编号
@property(nonatomic,copy)NSString *fbzt;//发布状态（0:草稿，1：正式）
@property(nonatomic,copy)NSString *yhzt;//隐患状态
@property(nonatomic,copy)NSString *sprId;//审批人Id
@property(nonatomic,copy)NSString *sprmc;//审批人名称
@property(nonatomic,copy)NSString *spsj;//审批时间
@property(nonatomic,copy)NSString *spzt;//审批状态
@property(nonatomic,copy)NSString *isChoose;
@property(nonatomic,copy)NSString *spjs;
@property(nonatomic,copy)NSString *sjbmId;
@property(nonatomic,copy)NSString *nm;
@property(nonatomic,copy)NSString *tplj;
@property(nonatomic,copy)NSString *ts;
@property(nonatomic,copy)NSString *isPush;

@end
