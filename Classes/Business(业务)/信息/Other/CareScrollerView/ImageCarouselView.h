//
//  ImageCarouselView.h
//  CardScrollerViewDemo
//
//  Created by issuser on 2018/4/9.
//  Copyright © 2018年 issuser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarouselSubview.h"

@protocol ImageCarouselViewDataSource;
@protocol ImageCarouselViewDelegate;

@interface ImageCarouselView : UIView

/**
 图片数组
 */
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, assign) id <ImageCarouselViewDataSource> dataSource;
@property (nonatomic, assign) id <ImageCarouselViewDelegate>   delegate;
/**
 当前显示页的index
 */
@property (nonatomic, assign) NSInteger currentPageIndex;
/**
 内部scrollView
 */
@property (nonatomic, strong) UIScrollView *scrollView;

@property(nonatomic,assign)NSInteger clickPageIndex;
- (instancetype)initWithFrame:(CGRect)frame withDataSource:(id<ImageCarouselViewDataSource>)dataSource withDelegate:(id<ImageCarouselViewDelegate>)delegate;

/**
 *  刷新视图
 */
- (void)reloadData;

@end


@protocol  ImageCarouselViewDelegate <NSObject>

- (CGSize)sizeForPageInCarouselView:(ImageCarouselView *)carouselView;

@optional

- (void)carouselView:(ImageCarouselView *)carouselView didScrollToPage:(NSInteger)pageNumber;

- (void)carouselView:(ImageCarouselView *)carouselView didSelectPageAtIndex:(NSInteger)index;

//- (void)carouselView:(ImageCarouselView *)carouselView didSelectPageAtIndex:(NSInteger)index location:(CGPoint)location;

@end


@protocol ImageCarouselViewDataSource <NSObject>

- (NSInteger)numberOfPagesInCarouselView:(ImageCarouselView *)carouselView;

- (CarouselCell *)carouselView:(ImageCarouselView *)carouselView cellForPageAtIndex:(NSUInteger)index;

@end

