//
//  CarouselCellInfo.m
//  CardScrollerViewDemo
//
//  Created by issuser on 2018/4/9.
//  Copyright © 2018年 issuser. All rights reserved.
//
#import "CarouselCellInfo.h"

@implementation CarouselCellInfo

+ (instancetype)cellInfoWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

@end
