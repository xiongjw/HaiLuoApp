//
//  CarouselSubview.h
//  CardScrollerViewDemo
//
//  Created by issuser on 2018/4/9.
//  Copyright © 2018年 issuser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarouselSubview : UIView

/**
 *  容器视图
 */
@property (nonatomic, strong) UIView *containerView;

@end

// cell基类
@interface CarouselCell : UIView

@property (nonatomic, assign) NSUInteger showTime;

@end

// image的cell派生类
@interface CarouselCellImageView : CarouselCell

@property (nonatomic, strong) UIImageView *imageView;
-(void)configDataWithView:(UIView *)infoView;
@end
