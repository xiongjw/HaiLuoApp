//
//  CarouselCellInfo.h
//  CardScrollerViewDemo
//
//  Created by issuser on 2018/4/9.
//  Copyright © 2018年 issuser. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarouselCellInfo : NSObject

/**
 页面的滚动间隔时间
 */
@property (nonatomic, assign) NSUInteger showTime;

/**
 图片名称
 */
@property (nonatomic, strong) NSString *imageName;

+ (instancetype)cellInfoWithDict:(NSDictionary *)dict;

@end
