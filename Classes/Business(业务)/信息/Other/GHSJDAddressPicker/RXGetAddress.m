//
//  RXGetAddress.m
//  JDAddress
//
//  Created by srx on 16/8/24.
//  Copyright © 2016年 https://github.com/srxboys. All rights reserved.
//

#import "RXGetAddress.h"

@implementation RXGetAddress
//获取本地地址
+ (NSArray *)getLocalAreaArray {
    NSString* path = [[NSBundle mainBundle] pathForResource:@"city" ofType:@"json"];
    if(path.length <= 0) {
        return nil;
    }
    
    NSData* data = [NSData dataWithContentsOfFile:path];
    id JsonObject =
    [NSJSONSerialization JSONObjectWithData:data
                                    options:NSJSONReadingAllowFragments
                                      error:nil];
    
    NSArray * arr = [[NSArray arrayWithObject:JsonObject] objectAtIndex:0];
    NSMutableArray * _allCitiesArr = [[NSMutableArray alloc] initWithArray:arr];
    
    for(NSInteger i = 0; i < _allCitiesArr.count; i++) {
        NSString * code = _allCitiesArr[i][@"code"];
        if([self screenmaskArea:code]) {
            [_allCitiesArr removeObjectAtIndex:i];
        }
    }
    
    
    return _allCitiesArr;
}

///屏蔽 哪些 省份
+ (BOOL)screenmaskArea:(NSString *)nameCode {
    //青海省 @"630000"
    //西藏自治区 @"540000"
    //海南区 @"150303"
    NSArray * screenMaskArr = @[];
    
    if([screenMaskArr containsObject:nameCode]) {
        return YES;
    }
    return NO;
}


@end
