//
//  CardInfoViewController.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardInfoViewController : ISSMJRefreshTableVC

@property (nonatomic,assign) NSInteger lastIndex;
@property (nonatomic,strong) NSDictionary *infoDic;

@end
