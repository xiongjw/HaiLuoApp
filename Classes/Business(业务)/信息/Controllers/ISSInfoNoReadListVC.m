//
//  ISSInfoNoReadListVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/23.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSInfoNoReadListVC.h"

#import "ISSFMDB.h"

#import "ListViewTableViewCell.h"
#import "ListCellViewController.h"

@interface ISSInfoNoReadListVC ()

@end

@implementation ISSInfoNoReadListVC

- (NSString *)getType
{
    NSDictionary *data = @{@"1":@"yh",@"2":@"wz",@"3":@"aq"};
    return data[self.type];
}
//数据请求
-(void)requestDataReresh:(NSString *)direction
{
    NSArray *ids = [[ISSFMDBUtil sharedInstance] queryTypeIds:self.type startIndex:self.resultDataList.count];
    NSLog(@"ids:%@",ids);
    
    NSDictionary *data = @{@"type":[self getType],@"idList":ids};
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"info/getUnReadInfoList" param:data view:nil successBlock:^(NSDictionary *resultData) {
        
        NSArray *list = [MessageCellModel arrayOfModelsFromDictionaries:resultData[@"data"][@"List"] error:nil] ;
        [weakself addResultDataToList:list direction:direction];
        
    } failureBlock:^(NSDictionary *resultData) {
        [weakself endRefreshing];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *navTitle = @"";
    if ([@"1" isEqualToString:self.type]) navTitle = LoadLanguagesKey(@"info_main_hiddenTroubleInfo");
    else if ([@"2" isEqualToString:self.type]) navTitle = LoadLanguagesKey(@"info_main_breakRuleInfo");
    else if ([@"3" isEqualToString:self.type]) navTitle = LoadLanguagesKey(@"info_main_safeInfo");
    
    [self setTitle:FormatString(@"%@ %@",LoadLanguagesKey(@"info_card_notRead"),navTitle) showBackBtn:YES];
    
    [self addHeaderAndFooterAction];
    [self performSelector:@selector(headerBeginRefreshing) withObject:nil afterDelay:.3];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView的代理
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListViewTableViewCell *listInfoCell = [tableView dequeueReusableCellWithIdentifier:@"ListCell"];
    if(listInfoCell==nil){
        
        listInfoCell = [[ListViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ListCell"];
        listInfoCell.layer.cornerRadius = 8;
    }
    MessageCellModel *model = self.resultDataList[indexPath.row];
    [listInfoCell configListDataWithModel:model];
    
    return listInfoCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 判断有没有查看权限
    if (![ISSUserData sharedInstance].operation_privilege_yh_detailView) {
        [self.view makeToast:LoadLanguagesKey(@"operation_privilege_yh_detailView")];
        return;
    }
    MessageCellModel *model = self.resultDataList[indexPath.row];
    
    ListViewTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell.noReadFlag.hidden) {
        
        
        BOOL isDelete = [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:self.type];
        if (isDelete)
        {
            cell.noReadFlag.hidden = YES;
            // 干掉这一行
            //[self removeRowOfIndexPath:indexPath];
            [self performSelector:@selector(removeRowOfIndexPath:) withObject:indexPath afterDelay:.3];
            // 让其它页面也刷新
            [[NSNotificationCenter defaultCenter] postNotificationName:@"readPushMsgSuccessNote" object:nil];
        }
    }
    
    ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
    listCellVC.hidesBottomBarWhenPushed = YES;
    listCellVC.itemId = model.id;
    listCellVC.comeType = 100;
    listCellVC.itemType = model.type;
    [self.navigationController pushViewController:listCellVC animated:YES];
    
}

- (void)removeRowOfIndexPath:(NSIndexPath *)indexPath
{
    [self.resultDataList removeObjectAtIndex:indexPath.row];
    [self.mTableView reloadData];
}

@end
