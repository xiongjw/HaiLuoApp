//
//  ISSInfoMainVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSInfoMainVC.h"
#import "ISSInfoTopView.h"
#import "ListViewTableViewCell.h"
#import "CardInfoViewController.h"
#import "ListCellViewController.h"
#import "RXJDAddressPickerView.h"
#import "UIView+LPWView.h"
#import "ISSBaseSearchVC.h"
#import "ISSSearchResultListVC.h"
#import <CoreLocation/CoreLocation.h>
#import "MessageCellModel.h"

#import "ISSDataDicUtil.h"
#import "ISSFMDB.h"

@interface ISSInfoMainVC ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>
{
    
    CGFloat topHeight;
    
    int selectTime;
    int messageItemBadge ;
    NSDictionary * totalDic;
}
@property(nonatomic , strong) ISSInfoTopView *infoTopView;
@property(nonatomic,strong)UITableView *listTable;
@property(nonatomic,strong)NSMutableArray *listDataArr;
@property(nonatomic,strong)NSMutableArray *topListArr;
//定位
@property(nonatomic,strong)RXJDAddressPickerView *addressPickerView;
@property (nonatomic,strong ) CLLocationManager *locationManager;//定位服务
@property (nonatomic,copy)    NSString *currentCity;//城市
@property (nonatomic,copy)    NSString * areaString;
@end

@implementation ISSInfoMainVC

- (void)insertPushMsgSuccess
{
    [self.infoTopView configNoReadData:YES];
    [self.listTable reloadData];
}

- (void)readPushMsgSuccess
{
    [self.infoTopView configNoReadData:YES];
    [self.listTable reloadData];
}

- (void)queryPushMsgSuccess
{
    [self.infoTopView configNoReadData:NO];
    
    [self.listTable reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listDataArr = [NSMutableArray new];
    totalDic = [[NSDictionary alloc]init];
    topHeight = 160;
    selectTime = 0;
    [self createUI];
    [self locatemap];
    [self createButtonClick];
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addDanger:) name:@"addDangerMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addBreak:) name:@"addBreakMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(insertPushMsgSuccess) name:@"insertPushMsgSuccessNote" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(readPushMsgSuccess) name:@"readPushMsgSuccessNote" object:nil];
    
    //  查询完毕，不然来不及显示
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(queryPushMsgSuccess) name:@"queryPushMsgSuccessNote" object:nil];
}
- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        // 查询数据字典，方便详情调用
        [[ISSDataDicUtil sharedInstance] searchParam];
    });
}

//通知
- (void)addDanger:(NSNotification *)noti{
    
    [self requestDataDownReresh];
}
- (void)addBreak:(NSNotification *)noti{
    
    [self requestDataDownReresh];
}
- (void)createUI{
    
    [self setTitle:LoadLanguagesKey(@"info_main_title") showBackBtn:NO];
    WeakSelf(self)
    self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItem:2 clickBlock:^{
        [weakself searchAction];
    }];
    
    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
    topView.backgroundColor = [UIColor themeColor];
    [self.view addSubview:topView];
    
    self.infoTopView = [[ISSInfoTopView alloc]init];
    self.infoTopView.backgroundColor = [UIColor whiteColor];
    self.infoTopView.layer.cornerRadius = 10;
    [self.view addSubview:self.infoTopView];
    [self.infoTopView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).offset(15);
        make.top.equalTo(self.view.mas_top).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.height.equalTo(@260);
    }];
  
    [self.view addSubview:self.listTable];
    [self.listTable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.infoTopView.mas_bottom).offset(10);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(-5);
    }];
    
    self.infoTopView.dangerMessageButton.tag = 1000;
    self.infoTopView.braekMessageButton.tag = 1001;
    self.infoTopView.safeMessageButton.tag = 1002;
    
    self.infoTopView.dangerTotalPag.hidden = NO;
    self.infoTopView.noChangePag.hidden = NO;
    self.infoTopView.noChangeLabel.hidden = NO;
    
    self.infoTopView.breakTotalPag.hidden = NO;
    
    self.infoTopView.safeTotalPag.hidden = NO;

    self.addressPickerView = [[RXJDAddressPickerView alloc]init];
    UIWindow *window =[[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self.addressPickerView];
    
    self.infoTopView.locationBtn.userInteractionEnabled = NO;
    [self.infoTopView.locationBtn addTarget:self action:@selector(locationChoose:) forControlEvents:UIControlEventTouchUpInside];
    //tableView的上拉和下拉
    self.listTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
    self.listTable.mj_footer= [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    self.listTable.mj_footer.hidden = YES;
    [self.listTable.mj_header beginRefreshing];
    
    // 放在查询完毕再弄
    //[self.infoTopView configNoReadData:NO];
    
}
//下拉和上拉
- (void)headerRereshing
{
    [self performSelector:@selector(requestDataDownReresh) withObject:@"down"];
}

- (void)footerRereshing {
    [self performSelector:@selector(requestDataUpReresh) withObject:@"up"];
}
//数据请求
- (void)requestDataDownReresh
{
    WeakSelf(self)
    NSDictionary *data = @{@"type":@"all",
                           @"lastId":@"",
                           };
    
    [X_HttpUtil apiRequest:@"info/getSpwsYHs" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSArray *arr =[MessageCellModel arrayOfModelsFromDictionaries:resultData[@"data"][@"list"] error:nil] ;
        
        totalDic = resultData[@"data"][@"totle"];
        [weakself.infoTopView configTopConfigWithData:totalDic];
        if (weakself.listTable.mj_footer.hidden) {
            weakself.listTable.mj_footer.hidden = NO;
            [weakself.listTable.mj_footer resetNoMoreData];
        }
        [_listDataArr removeAllObjects];
        
        if (arr.count < [PageSize integerValue])
        {   weakself.listTable.mj_footer.hidden = YES;
            [weakself.listTable.mj_footer endRefreshingWithNoMoreData];
        }
        if (arr.count > 0)
        {
            [_listDataArr addObjectsFromArray:arr];
        }
        [weakself.listTable reloadData];
         [weakself.listTable.mj_header endRefreshing];
    }failureBlock:^(NSDictionary *resultData) {
           [weakself.listTable.mj_header endRefreshing];
    }];
   
}

-(void)requestDataUpReresh{
    
    MessageCellModel *model = [_listDataArr lastObject];
    WeakSelf(self)
    NSDictionary *data = @{@"type":@"all",
                           @"lastId":model.mId
                           };
    
    [X_HttpUtil apiRequest:@"info/getSpwsYHs" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        [weakself.listTable.mj_footer endRefreshing];
        NSArray *arr =[MessageCellModel arrayOfModelsFromDictionaries:resultData[@"data"][@"list"] error:nil] ;
        
        totalDic = resultData[@"data"][@"totle"];
        [weakself.infoTopView configTopConfigWithData:totalDic];
        if (weakself.listTable.mj_footer.hidden) {
            weakself.listTable.mj_footer.hidden = NO;
            [weakself.listTable.mj_footer resetNoMoreData];
        }
        
        if (arr.count < [PageSize integerValue])
        {   weakself.listTable.mj_footer.hidden = YES;
            [weakself.listTable.mj_footer endRefreshingWithNoMoreData];
        }
        if (arr.count > 0)
        {
            [_listDataArr addObjectsFromArray:arr];
        }
        [weakself.listTable reloadData];
    }failureBlock:^(NSDictionary *resultData) {
        [weakself.listTable.mj_footer endRefreshing];
    }];
}
-(void)createButtonClick{
    
    
    [self.infoTopView.changeButton addTarget:self action:@selector(changSize:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.infoTopView.dangerMessageButton addTarget:self action:@selector(dangerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.infoTopView.braekMessageButton addTarget:self action:@selector(braekBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.infoTopView.safeMessageButton addTarget:self action:@selector(safeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - 搜索

- (void)pushSearchResultListVC:(NSString *)searchText searchItem:(NSDictionary *)searchItem
{
    ISSSearchResultListVC *vc = [[ISSSearchResultListVC alloc] init];
    vc.searchType = ISSSearch_Info;
    vc.searchText = searchText;
    vc.searchItem = searchItem;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchAction
{
    WeakSelf(self)
    ISSBaseSearchVC *vc = [[ISSBaseSearchVC alloc] init];
    vc.cateList = @[
                    @{@"name":LoadLanguagesKey(@"safe_main_all"),@"code":@"all",@"cacheKey":@"Message_searchAllInfo"},
                    @{@"name":LoadLanguagesKey(@"info_main_hiddenTroubleInfo"),@"code":@"yh",@"cacheKey":@"Message_searchDangerInfo"},
                    @{@"name":LoadLanguagesKey(@"info_main_breakRuleInfo"),@"code":@"wz",@"cacheKey":@"Message_searchBreakInfo"},
                    @{@"name":LoadLanguagesKey(@"info_main_safeInfo"),@"code":@"aq",@"cacheKey":@"Message_searchSafeInfo"}
                    ];
    vc.clickBlock = ^(NSDictionary *cateItem, NSString *searchText) {
        NSLog(@"cateItem:%@,searchText:%@",cateItem,searchText);
        [weakself pushSearchResultListVC:searchText searchItem:cateItem];
    };

    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
    [self presentViewController:nav animated:NO completion:NULL];
}
#pragma mark - button的点击
-(void)changSize:(UIButton*)btn{
    selectTime++;
    
    if(selectTime%2==1){
        
        btn.selected = YES;
    }else{
        
        btn.selected = NO;
    }
    self.infoTopView.changeButton.selected = btn.selected;
    if(btn.selected){
        [self.infoTopView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@160);
        }];
        [self.infoTopView layoutIfNeeded];
        
        //还原button
        [self.infoTopView.dangerMessageButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@30);
        }];
        self.infoTopView.dangerMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0,0, 0);
        [self.infoTopView.dangerMessageButton layoutIfNeeded];
        
        [self.infoTopView.braekMessageButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@30);
        }];
        self.infoTopView.braekMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0,0, 0);
        [self.infoTopView.braekMessageButton layoutIfNeeded];
        
        [self.infoTopView.safeMessageButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@30);
        }];
        self.infoTopView.safeMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0,0, 0);
        [self.infoTopView.safeMessageButton layoutIfNeeded];
        
        self.infoTopView.dangerTotalPag.hidden = YES;
        self.infoTopView.noChangePag.hidden = YES;
        self.infoTopView.noChangeLabel.hidden = YES;
        
        self.infoTopView.breakTotalPag.hidden = YES;
        
        self.infoTopView.safeTotalPag.hidden = YES;
    }
    else{
        
        [self.infoTopView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@260);
        }];
        [self.infoTopView layoutIfNeeded];
        
        //拉伸button
        [self.infoTopView.dangerMessageButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@130);
        }];
        self.infoTopView.dangerMessageButton.titleEdgeInsets = UIEdgeInsetsMake(self.infoTopView.dangerMessageButton.size.height, 0, -60, 0);
        [self.infoTopView.dangerMessageButton layoutIfNeeded];
        
        
        [self.infoTopView.braekMessageButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@130);
        }];
        self.infoTopView.braekMessageButton.titleEdgeInsets = UIEdgeInsetsMake(self.infoTopView.braekMessageButton.size.height, 0, -60, 0);
        [self.infoTopView.braekMessageButton layoutIfNeeded];
        
        [self.infoTopView.safeMessageButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@130);
        }];
        self.infoTopView.safeMessageButton.titleEdgeInsets = UIEdgeInsetsMake(self.infoTopView.safeMessageButton.size.height, 0, -60, 0);
        [self.infoTopView.safeMessageButton layoutIfNeeded];
        
        self.infoTopView.dangerTotalPag.hidden = NO;
        self.infoTopView.noChangePag.hidden = NO;
        self.infoTopView.noChangeLabel.hidden = NO;
        
        self.infoTopView.breakTotalPag.hidden = NO;
        
        self.infoTopView.safeTotalPag.hidden = NO;
    }
}

-(void)dangerBtnClick:(UIButton*)dangerBtn{
    
    CardInfoViewController *cardInfoVC = [[CardInfoViewController alloc]init];
    cardInfoVC.hidesBottomBarWhenPushed = YES;
    cardInfoVC.lastIndex = dangerBtn.tag - 1000;
    cardInfoVC.infoDic = totalDic;
    [self.navigationController pushViewController:cardInfoVC animated:YES];
}

-(void)braekBtnClick:(UIButton *)breakBtn{
    
    CardInfoViewController *cardInfoVC = [[CardInfoViewController alloc]init];
    cardInfoVC.hidesBottomBarWhenPushed = YES;
    cardInfoVC.lastIndex = breakBtn.tag - 1000;
    cardInfoVC.infoDic = totalDic;
    [self.navigationController pushViewController:cardInfoVC animated:YES];
}

-(void)safeBtnClick:(UIButton *)safeBtn{
    
    CardInfoViewController *cardInfoVC = [[CardInfoViewController alloc]init];
    cardInfoVC.hidesBottomBarWhenPushed = YES;
    cardInfoVC.lastIndex = safeBtn.tag - 1000;
    cardInfoVC.infoDic = totalDic;
    [self.navigationController pushViewController:cardInfoVC animated:YES];
}

-(void)locationChoose:(UIButton *)locationBtn{
    [self.infoTopView.locationBtn setTitle:self.areaString forState:UIControlStateNormal];
    [self.addressPickerView showAddress];
    WeakSelf(self)
    self.addressPickerView.completion = ^(NSString *address, NSString *addressCode){
        
        [weakself.infoTopView.locationBtn setTitle:address forState:UIControlStateNormal];
    };
    
}
#pragma mark - tableView的代理
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 130;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageCellModel *model = _listDataArr[indexPath.row];
    ListViewTableViewCell *listInfoCell = [tableView dequeueReusableCellWithIdentifier:@"ListCell"];
    if(listInfoCell==nil){
        
        listInfoCell = [[ListViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ListCell"];
        listInfoCell.layer.cornerRadius = 8;
    }
    [listInfoCell configListDataWithModel:model];
    return listInfoCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 判断有没有查看权限
    if (![ISSUserData sharedInstance].operation_privilege_yh_detailView) {
        [self.view makeToast:LoadLanguagesKey(@"operation_privilege_yh_detailView")];
        return;
    }
    
    MessageCellModel *model = _listDataArr[indexPath.row];
    
    ListViewTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell.noReadFlag.hidden) {
        cell.noReadFlag.hidden = YES;
        NSDictionary *data = @{@"yh":@"1",@"wz":@"2",@"aq":@"3"};
        [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:data[model.type]];
        // 重新统计
        [self.infoTopView configNoReadData:YES];
        // 让其它页面也刷新
        [[NSNotificationCenter defaultCenter] postNotificationName:@"readPushMsgSuccessNote" object:nil];
    }
    /*
    else
    {
        // 为了造数据弄的
        NSDictionary *data = @{@"yh":@"1",@"wz":@"2",@"aq":@"3"};
        [[ISSFMDBUtil sharedInstance] insertPushMsgToDB:@{@"id":model.id,@"type":data[model.type]}];
        return;
    }
     */
    
    ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
    listCellVC.hidesBottomBarWhenPushed = YES;
    listCellVC.itemId = model.id;
    listCellVC.itemType = model.type;
    listCellVC.comeType = 100;
    [self.navigationController pushViewController:listCellVC animated:YES];
}
//定位
- (void)locatemap{
    
    if ([CLLocationManager locationServicesEnabled]) {
        _locationManager = [[CLLocationManager alloc]init];
        _locationManager.delegate = self;
        [_locationManager requestAlwaysAuthorization];
        _currentCity = [[NSString alloc]init];
        [_locationManager requestWhenInUseAuthorization];
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        _locationManager.distanceFilter = 5.0;
        [_locationManager startUpdatingLocation];
    }
}
#pragma mark - 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请在设置中打开定位" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"打开定位" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *settingURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication]openURL:settingURL];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
    
    [self.infoTopView.locationBtn setTitle:@"定位失败" forState:UIControlStateNormal];
}
#pragma mark - 定位成功
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    
    CLLocation *currentLocation = [locations lastObject];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    //这里的代码是为了判断didUpdateLocations调用了几次 有可能会出现多次调用 为了避免不必要的麻烦 在这里加个if判断 如果大于1.0就return
    NSTimeInterval locationAge = -[currentLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 2.0){//如果调用已经一次，不再执行
        return;
    }
    //地理反编码 可以根据坐标(经纬度)确定位置信息(街道 门牌等)
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (placemarks.count > 0)
        {
            CLPlacemark *placeMark = placemarks[0];
            [self dealPlacemark:placeMark];
        }
        else if (error == nil && placemarks.count)
        {
            [self.infoTopView.locationBtn setTitle:@"定位失败" forState:UIControlStateNormal];
            //[self locatemap];
            NSLog(@"NO location and error return");
            [self getWeatherInfo:@"武汉"];
        }
        else if (error)
        {
            [self.infoTopView.locationBtn setTitle:@"定位失败" forState:UIControlStateNormal];
            //[self locatemap];
            NSLog(@"loction error:%@",error);
            [self getWeatherInfo:@"武汉"];
        }
        [_locationManager stopUpdatingLocation];
    }];
}

- (void)dealPlacemark:(CLPlacemark *)placemark
{
    NSString *city = placemark.locality;
    if (city) {
        _currentCity = city;
        self.addressInfo = FormatString(@"%@%@%@%@",placemark.administrativeArea,placemark.locality,placemark.subLocality,placemark.name);
    }
    else
    {
        //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得
        _currentCity = placemark.administrativeArea;
        self.addressInfo = FormatString(@"%@%@%@%@",placemark.administrativeArea,placemark.locality,placemark.subLocality,placemark.name);
    }

    //看需求定义一个全局变量来接收赋值
    //NSLog(@"当前国家 - %@",placemark.country);//当前国家
    //NSLog(@"当前城市 - %@",_currentCity);//当前城市
    //NSLog(@"当前位置 - %@",placemark.subLocality);//当前位置
    //NSLog(@"当前街道 - %@",placemark.thoroughfare);//当前街道
    //NSLog(@"具体地址 - %@",placemark.name);//具体地址
    self.areaString = placemark.subLocality;
    [self.infoTopView.locationBtn setTitle:self.areaString forState:UIControlStateNormal];
    
    [self getWeatherInfo:_currentCity];
}

#pragma mark - 处理天气
// 默认都是非主动
- (void)getWeatherInfo:(NSString *)city
{
    [self getWeatherInfo:city flag:NO];
}

// 获取天气信息,flag 主动YES、非主动NO
- (void)getWeatherInfo:(NSString *)city flag:(BOOL)flag
{
    ISSWeatherFMDBUtil *model = [[ISSWeatherFMDBUtil sharedInstance] queryWeatherDataByCityName:city];
    if (model)
    {
        if (flag) [self queryWeather:city hasCity:YES];
        else {
            // 判断时间久不久(暂定2个小时)
            if ([ISSDataDeal getCurrentTimeStamp] - model.queryTime > 2*60*60*1000) {
                [self queryWeather:city hasCity:YES];
            }
            else {
                NSLog(@"不用查询天气");
                [self.infoTopView configOldData:model];
            }
        }
    }
    else [self queryWeather:city hasCity:NO];
}

- (void)queryWeather:(NSString *)city hasCity:(BOOL)hasCity
{
    NSMutableDictionary *mutData = [NSMutableDictionary new];
    WeakSelf(self)
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        [X_HttpUtil getRequestWithIntactUrl:FormatString(@"http://api.help.bj.cn/apis/weather?id=%@",city) completionBlock:^(NSDictionary *resultData) {
            
            NSDictionary *weatherData = resultData;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakself.infoTopView configWeatherWithData:weatherData];
            });
            
            // 获取天气质量
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [X_HttpUtil getRequestWithIntactUrl:FormatString(@"http://api.help.bj.cn/apis/aqi3?id=%@",weatherData[@"cityen"]) completionBlock:^(NSDictionary *resultData) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakself.infoTopView configAqiWithData:resultData weatherData:weatherData];
                    });
                    
                    [mutData setObject:[NSString noNullString:weatherData[@"temp"]] forKey:@"temp"];
                    [mutData setObject:[NSString noNullString:weatherData[@"weather"]] forKey:@"weather"];
                    [mutData setObject:[NSString noNullString:weatherData[@"humidity"]] forKey:@"humidity"];
                    [mutData setObject:[NSString noNullString:weatherData[@"wd"]] forKey:@"wd"];
                    [mutData setObject:[NSString noNullString:resultData[@"level"]] forKey:@"level"];
                    // 缓存起来
                    if ([mutData[@"weather"] length] > 0)
                    {
                        if (hasCity)
                        {
                            [[ISSWeatherFMDBUtil sharedInstance] updateWeatherData:city
                                                                         queryTime:[ISSDataDeal getCurrentTimeStamp]
                                                                              data:mutData];
                        }
                        else
                        {
                            [[ISSWeatherFMDBUtil sharedInstance] insertWeatherData:city
                                                                         queryTime:[ISSDataDeal getCurrentTimeStamp]
                                                                              data:mutData];
                        }
                        
                    }
                    
                }];
            });
            
        }];
    });
}

#pragma mark - setter and get
-(UITableView *)listTable
{
    if (!_listTable) {
        _listTable = [[UITableView alloc]init];
        _listTable.delegate = self;
        _listTable.dataSource = self;
        _listTable.backgroundColor = [UIColor clearColor];
        _listTable.separatorStyle = UITableViewCellSeparatorStyleNone;

    }
    return _listTable;
}
-(NSMutableArray *)listDataArr{
    
    if(_listDataArr == nil){
        
        _listDataArr  = [[NSMutableArray alloc]init];
    }
    return _listDataArr;
}
-(NSMutableArray *)topListArr{
    
    if(_topListArr == nil){
        
        _topListArr = [[NSMutableArray alloc]init];
    }
    return _topListArr;
}
@end
