//
//  ListCellViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//


#import "ListCellViewController.h"

//隐患，违章，安全详情
#import "ISSInfoDetailHiddenDangerView.h"
#import "ISSInfoDetailBreakView.h"
#import "ISSInfoDetailSafeView.h"

#import "SDCycleScrollView.h" 

@interface ListCellViewController () <SDCycleScrollViewDelegate>

@end

@implementation ListCellViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createTopView];
    
    [self infoDetailRequest];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)createTopView
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 64)];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    
    WeakSelf(self)
    X_Button *btn = [ISSNavUtil getImageBtn:6 clickBlock:^{
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
    [btn setLeft:15];
    [btn setTop:20];
    [topView addSubview:btn];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, Screen_Width, 44)];
    
    if ([@"yh" isEqualToString:self.itemType]) titleLb.text = LoadLanguagesKey(@"infoDetail_title_hiddenTroubleInfo");
    else if ([@"wz" isEqualToString:self.itemType]) titleLb.text = LoadLanguagesKey(@"infoDetail_title_breakRuleInfo");
    else if ([@"aq" isEqualToString:self.itemType]) titleLb.text = LoadLanguagesKey(@"infoDetail_title_safeInfo");
    
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    titleLb.textColor = [UIColor describeColor_61];
    [topView addSubview:titleLb];
}

- (void)addTopScrollView:(NSArray *)topImageList
{
    SDCycleScrollView *topScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Width/2) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    topScrollView.imageURLStringsGroup = topImageList;
    [self.view addSubview:topScrollView];
    
    [self.mTableView setTop:CGRectGetMaxY(topScrollView.frame) + 10];
    [self.mTableView setHeight:Screen_Height - (CGRectGetMaxY(topScrollView.frame) + 10)];
}

- (void)addBottomScrollView:(NSArray *)bottomImageList
{
    SDCycleScrollView *bottomScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/2) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    bottomScrollView.imageURLStringsGroup = bottomImageList;
    
    self.mTableView.tableFooterView = bottomScrollView;
}

- (void)infoDetailRequest
{
    WeakSelf(self)
    NSDictionary *param =@{
                           @"id":self.itemId,
                           @"type":self.itemType
                           };
    [X_HttpUtil apiRequest:@"info/getSpwsYH" param:param view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSDictionary *data = resultData[@"data"];
        
        if([@"yh" isEqualToString:weakself.itemType])
        {
            //头部
            NSArray *topImageList = [[NSString noNullString:data[@"dangerPhoto"]] componentsSeparatedByString:@","];
            topImageList = [ISSDataDeal getNewImageList:topImageList];
            if (topImageList.count > 0)
            {
                [weakself addTopScrollView:topImageList];
            }
            else
            {
                [weakself.mTableView setTop:64 + 10];
                [weakself.mTableView setHeight:Screen_Height - (64 + 10)];
            }
            
            ISSInfoDetailHiddenDangerView *view = [[ISSInfoDetailHiddenDangerView alloc] initWithData:data];
            weakself.mTableView.tableHeaderView = view;
            
            // 判断有没有整改的
            NSString *repairPhoto = [NSString noNullString:data[@"repairPhoto"]];
            if (repairPhoto.length > 0) {
                NSArray *bottomImageList = [repairPhoto componentsSeparatedByString:@","];
                bottomImageList = [ISSDataDeal getNewImageList:bottomImageList];
                if (bottomImageList.count > 0) {
                    [self addBottomScrollView:bottomImageList];
                }
            }
        }
        else if ([@"wz" isEqualToString:weakself.itemType])
        {
            //头部
            NSArray *topImageList = [[NSString noNullString:data[@"tplj"]] componentsSeparatedByString:@","];
            topImageList = [ISSDataDeal getNewImageList:topImageList];
            if (topImageList.count > 0)
            {
                [weakself addTopScrollView:topImageList];
            }
            else
            {
                [weakself.mTableView setTop:64 + 10];
                [weakself.mTableView setHeight:Screen_Height - (64 + 10)];
            }
            
            ISSInfoDetailBreakView *view = [[ISSInfoDetailBreakView alloc] initWithData:data];
            weakself.mTableView.tableHeaderView = view;
        }
        else if ([@"aq" isEqualToString:weakself.itemType])
        {
            [weakself.mTableView setTop:64];
            [weakself.mTableView setHeight:Screen_Height - 64];
            
            ISSInfoDetailSafeView *view = [[ISSInfoDetailSafeView alloc] initWithData:data];
            weakself.mTableView.tableHeaderView = view;
        }
    } failureBlock:^(NSDictionary *resultData) {
        
    }];
}

@end

