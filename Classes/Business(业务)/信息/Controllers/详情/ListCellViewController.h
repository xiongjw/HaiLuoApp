//
//  ListCellViewController.h
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IssSafeListModel.h"

@interface ListCellViewController : ISSBaseTableVC

@property(nonatomic,strong) NSString *itemId;
@property(nonatomic,strong) NSString *itemType;

@property(nonatomic,assign) NSInteger comeType;//来源哪个页面

@property(nonatomic,assign) IssSafeListModel *safeModel;

@end
