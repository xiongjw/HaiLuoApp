//
//  ISSInfoNoReadListVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/23.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSInfoNoReadListVC : ISSMJRefreshTableVC

@property (nonatomic, copy) NSString *type;

@end
