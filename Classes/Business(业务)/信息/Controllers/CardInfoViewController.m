//
//  CardInfoViewController.m
//  HaiLuoApp
//
//  Created by issuser on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "CardInfoViewController.h"
#import "ImageCarouselView.h"
#import "CarouselSubview.h"
#import "CarouselCellInfo.h"
#import "ListViewTableViewCell.h"
#import "ListCellViewController.h"
#import "CardInfoView.h"
#import "ISSBaseSearchVC.h"
#import "ISSSearchResultListVC.h"
#import "MessageCellInfoModel.h"
#import "BraekCellInfoModel.h"
#import "SafeCellInfoModel.h"

#import "ISSFMDB.h"
#import "ISSInfoNoReadListVC.h"

@interface CardInfoViewController ()<ImageCarouselViewDelegate, ImageCarouselViewDataSource,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSArray *cellInfoArray;
@property (nonatomic,strong) ImageCarouselView *imageCarouselView;
@property (nonatomic,assign) NSUInteger pageWidth;
@property (nonatomic,assign) NSUInteger pageHeight;
@property (nonatomic,strong) CardInfoView *cardInfoView;

@property (nonatomic,assign) NSInteger currentIndex;
@end

@implementation CardInfoViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    [self setTitle:LoadLanguagesKey(@"tabbar_info") showBackBtn:YES];
    
    for (int i = 0; i < 3; i++) {
        [self.resultDataList addObject:[NSMutableArray new]];
    }
    
    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
    topView.backgroundColor = [UIColor themeColor];
    [self.view addSubview:topView];
    _imageCarouselView = [[ImageCarouselView alloc] initWithFrame:CGRectMake(0, 10, Screen_Width, self.pageHeight) withDataSource:self withDelegate:self];
    [_imageCarouselView.scrollView setContentOffset:CGPointMake(self.lastIndex * self.pageWidth, 0) animated:YES];
    
    if(self.lastIndex == 0) {
        _imageCarouselView.scrollView.contentOffset = CGPointMake(-self.pageWidth,0);
    }
    else if (self.lastIndex == 1) {
        _imageCarouselView.scrollView.contentOffset = CGPointMake(0,0);
    }
    else {
        _imageCarouselView.scrollView.contentOffset = CGPointMake(self.pageWidth,0);
    }
    
    [self.view addSubview:_imageCarouselView];
    
    self.mTableView.frame = CGRectMake(0, CGRectGetMaxY(_imageCarouselView.frame) + 20, Screen_Width, Screen_Height - 64 - (CGRectGetMaxY(_imageCarouselView.frame) + 20) - 5);
    
    [self addHeaderAndFooterAction];
    [self performSelector:@selector(headerBeginRefreshing) withObject:nil afterDelay:.3];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

//数据请求
-(void)requestDataReresh:(NSString *)direction
{
    __block NSMutableArray *currentList = self.resultDataList[_currentIndex];
    NSArray *typeList = @[@"yh",@"wz",@"aq"];
    NSDictionary *data = @{
                           @"type":typeList[_currentIndex],
                           @"lastId":[@"down" isEqualToString:direction] ? @"" : ((MessageCellModel *)[currentList lastObject]).mId
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"info/getSpwsYHs" param:data view:nil successBlock:^(NSDictionary *resultData) {
        
        [weakself endRefreshing];
        
        NSArray *list = [MessageCellModel arrayOfModelsFromDictionaries:resultData[@"data"][@"list"] error:nil] ;
        if ([@"down" isEqualToString:direction]) {
            
            if (weakself.mTableView.mj_footer.hidden) {
                weakself.mTableView.mj_footer.hidden = NO;
                [weakself.mTableView.mj_footer resetNoMoreData];
            }
            [currentList removeAllObjects];
        }
        if (list.count < [PageSize integerValue])
        {   weakself.mTableView.mj_footer.hidden = YES;
            [weakself.mTableView.mj_footer endRefreshingWithNoMoreData];
        }
        if (list.count > 0)
        {
            [currentList addObjectsFromArray:list];
        }
        [weakself.mTableView reloadData];
        
    } failureBlock:^(NSDictionary *resultData) {
        
        [weakself endRefreshing];
    }];
}

#pragma mark - 卡片滚动设置
- (CGSize)sizeForPageInCarouselView:(ImageCarouselView *)carouselView {
    return CGSizeMake(self.pageWidth, self.pageHeight);
}

- (NSInteger)numberOfPagesInCarouselView:(ImageCarouselView *)carouselView {
    return self.cellInfoArray.count;
}

- (CarouselCell *)carouselView:(ImageCarouselView *)carouselView cellForPageAtIndex:(NSUInteger)index {
    CarouselCellImageView *cell = [[CarouselCellImageView alloc] initWithFrame:CGRectMake(5, 0, self.pageWidth - 10, self.pageHeight)];
    cell.imageView.image = self.cellInfoArray[index];
    cell.showTime = 0;
    
    self.cardInfoView = [[CardInfoView alloc]initWithFrame:cell.imageView.frame];
    self.cardInfoView.tag = index;
    [self.cardInfoView configDataWithCardViewTag:index withCardInfoDic:self.infoDic];
    [cell configDataWithView:self.cardInfoView];
   
    return cell;
}

- (void)carouselView:(ImageCarouselView *)carouselView didScrollToPage:(NSInteger)pageNumber
{
    NSLog(@"pageNumber:%ld",(long)pageNumber);
    self.currentIndex = pageNumber;
    NSMutableArray *currentList = self.resultDataList[_currentIndex];
    if (currentList.count > 0) {
        [self.mTableView reloadData];
    }
    else
    {
        [self performSelector:@selector(headerBeginRefreshing) withObject:nil afterDelay:.3];
    }
}

- (void)carouselView:(ImageCarouselView *)carouselView didSelectPageAtIndex:(NSInteger)index
{
    NSString *type = FormatString(@"%ld",index + 1);
    NSInteger num = [[ISSFMDBUtil sharedInstance] queryTypeMsg:type];
    if (num > 0)
    {
        ISSInfoNoReadListVC *vc = [[ISSInfoNoReadListVC alloc] init];
        vc.type = type;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSUInteger)pageWidth {
    return Screen_Width * 0.8;
}

- (NSUInteger)pageHeight {
    return self.pageWidth * 0.6;
}

#pragma mark - tableView的代理
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 130;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultDataList[_currentIndex] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListViewTableViewCell *listInfoCell = [tableView dequeueReusableCellWithIdentifier:@"ListCell"];
    if(listInfoCell==nil){
        
        listInfoCell = [[ListViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ListCell"];
        listInfoCell.layer.cornerRadius = 8;
    }
    MessageCellModel *model = self.resultDataList[_currentIndex][indexPath.row];
    [listInfoCell configListDataWithModel:model];
   
    return listInfoCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 判断有没有查看权限
    if (![ISSUserData sharedInstance].operation_privilege_yh_detailView) {
        [self.view makeToast:LoadLanguagesKey(@"operation_privilege_yh_detailView")];
        return;
    }
    MessageCellModel *model = self.resultDataList[_currentIndex][indexPath.row];
    
    ListViewTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell.noReadFlag.hidden) {
        cell.noReadFlag.hidden = YES;
        NSDictionary *data = @{@"yh":@"1",@"wz":@"2",@"aq":@"3"};
        [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:data[model.type]];
        
        // 让其它页面也刷新
        [[NSNotificationCenter defaultCenter] postNotificationName:@"readPushMsgSuccessNote" object:nil];
    }
    
    ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
    listCellVC.hidesBottomBarWhenPushed = YES;
    listCellVC.itemId = model.id;
    listCellVC.comeType = 100;
    listCellVC.itemType = model.type;
    [self.navigationController pushViewController:listCellVC animated:YES];
    
}
#pragma mark setter and get
- (NSArray *)cellInfoArray {
    if (_cellInfoArray == nil) {
        
        NSArray *dictArr = @[[UIImage imageNamed:@"dangerCarBg"],[UIImage imageNamed:@"breakCarBg"],[UIImage imageNamed:@"safeCarBg"]];
        _cellInfoArray = dictArr;
    }
    return _cellInfoArray;
}

// 根据指定文本,字体和最大宽度计算尺寸
- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)width
{
    NSMutableDictionary *attrDict = [NSMutableDictionary dictionary];
    attrDict[NSFontAttributeName] = font;
    CGSize size = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrDict context:nil].size;
    return size;
}

@end

