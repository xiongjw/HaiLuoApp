//
//  ISSSearchPersonVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/24.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^searchChoosePersonBlock)(NSDictionary *data);

@interface ISSSearchPersonVC : ISSMJRefreshTableVC

@property (nonatomic,copy) searchChoosePersonBlock chooseBlock;

@end
