//
//  ISSBaseChoosePersonVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISSPersonModel.h"

typedef void (^choosePersonBlock)(ISSPersonModel *model);

// 这个类没用了， 之前是一次性请求
@interface ISSBaseChoosePersonVC : ISSBaseTableVC

@property (nonatomic,copy) choosePersonBlock chooseBlock;

@end
