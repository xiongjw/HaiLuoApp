//
//  ISSPersonModel.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

// 为了通过name搜索，才定义的model
@interface ISSPersonModel : NSObject

@property (nonatomic,copy) NSString *personId;
@property (nonatomic,copy) NSString *name;

@end
