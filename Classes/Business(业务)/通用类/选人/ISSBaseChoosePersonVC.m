//
//  ISSBaseChoosePersonVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseChoosePersonVC.h"

@interface ISSBaseChoosePersonVC ()
{
    BOOL _clickSearchBtn;
}

@property (nonatomic,strong) UITextField *searchField;

@property (nonatomic,strong) NSMutableArray *sectionTitleList;
@property (nonatomic,strong) NSMutableArray *sectionDataList;

@property (nonatomic,strong) NSMutableArray *searchResultList;

@end

@implementation ISSBaseChoosePersonVC

- (void)setUpTableSection:(NSArray *)list
{
    for (NSDictionary *item in list)
    {
        ISSPersonModel *model = [ISSPersonModel new];
        model.name = item[@"realName"];
        model.personId = item[@"id"];
        
        [self.resultDataList addObject:model];
    }
    
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    //create a temp sectionArray
    NSUInteger numberOfSections = [[collation sectionTitles] count];
    
    for (NSUInteger index = 0; index < numberOfSections; index++)
    {
        [self.sectionDataList addObject:[[NSMutableArray alloc]init]];
    }
    
    // insert Persons info into newSectionArray
    for (ISSPersonModel *model in self.resultDataList)
    {
        NSUInteger sectionIndex = [collation sectionForObject:model collationStringSelector:@selector(name)];
        [self.sectionDataList[sectionIndex] addObject:model];
    }
    
    //sort the person of each section
    for (NSUInteger index = 0; index < numberOfSections; index++)
    {
        NSMutableArray *personsForSection = self.sectionDataList[index];
        NSArray *sortedPersonsForSection = [collation sortedArrayFromArray:personsForSection collationStringSelector:@selector(name)];
        self.sectionDataList[index] = sortedPersonsForSection;
    }
    
    NSMutableArray *temp = [NSMutableArray new];
    
    [_sectionDataList enumerateObjectsUsingBlock:^(NSArray *arr, NSUInteger idx, BOOL *stop) {
        if (arr.count == 0) {
            [temp addObject:arr];
        } else {
            [self.sectionTitleList addObject:[collation sectionTitles][idx]];
        }
    }];
    
    [self.sectionDataList removeObjectsInArray:temp];
    
    [self.mTableView reloadData];
}

- (void)textFieldDidChangeText:(UITextField *)textField {
    
    if (textField.text.length == 0) {
        _clickSearchBtn = NO;
        [self.mTableView reloadData];
        return;
    }
    
    UITextRange *selectedRange = [textField markedTextRange];
    NSString *newText = [textField textInRange:selectedRange];
    NSLog(@"text:%@  ,newText:%@",textField.text,newText);
    if (newText.length == 0)
    {
        _clickSearchBtn = YES;
        [self.searchResultList removeAllObjects];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains [cd] %@",textField.text];
        
        for (ISSPersonModel *model in self.resultDataList)
        {
            NSArray *list = [@[model.name] filteredArrayUsingPredicate:predicate];
            if (list.count > 0) {
                [self.searchResultList addObject:model];
            }
        }
        [self.mTableView reloadData];
    }
}

- (void)createTopSearchView
{
    CGRect frame = CGRectMake(0, 7, Screen_Width - 50, 44 - 14);
    UIView *searchFieldView = [[UIView alloc] initWithFrame:frame];
    searchFieldView.backgroundColor = UIColorFromRGB(0xefefef);
    searchFieldView.layer.cornerRadius = CGRectGetHeight(frame)/2;
    searchFieldView.layer.masksToBounds = YES;
    
    self.searchField = [ISSCreateUIHelper makeTextField:self withFrame:CGRectMake(15, 0, frame.size.width - 30, frame.size.height) andPlaceholder:@"关键字" maxLength:12];
    //self.searchField.returnKeyType = UIReturnKeySearch;
    [searchFieldView addSubview:self.searchField];
    self.navigationItem.titleView = searchFieldView;
    
    [self.searchField addTarget:self
                         action:@selector(textFieldDidChangeText:)
               forControlEvents:UIControlEventEditingChanged];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"" showBackBtn:YES];
    
    [self createTopSearchView];
    
    WeakSelf(self)
    self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItemTitle:@"取消" titleColor:[UIColor describeColor_61] titleFont:[UIFont systemFontOfSize:14] clickBlock:^{
        [weakself dismissViewControllerAnimated:NO completion:NULL];
    }];
    
    self.sectionTitleList = [NSMutableArray new];
    self.sectionDataList = [NSMutableArray new];
    self.searchResultList = [NSMutableArray new];
    
    _clickSearchBtn = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([ISSDataStorage hasObject:@"OrgUserList"])
    {
        NSDictionary *data = [ISSDataStorage getObject:@"OrgUserList"];
        NSTimeInterval oldTime = [data[@"time"] longLongValue];
        NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970];
        // 目前暂定超过1天开始请求，因为这个数据一般不会变更
        if (nowTime - oldTime > 24*60*60) {
            [self queryList];
        }
        else
        {
            [self setUpTableSection:data[@"OrgUserList"]];
        }
    }
    else
    {
        // 没有缓存，那么就查询接口请求数据
        [self queryList];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_clickSearchBtn) {
        return 1;
    }
    return self.sectionDataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_clickSearchBtn) {
        return self.searchResultList.count;
    }
    return [self.sectionDataList[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    ISSPersonModel *model = nil;
    if (_clickSearchBtn) {
        model = self.searchResultList[indexPath.row];
    }
    else {
        model = self.sectionDataList[indexPath.section][indexPath.row];
    }
    cell.textLabel.text = model.name;
    if (_clickSearchBtn) {
        cell.textLabel.attributedText = [ISSPubfun setLabelAttributed:model.name range:NSMakeRange(0, self.searchField.text.length) color:[UIColor redColor] font:cell.textLabel.font];
    }
    return cell;
}

#pragma mark - UITableViewDelegate withTitle: colorType
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_clickSearchBtn) {
        return nil;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 30)];
    view.backgroundColor = ISSBackgroundColor;
    
    UILabel *titleLb = [UILabel initWithFrame:CGRectMake(15, 0, 200, 30)
                                         text:_sectionTitleList[section]];
    titleLb.textColor = [UIColor describeColor_66];
    titleLb.font = [UIFont systemFontOfSize:12];
    [view addSubview:titleLb];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (_clickSearchBtn) {
        return 0;
    }
    return 30;
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (_clickSearchBtn) {
        return nil;
    }
    return _sectionTitleList;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ISSPersonModel *model = nil;
    if (_clickSearchBtn) {
        model = self.searchResultList[indexPath.row];
    }
    else {
        model = self.sectionDataList[indexPath.section][indexPath.row];
    }
    if (_chooseBlock) {
        _chooseBlock(model);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 到时候修改成，一段时间请求一次，放缓存，一天拉取一次
- (void)queryList
{
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"job/getOrgUserList" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSArray *list = resultData[@"data"][@"list"];
        [weakself setUpTableSection:list];
        
        NSDictionary *data = @{
                               @"OrgUserList":list,
                               @"time":@([[NSDate date] timeIntervalSince1970])
                               };
        [ISSDataStorage saveObject:data key:@"OrgUserList"];
        
    }];
}

@end
