//
//  ISSChoosePersonVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/24.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^choosePersonBlock)(NSDictionary *data);

@interface ISSChoosePersonVC : ISSMJRefreshTableVC

@property (nonatomic,copy) choosePersonBlock chooseBlock;

@end
