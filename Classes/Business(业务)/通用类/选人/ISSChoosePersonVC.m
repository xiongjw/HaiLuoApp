//
//  ISSChoosePersonVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/24.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSChoosePersonVC.h"

#import "ISSSearchPersonVC.h"

@interface ISSChoosePersonVC ()

@end

@implementation ISSChoosePersonVC

- (void)goSearchVC
{
    WeakSelf(self)
    ISSSearchPersonVC *vc = [[ISSSearchPersonVC alloc] init];
    vc.chooseBlock = ^(NSDictionary *data) {
        [weakself afterChoose:data];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = FormatString(@"%@ %@",LoadLanguagesKey(@"levelSelected_navTitle"),self.title);
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        [weakself dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItem:4 clickBlock:^{
        [weakself goSearchVC];
    }];
    
    self.mTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 8)];
    
    [self addHeaderAndFooterAction];
    [self headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.detailTextLabel.textColor = [UIColor describeColor_9a];
    }
    NSDictionary *data = self.resultDataList[indexPath.row];
    cell.textLabel.text = data[@"realName"];
    cell.detailTextLabel.text = [NSString noNullString:data[@"orgName"]];
    return cell;
}

#pragma mark - UITableViewDelegate withTitle: colorType
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = self.resultDataList[indexPath.row];
    [self afterChoose:data];
}

- (void)afterChoose:(NSDictionary *)data
{
    if (_chooseBlock) {
        _chooseBlock(data);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)requestDataReresh:(NSString *)direction
{
    NSDictionary *data = @{@"condition":@"",
                           @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"job/getOrgUserList" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSArray *list = resultData[@"data"][@"list"];
        [weakself addResultDataToList:list direction:direction];
    } failureBlock:^(NSDictionary *resultData) {
        [weakself endRefreshing];
    }];
}

@end
