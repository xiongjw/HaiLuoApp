//
//  ISSSearchPersonVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/24.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSSearchPersonVC.h"

@interface ISSSearchPersonVC ()
{
    NSString *_lastKey;
}

@property (nonatomic,strong) UITextField *searchField;

@end

@implementation ISSSearchPersonVC

- (void)createTopSearchView
{
    CGRect frame = CGRectMake(0, 7, Screen_Width - 50, 44 - 14);
    UIView *searchFieldView = [[UIView alloc] initWithFrame:frame];
    searchFieldView.backgroundColor = UIColorFromRGB(0xefefef);
    searchFieldView.layer.cornerRadius = CGRectGetHeight(frame)/2;
    searchFieldView.layer.masksToBounds = YES;
    
    self.searchField = [ISSCreateUIHelper makeTextField:self withFrame:CGRectMake(15, 0, frame.size.width - 30, frame.size.height) andPlaceholder:@"关键字" maxLength:12];
    self.searchField.returnKeyType = UIReturnKeySearch;
    [searchFieldView addSubview:self.searchField];
    self.navigationItem.titleView = searchFieldView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"" showBackBtn:YES];
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
    [self createTopSearchView];
    
    self.mTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 8)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.detailTextLabel.textColor = [UIColor describeColor_9a];
    }
    NSDictionary *data = self.resultDataList[indexPath.row];
    cell.textLabel.text = data[@"realName"];
    cell.detailTextLabel.text = [NSString noNullString:data[@"orgName"]];
    return cell;
}

#pragma mark - UITableViewDelegate withTitle: colorType
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = self.resultDataList[indexPath.row];
    
    if (_chooseBlock) {
        _chooseBlock(data);
    }
    [self.navigationController popViewControllerAnimated:NO];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField.text.length > 0)
    {
        if (![textField.text isEqualToString:_lastKey]) {
            [self requestDataReresh:@"down"];
            _lastKey = textField.text;
        }
    }
    
    return YES;
}

- (void)requestDataReresh:(NSString *)direction
{
    NSDictionary *data = @{@"condition":self.searchField.text,
                           @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"job/getOrgUserList" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        NSArray *list = resultData[@"data"][@"list"];
        [weakself addResultDataToList:list direction:direction];
    } failureBlock:^(NSDictionary *resultData) {
        [weakself endRefreshing];
    }];
}

@end
