//
//  ISSBaseModel.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JSONModel.h"

@interface ISSBaseModel : JSONModel

@end
