//
//  ISSLevelSelectedVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/23.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^levelSelectedItemBlock)(NSDictionary *item);

@interface ISSLevelSelectedVC : ISSBaseTableVC

@property (nonatomic,assign) int level;
@property (nonatomic,assign) long long Id;//pID   父类

@property (nonatomic,copy) levelSelectedItemBlock levelSelectedItemBlock;

@end
