//
//  ISSLevelSelectedVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/23.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSLevelSelectedVC.h"

@interface ISSLevelSelectedVC ()

@end

@implementation ISSLevelSelectedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = LoadLanguagesKey(@"levelSelected_navTitle");
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        if (weakself.level == 1) [weakself dismissViewControllerAnimated:YES completion:NULL];
        else [weakself.navigationController popViewControllerAnimated:YES];
    }];
    
    self.mTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    // self.tableData 所有数据
    // self.resultDataList 每一个层级的数据
    
    for (NSDictionary *data in self.tableData) {
        if ([data[@"level"] intValue] == self.level)
        {
            if (self.level == 1) {
                [self.resultDataList addObject:data];
            }
            else
            {
                if (![data[@"parentId"] isKindOfClass:[NSNull class]] && [data[@"parentId"] longLongValue] == self.Id) {
                    [self.resultDataList addObject:data];
                }
            }
        }
    }
    
    if (self.level == 1)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedAction:) name:@"levelSelectedNote" object:nil];
    }
}

- (void)selectedAction:(NSNotification *)note
{
    if (_levelSelectedItemBlock) {
        _levelSelectedItemBlock(note.object);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    if (self.level == 1) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = self.resultDataList[indexPath.row];
    cell.textLabel.text = [NSString noNullString:data[@"name"]];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    if ([self checkHasNext:data]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *data = self.resultDataList[indexPath.row];
    if ([self checkHasNext:data])
    {
        ISSLevelSelectedVC *vc = [[ISSLevelSelectedVC alloc] init];
        vc.tableData = self.tableData;
        vc.level = self.level + 1;
        vc.Id = [data[@"id"] longLongValue];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        // 选中
        if (self.level > 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"levelSelectedNote" object:data];
            [self.navigationController popViewControllerAnimated:NO];
        }
        else
        {
            if (_levelSelectedItemBlock) {
                _levelSelectedItemBlock(data);
            }
            [self dismissViewControllerAnimated:YES completion:NULL];
        }
    }
    
}

- (BOOL)checkHasNext:(NSDictionary *)item
{
    BOOL flag = NO;
    for (NSDictionary *data in self.tableData)
    {
        //if (([data[@"level"] intValue] == self.level + 1) && [data[@"parentId"] isEqualToString:item[@"id"]])
        if (([data[@"level"] intValue] == self.level + 1) && ![data[@"parentId"] isKindOfClass:[NSNull class]] && [data[@"parentId"] longLongValue] == [item[@"id"] longLongValue])
        {
            flag = YES;
            break;
        }
    }
    return flag;
}

@end
