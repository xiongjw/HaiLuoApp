//
//  ISSFMDB.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/27.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSFMDB.h"

@implementation ISSFMDB

#pragma mark - 数据库有关
+ (FMDatabase *)dataBase
{
    NSString *path = [ISSPubfun getApplicationDocumentsDirectory];
    return [FMDatabase databaseWithPath:[path stringByAppendingPathComponent:@"ISSDataBase.db"]];
}

+ (void)createDB
{
    NSInteger lastBuildVersion = [[ISSDataStorage getObject:@"lastBuildVersion"] integerValue];
    NSInteger localBuildVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] integerValue];
    if (localBuildVersion > lastBuildVersion) {
        [ISSDataStorage saveObject:@(localBuildVersion) key:@"lastBuildVersion"];
        [self applicationUpdated:lastBuildVersion];
    }
}

+ (void)applicationUpdated:(NSInteger)lastBuildVersion
{
    // 应用全新安装，创建所有表
    if (lastBuildVersion == 0)
    {
        FMDatabase *db = [self dataBase];
        if ([db open])
        {
            //创建消息表
            BOOL result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS push_list (msgId varchar(64), type varchar(64), userId varchar(64))"];
            if (!result) {
                NSLog(@"建表push_list失败");
            }
            //创建天气表
            result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS weather_list (cityName varchar(64), queryTime bigint, temp varchar(64), weather varchar(64), humidity varchar(64), wd varchar(64), level varchar(64))"];
            if (!result) {
                NSLog(@"建表weather_list失败");
            }
            
            [db close];
        }
    }
    // 上一次已经是2了，只创建天气表，最新是3
    if (lastBuildVersion == 2)
    {
        FMDatabase *db = [self dataBase];
        if ([db open])
        {
            //创建天气表
            BOOL result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS weather_list (cityName varchar(64), queryTime bigint, temp varchar(64), weather varchar(64), humidity varchar(64), wd varchar(64), level varchar(64))"];
            if (!result) {
                NSLog(@"建表weather_list失败");
            }
            
            [db close];
        }
    }
    // 上一次已经是3了，最新是4
    if (lastBuildVersion == 3) {
        
    }
}
@end

#pragma mark - 消息推送有关
@implementation ISSFMDBUtil

+ (ISSFMDBUtil *)sharedInstance
{
    static ISSFMDBUtil *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[ISSFMDBUtil alloc] init];
    });
    return instance;
}

// 插入
- (void)insertPushMsgToDB:(NSDictionary *)data
{
    FMDatabase *db = [ISSFMDB dataBase];
    if ([db open])
    {
        BOOL result = [db executeUpdate:@"INSERT INTO push_list (msgId,type,userId) VALUES (?,?,?)", data[@"id"],data[@"type"],[ISSUserData sharedInstance].userId];
        
        if (result)
        {
            NSLog(@"插入消息成功");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"insertPushMsgSuccessNote" object:nil];
        }
        else NSLog(@"插入失败,msgId：%@",data[@"id"]);
        [db close];
    }
}

// 点击就删
- (BOOL)deletePushMsgById:(NSString *)msgId type:(NSString *)type
{
    BOOL result = NO;
    FMDatabase *db = [ISSFMDB dataBase];
    if ([db open]) {
        result = [db executeUpdate:@"DELETE FROM push_list WHERE msgId=? and type=? and userId=?",msgId,type,[ISSUserData sharedInstance].userId];
        
        if (result) NSLog(@"删除push消息成功:%@",msgId);
        else NSLog(@"删除push消息失败:%@",msgId);
        [db close];
    }
    return result;
}

// 存在表示未读
- (BOOL)isExistMsgById:(NSString *)msgId type:(NSString *)type
{
    BOOL result = NO;
    FMDatabase* db = [ISSFMDB dataBase];
    if ([db open]) {
        FMResultSet *resultSet = [db executeQuery:@"SELECT * FROM push_list WHERE msgId=? and type=? and userId=? LIMIT 0,1",msgId,type,[ISSUserData sharedInstance].userId];
        if ([resultSet next]) {
            result = YES;
        }
        [db close];
    }
    return result;
}

- (NSInteger)queryAllMsg
{
    NSInteger result = 0;
    FMDatabase* db = [ISSFMDB dataBase];
    if ([db open]) {
        FMResultSet *resultSet = [db executeQuery:@"SELECT COUNT(*) FROM push_list WHERE userId=?",[ISSUserData sharedInstance].userId];
        if ([resultSet next]) {
            result = [resultSet intForColumnIndex:0];
        }
        [db close];
    }
    return result;
}

- (NSInteger)queryDangerAndBreakMsg
{
    NSInteger result = 0;
    FMDatabase* db = [ISSFMDB dataBase];
    if ([db open]) {
        FMResultSet *resultSet = [db executeQuery:@"SELECT COUNT(*) FROM push_list WHERE userId=? and type in ('1','2')",[ISSUserData sharedInstance].userId];
        if ([resultSet next]) {
            result = [resultSet intForColumnIndex:0];
        }
        [db close];
    }
    return result;
}

- (NSInteger)queryTypeMsg:(NSString *)type
{
    NSInteger result = 0;
    FMDatabase* db = [ISSFMDB dataBase];
    if ([db open]) {
        FMResultSet *resultSet = [db executeQuery:@"SELECT COUNT(*) FROM push_list WHERE type=? and userId=?",type,[ISSUserData sharedInstance].userId];
        if ([resultSet next]) {
            result = [resultSet intForColumnIndex:0];
        }
        [db close];
    }
    return result;
}

//- (NSArray *)queryTypeIds:(NSString *)type pageNo:(NSInteger)paegNo
- (NSArray *)queryTypeIds:(NSString *)type startIndex:(NSInteger)startIndex
{
    NSMutableArray *ids = [NSMutableArray new];
    
    FMDatabase* db = [ISSFMDB dataBase];
    if ([db open]) {
        
        NSString *sql = FormatString(@"SELECT * FROM push_list WHERE type=? and userId=? LIMIT %ld,15",startIndex);
        FMResultSet *resultSet = [db executeQuery:sql,type,[ISSUserData sharedInstance].userId];
        
        while ([resultSet next]) {
            [ids addObject:[resultSet stringForColumn:@"msgId"]];
        }
        [db close];
    }
    return ids.copy;
    
}

@end

#pragma mark - 天气有关
@implementation ISSWeatherFMDBUtil

+ (ISSWeatherFMDBUtil *)sharedInstance
{
    static ISSWeatherFMDBUtil *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[ISSWeatherFMDBUtil alloc] init];
    });
    return instance;
}

- (void)insertWeatherData:(NSString *)cityName queryTime:(long long)queryTime data:(NSDictionary *)data
{
    FMDatabase *db = [ISSFMDB dataBase];
    if ([db open])
    {
        BOOL result = [db executeUpdate:@"INSERT INTO weather_list (cityName,queryTime,temp,weather,humidity,wd,level) VALUES (?,?,?,?,?,?,?)", cityName,@(queryTime),data[@"temp"],data[@"weather"],data[@"humidity"],data[@"wd"],data[@"level"]];
        
        if (result) {
            NSLog(@"插入天气成功");
        }
        else NSLog(@"插入天气失败,cityName：%@",cityName);
        [db close];
    }
}

- (ISSWeatherFMDBUtil *)queryWeatherDataByCityName:(NSString *)cityName
{
    ISSWeatherFMDBUtil *model = nil;
    FMDatabase* db = [ISSFMDB dataBase];
    if ([db open]) {
        FMResultSet *resultSet = [db executeQuery:@"SELECT * FROM weather_list WHERE cityName=?",cityName];
        if ([resultSet next])
        {
            model = [[ISSWeatherFMDBUtil alloc] init];
            model.cityName = cityName;
            model.queryTime = [resultSet longLongIntForColumn:@"queryTime"];
            model.temp = [resultSet stringForColumn:@"temp"];
            model.weather = [resultSet stringForColumn:@"weather"];
            model.humidity = [resultSet stringForColumn:@"humidity"];
            model.wd = [resultSet stringForColumn:@"wd"];
            model.level = [resultSet stringForColumn:@"level"];
        }
        [db close];
    }
    return model;
}

- (BOOL)updateWeatherData:(NSString *)cityName queryTime:(long long)queryTime data:(NSDictionary *)data
{
    FMDatabase *db = [ISSFMDB dataBase];
    if ([db open]) {
        NSString *sql = @"UPDATE weather_list SET queryTime=?,temp=?,weather=?,humidity=?,wd=?,level=? WHERE cityName=?";
        BOOL result = [db executeUpdate:sql,@(queryTime),data[@"temp"],data[@"weather"],data[@"humidity"],data[@"wd"],data[@"level"],cityName];
        if (result) {
            NSLog(@"更新%@天气成功",cityName);
        }
        else {
            NSLog(@"更新%@天气失败",cityName);
        }
        [db close];
    }
    return YES;
}

@end
