//
//  ISSFMDB.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/27.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FMDatabase.h"

@interface ISSFMDB : NSObject

+ (FMDatabase *)dataBase;

+ (void)createDB;

+ (void)applicationUpdated:(NSInteger)lastBuildVersion;

@end

@interface ISSFMDBUtil : NSObject

@property (nonatomic,  copy) NSString *msgId;
@property (nonatomic,  copy) NSString *type;
@property (nonatomic,  copy) NSString *userId;
//@property (nonatomic,assign) NSString *isRead;//已读

+ (ISSFMDBUtil *)sharedInstance;

- (void)insertPushMsgToDB:(NSDictionary *)data;
- (BOOL)deletePushMsgById:(NSString *)msgId type:(NSString *)type;
- (BOOL)isExistMsgById:(NSString *)msgId type:(NSString *)type;

- (NSInteger)queryAllMsg;
- (NSInteger)queryDangerAndBreakMsg;
- (NSInteger)queryTypeMsg:(NSString *)type;

//- (NSArray *)queryTypeIds:(NSString *)type pageNo:(NSInteger)paegNo;
- (NSArray *)queryTypeIds:(NSString *)type startIndex:(NSInteger)startIndex;

@end

@interface ISSWeatherFMDBUtil : NSObject

@property (nonatomic,  copy) NSString *cityName;
@property (nonatomic,assign) long long queryTime;
@property (nonatomic,  copy) NSString *temp;
@property (nonatomic,  copy) NSString *weather;
@property (nonatomic,  copy) NSString *humidity;
@property (nonatomic,  copy) NSString *wd;
@property (nonatomic,  copy) NSString *level;

+ (ISSWeatherFMDBUtil *)sharedInstance;

- (void)insertWeatherData:(NSString *)cityName queryTime:(long long)queryTime data:(NSDictionary *)data;

- (ISSWeatherFMDBUtil *)queryWeatherDataByCityName:(NSString *)cityName;

- (BOOL)updateWeatherData:(NSString *)cityName queryTime:(long long)queryTime data:(NSDictionary *)data;

@end
