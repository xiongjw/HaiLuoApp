//
//  ISSBaseRoundCell.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSBaseRoundCell : UITableViewCell

@property (nonatomic,strong) UIView *bgContentView;
@property (nonatomic,strong) UIView *lineView;

- (void)dealRoundCornerAndLine:(NSInteger)row
                  numberOfRows:(NSInteger)numberOfRows;

- (void)dealRoundCornerAndLine:(NSInteger)row
                  numberOfRows:(NSInteger)numberOfRows
                     rowHeight:(CGFloat)rowHeight;

@end
