//
//  ISSBaseRoundCell.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseRoundCell.h"

@implementation ISSBaseRoundCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _bgContentView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, Screen_Width - 30, 50)];
        _bgContentView.backgroundColor = [UIColor whiteColor];
        _bgContentView.layer.cornerRadius = 8;
        _bgContentView.layer.masksToBounds = YES;
        //_bgContentView.layer.borderWidth = 1;
        //_bgContentView.layer.borderColor = [UIColor sepLineColor].CGColor;
        [self.contentView addSubview:_bgContentView];
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(_bgContentView.frame) - 0.5, Screen_Width - 30, 0.5)];
        _lineView.backgroundColor = [UIColor sepLineColor];
        [self.contentView addSubview:_lineView];
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealRoundCornerAndLine:(NSInteger)row
                  numberOfRows:(NSInteger)numberOfRows
{
    _bgContentView.layer.cornerRadius = 0;
    _bgContentView.layer.masksToBounds = YES;
    
    if (numberOfRows == 1)
    {
        [self addRoundCorner:_bgContentView position:@"all"];
    }
    else if (numberOfRows == 2)
    {
        if (row == 0) {
            [self addRoundCorner:_bgContentView position:@"top"];
        }
        else
        {
            [self addRoundCorner:_bgContentView position:@"bottom"];
        }
    }
    else
    {
        if (row == 0)
        {
            [self addRoundCorner:_bgContentView position:@"top"];
        }
        else if (row == numberOfRows - 1)
        {
            [self addRoundCorner:_bgContentView position:@"bottom"];
        }
    }
    if (row == numberOfRows - 1) _lineView.hidden = YES;
    else _lineView.hidden = NO;
}

- (void)dealRoundCornerAndLine:(NSInteger)row
                  numberOfRows:(NSInteger)numberOfRows
                     rowHeight:(CGFloat)rowHeight
{
    // 处理高度
    [self.bgContentView setHeight:rowHeight];
    [self.lineView setCenterY:rowHeight - 0.5];
    
    [self dealRoundCornerAndLine:row numberOfRows:numberOfRows];
}

- (void)addRoundCorner:(UIView *)view position:(NSString *)position
{
    UIRectCorner corners = UIRectCornerAllCorners;
    if ([@"all" isEqualToString:position]) {
        
    }
    else if ([@"top" isEqualToString:position])
    {
        corners = UIRectCornerTopLeft | UIRectCornerTopRight;
    }
    else if ([@"bottom" isEqualToString:position])
    {
        corners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    }

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                               byRoundingCorners:corners
                                                     cornerRadii:CGSizeMake(8, 8)];//指定圆角位置 大小
    
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.frame = view.bounds;
    masklayer.path = path.CGPath;
    //masklayer.borderColor = [UIColor sepLineColor].CGColor;
    view.layer.mask = masklayer;
}

@end
