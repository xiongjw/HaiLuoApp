//
//  ISSBaseTextFieldCell.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TextFieldInputBlock)(NSString *text);

@interface ISSBaseTextFieldCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UILabel *leftLb;
@property (nonatomic,strong) UIView *fieldView;
@property (nonatomic,strong) UITextField *mTextField;
@property (nonatomic,strong) UILabel *rightLb;
@property (nonatomic,strong) UIImageView *rightIcon;

@property (nonatomic, copy) TextFieldInputBlock inputBlock;

@end
