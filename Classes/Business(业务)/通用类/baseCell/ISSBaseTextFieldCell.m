//
//  ISSBaseTextFieldCell.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseTextFieldCell.h"

@implementation ISSBaseTextFieldCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(5, 2, Screen_Width - 10, 46)];
        _bgView.layer.cornerRadius = 8;
        //_bgView.layer.masksToBounds = YES;
        //_bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
        _bgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_bgView];
        
        _leftLb = [UILabel oneLineLbWithX:10 y:0 fontSize:15 color:[UIColor titleColor_26] text:@"作业开始时间啊啊"];
        _leftLb.font = [UIFont boldSystemFontOfSize:15];
        //[_leftLb setCenterY:_bgView.centerY];
        _leftLb.adjustsFontSizeToFitWidth = YES;
        _leftLb.numberOfLines = 2;
        [_leftLb setHeight:50];
        [_bgView addSubview:_leftLb];
        
        CGFloat bgWidth = CGRectGetWidth(_bgView.frame);
        CGFloat leftPadding = CGRectGetMaxX(_leftLb.frame) + 10;
        //CGFloat leftPadding = 84;
        
        _rightIcon = [[UIImageView alloc] initWithFrame:CGRectMake(bgWidth - 10 - 30, 8, 30, 30)];
        _rightIcon.layer.cornerRadius = CGRectGetHeight(_rightIcon.frame)/2;
        _rightIcon.layer.masksToBounds = YES;
        _rightIcon.backgroundColor = UIColorFromRGB(0x2c68ec);
        _rightIcon.contentMode = UIViewContentModeCenter;
        _rightIcon.image = [UIImage imageNamed:@"icon_input"];
        [_bgView addSubview:_rightIcon];
        
        //CGFloat rightLbWidth = bgWidth - 15 - leftPadding;
        CGFloat rightLbWidth = CGRectGetMinX(_rightIcon.frame) - 10 - leftPadding;
        _rightLb = [UILabel oneLineLbWithX:leftPadding y:0 width:rightLbWidth fontSize:15 color:[UIColor describeColor_66] text:@""];
        _rightLb.textAlignment = NSTextAlignmentRight;
        [_rightLb setCenterY:_leftLb.centerY];
        [_bgView addSubview:_rightLb];
        
        _fieldView = [[UIView alloc] initWithFrame:CGRectMake(leftPadding, 5, bgWidth - 5 - leftPadding, 36)];
        _fieldView.layer.cornerRadius = 8;
        _fieldView.layer.masksToBounds = YES;
        _fieldView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_fieldView];
        
        _mTextField = [self makeTextField:CGRectMake(5, 0, CGRectGetWidth(_fieldView.frame) - 10, 36)];
        [_fieldView addSubview:_mTextField];
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, Screen_Width, 1)];
        _lineView.backgroundColor = [UIColor borderColor];
        [self.contentView addSubview:_lineView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _bgView.layer.shadowOffset =  CGSizeMake(0, 6);
    _bgView.layer.shadowOpacity = 0.3;
    _bgView.layer.shadowColor =  UIColorFromRGB(0x1c63ff).CGColor;
    
    _bgView.backgroundColor = UIColorFromRGB(0x1c63ff);
    _leftLb.textColor = [UIColor whiteColor];
    _fieldView.backgroundColor = UIColorFromRGB(0x4c84ff);
    _mTextField.textColor = [UIColor whiteColor];
    _rightLb.hidden = YES;
    _rightIcon.hidden = YES;
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    _bgView.layer.shadowOffset =  CGSizeMake(0, 0);
    _bgView.layer.shadowOpacity = 0;
    _bgView.layer.shadowColor =  [UIColor clearColor].CGColor;
    
    _bgView.backgroundColor = [UIColor clearColor];
    _leftLb.textColor = [UIColor titleColor_26];
    _fieldView.backgroundColor = [UIColor clearColor];
    _mTextField.textColor = [UIColor clearColor];
    _rightLb.hidden = NO;
    _rightLb.text = textField.text;
    _rightIcon.hidden = NO;
    
    if (_inputBlock) {
        _inputBlock(textField.text);
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (UITextField *) makeTextField:(CGRect)frame
{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.tintColor = [UIColor whiteColor];
    
    //[textField setAttributedPlaceholder:<#(NSAttributedString * _Nullable)#>];ios10
    [textField setValue:UIColorFromRGBA(0xffffff, 0.5) forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [UIColor whiteColor];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;  //垂直居中
    textField.font = [UIFont systemFontOfSize:14];
    
    textField.backgroundColor = [UIColor clearColor];
    textField.autocorrectionType = UITextAutocorrectionTypeNo;    // no auto correction support
    textField.keyboardType = UIKeyboardTypeDefault;
    
    textField.returnKeyType = UIReturnKeyDone;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;    // has a clear 'x' button to the right
    
    [textField setAccessibilityLabel:@"textField"];
    
    //textField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    textField.text = @"";
    textField.delegate = self;    // let us be the delegate so we know when the keyboard's "Done" button is pressed
    
    return textField;
}

@end
