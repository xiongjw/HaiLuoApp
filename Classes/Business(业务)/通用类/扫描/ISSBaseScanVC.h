//
//  ISSBaseScanVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ScanSuccessResultBlock)(NSDictionary *scanInfo);

@interface ISSBaseScanVC : ISSBaseVC

@property (nonatomic,  copy) ScanSuccessResultBlock resultBlock;
// 0 作业详情， 1、检维
@property (nonatomic,assign) NSInteger fromType;
// 对于作业来说，传一个正确的，看看扫的对不对
@property (nonatomic,  copy) NSString *originalCode;

@end
