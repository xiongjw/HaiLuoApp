//
//  ISSBaseScanVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseScanVC.h"

#import "SGQRCode.h"
#import "ISSScanSuccessView.h"

#import "ISSInfoMainVC.h"

@interface ISSBaseScanVC () <SGQRCodeScanManagerDelegate>
{
    BOOL _isRequest;
}

@property (nonatomic, strong) SGQRCodeScanManager *manager;
@property (nonatomic, strong) SGQRCodeScanningView *scanningView;
@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, strong) UIView *bottomView;

@end

@implementation ISSBaseScanVC

- (void)showSuccessView:(NSString *)codeInfo
{
    WeakSelf(self)
    ISSScanSuccessView *view = [[ISSScanSuccessView alloc] initWithCodeInfo:codeInfo block:^{
        if (_resultBlock) {
            _resultBlock(nil);
        }
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
    [view showInView:self.view.window];
}

- (void)checkAuth
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device) {
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (granted) {
                    [self startRunning];
                } else {
                    NSString *message = LoadLanguagesKey(@"scan_auth_alert");
                    
                    UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:message preferredStyle:UIAlertControllerStyleAlert];
                    // 响应方法-相册
                    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_ok") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    // 添加响应方式
                    [actionSheetController addAction:confirmAction];
                    // 显示
                    [self presentViewController:actionSheetController animated:YES completion:nil];
                    
                }
            });
        }];
        return;
    }
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:LoadLanguagesKey(@"scan_noCamera") preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *alertA = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_confirm") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertC addAction:alertA];
    [self presentViewController:alertC animated:YES completion:nil];
    
}

- (void)startRunning
{
    if (!_manager) {
        [self setupQRCodeScanning];
    }
    [self.scanningView addTimer];
    [self performSelector:@selector(managerStartRunning) withObject:nil afterDelay:.3];
}

- (void)managerStartRunning
{
    [_manager startRunning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:LoadLanguagesKey(@"scan_title") showBackBtn:YES];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view addSubview:self.scanningView];
    [self.view addSubview:self.promptLabel];
    [self.view addSubview:self.bottomView];
    [self checkAuth];
    
    _isRequest = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.scanningView removeTimer];
}

- (void)dealloc {
    NSLog(@"WBQRCodeScanningVC - dealloc");
    [self removeScanningView];
}

- (SGQRCodeScanningView *)scanningView
{
    if (!_scanningView) {
        _scanningView = [[SGQRCodeScanningView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        _scanningView.scanningImageName = @"SGQRCode.bundle/QRCodeScanningLineGrid";
        _scanningView.scanningAnimationStyle = ScanningAnimationStyleGrid;
        _scanningView.cornerColor = [UIColor orangeColor];
    }
    return _scanningView;
}
- (void)removeScanningView {
    [self.scanningView removeTimer];
    [self.scanningView removeFromSuperview];
    self.scanningView = nil;
}

- (void)setupQRCodeScanning
{
    self.manager = [SGQRCodeScanManager sharedManager];
    NSArray *arr = @[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    // AVCaptureSessionPreset1920x1080 推荐使用，对于小型的二维码读取率较高
    [_manager setupSessionPreset:AVCaptureSessionPreset1920x1080 metadataObjectTypes:arr currentController:self];
    [_manager cancelSampleBufferDelegate];
    _manager.delegate = self;
}

#pragma mark - - - SGQRCodeScanManagerDelegate
- (void)QRCodeScanManager:(SGQRCodeScanManager *)scanManager didOutputMetadataObjects:(NSArray *)metadataObjects {
    NSLog(@"metadataObjects - - %@", metadataObjects);
    if (metadataObjects != nil && metadataObjects.count > 0) {
        [scanManager playSoundName:@"SGQRCode.bundle/sound.caf"];
        [scanManager stopRunning];
        [self.scanningView removeTimer];
        
        AVMetadataMachineReadableCodeObject *obj = metadataObjects[0];
        NSString *codeInfo = [obj stringValue];
        if (self.fromType == 0) {
            if ([codeInfo isEqualToString:self.originalCode]) {
                [self showSuccessView:codeInfo];
            }
            else
            {
                // 提示，扫描的二维码有误，请扫描正确的
                WeakSelf(self)
                [self showAlertControllerWithMessage:LoadLanguagesKey(@"scan_scanRight") handler:^(UIAlertAction *action) {
                    //重新开启扫描
                    //[weakself performSelector:@selector(startRunning) withObject:nil afterDelay:5];
                    //[weakself performSelector:@selector(goBack) withObject:nil afterDelay:2];
                    [weakself goBack];
                }];
            }
        }
        else
        {
            [self queryDescription:codeInfo];
        }
        
    } else {
        NSLog(@"暂未识别出扫描的二维码");
    }
}

- (UILabel *)promptLabel
{
    if (!_promptLabel) {
        
        _promptLabel = [[UILabel alloc] init];
        _promptLabel.backgroundColor = [UIColor clearColor];
        _promptLabel.frame = CGRectMake(15, CGRectGetMaxY(_scanningView.contentView.frame) + 20, self.view.frame.size.width - 30, 36);
        _promptLabel.textAlignment = NSTextAlignmentCenter;
        _promptLabel.font = [UIFont boldSystemFontOfSize:15.0];
        //_promptLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _promptLabel.textColor = [UIColor whiteColor];
        _promptLabel.text = LoadLanguagesKey(@"scan_alert");
        _promptLabel.numberOfLines = 2;
    }
    return _promptLabel;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, Screen_Height - 64 - 90, Screen_Width, 90)];
        _bottomView.backgroundColor = [UIColor themeColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"job_detail_scan"]];
        [imageView setCenterX:_bottomView.centerX];
        [imageView setTop:20];
        [_bottomView addSubview:imageView];
        
        UILabel *lb = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(imageView.frame) + 15 width:Screen_Width fontSize:15 color:[UIColor whiteColor] text:LoadLanguagesKey(@"scan_title")];
        lb.font = [UIFont boldSystemFontOfSize:15];
        lb.textAlignment = NSTextAlignmentCenter;
        [_bottomView addSubview:lb];
    }
    return _bottomView;
}

- (void)queryDescription:(NSString *)codeInfo
{
    if (_isRequest) {
        return;
    }
    _isRequest = YES;
    // 请求接口判断，二维码是否合格
    NSDictionary *data = @{@"equipId":codeInfo,@"location":[self getAddress]};
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"device/currentExamine" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        _isRequest = NO;
        // 进入维检详情
        if (_resultBlock) {
            _resultBlock(resultData[@"data"]);
        }
        [weakself.navigationController popViewControllerAnimated:NO];
    } failureBlock:^(NSDictionary *resultData) {
        
        _isRequest = NO;
        //重新开启扫描
        //[weakself performSelector:@selector(startRunning) withObject:nil afterDelay:5];
        [weakself performSelector:@selector(goBack) withObject:nil afterDelay:2];
    }];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)getAddress
{
    NSString *str = @"未知地点";
    UINavigationController *nav = self.tabBarController.viewControllers[0];
    for (UIViewController *vc in nav.viewControllers)
    {
        if ([vc isKindOfClass:[ISSInfoMainVC class]]) {
            ISSInfoMainVC *infoVC = (ISSInfoMainVC *)vc;
            str = infoVC.addressInfo;
            break;
        }
    }
#if TARGET_IPHONE_SIMULATOR
    str = @"湖北省武汉市江夏区高新大道999号";
#endif
    return str;
}
@end
