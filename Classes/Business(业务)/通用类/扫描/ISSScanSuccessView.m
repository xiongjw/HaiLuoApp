//
//  ISSScanSuccessView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSScanSuccessView.h"

@implementation ISSScanSuccessView

- (instancetype)initWithCodeInfo:(NSString *)codeInfo block:(ScanSuccessClickBlock)block
{
    self = [super initWithFrame:ScreenFrame_ISS];
    if (self) {
        
        _clickBlock = block;
        
        self.backgroundColor = [UIColor themeColor];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, Screen_Height/4, Screen_Width - 30, 180)];
        view.backgroundColor = [UIColor whiteColor];
        view.layer.cornerRadius = 8;
        view.layer.masksToBounds = YES;
        [self addSubview:view];
        
        UIImageView *rightIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 45, 24, 24)];
        rightIcon.layer.cornerRadius = CGRectGetHeight(rightIcon.frame)/2;
        rightIcon.layer.masksToBounds = YES;
        rightIcon.backgroundColor = UIColorFromRGB(0x18da38);
        rightIcon.contentMode = UIViewContentModeCenter;
        rightIcon.image = [UIImage imageNamed:@"job_detail_scansuccess"];
        [view addSubview:rightIcon];
        
        UILabel *lb1 = [UILabel oneLineLbWithX:0 y:45 fontSize:15 color:[UIColor titleColor_26] text:@"扫码成功！"];
        [lb1 setCenterY:rightIcon.centerY];
        [lb1 setCenterX:view.centerX];
        [view addSubview:lb1];
        
        [rightIcon setLeft:CGRectGetMinX(lb1.frame) - 15 - 24];
        
        UILabel *lb2 = [UILabel oneLineLbWithX:0 y:105 width:CGRectGetWidth(view.frame) fontSize:15 color:[UIColor describeColor_9a] text:@"设备名称编号"];
        lb2.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lb2];
        
        UILabel *lb3 = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(lb2.frame) + 15 width:CGRectGetWidth(view.frame) fontSize:15 color:[UIColor describeColor_9a] text:codeInfo];
        lb3.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lb3];
        
        X_Button *btn = [ISSCreateUIHelper createInputSubmitBtnWithTitle:@"确定" posY:Screen_Height*3/4];
        [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
    return self;
}

- (void)showInView:(UIView *)aView
{
    [aView addSubview:self];
    
    self.alpha = 0;
    [UIView animateWithDuration:.3 animations:^{
        self.alpha = 1;
    }];
}

- (void)dismiss
{
    
    self.alpha = 1;
    [UIView animateWithDuration:.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (_clickBlock) {
            _clickBlock();
        }
        [self removeFromSuperview];
    }];
}

@end
