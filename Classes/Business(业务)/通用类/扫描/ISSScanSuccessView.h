//
//  ISSScanSuccessView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ScanSuccessClickBlock)(void);

@interface ISSScanSuccessView : UIView

@property (nonatomic, copy) ScanSuccessClickBlock clickBlock;

- (instancetype)initWithCodeInfo:(NSString *)codeInfo block:(ScanSuccessClickBlock)block;

- (void)showInView:(UIView *)aView;

@end
