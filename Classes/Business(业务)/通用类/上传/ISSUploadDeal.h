//
//  ISSUploadDeal.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/25.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ISSUploadImageBlock) (NSString *photoUrls);

@interface ISSUploadDeal : NSObject

+ (ISSUploadDeal *)sharedInstance;

- (void)uploadImage:(UIImage *)image view:(UIView *)view successBlock:(ISSUploadImageBlock)successBlock;
- (void)uploadImages:(NSArray *)images view:(UIView *)view successBlock:(ISSUploadImageBlock)successBlock;

@end
