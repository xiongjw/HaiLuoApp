//
//  ISSUploadDeal.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/25.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSUploadDeal.h"

@interface ISSUploadDeal()
{
    NSInteger _uploadIndex;
    NSArray *_imageList;
    
    NSMutableString *_uploadImageStrs;
}

@end

@implementation ISSUploadDeal

+ (ISSUploadDeal *)sharedInstance
{
    static ISSUploadDeal *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[ISSUploadDeal alloc] init];
    });
    return instance;
}

- (void)uploadImage:(UIImage *)image view:(UIView *)view block:(void (^)(NSString *photoUrl))block
{
    [X_HttpUtil apiRequest:@"upload/img" param:@{} view:view image:image successBlock:^(NSDictionary *resultData) {
        
        NSString *urlString = resultData[@"mess"];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        if (block) {
            block(urlString);
        }
    } failureBlock:NULL];
}

- (void)uploadImage:(UIImage *)image view:(UIView *)view successBlock:(ISSUploadImageBlock)successBlock
{
    [self uploadImage:image view:view block:successBlock];
}

- (void)uploadImages:(NSArray *)images view:(UIView *)view successBlock:(ISSUploadImageBlock)successBlock
{
    _imageList = images;
    _uploadIndex = 0;
    _uploadImageStrs = [NSMutableString new];
    [self uploadImages:view block:^{
        if (successBlock) {
            successBlock(_uploadImageStrs);
        }
    }];
}

- (void)uploadImages:(UIView *)view block:(void (^)(void))block
{
    UIImage *image = _imageList[_uploadIndex];
    
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"upload/img" param:@{} view:view image:image successBlock:^(NSDictionary *resultData) {
        
        _uploadIndex += 1;
        
        NSString *urlString = resultData[@"mess"];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        if (_uploadIndex == _imageList.count) {
            [_uploadImageStrs appendFormat:@"%@",urlString];
            if (block) {
                block();
            }
        }
        else
        {
            [_uploadImageStrs appendFormat:@"%@,",urlString];
            [weakself uploadImages:view block:block];
        }
    } failureBlock:NULL];
}

@end
