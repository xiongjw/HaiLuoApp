//
//  ISSTestRefreshVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/9.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSTestRefreshVC.h"

/**
 * 随机数据
 */
#define MJRandomData [NSString stringWithFormat:@"随机数据---%d", arc4random_uniform(1000000)]

@interface ISSTestRefreshVC ()
{
    NSInteger time;
}

@end

@implementation ISSTestRefreshVC

- (NSArray *)getList
{
    // 1.添加假数据
    NSMutableArray *list = [NSMutableArray new];
    NSInteger n = 0;
    if (time == 0) n = [PageSize integerValue];
    else if (time == 1) n = [PageSize integerValue];
    else n = [PageSize integerValue]/2;
    for (NSInteger i = 0; i < n; i++) {
        [list addObject:MJRandomData];
    }
    return list.copy;
}

- (void)addData:(NSString *)direction
{
    if ([@"down" isEqualToString:direction]) {
        time = 0;
        
        if (self.mTableView.mj_footer.hidden) {
            self.mTableView.mj_footer.hidden = NO;
            [self.mTableView.mj_footer resetNoMoreData];
        }
        [self.resultDataList removeAllObjects];
    }
    else time++;
    
    NSArray *list = [self getList];
    if (list.count < [PageSize integerValue])
    {   self.mTableView.mj_footer.hidden = YES;
        [self.mTableView.mj_footer endRefreshingWithNoMoreData];
    }
    
    if (list.count > 0)
    {
        [self.resultDataList addObjectsFromArray:list];
        [self.mTableView reloadData];
    }
}

- (void)requestDataReresh:(NSString *)direction
{
    WeakSelf(self)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //
        [weakself endRefreshing];
        [weakself addData:direction];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"test 刷新" showBackBtn:YES];
    time = 0;//模拟加载次数
    
    [self addHeaderAndFooterAction];
    //[self headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self headerBeginRefreshing];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        //cell.textLabel.textColor = [UIColor describeColor_33];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.resultDataList[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
