//
//  ISSTestPickerVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSTestPickerVC.h"

#import "X_PopPickerView.h"
#import "ISSPickerModel.h"

@interface ISSTestPickerVC ()<UITextFieldDelegate,X_PopPickerViewDelegate>
{
    ISSPickerModel *_dataModel;
    ISSPickerModel *_dateModel;
}

@end

@implementation ISSTestPickerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"选择器" showBackBtn:YES];
    //数据
    UITextField *dataField = [[UITextField alloc] initWithFrame:CGRectMake(30, 100, Screen_Width - 60, 40)];
    dataField.borderStyle = UITextBorderStyleRoundedRect;
    dataField.tag = 1;
    dataField.delegate = self;
    [self.view addSubview:dataField];
    
    _dataModel = [[ISSPickerModel alloc] init];
    _dataModel.type = 1;
    _dataModel.dataSoure = @[
                             @{@"name":@"身份证",@"code":@"1"},
                             @{@"name":@"军官证",@"code":@"2"}
                             ];
    
    //时间
    UITextField *dateField = [[UITextField alloc] initWithFrame:CGRectMake(30, 200, Screen_Width - 60, 40)];
    dateField.borderStyle = UITextBorderStyleRoundedRect;
    dateField.tag = 2;
    dateField.delegate = self;
    [self.view addSubview:dateField];
    
    _dateModel = [[ISSPickerModel alloc] init];
    _dateModel.type = 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 1)
    {
        X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:_dataModel];
        view.pickerTag = 1;
        [view showInView:self.view.window];
    }
    else
    {
        X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:_dateModel];
        view.pickerTag = 2;
        [view showInView:self.view.window];
    }
    return NO;
}

- (void)actionPopView:(NSDictionary *)item pickerTag:(NSInteger)pickerTag
{
    if (pickerTag == 1) {
        _dataModel.value = item[_dataModel.codeKey];
        _dataModel.showValue = item[_dataModel.nameKey];
        UITextField *field = ViewWithTag(self.view, pickerTag);
        field.text = _dataModel.showValue;
    }
    else if (pickerTag == 2)
    {
        _dateModel.value = item[_dataModel.codeKey];
        _dateModel.showValue = item[_dataModel.nameKey];
        UITextField *field = ViewWithTag(self.view, pickerTag);
        field.text = _dateModel.showValue;
    }
}

@end
