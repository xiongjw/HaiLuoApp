//
//  ISSBaseSearchVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ISSSearch_Info = 0, //信息
    ISSSearch_Safe = 1, //信息
    ISSSearch_Job = 2, //作业
    ISSSearch_Check = 3 //维检
} ISSSearchType;


typedef void (^GoSeachClickBlock)(NSDictionary *cateItem ,NSString *searchText);

@interface ISSBaseSearchVC : ISSBaseVC

@property (nonatomic) ISSSearchType searchType;//搜索类型
@property (nonatomic,strong) NSArray *cateList;//搜索分类，最好是list<Dictionary>模式

@property (nonatomic,copy) GoSeachClickBlock clickBlock;
/** eg
 NSArray *cateList = @[
 @{@"name":@"全部",@"code":@"allJob",@"cacheKey":@"Job_searchAllJob"},
 @{@"name":@"危险作业",@"code":@"dangerJob",@"cacheKey":@"Job_searchDangerJob"},
 ];
 */

@end
