//
//  ISSBaseSearchVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseSearchVC.h"

#import "ISSSeachSegScrollView.h"

@interface ISSBaseSearchVC () <UIScrollViewDelegate>
{
    ISSSeachSegScrollView *_segScrollView;
    NSInteger _currentSegIndex;
}
@property (nonatomic,strong) UITextField *searchField;
@property (nonatomic,strong) NSMutableArray *resultDataList;
@property (nonatomic,strong) UIScrollView *mScrollView;

@end

@implementation ISSBaseSearchVC

#pragma mark - 搜索有关
- (void)beginSearch:(NSString *)searchText
{
    [self saveSearchText:searchText];
    
    [self dismissViewControllerAnimated:NO completion:^{
        if (_clickBlock) {
            _clickBlock(self.cateList[_currentSegIndex],searchText);
        }
    }];
}

- (void)searchAction:(X_Button *)btn
{
    [self.searchField resignFirstResponder];
    [self beginSearch:btn.titleLabel.text];
}

- (void)saveSearchText:(NSString *)searchText
{
    [self.view endEditing:YES];
    
    NSMutableArray *currentList = self.resultDataList[_currentSegIndex];
    //判断是否搜索过
    for (NSString *str in currentList) {
        if ([searchText isEqualToString:str]) {
            [currentList removeObject:searchText];//新处理，将重复的位置靠前
            break;
        }
    }
    
    [currentList insertObject:searchText atIndex:0];
    
    NSInteger maxResult = 10;//最多存10个
    if (currentList.count > maxResult) {
        [currentList removeLastObject];
    }
    [ISSDataStorage saveObject:currentList key:self.cateList[_currentSegIndex][@"cacheKey"]];
    
    //重新绘制
}

- (void)deleteAll
{
    NSMutableArray *searchList = self.resultDataList[_currentSegIndex];
    [searchList removeAllObjects];
    
    [ISSDataStorage removeObject:self.cateList[_currentSegIndex][@"cacheKey"]];
    
    [self addSubViewToScrollView:nil];
}

- (void)deleteAction
{
    NSMutableArray *searchList = self.resultDataList[_currentSegIndex];
    if (searchList.count == 0) {
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:LoadLanguagesKey(@"search_deleteAll") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_confirm") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              //响应事件
                                                              [self deleteAll];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_cancel") style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             //响应事件
                                                         }];
    
    [alertController addAction:defaultAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)createTopSearchView
{
    CGRect frame = CGRectMake(0, 7, Screen_Width - 50, 44 - 14);
    UIView *searchFieldView = [[UIView alloc] initWithFrame:frame];
    searchFieldView.backgroundColor = UIColorFromRGB(0xefefef);
    searchFieldView.layer.cornerRadius = CGRectGetHeight(frame)/2;
    searchFieldView.layer.masksToBounds = YES;
    
    self.searchField = [ISSCreateUIHelper makeTextField:self withFrame:CGRectMake(15, 0, frame.size.width - 30, frame.size.height) andPlaceholder:LoadLanguagesKey(@"search_key") maxLength:12];
    self.searchField.returnKeyType = UIReturnKeySearch;
    [searchFieldView addSubview:self.searchField];
    self.navigationItem.titleView = searchFieldView;
}

#pragma mark - 滑动切换有关
- (void)dealScrollFinish
{
    //NSDictionary *item = self.cateList[_currentSegIndex];
}

- (void)dealClickTopBtn:(NSInteger)index
{
    _currentSegIndex = index;
    
    CGRect frame = self.mScrollView.frame;
    frame.origin.x = _currentSegIndex * Screen_Width;
    
    [self.mScrollView scrollRectToVisible:frame animated:YES];
    
    [self dealScrollFinish];
}

- (void)addSubViewToScrollView:(UIScrollView *)scrollView
{
    NSDictionary *item = nil;
    if (!scrollView) {
        scrollView = [self.mScrollView viewWithTag:100 + _currentSegIndex];
        [scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        item = self.cateList[_currentSegIndex];
    }
    else
    {
        item = self.cateList[scrollView.tag - 100];
    }
    
    UILabel *historyLb = [UILabel oneLineLbWithX:15 y:15 fontSize:14 color:UIColorFromRGB(0x757575) text:LoadLanguagesKey(@"search_job_history")];
    [scrollView addSubview:historyLb];
    
    UIImage *deleteImage = [UIImage imageNamed:@"search_delete"];
    X_Button *deleteBtn = [X_Button buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(Screen_Width - (30 + deleteImage.size.height), 0, 30 + deleteImage.size.width, 30 + deleteImage.size.height);
    [deleteBtn setImage:deleteImage forState:UIControlStateNormal];
    [deleteBtn setCenterY:historyLb.centerY];
    [deleteBtn addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:deleteBtn];
    
    NSMutableArray *searchList = [NSMutableArray new];
    NSArray *cacheList = [ISSDataStorage getObject:item[@"cacheKey"]];
    if (cacheList.count > 0) {
        [searchList addObjectsFromArray:cacheList];
    }
    [self.resultDataList addObject:searchList];
    
    if (searchList.count > 0)
    {
        X_Button *searchBtn = nil;
        CGFloat posX = 10;
        CGFloat posY = CGRectGetMaxY(historyLb.frame) + 15;
        for (int i = 0; i < searchList.count; i++)
        {
            NSString *searchStr = searchList[i];
            CGSize searchSize = [searchStr sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}];
            
            if (posX + searchSize.width + 10 > Screen_Width) {
                posX = 10;
                posY = CGRectGetMaxY(searchBtn.frame) + 10;
            }
            searchBtn = [X_Button buttonWithType:UIButtonTypeCustom];
            searchBtn.backgroundColor = RGB(245, 245, 245);
            searchBtn.frame = CGRectMake(posX, posY, searchSize.width + 30, 30);
            searchBtn.layer.cornerRadius = CGRectGetHeight(searchBtn.frame)/2;
            searchBtn.layer.masksToBounds = YES;
            searchBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            [searchBtn setTitleColor:[UIColor describeColor_61] forState:UIControlStateNormal];
            [searchBtn setTitle:searchStr forState:UIControlStateNormal];
            [searchBtn addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:searchBtn];
            
            posX = CGRectGetMaxX(searchBtn.frame) + 10;
        }
        
        [scrollView setContentSize:CGSizeMake(Screen_Width, CGRectGetMaxY(searchBtn.frame) + 15)];
    }
    else
    {
        UILabel *emptyLb = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(historyLb.frame) + 50 width:Screen_Width fontSize:14 color:[UIColor lightGrayColor] text:LoadLanguagesKey(@"search_empty")];
        emptyLb.textAlignment = NSTextAlignmentCenter;
        [scrollView addSubview:emptyLb];
        
        [scrollView setContentSize:CGSizeMake(Screen_Width, CGRectGetMaxY(emptyLb.frame) + 50)];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"" showBackBtn:YES];
    
    [self createTopSearchView];
    
    WeakSelf(self)
    self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItemTitle:LoadLanguagesKey(@"btn_cancel") titleColor:[UIColor describeColor_61] titleFont:[UIFont systemFontOfSize:14] clickBlock:^{
        [weakself dismissViewControllerAnimated:NO completion:NULL];
    }];
    
    self.resultDataList = [NSMutableArray new];
    
    _currentSegIndex = 0;
    if (self.cateList.count > 1)
    {
        CGFloat segViewHeight = 44;
        
        _segScrollView = [[ISSSeachSegScrollView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, segViewHeight) cateList:self.cateList clickBlock:^(NSInteger index) {
            [weakself dealClickTopBtn:index];
        }];
        [self.view addSubview:_segScrollView];
        
        CGRect frame = CGRectMake(0, segViewHeight + 5, Screen_Width, Screen_Height - 64  - (segViewHeight + 5));
        self.mScrollView = [[UIScrollView alloc] initWithFrame:frame];
        self.mScrollView.delegate = self;
        self.mScrollView.pagingEnabled = YES;
        self.mScrollView.showsHorizontalScrollIndicator = NO;
        self.mScrollView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:self.mScrollView];
        
        for (int i = 0; i < self.cateList.count; i++)
        {
            UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(Screen_Width * i, 0, Screen_Width, frame.size.height)];
            scrollView.backgroundColor = [UIColor whiteColor];
            scrollView.showsHorizontalScrollIndicator = NO;
            //scrollView.showsVerticalScrollIndicator = NO;
            scrollView.tag = 100 + i;
            [self.mScrollView addSubview:scrollView];
            
            [self addSubViewToScrollView:scrollView];
        }
        [self.mScrollView setContentSize:CGSizeMake(Screen_Width *self.cateList.count, frame.size.height)];
    }
    else
    {
        CGRect frame = CGRectMake(0, 5, Screen_Width, Screen_Height - 64  - 5);
        self.mScrollView = [[UIScrollView alloc] initWithFrame:frame];
        self.mScrollView.backgroundColor = [UIColor whiteColor];
        self.mScrollView.delegate = self;
        self.mScrollView.pagingEnabled = YES;
        self.mScrollView.showsHorizontalScrollIndicator = NO;
        self.mScrollView.showsVerticalScrollIndicator = NO;
        self.mScrollView.tag = 100;
        [self.view addSubview:self.mScrollView];
        
        [self addSubViewToScrollView:self.mScrollView];
        
        [self.mScrollView setContentSize:CGSizeMake(Screen_Width, frame.size.height)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.searchField becomeFirstResponder];
}

#pragma mark - UIScrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mScrollView)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        _currentSegIndex = page;
        
        [_segScrollView dealMoveLineFinish:_currentSegIndex];
        
        [self dealScrollFinish];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField.text > 0) {
        [self beginSearch:textField.text];
    }
    return YES;
}

@end
