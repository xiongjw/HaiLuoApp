//
//  ISSSearchResultListVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSSearchResultListVC.h"
//信息
#import "MessageCellModel.h"
#import "ListViewTableViewCell.h"
#import "ListCellViewController.h"
//安全
#import "IssSafeListModel.h"
//作业
#import "ISSJobListCell.h"
#import "ISSJobDetailVC.h"

//维检
#import "ISSCheckMaintainListCell.h"
#import "ISSCheckMaintainDetailVC.h"

#import "ISSFMDB.h"

@interface ISSSearchResultListVC ()

@end

@implementation ISSSearchResultListVC

- (void)requestDataReresh:(NSString *)direction
{
    WeakSelf(self)
    if (self.searchType == ISSSearch_Info)
    {
        NSDictionary *data = @{
                               @"type":self.searchItem[@"code"],
                               @"title":self.searchText,
                               @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                               };
        [X_HttpUtil apiRequest:@"info/searchInfos" param:data view:nil successBlock:^(NSDictionary *resultData) {
            
            [weakself addResultDataToList:resultData[@"data"][@"list"] direction:direction];
        } failureBlock:^(NSDictionary *resultData) {
            [weakself endRefreshing];
        }];
    }
    else if (self.searchType == ISSSearch_Safe)
    {
        NSDictionary *data = @{
                               @"type":self.searchItem[@"code"],
                               @"condition":self.searchText,
                               @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                               };
        [X_HttpUtil apiRequest:@"safety/conditionsearch" param:data view:nil successBlock:^(NSDictionary *resultData) {
            
            [weakself addResultDataToList:resultData[@"data"][@"list"] direction:direction];
        } failureBlock:^(NSDictionary *resultData) {
            [weakself endRefreshing];
        }];
    }
    else if (self.searchType == ISSSearch_Job)
    {
        NSDictionary *data = @{
                               @"type":@([self.searchItem[@"code"] intValue]),
                               @"keyWord":self.searchText,
                               @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                               };
        [X_HttpUtil apiRequest:@"job/searchWork" param:data view:nil successBlock:^(NSDictionary *resultData) {
            
            [weakself addResultDataToList:resultData[@"data"][@"list"] direction:direction];
        } failureBlock:^(NSDictionary *resultData) {
            [weakself endRefreshing];
        }];
    }
    else if (self.searchType == ISSSearch_Check)
    {
        NSDictionary *data = @{
                               @"condition":self.searchText,
                               @"lastId":[@"down" isEqualToString:direction] ? @"" : [self.resultDataList lastObject][@"id"]
                               };
        [X_HttpUtil apiRequest:@"device/conditionSearch" param:data view:nil successBlock:^(NSDictionary *resultData) {
            
            [weakself addResultDataToList:resultData[@"data"][@"list"] direction:direction];
        } failureBlock:^(NSDictionary *resultData) {
            [weakself endRefreshing];
        }];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:LoadLanguagesKey(@"search_resultList") showBackBtn:YES];
    
    [self addHeaderAndFooterAction];
    
    [self headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchType == ISSSearch_Info)
    {
        NSArray *newList = [MessageCellModel arrayOfModelsFromDictionaries:self.resultDataList error:nil];
        MessageCellModel *model = newList[indexPath.row];
        
        static NSString *CellIdentifier = @"ListViewTableViewCell1";
        ListViewTableViewCell *listInfoCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(listInfoCell==nil){
            
            listInfoCell = [[ListViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ListCell"];
            listInfoCell.layer.cornerRadius = 8;
        }
        [listInfoCell configListDataWithModel:model];
        return listInfoCell;
    }
    else if (self.searchType == ISSSearch_Safe)
    {
        NSArray *newList = [IssSafeListModel arrayOfModelsFromDictionaries:self.resultDataList error:nil];
        IssSafeListModel *model = newList[indexPath.row];
        
        static NSString *CellIdentifier = @"ListViewTableViewCell2";
        ListViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ListViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.layer.cornerRadius = 8;
        }
        [cell configListDataWithSafeModel:model];
        return cell;
    }
    else if (self.searchType == ISSSearch_Job)
    {
        static NSString *CellIdentifier = @"ISSJobListCell";
        ISSJobListCell *cell = (ISSJobListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSJobListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [cell layoutCell:self.resultDataList[indexPath.row]];
        return cell;
    }
    else if (self.searchType == ISSSearch_Check)
    {
        static NSString *CellIdentifier = @"ISSCheckMaintainListCell";
        ISSCheckMaintainListCell *cell = (ISSCheckMaintainListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSCheckMaintainListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [cell layoutCell:self.resultDataList[indexPath.row]];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchType == ISSSearch_Info) return 130;
    else if (self.searchType == ISSSearch_Safe) return 130;
    else if (self.searchType == ISSSearch_Job) return 120;
    else if (self.searchType == ISSSearch_Check) return 110;
    else return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchType == ISSSearch_Info)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        // 判断有没有查看权限
        if (![ISSUserData sharedInstance].operation_privilege_yh_detailView) {
            [self.view makeToast:LoadLanguagesKey(@"operation_privilege_yh_detailView")];
            return;
        }
        MessageCellModel *model = [MessageCellModel arrayOfModelsFromDictionaries:self.resultDataList error:nil][indexPath.row];
        
        ListViewTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (!cell.noReadFlag.hidden) {
            cell.noReadFlag.hidden = YES;
            NSDictionary *data = @{@"yh":@"1",@"wz":@"2",@"aq":@"3"};
            [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:data[model.type]];
            
            // 让其它页面也刷新
            [[NSNotificationCenter defaultCenter] postNotificationName:@"readPushMsgSuccessNote" object:nil];
        }
        
        ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
        listCellVC.hidesBottomBarWhenPushed = YES;
        listCellVC.itemId = model.id;
        listCellVC.itemType = model.type;
        listCellVC.comeType = 100;
        [self.navigationController pushViewController:listCellVC animated:YES];
    }
    else if (self.searchType == ISSSearch_Safe)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        // 判断有没有查看权限
        if (![ISSUserData sharedInstance].operation_privilege_yh_detailView) {
            [self.view makeToast:LoadLanguagesKey(@"operation_privilege_yh_detailView")];
            return;
        }
        IssSafeListModel *model =  [IssSafeListModel arrayOfModelsFromDictionaries:self.resultDataList error:nil][indexPath.row];
        
        ListViewTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (!cell.noReadFlag.hidden) {
            cell.noReadFlag.hidden = YES;
            
            if ([@"yh" isEqualToString:model.yhlxbm])
                [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:@"1"];
            else if ([@"wz" isEqualToString:model.yhlxbm])
                [[ISSFMDBUtil sharedInstance] deletePushMsgById:model.id type:@"2"];
            
            // 让其它页面也刷新
            [[NSNotificationCenter defaultCenter] postNotificationName:@"readPushMsgSuccessNote" object:nil];
        }
        
        ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
        listCellVC.hidesBottomBarWhenPushed = YES;
        listCellVC.itemId = model.id;
        listCellVC.safeModel = model;
        listCellVC.itemType = model.yhlxbm;
        [self.navigationController pushViewController:listCellVC animated:YES];
    }
    else if (self.searchType == ISSSearch_Job)
    {
        NSDictionary *data = self.resultDataList[indexPath.row];
        ISSJobDetailVC *vc = [[ISSJobDetailVC alloc] init];
        vc.jobId = [data[@"id"] longValue];
        vc.jobList = self.jobList;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.searchType == ISSSearch_Check)
    {
        NSDictionary *data = self.resultDataList[indexPath.row];
        ISSCheckMaintainDetailVC *vc = [[ISSCheckMaintainDetailVC alloc] init];
        vc.equipId = data[@"id"];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
