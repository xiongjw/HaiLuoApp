//
//  ISSSearchResultListVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISSBaseSearchVC.h"

@interface ISSSearchResultListVC : ISSMJRefreshTableVC

@property (nonatomic) ISSSearchType searchType;//搜索类型

@property (nonatomic,copy) NSString *searchText;
@property (nonatomic,strong) NSDictionary *searchItem;
//安全所需额外参数
@property (nonatomic,assign) NSInteger mainType;
@property (nonatomic,assign) NSInteger currentSegIndex;
//作业所需额外参数
@property (nonatomic,strong) NSArray *jobList;

@end
