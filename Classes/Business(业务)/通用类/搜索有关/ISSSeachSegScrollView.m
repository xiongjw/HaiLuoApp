//
//  ISSSeachSegScrollView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSSeachSegScrollView.h"

@interface ISSSeachSegScrollView ()
{
    UIView *_animatedLine;
    BOOL _needScroll;
}

@end

@implementation ISSSeachSegScrollView

- (void)clickAction:(X_Button *)btn
{
    if (btn.selected) {
        return;
    }
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[X_Button class]]) {
            ((X_Button *)view).selected = NO;
        }
    }
    
    btn.selected = YES;
    [UIView animateWithDuration:.3 animations:^{
        [_animatedLine setLeft:CGRectGetMinX(btn.frame)];
    }];
    
    if (_clickBlock) {
        _clickBlock(btn.tag - 10);
    }
}

- (instancetype)initWithFrame:(CGRect)frame cateList:(NSArray *)cateList clickBlock:(SeachSegClickBlock)clickBlock
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _needScroll = [self needScroll:cateList];
        _clickBlock = clickBlock;
        
        self.backgroundColor = [UIColor whiteColor];
        self.showsHorizontalScrollIndicator = NO;
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 0.5)];
        line.backgroundColor = [UIColor borderColor];
        [self addSubview:line];
        
        CGFloat padding = 15;
        CGFloat btnWidth = (Screen_Width - padding*(cateList.count + 1))/cateList.count;
        
        CGFloat posX = padding;
        
        X_Button *segBtn = nil;
        for (int i = 0; i < cateList.count; i++)
        {
            if (_needScroll) {
                CGSize size = [cateList[i][@"name"] sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]}];
                btnWidth = size.width + 30;
            }
            segBtn = [X_Button buttonWithType:UIButtonTypeCustom];
            segBtn.frame = CGRectMake(posX, 0, btnWidth, frame.size.height);
            [segBtn setTitleColor:[UIColor describeColor_61] forState:UIControlStateNormal];
            [segBtn setTitleColor:[UIColor themeColor] forState:UIControlStateSelected];
            segBtn.titleLabel.font = [UIFont systemFontOfSize:13];
            segBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [segBtn setTitle:cateList[i][@"name"] forState:UIControlStateNormal];
            segBtn.tag = i + 10;
            [segBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:segBtn];
            
            posX = CGRectGetMaxX(segBtn.frame) + padding;
            
            if (i == 0)
            {
                segBtn.selected = YES;
                
                _animatedLine = [[UIView alloc] initWithFrame:CGRectMake(segBtn.left, frame.size.height - 2, segBtn.width, 2)];
                _animatedLine.backgroundColor = [UIColor themeColor];
                [self addSubview:_animatedLine];
            }
        }
        if (_needScroll) {
            [self setContentSize:CGSizeMake(posX, CGRectGetHeight(self.frame))];
        }
        
    }
    return self;
}

- (void)dealMoveLineFinish:(NSInteger)segIndex
{
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[X_Button class]]) {
            ((X_Button *)view).selected = NO;
        }
    }
    X_Button *btn = [self viewWithTag:10 + segIndex];
    btn.selected = YES;
    [UIView animateWithDuration:.3 animations:^{
        [_animatedLine setLeft:CGRectGetMinX(btn.frame)];
    }];
}


- (BOOL)needScroll:(NSArray *)list
{
    BOOL flag = NO;
    CGFloat padding = 15;
    CGFloat posX = padding;
    for (NSDictionary *data in list)
    {
        CGSize size = [data[@"name"] sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]}];
        posX += (size.width + 30) + padding;
    }
    if (posX > Screen_Width) {
        flag = YES;
    }
    return flag;
}

@end
