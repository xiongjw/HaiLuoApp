//
//  ISSSeachSegScrollView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SeachSegClickBlock)(NSInteger index);

@interface ISSSeachSegScrollView : UIScrollView

@property (nonatomic, copy) SeachSegClickBlock clickBlock;

- (instancetype)initWithFrame:(CGRect)frame cateList:(NSArray *)cateList clickBlock:(SeachSegClickBlock)clickBlock;

- (void)dealMoveLineFinish:(NSInteger)segIndex;

@end
