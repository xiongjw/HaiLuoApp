//
//  ISSFindPwdVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSFindPwdVC.h"

#import "NSString+RegexCheck.h"

#import "ISSPwdStrongView.h"

@interface ISSFindPwdVC ()<UITextFieldDelegate>
{
    NSTimer *_codeTimer;
    long _currentTime;
    
    ISSPwdStrongView *_strongView;
}

@property (nonatomic,strong) X_Button *codeBtn;

//@property (nonatomic,strong) UITextField *emailField;

@end

@implementation ISSFindPwdVC

- (void)Countdown
{
    _currentTime --;
    if (_currentTime == 0)
    {
        _currentTime = 60;
        self.codeBtn.enabled = YES;
        [self.codeBtn setTitle:LoadLanguagesKey(@"forgetPwd_btn_reGetCode") forState:UIControlStateNormal];
        
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
    else
    {
        [self.codeBtn setTitle:[NSString stringWithFormat:@"%ldS",_currentTime] forState:UIControlStateNormal];
    }
}

- (void)codeRequestSuccess
{
    if (_codeTimer) {
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
    _codeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(Countdown) userInfo:nil repeats:YES];
    
    self.codeBtn.enabled = NO;
    [self.codeBtn setTitle:@"60S" forState:UIControlStateNormal];
}

- (void)sendAction
{
    UITextField *loginField = [self.view viewWithTag:100];
    if (loginField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_phone")];
        return;
    }
    if (![loginField.text isValidateMobile]) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_rightPhone")];
        return;
    }
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"getCode" param:@{@"phone":loginField.text} view:self.view.window successBlock:^(NSDictionary *resultData) {
        
        [weakself.view makeToast:LoadLanguagesKey(@"forgetPwd_sendCodeSuccess")];
        [weakself codeRequestSuccess];
    }];
    
}

- (void)confirmAction
{
    UITextField *loginField = [self.view viewWithTag:100];
    if (loginField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_phone")];
        return;
    }
    if (![loginField.text isValidateMobile]) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_rightPhone")];
        return;
    }
    UITextField *codeField = [self.view viewWithTag:101];
    if (codeField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_code")];
        return;
    }
    UITextField *pwdField = [self.view viewWithTag:102];
    UITextField *pwdFieldAgain = [self.view viewWithTag:103];
    if (pwdField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_newPwd")];
        return;
    }
    if (![pwdField.text isValidatePassword]) {
        [self.view makeToast:LoadLanguagesKey(@"login_editPwd_alert2")];//Abc@123456
        return;
    }
    if (![pwdField.text isEqualToString:pwdFieldAgain.text]) {
        [self.view makeToast:LoadLanguagesKey(@"login_editPwd_alert3")];
        return;
    }
    NSDictionary *data = @{
                           @"phone":loginField.text,
                           @"code":codeField.text,
                           @"password":[pwdField.text md5]
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"findPassword" param:data view:self.view.window successBlock:^(NSDictionary *resultData) {
        //[ShareApp setWindowRootVC:1];
        // 看看是重新登录还是怎么
        [weakself showAlertControllerWithMessage:LoadLanguagesKey(@"forgetPwd_btn_findPwdSuccess") handler:^(UIAlertAction *action) {
            if (_findPwdSuccessBlock) {
                _findPwdSuccessBlock();
            }
            [weakself.navigationController popViewControllerAnimated:YES];
        }];
    }];
    
}

- (void)textFieldDidChange:(NSNotification *)note
{
    UITextField *textField = note.object;
    if (textField.tag == 102) {
        [_strongView changeColor:textField.text];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    WeakSelf(self)
    X_Button *backBtn = [ISSNavUtil getImageBtn:6 clickBlock:^{
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
    [backBtn setLeft:15];
    [backBtn setTop:20];
    [self.view addSubview:backBtn];
    
    UILabel *forgetLb = [UILabel oneLineLbWithX:0 y:Screen_Height/3 width:Screen_Width fontSize:18 color:[UIColor describeColor_61] text:LoadLanguagesKey(@"forgetPwd_navTitle")];
    forgetLb.textAlignment = NSTextAlignmentCenter;
    forgetLb.font = [UIFont boldSystemFontOfSize:18];
    [forgetLb setCenterY:42];
    [self.view addSubview:forgetLb];
    
    UIView *fieldView = nil;
    UITextField *textField = nil;
    CGFloat posY = 90;
    NSArray *placeHolders = @[LoadLanguagesKey(@"forgetPwd_input_phone"),
                              LoadLanguagesKey(@"forgetPwd_input_code"),
                              LoadLanguagesKey(@"forgetPwd_input_newPwd"),
                              LoadLanguagesKey(@"forgetPwd_input_newPwd_again")
                              ];
    NSArray *maxLength = @[@"20",@"6",@"16",@"16"];
    for (int i = 0; i < placeHolders.count; i++)
    {
        fieldView = [[UIView alloc] initWithFrame:CGRectMake(35, posY, Screen_Width - 70, 50)];
        fieldView.backgroundColor = RGB(244, 244, 244);
        fieldView.layer.cornerRadius = 5;
        fieldView.layer.masksToBounds = YES;
        [self.view addSubview:fieldView];
        
        textField = [ISSCreateUIHelper makeTextField:self
                                           withFrame:CGRectMake(15, 0, fieldView.frame.size.width - 20, fieldView.frame.size.height)
                                              andTag:i + 100
                                      andPlaceholder:placeHolders[i]
                                           maxLength:[maxLength[i] integerValue]];
        [fieldView addSubview:textField];
        if (i == 0) {
            textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if (i == 1)
        {
            [fieldView setWidth:fieldView.width - 100 - 10];
            textField.keyboardType = UIKeyboardTypeNumberPad;
            [textField setWidth:fieldView.width - 10];
            
            self.codeBtn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"forgetPwd_btn_getCode") posY:0];
            self.codeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            self.codeBtn.frame = CGRectMake(CGRectGetMaxX(fieldView.frame) + 10, CGRectGetMinY(fieldView.frame), 100, fieldView.frame.size.height);
            [self.codeBtn addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.codeBtn];
        }
        else if (i >= 2) textField.secureTextEntry = YES;
    
        posY = CGRectGetMaxY(fieldView.frame) + 15;
        
        if (i == 1)
        {
            UILabel *alertLb = [UILabel mutLineLbWithX:35 y:posY + 15 width:Screen_Width - 70 fontSize:14 color:[UIColor describeColor_9a] text:LoadLanguagesKey(@"login_editPwd_alert2")];
            [self.view addSubview:alertLb];
            
            posY = CGRectGetMaxY(alertLb.frame) + 10;
        }
        else if (i == 2)
        {
            _strongView = [[ISSPwdStrongView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(fieldView.frame) + 15, Screen_Width, 70)];
            [self.view addSubview:_strongView];
            
            posY = CGRectGetMaxY(_strongView.frame);
        }
    }
    
    //确定位置
    X_Button *btn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:CGRectGetMaxY(fieldView.frame) + 50];
    [btn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    
    _currentTime = 60;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_codeTimer) {
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
