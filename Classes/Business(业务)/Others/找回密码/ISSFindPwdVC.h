//
//  ISSFindPwdVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ISSFindPwdSuccessBlock)(void);

@interface ISSFindPwdVC : ISSBaseVC

@property (nonatomic, copy) ISSFindPwdSuccessBlock findPwdSuccessBlock;

@end
