//
//  AppDelegate.h
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//设置rootNav
- (void)setWindowRootVC:(NSInteger)type;
//退出登录
- (void)logoutSystem:(NSString *)alertMsg;
- (void)logoutSystem;
//推送是否接收
- (void)isRecesived;

//设置tabBar的badgeValue
//- (void)setTabBarBadgeValueFirst;
- (void)setTabBarBadgeValue:(NSInteger)badgeValue selectedIndex:(NSUInteger)selectedIndex;

@end

