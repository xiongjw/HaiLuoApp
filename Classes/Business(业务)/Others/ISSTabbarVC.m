//
//  ISSTabbarVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSTabbarVC.h"

#import "ISSInfoMainVC.h"
#import "ISSSafeMainVC.h"
#import "ISSJobMainVC.h"
#import "ISSCheckMaintainVC.h"
#import "ISSPersonalVC.h"

@interface ISSTabbarVC ()
{
    NSArray       *titlesArray;
    NSArray       *normalImageArray;
    NSArray       *lightImageArray;
}

@end

@implementation ISSTabbarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    titlesArray = @[
                    LoadLanguagesKey(@"tabbar_info"),
                    LoadLanguagesKey(@"tabbar_security"),
                    LoadLanguagesKey(@"tabbar_job"),
                    LoadLanguagesKey(@"tabbar_check"),
                    LoadLanguagesKey(@"tabbar_mine")
                    ];
    normalImageArray = @[@"tabbar_info",@"tabbar_safe",@"tabbar_job",@"tabbar_check",@"tabbar_person"];
    lightImageArray = @[@"tabbar_info_selected",@"tabbar_safe_selected",@"tabbar_job_selected",@"tabbar_check_selected",@"tabbar_person_selected"];
    [self createTabbar];
}

- (UIImage *)getImage:(NSString *)imageName
{
    return [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (UITabBarItem *)getTabBarItem:(NSInteger)index
{
    return [[UITabBarItem alloc] initWithTitle:titlesArray[index]
                                         image:[self getImage:normalImageArray[index]]
                                 selectedImage:[self getImage:lightImageArray[index]]];
}

- (void)createTabbar
{
    self.tabBar.backgroundColor = [UIColor whiteColor];
    [self.tabBar setShadowImage:[[UIImage alloc] init]];
    self.tabBar.backgroundImage = [UIImage new];
    
    [self dropShadowWithOffset:CGSizeMake(0, -2)
                        radius:1
                         color:[UIColor lightGrayColor]
                       opacity:0.3];
    
    self.tabBar.tintColor = [UIColor themeColor];
    
    NSMutableArray *navArray = [NSMutableArray new];
    
    if ([ISSUserData sharedInstance].menus_privilege_yh)
    {
        ISSInfoMainVC *vc0 = [[ISSInfoMainVC alloc] init];
        ISSNavigationController *nav0 = [[ISSNavigationController alloc] initWithRootViewController:vc0];
        nav0.tabBarItem = [self getTabBarItem:0];
        [navArray addObject:nav0];
        
        ISSSafeMainVC *vc1 = [[ISSSafeMainVC alloc] init];
        ISSNavigationController *nav1 = [[ISSNavigationController alloc] initWithRootViewController:vc1];
        nav1.tabBarItem = [self getTabBarItem:1];
        [navArray addObject:nav1];
    }
    if ([ISSUserData sharedInstance].menus_privilege_zy)
    {
        ISSJobMainVC *vc2 = [[ISSJobMainVC alloc] init];
        ISSNavigationController *nav2 = [[ISSNavigationController alloc] initWithRootViewController:vc2];
        nav2.tabBarItem = [self getTabBarItem:2];
        [navArray addObject:nav2];
    }
    if ([ISSUserData sharedInstance].menus_privilege_jx)
    {
        ISSCheckMaintainVC *vc3 = [[ISSCheckMaintainVC alloc] init];
        ISSNavigationController *nav3 = [[ISSNavigationController alloc] initWithRootViewController:vc3];
        nav3.tabBarItem = [self getTabBarItem:3];
        [navArray addObject:nav3];
    }
    
    ISSPersonalVC *vc4 = [[ISSPersonalVC alloc] init];
    ISSNavigationController *nav4 = [[ISSNavigationController alloc] initWithRootViewController:vc4];
    nav4.tabBarItem = [self getTabBarItem:4];
    [navArray addObject:nav4];
    
    self.viewControllers = navArray.copy;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dropShadowWithOffset:(CGSize)offset
                      radius:(CGFloat)radius
                       color:(UIColor *)color
                     opacity:(CGFloat)opacity {
    
    // Creating shadow path for better performance
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, self.tabBar.bounds);
    self.tabBar.layer.shadowPath = path;
    CGPathCloseSubpath(path);
    CGPathRelease(path);
    
    self.tabBar.layer.shadowColor = color.CGColor;
    self.tabBar.layer.shadowOffset = offset;
    self.tabBar.layer.shadowRadius = radius;
    self.tabBar.layer.shadowOpacity = opacity;
    
    // Default clipsToBounds is YES, will clip off the shadow, so we disable it.
    self.tabBar.clipsToBounds = NO;
}

@end
