//
//  AppDelegate.m
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "AppDelegate.h"

#import "ISSLoginVC.h"
#import "ISSTabbarVC.h"

#import <UMCommon/UMCommon.h>
#import <UMPush/UMessage.h>

#import "ISSFMDB.h"
#import "ListCellViewController.h"

@interface AppDelegate () <UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [ISSFMDB createDB];
    // 注册友盟
    [self registerUmeng:launchOptions];
    // rootVC配置
    [self dealChooseRootVC];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    __block UIBackgroundTaskIdentifier bgTaskID;
    bgTaskID = [application beginBackgroundTaskWithExpirationHandler:^ {
        
        //不管有没有完成，结束background_task任务
        [application endBackgroundTask: bgTaskID];
        bgTaskID = UIBackgroundTaskInvalid;
    }];
    // 开始统计角标
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[ISSFMDBUtil sharedInstance] queryAllMsg];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    [UMessage registerDeviceToken:deviceToken];
    
    NSString *deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""]
                                     stringByReplacingOccurrencesOfString:@">" withString:@""]
                                    stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"deviceTokenString:%@", deviceTokenString);
    
    //NSLog(@"deviceIDForIntegration:%@",[UMConfigure deviceIDForIntegration]);
    
    if ([ISSDataStorage hasObject:@"ISSUSerTokenKey"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self isRecesived];
        });
    }
    
}

#pragma mark - 获取是否推送
- (void)isRecesived
{
    // 获取是否推送，开始打tag 或者移除 tag
    [X_HttpUtil apiRequest:@"infoPush/isRecesived" param:@{} view:nil showErrorMessage:NO successBlock:^(NSDictionary *resultData) {
        //
        NSDictionary *data = resultData[@"data"];
        bool result = [data[@"result"] boolValue];
        NSLog(@"tag:%@",data[@"tag"]);
        // 自己测试都开启
        if ([@"xjw" isEqualToString:[ISSDataStorage getObject:@"ISSUserName"]]) result = YES;
        
        if (result) {
            //添加tag的先决条件是已经成功获取到device_token
            [UMessage addTags:data[@"tag"] response:^(id  _Nullable responseObject, NSInteger remain, NSError * _Nullable error) {
                NSLog(@"addTagsSuccess");
            }];
        }
        else
        {
            [UMessage deleteTags:data[@"tag"] response:^(id  _Nullable responseObject, NSInteger remain, NSError * _Nullable error) {
                NSLog(@"deleteTagsSuccess");
            }];
        }
        
    } failureBlock:NULL];
}

// iOS10以下
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    if([[[UIDevice currentDevice] systemVersion] intValue] < 10)
    {
        NSLog(@"userInfo1:%@",userInfo);
        [UMessage setAutoAlert:NO];
        [UMessage didReceiveRemoteNotification:userInfo];
        
        [self dealClickNotification:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
    }
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"userInfo2:%@",userInfo);
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]])
    {
        [UMessage setAutoAlert:NO];
        [UMessage didReceiveRemoteNotification:userInfo];
        
        [self dealClickNotification:userInfo];
    }
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    NSLog(@"userInfo3:%@",userInfo);
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]])
    {
        //应用处于后台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
        [self dealClickNotification:userInfo];
    }
}

- (void)dealClickNotification:(NSDictionary *)userInfo
{
    // 有权限才会接收通知
    if (userInfo[@"type"])
    {
        NSDictionary *data = @{
                               @"id":userInfo[@"id"],
                               @"type":FormatString(@"%ld",(long)[userInfo[@"type"] integerValue])
                               };
        //开始跳转
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
        {
            // 在应用内
            [[ISSFMDBUtil sharedInstance] insertPushMsgToDB:data];
        }
        else
        {
            // 打开应用
            // 这里不插数据， 因为要进入详情，就直接已读了
            //[[ISSFMDBUtil sharedInstance] insertPushMsgToDB:data];
            [self beginToJump:data];
        }
    }
    
}

- (void)beginToJump:(NSDictionary *)data
{
    if ([self.window.rootViewController isKindOfClass:[ISSTabbarVC class]])
    {
        //NSString *type = data[@"type"];
        ISSTabbarVC *tabVC = (ISSTabbarVC *)self.window.rootViewController;
        //tabVC.selectedIndex = 1;
        
        ISSNavigationController *selectedNav = (ISSNavigationController *)tabVC.selectedViewController;
        [selectedNav.presentedViewController dismissViewControllerAnimated:NO completion:nil];
        [selectedNav popToRootViewControllerAnimated:NO];
        
        // 开始进入详情
        ListCellViewController *listCellVC = [[ListCellViewController alloc]init];
        listCellVC.hidesBottomBarWhenPushed = YES;
        listCellVC.itemId = data[@"id"];
        
        if ([@"1" isEqualToString:data[@"type"]]) listCellVC.itemType = @"yh";
        else if ([@"2" isEqualToString:data[@"type"]]) listCellVC.itemType = @"wz";
        else if ([@"3" isEqualToString:data[@"type"]]) listCellVC.itemType = @"aq";
        
        listCellVC.comeType = 100;
        [selectedNav pushViewController:listCellVC animated:YES];
    }
}

#pragma mark - 注册友盟
- (void)registerUmeng:(NSDictionary *)launchOptions
{
    [UMConfigure initWithAppkey:@"5ae19b108f4a9d694a000036" channel:@"App Store"];
    //[UMConfigure initWithAppkey:@"5ae2dc70f29d9802400001a7" channel:@"App Store"]; //demo
    
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    
    UMessageRegisterEntity *entity = [[UMessageRegisterEntity alloc] init];
    entity.types = UMessageAuthorizationOptionBadge | UMessageAuthorizationOptionAlert | UMessageAuthorizationOptionSound;
    //entity.types = UMessageAuthorizationOptionBadge;
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            NSLog(@"registerSuccess");
        }
        else
        {
            NSLog(@"%@",[error description]);
        }
    }];
}

#pragma mark - rootVC
- (void)dealChooseRootVC
{
    bool isLoginSuccess = [[ISSDataStorage getObject:@"isLoginSuccess"] boolValue];
    if (isLoginSuccess)
    {
        // 以前已经登录过，但因为没有保存登录数据，登录接口新增的，所有需要重新登录
        NSDictionary *data = [ISSDataStorage getObject:@"ISSLoginDataKey"];
        if (data)
        {
            [[ISSUserData sharedInstance] setCacheLoginData:data];
            [self setWindowRootVC:1];
        }
        else
        {
            [self setWindowRootVC:2];
        }
    }
    else  [self setWindowRootVC:0];
}

// 0 登录、1首页、2注销
- (void)setWindowRootVC:(NSInteger)type
{
    if (self.window == nil) {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    // login
    if (type == 0 ||type == 2)
    {
        if (type == 2) {
            //取消所有网络请求
            [X_HttpUtil cancelAllRequest];
            //清空通知栏
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            //清空用户数据
            [[ISSUserData sharedInstance] clearUserData];
            
            [ISSDataStorage removeObject:@"isLoginSuccess"];
        }
        ISSLoginVC *vc = [[ISSLoginVC alloc] init];
        ISSNavigationController *nav = [[ISSNavigationController alloc] initWithRootViewController:vc];
        self.window.rootViewController = nav;
    }
    //tabbar
    else
    {
        ISSTabbarVC *tabVC = [[ISSTabbarVC alloc] init];
        self.window.rootViewController = tabVC;
        
        WeakSelf(self)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // 同时拉取用户信息,数据库查询需要依赖用户id。
            [[ISSUserData sharedInstance] queryUserInfo:nil successBlock:^{
                
                [weakself performSelector:@selector(setTabBarBadgeValueFirst) withObject:nil];
            }];
        });
    }
    [self.window makeKeyAndVisible];
}

- (void)setTabBarBadgeValue:(NSInteger)badgeValue selectedIndex:(NSUInteger)selectedIndex
{
    if (![ISSUserData sharedInstance].menus_privilege_yh) {
        return;
    }
    if ([self.window.rootViewController isKindOfClass:[ISSTabbarVC class]])
    {
        ISSTabbarVC *tabVC = (ISSTabbarVC *)self.window.rootViewController;
        
        UITabBarItem *item = [tabVC.tabBar.items objectAtIndex:selectedIndex];
        if (badgeValue == 0) {
            item.badgeValue = nil;
        }
        else
        {
            item.badgeValue = FormatString(@"%ld",(long)badgeValue);
        }
        // 如果没有点击过第2个tab，通知传输不到
        if (selectedIndex == 0)
        {
            NSInteger num1 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"1"];
            NSInteger num2 = [[ISSFMDBUtil sharedInstance] queryTypeMsg:@"2"];
            [self setTabBarBadgeValue:num1 + num2 selectedIndex:1];
        }
    }
}

- (void)setTabBarBadgeValueFirst
{
    if (![ISSUserData sharedInstance].menus_privilege_yh) {
        return;
    }
    if ([self.window.rootViewController isKindOfClass:[ISSTabbarVC class]])
    {
        ISSTabbarVC *tabVC = (ISSTabbarVC *)self.window.rootViewController;
        
        NSInteger all = [[ISSFMDBUtil sharedInstance] queryAllMsg];
        NSInteger two = [[ISSFMDBUtil sharedInstance] queryDangerAndBreakMsg];
        
        UITabBarItem *item0 = [tabVC.tabBar.items objectAtIndex:0];
        
        if (all == 0) item0.badgeValue = nil;
        else item0.badgeValue = FormatString(@"%ld",(long)all);
        
        UITabBarItem *item1 = [tabVC.tabBar.items objectAtIndex:1];
        if (two == 0) item1.badgeValue = nil;
        else item1.badgeValue = FormatString(@"%ld",(long)two);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"queryPushMsgSuccessNote" object:nil];
    }
}

- (void)logoutSystem
{
    WeakSelf(self)
    [self.window.rootViewController showAlertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:LoadLanguagesKey(@"mine_logout_alert") btnList:@[LoadLanguagesKey(@"btn_cancel"),LoadLanguagesKey(@"btn_confirm")] handler:^(NSInteger index) {
        if (index == 1) {
            [weakself setWindowRootVC:2];
        }
    }];
}

- (void)logoutSystem:(NSString *)alertMsg
{
    WeakSelf(self)
    [self.window.rootViewController showAlertControllerWithMessage:alertMsg handler:^(UIAlertAction *action) {
         [weakself setWindowRootVC:2];
    }];
}

@end
