//
//  ISSVerifyPhoneVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^VerifyPhoneSuccessBlock)(void);

@interface ISSVerifyPhoneVC : ISSBaseVC

@property (nonatomic,copy) NSString *phoneStr;

@property (nonatomic,copy) VerifyPhoneSuccessBlock verifyPhoneSuccessBlock;

@end
