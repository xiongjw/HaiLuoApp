//
//  ISSEditPwdVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSEditPwdVC.h"

#import "ISSCreateUIHelper.h"
#import "ISSPwdStrongView.h"

#import "NSString+RegexCheck.h"

@interface ISSEditPwdVC () <UITextFieldDelegate>
{
    ISSPwdStrongView *_strongView;
}

@end

@implementation ISSEditPwdVC

- (void)confirmAction
{
    UITextField *field1 = [self.view viewWithTag:100];
    UITextField *field2 = [self.view viewWithTag:101];
    if (![field1.text isValidatePassword]) {
        [self.view makeToast:LoadLanguagesKey(@"login_editPwd_alert2")];//Abc@123456
        return;
    }
    if (![field1.text isEqualToString:field2.text]) {
        [self.view makeToast:LoadLanguagesKey(@"login_editPwd_alert3")];
        return;
    }
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"updatePassword" param:@{@"password":[field1.text md5]} view:self.view successBlock:^(NSDictionary *resultData) {
        
        //[ISSDataStorage saveObject:field1.text key:@"ISSPassWord"];
        [ISSDataStorage removeObject:@"ISSPassWord"];
        if (_editPwdSuccessBlock) {
            [weakself dismissViewControllerAnimated:YES completion:^{
                _editPwdSuccessBlock();
            }];
        }
        else {
            [weakself dismissViewControllerAnimated:YES completion:NULL];
        }
        
    }];
}

- (void)textFieldDidChange:(NSNotification *)note
{
    UITextField *textField = note.object;
    if (textField.tag == 100) {
        [_strongView changeColor:textField.text];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (!self.isFromLogin)
    {
        WeakSelf(self)
        X_Button *backBtn = [ISSNavUtil getImageBtn:6 clickBlock:^{
            [weakself dismissViewControllerAnimated:YES completion:NULL];
        }];
        [backBtn setLeft:15];
        [backBtn setTop:20];
        [self.view addSubview:backBtn];
    }
    
    UILabel *titleLb = [UILabel oneLineLbWithX:0 y:0 width:Screen_Width fontSize:18 color:[UIColor describeColor_61] text:LoadLanguagesKey(@"login_editPwd")];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    [titleLb setCenterY:42];
    [self.view addSubview:titleLb];
    
    if (self.isFromLogin)
    {
        UILabel *alertLb = [UILabel mutLineLbWithX:35 y:110 width:Screen_Width - 70 fontSize:16 color:UIColorFromRGB(0x1c63ff) text:LoadLanguagesKey(@"login_editPwd_alert")];
        [self.view addSubview:alertLb];
    }
    
    UILabel *alertLb = [UILabel mutLineLbWithX:35 y:180 width:Screen_Width - 70 fontSize:14 color:[UIColor describeColor_9a] text:LoadLanguagesKey(@"login_editPwd_alert2")];
    [self.view addSubview:alertLb];
    
    
    UIView *fieldView = [[UIView alloc] initWithFrame:CGRectMake(35, CGRectGetMaxY(alertLb.frame) + 15, Screen_Width - 70, 50)];
    fieldView.backgroundColor = RGB(244, 244, 244);
    fieldView.layer.cornerRadius = 5;
    fieldView.layer.masksToBounds = YES;
    [self.view addSubview:fieldView];
    
    UITextField *textField = [ISSCreateUIHelper makeTextField:self
                                                    withFrame:CGRectMake(15, 0, fieldView.frame.size.width - 20, fieldView.frame.size.height)
                                                       andTag:100
                                               andPlaceholder:LoadLanguagesKey(@"forgetPwd_input_newPwd")
                                                    maxLength:16];
    [fieldView addSubview:textField];
    textField.secureTextEntry = YES;
    //[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventValueChanged];
    
    _strongView = [[ISSPwdStrongView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(fieldView.frame) + 15, Screen_Width, 70)];
    [self.view addSubview:_strongView];
    
    fieldView = [[UIView alloc] initWithFrame:CGRectMake(35, CGRectGetMaxY(_strongView.frame), Screen_Width - 70, 50)];
    fieldView.backgroundColor = RGB(244, 244, 244);
    fieldView.layer.cornerRadius = 5;
    fieldView.layer.masksToBounds = YES;
    [self.view addSubview:fieldView];
    
    textField = [ISSCreateUIHelper makeTextField:self
                                       withFrame:CGRectMake(15, 0, fieldView.frame.size.width - 20, fieldView.frame.size.height)
                                          andTag:101
                                  andPlaceholder:LoadLanguagesKey(@"forgetPwd_input_newPwd_again")
                                       maxLength:16];
    [fieldView addSubview:textField];
    textField.secureTextEntry = YES;
    
    X_Button *btn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:Screen_Height - 100];
    [btn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
