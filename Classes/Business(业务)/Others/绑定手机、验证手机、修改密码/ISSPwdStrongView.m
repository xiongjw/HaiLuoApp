//
//  ISSPwdStrongView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSPwdStrongView.h"

@interface ISSPwdStrongView ()
{
    UIView *_lineView1;
    UIView *_lineView2;
    UIView *_lineView3;
    
    UILabel *_showLb;
}

@end

@implementation ISSPwdStrongView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *titleStr = LoadLanguagesKey(@"login_editPwd_pwdStrong");
        CGSize size = [titleStr sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}];
        CGFloat btnWidth = MIN(120, size.width);
        
        CGFloat lineWidth = (Screen_Width - 100 - btnWidth - 20 - 8*2)/3;
        
        UILabel *titleLb = [UILabel oneLineLbWithX:50 y:0 fontSize:14 color:UIColorFromRGB(0x1c63ff) text:titleStr];
        //[titleLb setCenterY:frame.size.height/2];
        [titleLb setHeight:frame.size.height];
        [titleLb setWidth:btnWidth];
        titleLb.numberOfLines = 2;
        [self addSubview:titleLb];
        
        _lineView1 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLb.frame) + 20, 0, lineWidth, 4)];
        _lineView1.backgroundColor = RGB(213, 213, 213);
        [_lineView1 setCenterY:frame.size.height/2];
        [self addSubview:_lineView1];
        
        _lineView2 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lineView1.frame) + 8, 0, lineWidth, 4)];
        _lineView2.backgroundColor = RGB(213, 213, 213);
        [_lineView2 setCenterY:frame.size.height/2];
        [self addSubview:_lineView2];
        
        _lineView3 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lineView2.frame) + 8, 0, lineWidth, 4)];
        _lineView3.backgroundColor = RGB(213, 213, 213);
        [_lineView3 setCenterY:frame.size.height/2];
        [self addSubview:_lineView3];
        
        _showLb = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(_lineView1.frame) + 10 width:lineWidth fontSize:14 color:[UIColor describeColor_9a] text:@""];
        _showLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_showLb];
        
    }
    return self;
}

- (void)changeColor:(NSString *)text
{
    if (text.length == 0)
    {
        _showLb.hidden = YES;
        _lineView1.backgroundColor = RGB(213, 213, 213);
        _lineView2.backgroundColor = RGB(213, 213, 213);
        _lineView3.backgroundColor = RGB(213, 213, 213);
    }
    else
    {
        _showLb.hidden = NO;
        if (text.length <= 6)
        {
            _lineView1.backgroundColor = RGB(105, 222, 69);
            _lineView2.backgroundColor = RGB(213, 213, 213);
            _lineView3.backgroundColor = RGB(213, 213, 213);
            
            _showLb.text = @"弱";
            [_showLb setCenterX:_lineView1.centerX];
        }
        else if (text.length >= 10)
        {
            _lineView1.backgroundColor = RGB(105, 222, 69);
            _lineView2.backgroundColor = RGB(105, 222, 69);
            _lineView3.backgroundColor = RGB(105, 222, 69);
            
            _showLb.text = @"强";
            [_showLb setCenterX:_lineView3.centerX];
        }
        else
        {
            _lineView1.backgroundColor = RGB(105, 222, 69);
            _lineView2.backgroundColor = RGB(105, 222, 69);
            _lineView3.backgroundColor = RGB(213, 213, 213);
            
            _showLb.text = @"中";
            [_showLb setCenterX:_lineView2.centerX];
        }
    }
}

@end
