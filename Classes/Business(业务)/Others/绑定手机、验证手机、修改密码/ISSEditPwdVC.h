//
//  ISSEditPwdVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^EditPwdSuccessBlock)(void);

@interface ISSEditPwdVC : ISSBaseVC

@property (nonatomic,assign) bool isFromLogin;//从登录修改密码

@property (nonatomic,copy) EditPwdSuccessBlock editPwdSuccessBlock;

@end
