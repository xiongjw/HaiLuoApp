//
//  ISSVerifyPhoneVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSVerifyPhoneVC.h"

#import "OpenUDID.h"

@interface ISSVerifyPhoneVC ()
{
    NSTimer *_codeTimer;
    long _currentTime;
}

@property (nonatomic,strong) X_Button *codeBtn;

@end

@implementation ISSVerifyPhoneVC

- (void)Countdown
{
    _currentTime --;
    if (_currentTime == 0)
    {
        _currentTime = 60;
        self.codeBtn.enabled = YES;
        [self.codeBtn setTitle:LoadLanguagesKey(@"forgetPwd_btn_reGetCode") forState:UIControlStateNormal];
        
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
    else
    {
        [self.codeBtn setTitle:[NSString stringWithFormat:@"%ldS",_currentTime] forState:UIControlStateNormal];
    }
}

- (void)codeRequestSuccess
{
    if (_codeTimer) {
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
    _codeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(Countdown) userInfo:nil repeats:YES];
    
    self.codeBtn.enabled = NO;
    [self.codeBtn setTitle:@"60S" forState:UIControlStateNormal];
}

- (void)sendAction
{
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"getCode" param:@{@"phone":self.phoneStr} view:self.view.window successBlock:^(NSDictionary *resultData) {
        
        [weakself.view makeToast:LoadLanguagesKey(@"forgetPwd_sendCodeSuccess")];
        [weakself codeRequestSuccess];
    }];
    
}

- (void)confirmAction
{
    UITextField *codeField = [self.view viewWithTag:101];
    if (codeField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_code")];
        return;
    }
    NSDictionary *data = @{
                           @"phone":self.phoneStr,
                           @"code":codeField.text,
                           @"phoneCode":[OpenUDID value],
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"cheakCode" param:data view:self.view.window successBlock:^(NSDictionary *resultData) {
        
        if (_verifyPhoneSuccessBlock) {
            [weakself dismissViewControllerAnimated:YES completion:^{
                _verifyPhoneSuccessBlock();
            }];
        }
        else {
            [weakself dismissViewControllerAnimated:YES completion:NULL];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLb = [UILabel oneLineLbWithX:0 y:0 width:Screen_Width fontSize:18 color:[UIColor describeColor_61] text:LoadLanguagesKey(@"login_validatePhone")];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    [titleLb setCenterY:42];
    [self.view addSubview:titleLb];
    
    UIView *fieldView = nil;
    UITextField *textField = nil;
    CGFloat posY = Screen_Height/3;
    NSArray *placeHolders = @[LoadLanguagesKey(@"forgetPwd_input_phone"),
                              LoadLanguagesKey(@"forgetPwd_input_code")];
    for (int i = 0; i < placeHolders.count; i++)
    {
        fieldView = [[UIView alloc] initWithFrame:CGRectMake(35, posY, Screen_Width - 70, 50)];
        fieldView.backgroundColor = RGB(244, 244, 244);
        fieldView.layer.cornerRadius = 5;
        fieldView.layer.masksToBounds = YES;
        [self.view addSubview:fieldView];
        
        textField = [ISSCreateUIHelper makeTextField:self
                                           withFrame:CGRectMake(15, 0, fieldView.frame.size.width - 20, fieldView.frame.size.height)
                                              andTag:i + 100
                                      andPlaceholder:placeHolders[i]
                                           maxLength:20];
        [fieldView addSubview:textField];
        if (i == 0) {
            textField.keyboardType = UIKeyboardTypeNumberPad;
            //textField.text = self.phoneStr;
            textField.text = self.phoneStr;
            if (self.phoneStr.length >= 11) {
                textField.text = FormatString(@"%@****%@",[self.phoneStr substringToIndex:3],[self.phoneStr substringFromIndex:7]);
            }
            textField.enabled = NO;
        }
        else if (i == 1)
        {
            [fieldView setWidth:fieldView.width - 100 - 10];
            textField.keyboardType = UIKeyboardTypeNumberPad;
            [textField setWidth:fieldView.width - 10];
            
            self.codeBtn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"forgetPwd_btn_getCode") posY:0];
            self.codeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            self.codeBtn.frame = CGRectMake(CGRectGetMaxX(fieldView.frame) + 10, CGRectGetMinY(fieldView.frame), 100, fieldView.frame.size.height);
            [self.codeBtn addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.codeBtn];
        }
        
        posY = CGRectGetMaxY(fieldView.frame) + 15;
    }
    
    //确定位置
    X_Button *btn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:Screen_Height - 100];
    [btn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    _currentTime = 60;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
