//
//  ISSBindPhoneVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBindPhoneVC.h"

#import "NSString+RegexCheck.h"

@interface ISSBindPhoneVC ()
{
    NSTimer *_codeTimer;
    long _currentTime;
}

@property (nonatomic,strong) X_Button *codeBtn;

@end

@implementation ISSBindPhoneVC

- (void)Countdown
{
    _currentTime --;
    if (_currentTime == 0)
    {
        _currentTime = 60;
        self.codeBtn.enabled = YES;
        [self.codeBtn setTitle:LoadLanguagesKey(@"forgetPwd_btn_reGetCode") forState:UIControlStateNormal];
        
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
    else
    {
        [self.codeBtn setTitle:[NSString stringWithFormat:@"%ldS",_currentTime] forState:UIControlStateNormal];
    }
}

- (void)codeRequestSuccess
{
    if (_codeTimer) {
        [_codeTimer invalidate];
        _codeTimer = nil;
    }
    _codeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(Countdown) userInfo:nil repeats:YES];
    
    self.codeBtn.enabled = NO;
    [self.codeBtn setTitle:@"60S" forState:UIControlStateNormal];
}

- (void)sendAction
{
    UITextField *loginField = [self.view viewWithTag:100];
    if (loginField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_phone")];
        return;
    }
    if (![loginField.text isValidateMobile]) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_rightPhone")];
        return;
    }
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"getMobileVerificationCode" param:@{@"phone":loginField.text} view:self.view.window successBlock:^(NSDictionary *resultData) {
        
        [weakself.view makeToast:LoadLanguagesKey(@"forgetPwd_sendCodeSuccess")];
        [weakself codeRequestSuccess];
    }];
    
}

- (void)confirmAction
{
    UITextField *loginField = [self.view viewWithTag:100];
    if (loginField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_phone")];
        return;
    }
    if (![loginField.text isValidateMobile]) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_rightPhone")];
        return;
    }
    UITextField *codeField = [self.view viewWithTag:101];
    if (codeField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"forgetPwd_input_code")];
        return;
    }
    NSDictionary *data = @{
                           @"phone":loginField.text,
                           @"userName":[ISSDataStorage getObject:@"ISSUserName"],
                           @"code":codeField.text
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"updatePhone" param:data view:self.view.window successBlock:^(NSDictionary *resultData) {
        
        if (_bindPhoneSuccessBlock) {
            [weakself dismissViewControllerAnimated:YES completion:^{
                _bindPhoneSuccessBlock(loginField.text);
            }];
        }
        else {
            [weakself dismissViewControllerAnimated:YES completion:NULL];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (!self.isFromLogin)
    {
        WeakSelf(self)
        X_Button *backBtn = [ISSNavUtil getImageBtn:6 clickBlock:^{
            [weakself dismissViewControllerAnimated:YES completion:NULL];
        }];
        [backBtn setLeft:15];
        [backBtn setTop:20];
        [self.view addSubview:backBtn];
    }
    
    UILabel *titleLb = [UILabel oneLineLbWithX:0 y:0 width:Screen_Width fontSize:18 color:[UIColor describeColor_61] text:self.isFromLogin ? LoadLanguagesKey(@"login_bindPhone") : LoadLanguagesKey(@"mine_personal_changeBindPhone")];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    [titleLb setCenterY:42];
    [self.view addSubview:titleLb];
    
    UIView *fieldView = nil;
    UITextField *textField = nil;
    CGFloat posY = Screen_Height/3;
    NSArray *placeHolders = @[LoadLanguagesKey(@"forgetPwd_input_phone"),
                              LoadLanguagesKey(@"forgetPwd_input_code")];
    for (int i = 0; i < placeHolders.count; i++)
    {
        fieldView = [[UIView alloc] initWithFrame:CGRectMake(35, posY, Screen_Width - 70, 50)];
        fieldView.backgroundColor = RGB(244, 244, 244);
        fieldView.layer.cornerRadius = 5;
        fieldView.layer.masksToBounds = YES;
        [self.view addSubview:fieldView];
        
        textField = [ISSCreateUIHelper makeTextField:self
                                           withFrame:CGRectMake(15, 0, fieldView.frame.size.width - 20, fieldView.frame.size.height)
                                              andTag:i + 100
                                      andPlaceholder:placeHolders[i]
                                           maxLength:20];
        [fieldView addSubview:textField];
        if (i == 0) {
            textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if (i == 1)
        {
            [fieldView setWidth:fieldView.width - 100 - 10];
            textField.keyboardType = UIKeyboardTypeNumberPad;
            [textField setWidth:fieldView.width - 10];
            
            self.codeBtn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"forgetPwd_btn_getCode") posY:0];
            self.codeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            self.codeBtn.frame = CGRectMake(CGRectGetMaxX(fieldView.frame) + 10, CGRectGetMinY(fieldView.frame), 100, fieldView.frame.size.height);
            [self.codeBtn addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.codeBtn];
        }
        
        posY = CGRectGetMaxY(fieldView.frame) + 15;
    }
    
    //确定位置
    X_Button *btn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:Screen_Height - 100];
    [btn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    _currentTime = 60;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
