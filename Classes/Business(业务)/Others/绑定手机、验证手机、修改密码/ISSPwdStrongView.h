//
//  ISSPwdStrongView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSPwdStrongView : UIView

- (void)changeColor:(NSString *)text;

@end
