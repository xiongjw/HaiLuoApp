//
//  ISSBindPhoneVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^BindPhoneSuccessBlock)(NSString *bindPhone);

@interface ISSBindPhoneVC : ISSBaseVC

@property (nonatomic,assign) bool isFromLogin;//从登录过来

@property (nonatomic,copy) BindPhoneSuccessBlock bindPhoneSuccessBlock;

@end
