//
//  ISSLoginVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSLoginVC.h"

#import "ISSFindPwdVC.h"

#import "NSString+RegexCheck.h"

#import "OpenUDID.h"

#import "ISSBindPhoneVC.h"
#import "ISSVerifyPhoneVC.h"
#import "ISSEditPwdVC.h"

@interface ISSLoginVC () <UITextFieldDelegate>

@end

@implementation ISSLoginVC

// 修改密码
- (void)editPwd:(BOOL)pwdIsEasy block:(void (^)(void))block
{
    if (pwdIsEasy)
    {
        ISSEditPwdVC *vc = [[ISSEditPwdVC alloc] init];
        vc.isFromLogin = YES;
        vc.editPwdSuccessBlock = ^{
            if (block) block();
        };
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else if (block) block();
}
// 绑定手机
- (void)bindPhone:(BOOL)phoneIsNull block:(void (^)(void))block
{
    if (phoneIsNull)
    {
        ISSBindPhoneVC *vc = [[ISSBindPhoneVC alloc] init];
        vc.isFromLogin = YES;
        vc.bindPhoneSuccessBlock = ^(NSString *bindPhone) {
            if (block) block();
        };
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else if (block) block();
}
// 验证手机
- (void)verifyPhone:(BOOL)newPhone phoneStr:(NSString *)phoneStr block:(void (^)(void))block
{
    if (newPhone)
    {
        ISSVerifyPhoneVC *vc = [[ISSVerifyPhoneVC alloc] init];
        vc.phoneStr = phoneStr;
        vc.verifyPhoneSuccessBlock = ^{
            if (block) block();
        };
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else if (block) block();
}

- (void)dealCheck:(NSDictionary *)resultData block:(void (^)(void))block;
{
    // 为了苹果审核，或其它指定的特殊账号，不进行几轮检测， 直接进入主界面
    NSArray *list = @[@"demo",@"xjw"];//demo是苹果审核账号,xjw是个人测试账号
    NSString *userName = [ISSDataStorage getObject:@"ISSUserName"];
    if ([list containsObject:userName]) {
        if (block) block();
        return;
    }
    NSDictionary *data = resultData[@"data"];
    //bool pwdIsEasy = [data[@"passwordIsEasy"] boolValue];//密码是否简单
    bool pwdIsEasy = ![[ISSDataStorage getObject:@"ISSPassWord"] isValidatePassword];
    BOOL phoneIsNull = [data[@"phoneIsNull"] boolValue];//是否绑定过手机
    BOOL newPhone = [data[@"newPhone"] boolValue];//是否是新设备
    NSString *phone = [NSString noNullString:data[@"phone"]];
    
    WeakSelf(self)
    // 修改密码
    [self editPwd:pwdIsEasy block:^{
        // 绑定手机
        [weakself bindPhone:phoneIsNull block:^{
            // 新设备
            [weakself verifyPhone:newPhone phoneStr:phone block:^{
                if (block) block();
            }];
        }];
    }];
}

- (void)loginAction
{
    UITextField *loginField = [self.view viewWithTag:100];
    if (loginField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"login_input_username")];
        return;
    }
    UITextField *pwdField = [self.view viewWithTag:101];
    if (pwdField.text.length == 0) {
        [self.view makeToast:LoadLanguagesKey(@"login_input_password")];
        return;
    }
    NSDictionary *data = @{
                           @"userName":loginField.text,
                           @"password":[pwdField.text md5],
                           @"phoneCode":[OpenUDID value],
                           @"phoneType":@"iOS"
                           };
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"login" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        [[ISSUserData sharedInstance] saveLoginData:resultData[@"data"]];
        // 记住用户名、密码
        [ISSDataStorage saveObject:loginField.text key:@"ISSUserName"];
        [ISSDataStorage saveObject:pwdField.text key:@"ISSPassWord"];
        // 同时拉取用户信息
        [[ISSUserData sharedInstance] queryUserInfo];
        [ShareApp isRecesived];
        // 只有都符合要求的才算登录成功
        [weakself dealCheck:resultData block:^{
            
            [ISSDataStorage saveObject:@(YES) key:@"isLoginSuccess"];
            [ShareApp setWindowRootVC:1];
        }];
        
    }];
    
}

- (void)giveValueToPwd:(NSString *)text
{
    UITextField *pwdField = [self.view viewWithTag:101];
    pwdField.text = text;
}

- (void)clearPwd
{
    //[ISSDataStorage removeObject:@"ISSPassWord"];
    [self giveValueToPwd:@""];
}

- (void)findPwdAction
{
    WeakSelf(self)
    ISSFindPwdVC *vc = [[ISSFindPwdVC alloc] init];
    vc.findPwdSuccessBlock = ^{
        [weakself clearPwd];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImage *image = [UIImage imageNamed:@"login_logo"];
    //确定位置
    CGFloat height = 54*Screen_Height/(54+80);
    UIImageView *logoView = [[UIImageView alloc] initWithImage:image];
    [logoView setCenterX:self.view.centerX];
    [logoView setCenterY:height/2];
    [self.view addSubview:logoView];
    
    UIView *fieldView = nil;
    UITextField *textField = nil;
    CGFloat posY = height;
    for (int i = 0; i < 2; i++)
    {
        fieldView = [[UIView alloc] initWithFrame:CGRectMake(35, posY, Screen_Width - 70, 50)];
        fieldView.backgroundColor = RGB(244, 244, 244);
        fieldView.layer.cornerRadius = 5;
        fieldView.layer.masksToBounds = YES;
        [self.view addSubview:fieldView];
        
        textField = [ISSCreateUIHelper makeTextField:self
                                           withFrame:CGRectMake(15, 0, fieldView.frame.size.width - 20, fieldView.frame.size.height)
                                              andTag:i + 100
                                      andPlaceholder:i == 0 ? LoadLanguagesKey(@"login_input_username") : LoadLanguagesKey(@"login_input_password")
                                           maxLength:20];
        [fieldView addSubview:textField];
        if (i == 0) {
            //textField.keyboardType = UIKeyboardTypeNumberPad;
            if ([ISSDataStorage hasObject:@"ISSUserName"]) textField.text = [ISSDataStorage getObject:@"ISSUserName"];
        }
        else if (i == 1) {
            textField.secureTextEntry = YES;
            if ([ISSDataStorage hasObject:@"ISSPassWord"]) textField.text = [ISSDataStorage getObject:@"ISSPassWord"];
        }
        
        posY = CGRectGetMaxY(fieldView.frame) + 15;
    }
    
    //确定位置
    CGFloat bottomHeight = Screen_Height - CGRectGetMaxY(fieldView.frame);
    X_Button *loginBtn = [ISSCreateUIHelper createSubmitBtnWithTitle:LoadLanguagesKey(@"btn_login") posY:CGRectGetMaxY(fieldView.frame) + bottomHeight/4];
    [loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    
    NSString *findPwdStr = LoadLanguagesKey(@"login_btn_findPwd");
    CGSize findSize = [findPwdStr sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]}];
    NSString *alertStr = LoadLanguagesKey(@"login_alert_forgetPwd");
    UILabel *alertLb = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(loginBtn.frame) + 30 fontSize:12 color:[UIColor lightGrayColor] text:alertStr];
    [alertLb setLeft:(Screen_Width - (CGRectGetWidth(alertLb.frame) + 10 + findSize.width))/2];
    [self.view addSubview:alertLb];
    
    X_Button *findBtn = [X_Button buttonWithType:UIButtonTypeCustom];
    findBtn.frame = CGRectMake(CGRectGetMaxX(alertLb.frame) - 10, 0, findSize.width + 40, 40);
    [findBtn setCenterY:alertLb.centerY];
    findBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [findBtn setTitleColor:RGB(36, 104, 246) forState:UIControlStateNormal];
    [findBtn setTitleColor:RGBA(36, 104, 246,0.7) forState:UIControlStateHighlighted];
    [findBtn setTitle:findPwdStr forState:UIControlStateNormal];
    [findBtn addTarget:self action:@selector(findPwdAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:findBtn];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //[self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
