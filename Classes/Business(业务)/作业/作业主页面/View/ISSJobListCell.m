//
//  ISSJobListCell.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobListCell.h"

@implementation ISSJobListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _bgContentView = [[UIView alloc] initWithFrame:CGRectMake(15, 10, Screen_Width - 30, 110)];
        _bgContentView.backgroundColor = [UIColor whiteColor];
        _bgContentView.layer.cornerRadius = 8;
        _bgContentView.layer.masksToBounds = YES;
        _bgContentView.layer.borderWidth = 0.5;
        _bgContentView.layer.borderColor = [UIColor borderColor].CGColor;
        [self.contentView addSubview:_bgContentView];
        
        _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 0, 39, 39)];
        _iconView.image = [UIImage imageNamed:@"job_list_danger"];
        [_bgContentView addSubview:_iconView];
        
        _titleLb = [UILabel oneLineLbWithX:45 y:0 width:100 fontSize:14 color:[UIColor titleColor_26] text:@"项目名称"];
        [_titleLb setCenterY:_iconView.centerY];
        [_bgContentView addSubview:_titleLb];
        
        NSString *dateText = @"2018年12月12日 12:59:00 ";
        CGSize dateSize = [dateText sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]}];
        _dateLb = [UILabel oneLineLbWithX:CGRectGetWidth(_bgContentView.frame) - 10 - (dateSize.width + 5) y:0 width:dateSize.width + 5 fontSize:12 color:[UIColor describeColor_61] text:dateText];
        [_dateLb setCenterY:_iconView.centerY];
        _dateLb.textAlignment = NSTextAlignmentRight;
        [_bgContentView addSubview:_dateLb];
        
        [_titleLb setWidth:CGRectGetMinX(_dateLb.frame) - CGRectGetMinX(_titleLb.frame)];
        
        _descLb = [UILabel mutLineLbWithX:15 y:40 width:CGRectGetWidth(_bgContentView.frame) - 30 fontSize:13 color:[UIColor describeColor_61] text:@""];
        [_bgContentView addSubview:_descLb];
        
        _addressLb = [UILabel oneLineLbWithX:CGRectGetMinX(_descLb.frame) y:0 fontSize:12 color:[UIColor lightGrayColor] text:@"作业地点"];
        [_addressLb setTop:CGRectGetHeight(_bgContentView.frame) - 8 - CGRectGetHeight(_addressLb.frame)];
        [_bgContentView addSubview:_addressLb];
        
        _closeLb = [UILabel oneLineLbWithX:0 y:0 fontSize:12 color:[UIColor redColor] text:LoadLanguagesKey(@"job_main_unClose")];
        [_closeLb setLeft:CGRectGetWidth(_bgContentView.frame) - 10 - CGRectGetWidth(_closeLb.frame)];
        [_closeLb setCenterY:_addressLb.centerY];
        [_bgContentView addSubview:_closeLb];
        
        _approvalLb = [UILabel oneLineLbWithX:0 y:CGRectGetMinY(_closeLb.frame) fontSize:12 color:[UIColor redColor] text:LoadLanguagesKey(@"job_main_unApprove")];
        [_approvalLb setLeft:CGRectGetMinX(_closeLb.frame) - 20 - CGRectGetWidth(_approvalLb.frame)];
        [_bgContentView addSubview:_approvalLb];
        
        [_addressLb setWidth:CGRectGetMinX(_approvalLb.frame) - 10 - CGRectGetMinX(_addressLb.frame)];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = ISSBackgroundColor;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutCell:(NSDictionary *)data
{
    if ([data[@"isDanger"] integerValue] == 1) _iconView.image = [UIImage imageNamed:@"job_list_danger"];
    else _iconView.image = [UIImage imageNamed:@"job_list_normal"];
    _titleLb.text = [NSString noNullString:data[@"name"]];
    
    _descLb.text = [NSString noNullString:data[@"content"]];
    _descLb.frame = [ISSPubfun getLimitLabelRect:_descLb limitLine:2];
    
    _dateLb.text = [ISSDataDeal getDateStrFromResponseData:[data[@"createTime"] longLongValue] format:@"yyyy年MM月dd日 HH:mm"];
    
    /*
     "job_main_unApprove" = "未审批";
     "job_main_approved" = "已审批";
     "job_main_unClose" = "未关闭";
     "job_main_closed" = "已关闭";
     */
    
    // 作业状态
    /*
    0.新建
    1.撤销
    2.负责人已扫码开始
    3.已开工完成
    4.负责人结束扫码
    5.结束
     */
    NSInteger status = [data[@"status"] integerValue];
    if (status < 3) {
        _approvalLb.text = LoadLanguagesKey(@"job_main_unApprove");
        _approvalLb.textColor = [UIColor redColor];
    }
    else
    {
        _approvalLb.text = LoadLanguagesKey(@"job_main_approved");
        _approvalLb.textColor = [UIColor darkGrayColor];
    }
    
    if (status == 1)
    {
        _closeLb.text = LoadLanguagesKey(@"job_main_cancel");
        _closeLb.textColor = [UIColor redColor];
    }
    else
    {
        // status
        //NSInteger status = [data[@"status"] integerValue];
        if (status == 5) {
            _closeLb.text = LoadLanguagesKey(@"job_main_closed");
            _closeLb.textColor = [UIColor darkGrayColor];
        }
        else
        {
            _closeLb.text = LoadLanguagesKey(@"job_main_unClose");
            _closeLb.textColor = [UIColor redColor];
        }
    }
    
    
    _addressLb.text = [NSString noNullString:data[@"part"]];
}

@end
