//
//  ISSJobSegView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobSegView.h"

@interface ISSJobSegView ()
{
    UIView *_animatedLine;
    BOOL _needScroll;
}

@end

@implementation ISSJobSegView

- (void)clickAction:(X_Button *)btn
{
    if (btn.selected) {
        return;
    }
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[X_Button class]]) {
            ((X_Button *)view).selected = NO;
        }
    }
    
    btn.selected = YES;
    [_animatedLine setWidth:btn.width];
    [UIView animateWithDuration:.3 animations:^{
        [_animatedLine setLeft:CGRectGetMinX(btn.frame)];
    }];
    
    if (_needScroll)
    {
        if (_animatedLine.centerX > Screen_Width/2) {
            [self setContentOffset:CGPointMake(MIN(_animatedLine.centerX - Screen_Width/2, self.contentSize.width - Screen_Width), 0) animated:YES];
        }
        else {
            [self setContentOffset:CGPointMake(MAX(_animatedLine.centerX - Screen_Width/2, 0), 0) animated:YES];
        }
    }
    
    if (_clickBlock) {
        _clickBlock(btn.tag - 10);
    }
}

- (instancetype)initWithFrame:(CGRect)frame segList:(NSArray *)segList clickBlock:(JobSegClickBlock)clickBlock
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _needScroll = [self needScroll:segList];
        _clickBlock = clickBlock;
        
        self.backgroundColor = [UIColor whiteColor];
        self.showsHorizontalScrollIndicator = NO;
        CGFloat padding = 15;
        CGFloat btnWidth = (Screen_Width - 15*(segList.count + 1))/segList.count;
        CGFloat posX = padding;
        
        X_Button *segBtn = nil;
        NSString *name = @"";
        for (int i = 0; i < segList.count; i++)
        {
            name = [NSString noNullString:segList[i][@"name"]];
            if (_needScroll) {
                CGSize size = [name sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]}];
                btnWidth = size.width + 40;
            }
            segBtn = [X_Button buttonWithType:UIButtonTypeCustom];
            segBtn.frame = CGRectMake(posX, 0, btnWidth, frame.size.height);
            [segBtn setTitleColor:[UIColor describeColor_61] forState:UIControlStateNormal];
            [segBtn setTitleColor:[UIColor titleColor_26] forState:UIControlStateSelected];
            segBtn.titleLabel.font = [UIFont systemFontOfSize:13];
            [segBtn setTitle:name forState:UIControlStateNormal];
            segBtn.tag = i + 10;
            [segBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:segBtn];
            
            posX = CGRectGetMaxX(segBtn.frame) + padding;
            
            if (i == 0)
            {
                segBtn.selected = YES;
                
                _animatedLine = [[UIView alloc] initWithFrame:CGRectMake(segBtn.left, frame.size.height - 2, segBtn.width, 2)];
                _animatedLine.backgroundColor = [UIColor themeColor];
                [self addSubview:_animatedLine];
            }
        }
        if (_needScroll) {
            [self setContentSize:CGSizeMake(posX, CGRectGetHeight(self.frame))];
        }
    }
    return self;
}

- (void)dealMoveLineFinish:(NSInteger)segIndex
{
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[X_Button class]]) {
            ((X_Button *)view).selected = NO;
        }
    }
    X_Button *btn = [self viewWithTag:10 + segIndex];
    btn.selected = YES;
    [_animatedLine setWidth:btn.width];
    [UIView animateWithDuration:.3 animations:^{
        [_animatedLine setLeft:CGRectGetMinX(btn.frame)];
    }];
    if (_needScroll)
    {
        if (_animatedLine.centerX > Screen_Width/2) {
            [self setContentOffset:CGPointMake(MIN(_animatedLine.centerX - Screen_Width/2, self.contentSize.width - Screen_Width), 0) animated:YES];
        }
        else {
            [self setContentOffset:CGPointMake(MAX(_animatedLine.centerX - Screen_Width/2, 0), 0) animated:YES];
        }
    }
}

- (void)updateSegView:(NSArray *)segList
{
    for (UIView *view in self.subviews)
    {
        if ([view isKindOfClass:[X_Button class]])
        {
            X_Button *btn = (X_Button *)view;
            [btn setTitle:segList[btn.tag - 10][@"name"] forState:UIControlStateNormal];
        }
    }
}

- (BOOL)needScroll:(NSArray *)list
{
    BOOL flag = NO;
    CGFloat padding = 15;
    CGFloat posX = padding;
    for (NSDictionary *data in list)
    {
        CGSize size = [[NSString noNullString:data[@"name"]] sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]}];
        posX += (size.width + 40) + padding;
    }
    if (posX > Screen_Width) {
        flag = YES;
    }
    return flag;
}

@end
