//
//  ISSJobListCell.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobListCell : UITableViewCell
{
    UIView *_bgContentView;
    UIImageView *_iconView;
    UILabel *_titleLb;
    UILabel *_dateLb;
    UILabel *_descLb;
    UILabel *_addressLb;
    UILabel *_approvalLb;
    UILabel *_closeLb;
}

- (void)layoutCell:(NSDictionary *)data;

@end
