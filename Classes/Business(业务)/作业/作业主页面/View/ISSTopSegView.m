//
//  ISSTopSegView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSTopSegView.h"

@interface ISSTopSegView()

@property (nonatomic,strong) NSArray *segList;

@end

@implementation ISSTopSegView

- (void)clickAction:(X_Button *)btn
{
    if (btn.selected) {
        if (_clickBlock) {
            _clickBlock(btn.tag - 1);
        }
    }
    else
    {
        for (UIView *view in self.subviews) {
            if ([view isKindOfClass:[X_Button class]]) {
                ((X_Button *)view).selected = NO;
            }
        }
        
        btn.selected = YES;
        if (_clickBlock) {
            _clickBlock(btn.tag - 1);
        }
    }
    
}

- (instancetype)initWithFrame:(CGRect)frame segList:(NSArray *)segList clickBlock:(TopSegClickBlock)clickBlock
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _segList = segList;
        _clickBlock = clickBlock;
        
        self.backgroundColor = [UIColor whiteColor];
        CGFloat padding = 25;
        //CGFloat btnWidth = (Screen_Width - padding*(segList.count + 1))/segList.count;
        CGFloat btnWidth = (Screen_Width - padding*(segList.count + 1))/5;
        CGFloat posX = padding;
        
        X_Button *segBtn = nil;
        for (int i = 0; i < segList.count; i++)
        {
            segBtn = [X_Button buttonWithType:UIButtonTypeCustom];
            segBtn.frame = CGRectMake(posX, (frame.size.height - 30)/2, i == 2 ? btnWidth : btnWidth*2, 30);
            segBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            [segBtn setTitle:segList[i] forState:UIControlStateNormal];
            if (i == 0) {
                [segBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x16246f)] forState:UIControlStateNormal];
                [segBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xff6000)] forState:UIControlStateSelected];
                [segBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [segBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
                segBtn.selected = YES;
            }
            else if (i == 1) {
                [segBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x16246f)] forState:UIControlStateNormal];
                [segBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x177b98)] forState:UIControlStateSelected];
                [segBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [segBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            }
            else {
                [segBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x16246f)] forState:UIControlStateNormal];
                [segBtn setBackgroundImage:[UIImage imageWithColor:RGB(255, 255, 255)] forState:UIControlStateSelected];
                [segBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [segBtn setTitleColor:[UIColor themeColor] forState:UIControlStateSelected];
            }
            if ([segList[i] length] > 6) {
                segBtn.frame = CGRectMake(posX, (frame.size.height - 34)/2, i == 2 ? btnWidth : btnWidth*2, 34);
                segBtn.titleLabel.numberOfLines = 0;
            }
            
            segBtn.layer.cornerRadius = CGRectGetHeight(segBtn.frame)/2;
            segBtn.layer.masksToBounds = YES;
            segBtn.tag = i + 1;
            [segBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:segBtn];
            
            posX = CGRectGetMaxX(segBtn.frame) + padding;

        }
    }
    return self;
}

- (void)updateSegView:(NSDictionary *)data
{
    NSArray *list = data[@"list"];
    X_Button *btn = nil;
    for (int i = 0; i < list.count; i++)
    {
        btn = [self viewWithTag:i + 1];
        [btn setTitle:FormatString(@"%@ %ld",_segList[i],[list[[self getIndex:i]][@"total"] longValue]) forState:UIControlStateNormal];
    }
}

- (NSInteger)getIndex:(int)i
{
    if (i == 1) return 0;
    else if (i == 0) return 1;
    else return i;
}

@end
