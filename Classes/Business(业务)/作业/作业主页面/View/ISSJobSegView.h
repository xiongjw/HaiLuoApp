//
//  ISSJobSegView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^JobSegClickBlock)(NSInteger index);

@interface ISSJobSegView : UIScrollView

@property (nonatomic, copy) JobSegClickBlock clickBlock;

- (instancetype)initWithFrame:(CGRect)frame segList:(NSArray *)segList clickBlock:(JobSegClickBlock)clickBlock;

- (void)dealMoveLineFinish:(NSInteger)segIndex;

- (void)updateSegView:(NSArray *)segList;

@end
