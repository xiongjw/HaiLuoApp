//
//  ISSTopSegView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TopSegClickBlock)(NSInteger index);

@interface ISSTopSegView : UIView

@property (nonatomic, copy) TopSegClickBlock clickBlock;

- (instancetype)initWithFrame:(CGRect)frame segList:(NSArray *)segList clickBlock:(TopSegClickBlock)clickBlock;

- (void)updateSegView:(NSDictionary *)data;

@end
