//
//  ISSJobMainVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobMainVC : ISSBaseVC

//- (void)updateTopSegView:(NSInteger)total;
- (void)updateTopSegView:(NSDictionary *)data;

@end
