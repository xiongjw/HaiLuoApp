//
//  ISSJobMainVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobMainVC.h"

#import "ISSJobSonVC.h"
#import "ISSBaseSearchVC.h"
#import "ISSAddJobVC.h"
#import "ISSSearchResultListVC.h"

#import "ISSTopSegView.h"

@interface ISSJobMainVC ()
{
    ISSTopSegView *_topSegView;
    NSInteger _currentTopIndex;
    
    X_Button *_rightAddBtn;
}

@property (nonatomic,strong) NSArray *jobTypeList;
@property (nonatomic,strong) NSMutableArray *showJobTypeList;

@end

@implementation ISSJobMainVC

- (void)updateTopSegView:(NSDictionary *)data
{
    [_topSegView updateSegView:data];
}

- (void)goAddVC
{
    ISSAddJobVC *vc = [[ISSAddJobVC alloc] init];
    //vc.jobType = _currentTopIndex;
    vc.jobType = _currentTopIndex == 0 ? 1 : 0;
    vc.jobTypeList = self.jobTypeList;
    vc.successBlock = ^{
        //列表刷新
        ISSJobSonVC *vc = self.childViewControllers[_currentTopIndex];
        [vc resfreshList];
    };
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
    [self presentViewController:nav animated:YES completion:NULL];
}

- (void)addAction
{
    // 判断 jobTypeList
    
    /*
    if (self.jobTypeList.count == 0) {
        WeakSelf(self)
        [self getJobType:^{
            [weakself goAddVC];
        }];
    }
    else
    {
        [self goAddVC];
    }
     
     */
    
    // 不判断了，要求这样 2018.6.7
    [self goAddVC];
}

- (void)pushSearchResultListVC:(NSString *)searchText searchItem:(NSDictionary *)searchItem
{
    ISSSearchResultListVC *vc = [[ISSSearchResultListVC alloc] init];
    vc.searchType = ISSSearch_Job;
    vc.searchText = searchText;
    vc.searchItem = searchItem;
    vc.jobList = self.jobTypeList;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchAction
{
    WeakSelf(self)
    ISSBaseSearchVC *vc = [[ISSBaseSearchVC alloc] init];
    vc.cateList = @[
                    @{@"name":LoadLanguagesKey(@"search_job_all"),@"code":@"-1",@"cacheKey":@"Job_searchAllJob"},
                    @{@"name":LoadLanguagesKey(@"search_job_dangerJob"),@"code":@"0",@"cacheKey":@"Job_searchDangerJob"},
                    @{@"name":LoadLanguagesKey(@"search_job_normalJob"),@"code":@"1",@"cacheKey":@"Job_searchNormalJob"},
                    @{@"name":LoadLanguagesKey(@"search_job_myJob"),@"code":@"2",@"cacheKey":@"Job_searchMyJob"}
                    ];
    vc.clickBlock = ^(NSDictionary *cateItem, NSString *searchText) {
        [weakself pushSearchResultListVC:searchText searchItem:cateItem];
    };
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
    [self presentViewController:nav animated:NO completion:NULL];
}

- (void)createRightBar
{
    if ([ISSUserData sharedInstance].operation_privilege_zy_add)
    {
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = 0;
        
        WeakSelf(self)
        _rightAddBtn = [ISSNavUtil getImageBtn:3 clickBlock:^{
            [weakself addAction];
        }];
        UIBarButtonItem *rightItem1 = [[UIBarButtonItem alloc] initWithCustomView:_rightAddBtn];
        UIBarButtonItem *rightItem2 = [ISSNavUtil getBarItem:self btnType:2 clickAction:@selector(searchAction)];
        
        NSArray *buttonArray = [[NSArray alloc]
                                initWithObjects:rightItem1,negativeSeperator,rightItem2, nil];
        
        self.navigationItem.rightBarButtonItems = buttonArray;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItem:self btnType:2 clickAction:@selector(searchAction)];
    }
}

- (void)addChildView:(NSInteger)tag sonList:(NSArray *)sonList
{
    ISSJobSonVC *vc = [[ISSJobSonVC alloc] init];
    vc.topHeight = _topSegView.height;
    vc.sonList = sonList;
    vc.mainType = tag - 997;
    vc.view.frame = CGRectMake(0, CGRectGetMaxY(_topSegView.frame), Screen_Width, vc.mScrollView.height);
    vc.view.tag = tag;
    [self.view addSubview:vc.view];
    [self addChildViewController:vc];
}

- (void)dealClickTopBtn:(NSInteger)index
{
    if (self.jobTypeList.count == 0) {
        [self getJobType];
        return;
    }
    if (index == _currentTopIndex) {
        return;
    }
    UIView *v1 = [self.view viewWithTag:998];
    UIView *v2 = [self.view viewWithTag:997];
    UIView *v3 = [self.view viewWithTag:999];
    if (index == 0)
    {
        if (_rightAddBtn) _rightAddBtn.hidden  = NO;
        
        if (v1) v1.hidden = NO;
        else
        {
            [self addChildView:998 sonList:self.showJobTypeList];
        }
        if (v2) v2.hidden = YES;
        if (v3) v3.hidden = YES;
    }
    else if (index == 1)
    {
        if (_rightAddBtn) _rightAddBtn.hidden  = NO;
        
        if (v1) v1.hidden = YES;
        if (v2) v2.hidden = NO;
        else
        {
            [self addChildView:997 sonList:self.showJobTypeList];
        }
        if (v3) v3.hidden = YES;
    }
    else
    {
        if (_rightAddBtn) _rightAddBtn.hidden  = YES;
        
        if (v1) v1.hidden = YES;
        if (v2) v2.hidden = YES;
        if (v3) v3.hidden = NO;
        else
        {
            [self addChildView:999 sonList:@[
                                             @{@"id":@(0),@"name":LoadLanguagesKey(@"job_main_myApprove")},
                                             @{@"id":@(1),@"name":LoadLanguagesKey(@"job_main_review")},
                                             @{@"id":@(2),@"name":LoadLanguagesKey(@"job_main_myRelease")}
                                             ]];
        }
    }
    
    _currentTopIndex = index;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:LoadLanguagesKey(@"tabbar_job") showBackBtn:NO];
    [self createRightBar];
    
    _currentTopIndex = -1;
    WeakSelf(self)
    _topSegView = [[ISSTopSegView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 46) segList:@[LoadLanguagesKey(@"job_main_dangerJob"),LoadLanguagesKey(@"job_main_normalJob"),LoadLanguagesKey(@"job_main_mine")] clickBlock:^(NSInteger index) {
        [weakself dealClickTopBtn:index];
    }];
    _topSegView.backgroundColor = [UIColor themeColor];
    [self.view addSubview:_topSegView];
    
    self.showJobTypeList = [NSMutableArray new];
    [self.showJobTypeList addObject:@{@"id":@"",@"name":@"全部"}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.jobTypeList.count == 0) {
        [self getJobType];
    }
}

- (void)getJobType
{
    [self getJobType:nil];
}

- (void)getJobType:(void (^)(void))block
{
    WeakSelf(self)
    [X_HttpUtil apiRequest:@"job/getJobType" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
        
        //NSLog(@"resultData:%@",resultData);
        weakself.jobTypeList = resultData[@"data"][@"list"];
        [weakself.showJobTypeList addObjectsFromArray:weakself.jobTypeList];
        if (weakself.jobTypeList.count > 0)
        {
            if (block) block();
            else [weakself dealClickTopBtn:0];
        }
        else {
            //[self.view makeToast:@""];
        }
    }];
}

@end
