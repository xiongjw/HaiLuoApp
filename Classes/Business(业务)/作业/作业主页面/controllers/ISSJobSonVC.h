//
//  ISSJobSonVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobSonVC : ISSBaseVC

@property (nonatomic,strong) UIScrollView *mScrollView;

@property (nonatomic) CGFloat topHeight;
@property (nonatomic,assign) NSInteger mainType;
@property (nonatomic,strong) NSArray *sonList;

- (void)resfreshList;

@end
