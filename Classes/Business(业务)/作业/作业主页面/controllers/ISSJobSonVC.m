//
//  ISSJobSonVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobSonVC.h"

#import "ISSJobSegView.h"

#import "ISSJobListCell.h"

#import "ISSJobDetailVC.h"

#import "ISSJobMainVC.h"

@interface ISSJobSonVC () <UITableViewDelegate,UITableViewDataSource>
{
    ISSJobSegView *_segView;
    NSInteger _currentSegIndex;
}

@property (nonatomic,strong) NSMutableArray *resultDataList;

@end

@implementation ISSJobSonVC

- (void)resfreshList
{
    UITableView *mTableView = [self.mScrollView viewWithTag:99 + _currentSegIndex];
    [mTableView.mj_header beginRefreshing];
}

- (void)dealScrollFinish
{
    NSMutableArray *list = self.resultDataList[_currentSegIndex];
    if (list.count == 0)
    {
        UITableView *mTableView = [self.mScrollView viewWithTag:99 + _currentSegIndex];
        [mTableView.mj_header beginRefreshing];
    }
}

- (void)dealClickTopBtn:(NSInteger)index
{
    _currentSegIndex = index;
    
    CGRect frame = self.mScrollView.frame;
    frame.origin.x = _currentSegIndex * Screen_Width;
    
    [self.mScrollView scrollRectToVisible:frame animated:YES];
    
    [self dealScrollFinish];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.resultDataList = [NSMutableArray new];
    _currentSegIndex = 0;
    
    CGFloat segViewHeight = 44;
    WeakSelf(self)
    _segView = [[ISSJobSegView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, segViewHeight) segList:self.sonList clickBlock:^(NSInteger index) {
        [weakself dealClickTopBtn:index];
    }];
    [self.view addSubview:_segView];
    
    CGRect frame = CGRectMake(0, segViewHeight, Screen_Width, Screen_Height - 64 - self.topHeight  - segViewHeight - Tabbar_Height - 5);
    self.mScrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.mScrollView.delegate = self;
    self.mScrollView.pagingEnabled = YES;
    self.mScrollView.showsHorizontalScrollIndicator = NO;
    self.mScrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.mScrollView];
    
    for (int i = 0; i < self.sonList.count; i++)
    {
        [self.resultDataList addObject:[NSMutableArray new]];
        
        UITableView *mTableView = [[UITableView alloc] initWithFrame:CGRectMake(i*Screen_Width, 0, Screen_Width, frame.size.height) style:UITableViewStylePlain];
        mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        mTableView.tag = 99 + i;
        mTableView.dataSource = self;
        mTableView.delegate = self;
        
        [mTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        mTableView.backgroundColor = [UIColor clearColor];
        mTableView.backgroundView = nil;
        [self.mScrollView addSubview:mTableView];
        
        mTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
        mTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
        mTableView.mj_footer.hidden = YES;
    }
    [self.mScrollView setContentSize:CGSizeMake(Screen_Width *self.sonList.count, frame.size.height)];
    
    UITableView *mTableView = [self.mScrollView viewWithTag:99 + _currentSegIndex];
    [mTableView.mj_header beginRefreshing];
    
    
    // 增加通知，如果作业撤销了，这里要刷新
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshJobList) name:@"refreshJobListNote" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshJobListNote" object:nil];
}

- (void)refreshJobList
{
    [self resfreshList];
}

- (void)headerRereshing
{
    [self performSelector:@selector(requestDataReresh:) withObject:@"down"];
}

- (void)footerRereshing {
    [self performSelector:@selector(requestDataReresh:) withObject:@"up"];
}

- (void)updateSegView:(NSDictionary *)data
{
    [_segView updateSegView:[self getUpdateList:data[@"list"][self.mainType]]];
    
    ISSJobMainVC *vc = (ISSJobMainVC *)self.parentViewController;
    [vc updateTopSegView:data];
}

- (void)requestDataReresh:(NSString *)direction
{
    WeakSelf(self)
    if ([@"down" isEqualToString:direction])
    {
        // 请求作业总数
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            [X_HttpUtil apiRequest:@"job/getJobTotal" param:@{@"type":@(self.mainType)} view:nil showErrorMessage:NO successBlock:^(NSDictionary *resultData) {
                //
                NSDictionary *data = resultData[@"data"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [weakself updateSegView:data];
                });
                
            } failureBlock:NULL];
        });
        
    }
    
    // 请求列表
    __block NSMutableArray *currentList = self.resultDataList[_currentSegIndex];
    NSDictionary *data = @{
                           @"type":@(self.mainType),
                           @"item":self.sonList[_currentSegIndex][@"id"],
                           @"lastId":[@"down" isEqualToString:direction] ? @"" : [currentList lastObject][@"id"]
                           };
    
    [X_HttpUtil apiRequest:@"job/queryJob" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        
        UITableView *mTableView = [weakself.mScrollView viewWithTag:99 + _currentSegIndex];
        if (mTableView.mj_header.isRefreshing) [mTableView.mj_header endRefreshing];
        if (mTableView.mj_footer.isRefreshing) [mTableView.mj_footer endRefreshing];
        
        NSArray *list = resultData[@"data"][@"list"];
        if ([@"down" isEqualToString:direction]) {
            
            if (mTableView.mj_footer.hidden) {
                mTableView.mj_footer.hidden = NO;
                [mTableView.mj_footer resetNoMoreData];
            }
            [currentList removeAllObjects];
        }
        if (list.count < [PageSize integerValue])
        {   mTableView.mj_footer.hidden = YES;
            [mTableView.mj_footer endRefreshingWithNoMoreData];
        }
        if (list.count > 0)
        {
            [currentList addObjectsFromArray:list];
        }
        [mTableView reloadData];
    } failureBlock:^(NSDictionary *resultData) {
        
        UITableView *mTableView = [weakself.mScrollView viewWithTag:99 + _currentSegIndex];
        if (mTableView.mj_header.isRefreshing) [mTableView.mj_header endRefreshing];
        if (mTableView.mj_footer.isRefreshing) [mTableView.mj_footer endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultDataList[_currentSegIndex] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ISSJobListCell";
    ISSJobListCell *cell = (ISSJobListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ISSJobListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSMutableArray *list = self.resultDataList[_currentSegIndex];
    [cell layoutCell:list[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = self.resultDataList[_currentSegIndex][indexPath.row];
    ISSJobDetailVC *vc = [[ISSJobDetailVC alloc] init];
    vc.jobId = [data[@"id"] longValue];
    vc.jobList = self.sonList;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UIScrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mScrollView)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        _currentSegIndex = page;
        
        [_segView dealMoveLineFinish:_currentSegIndex];
        
        [self dealScrollFinish];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}


- (NSArray *)getUpdateList:(NSDictionary *)total
{
    NSMutableArray *newSonList = [NSMutableArray new];
    for (NSDictionary *data in self.sonList)
    {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] initWithDictionary:data];
        
        id obj = data[@"id"];
        // 全部
        if ([obj isKindOfClass:[NSString class]]) {
            [item setObject:FormatString(@"%@ %d",data[@"name"],[total[@"total"] intValue]) forKey:@"name"];
        }
        else
        {
            [item setObject:FormatString(@"%@ %d",data[@"name"],[self getSingleTotal:total itemId:[data[@"id"] longValue]]) forKey:@"name"];
        }
        [newSonList addObject:item];
    }
    return newSonList.copy;
}

- (int)getSingleTotal:(NSDictionary *)total itemId:(long)itemId
{
    int singleTotal = 0;
    NSArray *list = total[@"list"];
    for (NSDictionary *data in list)
    {
        if ([data[@"id"] isKindOfClass:[NSString class]]) {
            continue;
        }
        if ([data[@"id"] longValue] == itemId) {
            singleTotal = [data[@"count"] intValue];
            break;
        }
    }
    return singleTotal;
}

@end
