//
//  ISSEditJobVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^EditJobSuccessBlock)(void);

@interface ISSEditJobVC : ISSBaseTableVC

@property (nonatomic, copy) EditJobSuccessBlock successBlock;

@property (nonatomic,strong) NSDictionary *detailData;

@end
