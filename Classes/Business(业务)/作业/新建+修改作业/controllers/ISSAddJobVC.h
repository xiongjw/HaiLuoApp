//
//  ISSAddJobVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^AddJobSuccessBlock)(void);

@interface ISSAddJobVC : ISSBaseTableVC

// 作业类型（危险作业，常规作业）
@property (nonatomic,assign) NSInteger jobType;
@property (nonatomic,strong) NSArray *jobTypeList;

@property (nonatomic, copy) AddJobSuccessBlock successBlock;

@end
