//
//  ISSEditJobVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSEditJobVC.h"

#import "ISSJobInputCell.h"
#import "ISSBaseTextFieldCell.h"

#import "ISSJobDetailCellModel.h"

#import "ISSJobInputModel.h"
#import "ISSJobCreateForm.h"

#import "X_PopPickerView.h"
#import "ISSChoosePersonVC.h"

@interface ISSEditJobVC ()

@end

@implementation ISSEditJobVC

- (BOOL)checkHasSamePerson:(NSMutableDictionary *)mut
{
    NSArray *list = @[mut[@"guardian"],mut[@"auditor"],mut[@"approver"]];
    
    NSMutableSet *set = [NSMutableSet new];
    for (int i = 0; i < list.count; i++) {
        [set addObject:list[i]];
    }
    if (list.count == set.count) {
        return NO;
    }
    return YES;
}

- (void)submitAction
{
    NSMutableDictionary *mutDic = [NSMutableDictionary new];
    for (NSArray *list in self.resultDataList)
    {
        for (ISSJobInputModel *model in list)
        {
            // 非必填
            if ([@"startTime" isEqualToString:model.code])
            {
                [mutDic setObject:@([ISSDataDeal getTimeStamp:model.value]) forKey:model.code];
            }
            else if ([@"endTime" isEqualToString:model.code])
            {
                if ([ISSDataDeal getTimeStamp:model.value] > 0)
                {
                    if ([mutDic[@"startTime"] longLongValue] >= [ISSDataDeal getTimeStamp:model.value]) {
                        // 时间 结束要晚于开始
                        [self.view makeToast:LoadLanguagesKey(@"job_add_alert2")];
                        return;
                    }
                    [mutDic setObject:@([ISSDataDeal getTimeStamp:model.value]) forKey:model.code];
                }
                else
                {
                    [mutDic setObject:@(0) forKey:model.code];
                }
            }
            else if ([@"numberPeople" isEqualToString:model.code])
            {
                [mutDic setObject:@([model.value intValue]) forKey:model.code];
            }
            else
            {
                [mutDic setObject:model.value forKey:model.code];
            }
        }
    }
    [mutDic setObject:self.detailData[@"work"][@"id"] forKey:@"id"];
    
    // 四个人不能是同一个人
    if ([self checkHasSamePerson:mutDic]) {
        [self.view makeToast:LoadLanguagesKey(@"job_add_alertSame")];
        return;
    }
    
    [X_HttpUtil apiRequest:@"job/modifyJob" param:mutDic view:self.view successBlock:^(NSDictionary *resultData) {
        
        [self.view makeToast:LoadLanguagesKey(@"job_detail_editSuccess")];
        if (_successBlock) {
            _successBlock();
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = LoadLanguagesKey(@"job_edit_navTitle");
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        [weakself dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 120-64)];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    
    self.mTableView.frame = CGRectMake(0, CGRectGetMaxY(topView.frame), Screen_Width, Screen_Height - 64 - CGRectGetMaxY(topView.frame));
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 100)];
    self.mTableView.tableFooterView = footView;
    X_Button *btn = [ISSCreateUIHelper createInputSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:50];
    [btn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:btn];

    self.resultDataList = [ISSJobCreateForm createEditJobForm:self.detailData[@"work"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.resultDataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultDataList[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __block ISSJobInputModel *model = self.resultDataList[indexPath.section][indexPath.row];
    if (model.cellType == 1)
    {
        static NSString *CellIdentifier = @"ISSBaseTextFieldCell";
        ISSBaseTextFieldCell *cell = (ISSBaseTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSBaseTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.leftLb.text = model.title;
        cell.rightLb.text = model.showValue;
        //cell.mTextField.placeholder = model.placeHolder;
        //[cell.mTextField setValue:UIColorFromRGBA(0xffffff, 0.5) forKeyPath:@"_placeholderLabel.textColor"];
        cell.mTextField.text = @"";
        if (model.showValue.length > 0) {
            cell.mTextField.text = model.showValue;
        }
        cell.mTextField.keyboardType = UIKeyboardTypeNumberPad;
        cell.inputBlock = ^(NSString *text) {
            model.value = text;
            model.showValue = text;
        };
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"ISSJobInputCell";
        ISSJobInputCell *cell = (ISSJobInputCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSJobInputCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.leftLb.text = model.title;
        cell.rightLb.text = model.showValue;
        
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    __block ISSJobInputModel *model = self.resultDataList[indexPath.section][indexPath.row];
    WeakSelf(self)
    if (model.cellType == 3)
    {
        X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:model.pickerModel];
        view.pickerTag = indexPath.row;
        [view showInView:self.view.window];
    }
    else
    {
        ISSChoosePersonVC *vc = [[ISSChoosePersonVC alloc] init];
        vc.title = model.title;
        vc.chooseBlock = ^(NSDictionary *data) {
            model.showValue = data[@"realName"];
            model.value = data[@"id"];
            [weakself.mTableView reloadRowsAtIndexPaths:@[indexPath]
                                       withRowAnimation:UITableViewRowAnimationAutomatic];
        };
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
        [self presentViewController:nav animated:YES completion:NULL];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    if (section == 0) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 1)];
    }
    else {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 5)];
    }
    view.backgroundColor = [UIColor clearColor];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(view.frame) - 1, Screen_Width, 1)];
    lineView.backgroundColor = [UIColor borderColor];
    [view addSubview:lineView];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0 ? 1 : 5;
}

//
- (void)actionPopView:(NSDictionary *)item pickerTag:(NSInteger)pickerTag
{
    ISSJobInputModel *model = self.resultDataList[0][pickerTag];
    model.pickerModel.value = item[model.pickerModel.codeKey];
    model.pickerModel.showValue = item[model.pickerModel.nameKey];
    
    model.value = model.pickerModel.value;
    model.showValue = model.pickerModel.showValue;
    [self.mTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:pickerTag inSection:0]]
                           withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
