//
//  ISSAddJobVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSAddJobVC.h"

#import "ISSJobCreateForm.h"

#import "ISSBaseTextFieldCell.h"
#import "ISSJobInputCell.h"

#import "ISSJobInputModel.h"

#import "X_PopPickerView.h"

//#import "ISSBaseChoosePersonVC.h"
#import "ISSChoosePersonVC.h"

#import "ISSLevelSelectedVC.h"

#import "ISSChooseDepartmentVC.h"

@interface ISSAddJobVC ()
{
    ISSJobInputModel *_gasDetectorModel;
    
    id _equipmentId;
}

@end

@implementation ISSAddJobVC

- (BOOL)checkHasSamePerson:(NSMutableDictionary *)mut
{
    NSArray *list = nil;
    if (mut[@"gas"]) {
        list = @[mut[@"gas"],mut[@"leading"],mut[@"guardian"],mut[@"auditor"],mut[@"approver"]];
    }
    else
        list = @[mut[@"leading"],mut[@"guardian"],mut[@"auditor"],mut[@"approver"]];
    
    NSMutableSet *set = [NSMutableSet new];
    for (int i = 0; i < list.count; i++) {
        [set addObject:list[i]];
    }
    if (list.count == set.count) {
        return NO;
    }
    return YES;
}

- (void)submitAction
{
    bool check = NO;
    NSMutableDictionary *mutDic = [NSMutableDictionary new];
    for (NSArray *list in self.resultDataList)
    {
        for (ISSJobInputModel *model in list)
        {
            // 开始校验非空
            if (model.value.length == 0) {
                check = YES;
                break;
            }
            
            //  增加入参
            if ([@"startTime" isEqualToString:model.code])
            {
                [mutDic setObject:@([ISSDataDeal getTimeStamp:model.value]) forKey:model.code];
            }
            else if ([@"endTime" isEqualToString:model.code])
            {
                if ([ISSDataDeal getTimeStamp:model.value] > 0)
                {
                    if ([mutDic[@"startTime"] longLongValue] >= [ISSDataDeal getTimeStamp:model.value]) {
                        // 时间 结束要晚于开始
                        [self.view makeToast:LoadLanguagesKey(@"job_add_alert2")];
                        return;
                    }
                    [mutDic setObject:@([ISSDataDeal getTimeStamp:model.value]) forKey:model.code];
                }
                else
                {
                    [mutDic setObject:@(0) forKey:model.code];
                }
            }
            else if ([@"type" isEqualToString:model.code])
            {
                [mutDic setObject:model.value forKey:model.code];
            }
            /*
            else if ([@"gas" isEqualToString:model.code])
            {
                if (self.jobType == 1)
                {
                    ISSJobInputModel *inputModel = self.resultDataList[1][0];
                    // 如果选择 有限空间作业 ，有气体检测人，没有负责人，反之
                    if ([@"有限空间作业" isEqualToString:inputModel.showValue]) {
                        [mutDic setObject:model.value forKey:model.code];
                    }
                }
            }
             */
            else
            {
                [mutDic setObject:model.value forKey:model.code];
            }
        }
        if (check) {
            break;
        }
    }
    
    if (check) {
        [self.view makeToast:LoadLanguagesKey(@"job_add_alertOver")];
        return;
    }
    [mutDic setObject:@(self.jobType) forKey:@"isDanger"];
    [mutDic setObject:_equipmentId forKey:@"equipmentId"];
    
    NSLog(@"mutDic:%@",mutDic);
    // 四个人不能是同一个人
    if ([self checkHasSamePerson:mutDic]) {
        [self.view makeToast:LoadLanguagesKey(@"job_add_alertSame")];
        return;
    }
    
    WeakSelf(self)
    [self showAlertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:LoadLanguagesKey(@"job_add_alertConfirm") btnList:@[LoadLanguagesKey(@"alert_btn_wait"),LoadLanguagesKey(@"alert_btn_confirmAgain")] handler:^(NSInteger index) {
        if (index == 1)
        {
            [X_HttpUtil apiRequest:@"job/createJob" param:mutDic view:weakself.view.window successBlock:^(NSDictionary *resultData) {
                
                //[weakself.view makeToast:LoadLanguagesKey(@"job_detail_submitSuccess")];
                if (_successBlock) {
                    _successBlock();
                }
                [weakself dismissViewControllerAnimated:YES completion:nil];
            }];
        }
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.jobType == 1) self.navigationItem.title = LoadLanguagesKey(@"job_add_navTitle_danger");
    else self.navigationItem.title = LoadLanguagesKey(@"job_add_navTitle_normal");
    
    WeakSelf(self)
    self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:6 clickBlock:^{
        [weakself dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 80)];
    self.mTableView.tableFooterView = footView;
    X_Button *btn = [ISSCreateUIHelper createInputSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:20];
    [btn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:btn];
    
    self.resultDataList = [ISSJobCreateForm createAddJobForm:self.jobType];
    
    //赋值
    ISSJobInputModel *model = [self.resultDataList[4] firstObject];
    model.pickerModel.dataSoure = self.jobTypeList;
    model.pickerModel.codeKey = @"id";
    
    // 危险作业进来，如果选择 危险类型，有限空间，显示气体检测人，不显示负责人，反正，则显示负责人，不显示气体检测人
    _gasDetectorModel = [[ISSJobInputModel alloc] init];
    _gasDetectorModel.code = @"gas";
    _gasDetectorModel.title = LoadLanguagesKey(@"job_base_GasDetector");
    _gasDetectorModel.cellType = 4;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.resultDataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultDataList[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __block ISSJobInputModel *model = self.resultDataList[indexPath.section][indexPath.row];
    
    // radio的
    if (model.cellType == 5)
    {
        static NSString *CellIdentifier = @"ISSJobRadioCell";
        ISSJobRadioCell *cell = (ISSJobRadioCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSJobRadioCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.leftLb.text = model.title;
        if (self.jobType == 1) {
            cell.leftRadio.image = [UIImage imageNamed:@"add_cell_ratioseltct"];
            cell.rightRadio.image = [UIImage imageNamed:@"add_cell_ratio"];
        }
        else
        {
            cell.leftRadio.image = [UIImage imageNamed:@"add_cell_ratio"];
            cell.rightRadio.image = [UIImage imageNamed:@"add_cell_ratioseltct"];
        }
        
        return cell;
    }
    // 输入的
    else if (model.cellType == 1)
    {
        static NSString *CellIdentifier = @"ISSBaseTextFieldCell";
        ISSBaseTextFieldCell *cell = (ISSBaseTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSBaseTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.leftLb.text = model.title;
        cell.rightLb.text = model.showValue;
        //cell.mTextField.placeholder = model.placeHolder;
        //[cell.mTextField setValue:UIColorFromRGBA(0xffffff, 0.5) forKeyPath:@"_placeholderLabel.textColor"];
        cell.mTextField.text = @"";
        if (model.showValue.length > 0) {
            cell.mTextField.text = model.showValue;
        }
        if ([@"numberPeople" isEqualToString:model.code]) {
            cell.mTextField.keyboardType = UIKeyboardTypeNumberPad;
        }
        else
        {
            cell.mTextField.keyboardType = UIKeyboardTypeDefault;
        }
        cell.inputBlock = ^(NSString *text) {
            model.value = text;
            model.showValue = text;
        };
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"ISSJobInputCell";
        ISSJobInputCell *cell = (ISSJobInputCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ISSJobInputCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (model.cellType == 4 || model.cellType == 2) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.leftLb.text = model.title;
        cell.rightLb.text = model.showValue;
        
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    WeakSelf(self)
    __block ISSJobInputModel *inputModel = self.resultDataList[indexPath.section][indexPath.row];
    // 数据选择器
    if (inputModel.cellType == 2)
    {
        [self.view endEditing:YES];
        // 危险作业类别
        if (indexPath.section == 1)
        {
            if (inputModel.pickerModel.dataSoure.count > 0)
            {
                X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
                view.pickerTag = indexPath.section*10 + indexPath.row;
                [view showInView:weakself.view.window];
            }
            else
            {
                [X_HttpUtil apiRequest:@"dictionaries/WXZYLB" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
                    NSLog(@"resultData:%@",resultData);
                    
                    inputModel.pickerModel.dataSoure = resultData[@"data"][@"list"];
                    //inputModel.pickerModel.codeKey = @"code";
                    //inputModel.pickerModel.nameKey = @"name";
                    
                    X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
                    view.pickerTag = indexPath.section*10 + indexPath.row;
                    [view showInView:weakself.view.window];
                }];
            }
        }
        // 设备名称
        else if (indexPath.section == 3)
        {
            ISSJobInputModel *model = self.resultDataList[3][0];
            if (model.value == nil || model.value.length == 0) {
                [self.view makeToast:LoadLanguagesKey(@"job_add_alert1")];
                return;
            }
            else
            {
                if (inputModel.pickerModel.dataSoure.count > 0)
                {
                    X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
                    view.pickerTag = indexPath.section*10 + indexPath.row;
                    [view showInView:self.view.window];
                }
                else
                {
                    // 请求设备类型,取上面选择的type
                    [X_HttpUtil apiRequest:@"device/queryByType" param:@{@"type":@([model.value longLongValue])} view:self.view successBlock:^(NSDictionary *resultData) {
                        NSLog(@"resultData:%@",resultData);
                        
                        inputModel.pickerModel.dataSoure = resultData[@"data"][@"list"];
                        inputModel.pickerModel.codeKey = @"equipCode";
                        inputModel.pickerModel.nameKey = @"name";
                        
                        X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
                        view.pickerTag = indexPath.section*10 + indexPath.row;
                        [view showInView:weakself.view.window];
                    }];
                }
                
            }
        }
        
        else if (indexPath.section == 4)
        {
            // 作业区域
            if (indexPath.row == 0)
            {
                if (inputModel.pickerModel.dataSoure.count > 0)
                {
                    X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
                    view.pickerTag = indexPath.section*10 + indexPath.row;
                    [view showInView:self.view.window];
                }
                else
                {
                    WeakSelf(self)
                    [X_HttpUtil apiRequest:@"job/getJobType" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
                        
                        //NSLog(@"resultData:%@",resultData);
                        weakself.jobTypeList = resultData[@"data"][@"list"];
                        inputModel.pickerModel.dataSoure = weakself.jobTypeList;
                        
                        X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
                        view.pickerTag = indexPath.section*10 + indexPath.row;
                        [view showInView:weakself.view.window];
                        
                    }];
                }
                
            }
            // 作业单位
            else
            {
                //之前有默认值不可选，现在有默认值 也可以去选了
                ISSChooseDepartmentVC *vc = [[ISSChooseDepartmentVC alloc] init];
                vc.chooseBlock = ^(NSDictionary *data) {
                    
                    inputModel.value = data[@"ID_"];
                    inputModel.showValue = data[@"ORG_NAME_"];
                    [weakself.mTableView reloadRowsAtIndexPaths:@[indexPath]
                                               withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                };
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
                [self presentViewController:nav animated:YES completion:NULL];
            }
            
        }
        
    }
    else if (inputModel.cellType == 3)
    {
        [self.view endEditing:YES];
        X_PopPickerView *view = [[X_PopPickerView alloc] initWithFactor:self pickerModel:inputModel.pickerModel];
        view.pickerTag = indexPath.section*10 + indexPath.row;
        [view showInView:self.view.window];
    }
    else if (inputModel.cellType == 4)
    {
        if (indexPath.section == 1 || indexPath.section == 2)
        {
            ISSChoosePersonVC *vc = [[ISSChoosePersonVC alloc] init];
            vc.title = inputModel.title;
            vc.chooseBlock = ^(NSDictionary *data) {
                inputModel.showValue = data[@"realName"];
                inputModel.value = data[@"id"];
                [weakself.mTableView reloadRowsAtIndexPaths:@[indexPath]
                                           withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
            [self presentViewController:nav animated:YES completion:NULL];
        }
        else
        {
            if (indexPath.row == 0)
            {
                // 请求设备类型
                if (inputModel.cellList.count > 0)
                {
                    ISSLevelSelectedVC *vc = [[ISSLevelSelectedVC alloc] init];
                    vc.level = 1;
                    vc.tableData = inputModel.cellList;
                    vc.levelSelectedItemBlock = ^(NSDictionary *item) {
                        inputModel.value = FormatString(@"%lld",[item[@"id"] longLongValue]);
                        inputModel.showValue = item[@"name"];
                        [weakself.mTableView reloadRowsAtIndexPaths:@[indexPath]
                                                   withRowAnimation:UITableViewRowAnimationAutomatic];
                    };
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                    [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
                    [self presentViewController:nav animated:YES completion:NULL];
                }
                else
                {
                    [X_HttpUtil apiRequest:@"device/getType" param:@{} view:self.view successBlock:^(NSDictionary *resultData) {
                        
                        NSLog(@"resultData:%@",resultData);
                        inputModel.cellList = resultData[@"data"][@"list"];
                        ISSLevelSelectedVC *vc = [[ISSLevelSelectedVC alloc] init];
                        vc.level = 1;
                        vc.tableData = inputModel.cellList;
                        vc.levelSelectedItemBlock = ^(NSDictionary *item) {
                            inputModel.value = FormatString(@"%lld",[item[@"id"] longLongValue]);
                            inputModel.showValue = item[@"name"];
                            [weakself.mTableView reloadRowsAtIndexPaths:@[indexPath]
                                                       withRowAnimation:UITableViewRowAnimationAutomatic];
                        };
                        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                        [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
                        [self presentViewController:nav animated:YES completion:NULL];
                    }];
                }
                
            }
            else if (indexPath.row == 1)
            {
                
            }
            else if (indexPath.row == 2)
            {
                // 由row1 带出来值
                return;
            }
        }
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.jobType == 0 && section == 1) {
        return nil;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 5)];
    view.backgroundColor = [UIColor clearColor];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(view.frame) - 1, Screen_Width, 1)];
    lineView.backgroundColor = [UIColor borderColor];
    [view addSubview:lineView];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.jobType == 0 && section == 1) {
        return 0.01;
    }
    return 5;
}

- (void)actionPopView:(NSDictionary *)item pickerTag:(NSInteger)pickerTag
{
    NSInteger section = pickerTag/10;
    NSInteger row = pickerTag%10;
    NSLog(@"section:%ld----row:%ld",(long)section,(long)row);
    ISSJobInputModel *model = self.resultDataList[section][row];
    
    // 危险作业类型
    if (section == 1)
    {
        model.pickerModel.value = item[model.pickerModel.codeKey];
        model.pickerModel.showValue = item[model.pickerModel.nameKey];
        
        model.value = model.pickerModel.value;
        model.showValue = model.pickerModel.showValue;
        
        // 有限作业空间
        if ([@"有限空间作业" isEqualToString:model.showValue]) {
            // 增加气体检测人
            NSMutableArray *section1 = self.resultDataList[1];
            if (section1.count == 1) {
                [section1 addObject:_gasDetectorModel];
            }
        }
        else
        {
            // 移除气体检测人
            NSMutableArray *section1 = self.resultDataList[1];
            if (section1.count == 2) {
                [section1 removeLastObject];
            }
        }
        [self.mTableView reloadData];
    }
    //  设备名称
    else if (section == 3 && row == 1)
    {
        model.pickerModel.value = item[model.pickerModel.codeKey];
        model.pickerModel.showValue = item[model.pickerModel.nameKey];
        
        model.value = model.pickerModel.showValue;
        model.showValue = model.pickerModel.showValue;
        //
        ISSJobInputModel *codeModel = self.resultDataList[section][2];
        codeModel.value = model.pickerModel.value;
        codeModel.showValue = codeModel.value;
        
        _equipmentId = item[@"id"];
        
        [self.mTableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    }
    else
    {
        model.pickerModel.value = item[model.pickerModel.codeKey];
        model.pickerModel.showValue = item[model.pickerModel.nameKey];
        
        model.value = model.pickerModel.value;
        model.showValue = model.pickerModel.showValue;
        [self.mTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]]
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
@end
