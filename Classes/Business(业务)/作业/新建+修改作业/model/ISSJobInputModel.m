//
//  ISSJobInputModel.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobInputModel.h"

@implementation ISSJobInputModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.placeHolder = @"";
        self.value = @"";
        self.showValue = @"";
    }
    return self;
}

@end
