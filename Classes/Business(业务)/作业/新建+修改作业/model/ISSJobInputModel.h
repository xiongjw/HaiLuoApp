//
//  ISSJobInputModel.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ISSPickerModel.h"

@interface ISSJobInputModel : NSObject

@property (nonatomic,  copy) NSString *code;//传入服务器的key
@property (nonatomic,  copy) NSString *title;
@property (nonatomic,  copy) NSString *value;//如果有数据选择器，可能需要这个参数
@property (nonatomic,  copy) NSString *showValue;
@property (nonatomic,strong) NSArray *cellList;

// 0、不可编辑模式；1、输入；2、data选择器；3、时间选择器;4、新页面
@property (nonatomic,assign) NSInteger cellType;

@property (nonatomic,  copy) NSString *placeHolder;

@property (nonatomic,strong) ISSPickerModel *pickerModel;

@end
