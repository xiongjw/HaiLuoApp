//
//  ISSJobCreateForm.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobCreateForm.h"

#import "ISSJobInputModel.h"

@implementation ISSJobCreateForm

+ (NSMutableArray *)createAddJobForm:(NSInteger)jobType
{
    NSMutableArray *list = [NSMutableArray new];
    
    //项目名称、
    NSMutableArray *section0 = [NSMutableArray new];
    
    ISSJobInputModel *model = [[ISSJobInputModel alloc] init];
    model.code = @"name";
    model.title = LoadLanguagesKey(@"job_base_projectName");
    model.cellType = 1;
    [section0 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"startTime";
    model.title = LoadLanguagesKey(@"job_base_startTime");
    model.cellType = 3;
    ISSPickerModel *dateModel = [[ISSPickerModel alloc] init];
    dateModel.type = 3;
    model.pickerModel = dateModel;
    [section0 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"endTime";
    model.title = LoadLanguagesKey(@"job_base_endTime");
    model.cellType = 3;
    dateModel = [[ISSPickerModel alloc] init];
    dateModel.type = 3;
    model.pickerModel = dateModel;
    [section0 addObject:model];
    
    [list addObject:section0];
    
    // 是否危险作业
    NSMutableArray *section1 = [NSMutableArray new];
    /*
    model = [[ISSJobInputModel alloc] init];
    model.code = @"isDanger";
    model.title = LoadLanguagesKey(@"job_base_isDanger");
    model.cellType = 5;
    [section1 addObject:model];
    */
    if (jobType == 1)
    {
        model = [[ISSJobInputModel alloc] init];
        model.code = @"type";
        model.title = LoadLanguagesKey(@"job_base_dangerJobType");
        model.cellType = 2;
        ISSPickerModel *dataModel = [[ISSPickerModel alloc] init];
        dataModel.type = 1;
        dataModel.dataSoure = @[];
        model.pickerModel = dataModel;
        [section1 addObject:model];
        
        model = [[ISSJobInputModel alloc] init];
        model.code = @"gas";
        model.title = LoadLanguagesKey(@"job_base_GasDetector");
        model.cellType = 4;
        [section1 addObject:model];
    }
    
    [list addObject:section1];
    //
    NSMutableArray *section2 = [NSMutableArray new];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"leading";
    model.title = LoadLanguagesKey(@"job_base_charge");
    model.cellType = 4;
    [section2 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"guardian";
    model.title = LoadLanguagesKey(@"job_base_guardian");
    model.cellType = 4;
    [section2 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"auditor";
    model.title = LoadLanguagesKey(@"job_base_auditor");
    model.cellType = 4;
    [section2 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"approver";
    model.title = LoadLanguagesKey(@"job_base_approver");
    model.cellType = 4;
    [section2 addObject:model];
    
    [list addObject:section2];
    
    //
    NSMutableArray *section3 = [NSMutableArray new];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"equipmentType";
    model.title = LoadLanguagesKey(@"job_base_equipmentType");
    model.cellType = 4;
    [section3 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"equipmentName";
    model.title = LoadLanguagesKey(@"job_base_equipmentName");
    model.cellType = 2;
    ISSPickerModel *dataModel = [[ISSPickerModel alloc] init];
    dataModel.type = 1;
    dataModel.dataSoure = @[];
    model.pickerModel = dataModel;
    [section3 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"equipmentCode";
    model.title = LoadLanguagesKey(@"job_base_equipmentCode");
    model.cellType = 0;
    [section3 addObject:model];
    
    [list addObject:section3];
    
    NSMutableArray *section4 = [NSMutableArray new];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"area";
    model.title = LoadLanguagesKey(@"job_base_equipmentArea");
    model.cellType = 2;
    dataModel = [[ISSPickerModel alloc] init];
    dataModel.type = 1;
    dataModel.dataSoure = @[];
    model.pickerModel = dataModel;
    [section4 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"part";
    model.title = LoadLanguagesKey(@"job_base_point");
    model.cellType = 1;
    [section4 addObject:model];
    
    // 作业单位暂时固定成登录用户所属公司，不让修改
    model = [[ISSJobInputModel alloc] init];
    model.code = @"company";
    model.title = LoadLanguagesKey(@"job_base_Organization");
    model.cellType = 2;
    model.value = [ISSUserData sharedInstance].orgId;
    model.showValue = [ISSUserData sharedInstance].orgName;
    [section4 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"numberPeople";
    model.title = LoadLanguagesKey(@"job_base_num");
    model.cellType = 1;
    [section4 addObject:model];
    
    [list addObject:section4];
    //
    NSMutableArray *section5 = [NSMutableArray new];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"content";
    model.title = LoadLanguagesKey(@"job_base_content");
    model.cellType = 1;
    [section5 addObject:model];
    
    [list addObject:section5];
    
    return list;
}

+ (NSMutableArray *)createEditJobForm:(NSDictionary *)data
{
    NSMutableArray *list = [NSMutableArray new];
    
    NSMutableArray *section0 = [NSMutableArray new];
    ISSJobInputModel *model = [[ISSJobInputModel alloc] init];
    model.code = @"startTime";
    model.title = LoadLanguagesKey(@"job_base_startTime");
    model.value = [ISSDataDeal getDateStrFromResponseData:[data[@"startTime"] longLongValue]];
    model.showValue = model.value;
    model.cellType = 3;
    ISSPickerModel *dateModel = [[ISSPickerModel alloc] init];
    dateModel.type = 3;
    dateModel.value = model.value;
    dateModel.showValue = model.value;
    model.pickerModel = dateModel;
    [section0 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"endTime";
    model.title = LoadLanguagesKey(@"job_base_endTime");
    if ([data[@"endTime"] longLongValue] > 0) {
        model.value = [ISSDataDeal getDateStrFromResponseData:[data[@"endTime"] longLongValue]];
        model.showValue = model.value;
    }
    
    model.cellType = 3;
    dateModel = [[ISSPickerModel alloc] init];
    dateModel.type = 3;
    if ([data[@"endTime"] longLongValue] > 0) {
        dateModel.value = model.value;
        dateModel.showValue = model.value;
    }
    model.pickerModel = dateModel;
    [section0 addObject:model];
    
    [list addObject:section0];
    
    NSMutableArray *section1 = [NSMutableArray new];
    model = [[ISSJobInputModel alloc] init];
    model.code = @"guardian";
    model.title = LoadLanguagesKey(@"job_base_guardian");
    model.value = data[@"guardian"];
    model.showValue = [NSString noNullString:data[@"guardianName"]];
    model.cellType = 4;
    [section1 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"auditor";
    model.title = LoadLanguagesKey(@"job_base_auditor");
    model.value = data[@"auditor"];
    model.showValue = [NSString noNullString:data[@"auditorName"]];
    model.cellType = 4;
    [section1 addObject:model];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"approver";
    model.title = LoadLanguagesKey(@"job_base_approver");
    model.value = data[@"approver"];
    model.showValue = [NSString noNullString:data[@"approverName"]];
    model.cellType = 4;
    [section1 addObject:model];
    
    [list addObject:section1];
    
    NSMutableArray *section2 = [NSMutableArray new];
    
    model = [[ISSJobInputModel alloc] init];
    model.code = @"numberPeople";
    model.title = LoadLanguagesKey(@"job_base_num");
    model.cellType = 1;
    //model.value = [NSString noNullString:data[@"numberPeople"]];
    model.value = [NSString stringWithFormat:@"%lld",[data[@"numberPeople"] longLongValue]];
    model.showValue = model.value;
    [section2 addObject:model];
    
    [list addObject:section2];
    
    return list;
}

@end
