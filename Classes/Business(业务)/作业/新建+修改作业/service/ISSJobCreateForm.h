//
//  ISSJobCreateForm.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSJobCreateForm : NSObject

+ (NSMutableArray *)createAddJobForm:(NSInteger)jobType;

+ (NSMutableArray *)createEditJobForm:(NSDictionary *)data;

@end
