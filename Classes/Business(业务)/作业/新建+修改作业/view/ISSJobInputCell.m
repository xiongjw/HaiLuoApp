//
//  ISSJobInputCell.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobInputCell.h"

@implementation ISSJobInputCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _leftLb = [UILabel oneLineLbWithX:15 y:0 fontSize:15 color:[UIColor titleColor_26] text:@"作业开始时间时间"];
        _leftLb.font = [UIFont boldSystemFontOfSize:15];
        //[_leftLb setCenterY:25];
        _leftLb.adjustsFontSizeToFitWidth = YES;
        _leftLb.numberOfLines = 2;
        [_leftLb setHeight:50];
        [self.contentView addSubview:_leftLb];
        
        _rightLb = [UILabel oneLineLbWithX:CGRectGetMaxX(_leftLb.frame) + 15 y:0 width:Screen_Width - 30 - (CGRectGetMaxX(_leftLb.frame) + 15) fontSize:15 color:[UIColor describeColor_66] text:@""];
        _rightLb.textAlignment = NSTextAlignmentRight;
        [_rightLb setCenterY:_leftLb.centerY];
        [self.contentView addSubview:_rightLb];
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, Screen_Width, 1)];
        _lineView.backgroundColor = [UIColor borderColor];
        [self.contentView addSubview:_lineView];
        
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

@implementation ISSJobRadioCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _leftLb = [UILabel oneLineLbWithX:15 y:0 fontSize:15 color:[UIColor titleColor_26] text:@"作业开始时间时间"];
        _leftLb.font = [UIFont boldSystemFontOfSize:15];
        _leftLb.adjustsFontSizeToFitWidth = YES;
        [_leftLb setCenterY:25];
        [self.contentView addSubview:_leftLb];
        
        UILabel *rightLb = [UILabel oneLineLbWithX:0 y:0 fontSize:15 color:[UIColor describeColor_66] text:LoadLanguagesKey(@"job_base_no")];
        [rightLb setCenterY:25];
        [rightLb setLeft:Screen_Width - 35 - CGRectGetWidth(rightLb.frame)];
        [self.contentView addSubview:rightLb];
        
        _rightRadio = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"add_cell_ratioseltct"]];
        [_rightRadio setCenterY:25];
        [_rightRadio setLeft:CGRectGetMinX(rightLb.frame) - 10 - CGRectGetWidth(_rightRadio.frame)];
        [self.contentView addSubview:_rightRadio];
        
        UILabel *leftLb = [UILabel oneLineLbWithX:0 y:0 fontSize:15 color:[UIColor describeColor_66] text:LoadLanguagesKey(@"job_base_yes")];
        [leftLb setCenterY:25];
        [leftLb setLeft:CGRectGetMinX(_rightRadio.frame) - 40 - CGRectGetWidth(leftLb.frame)];
        [self.contentView addSubview:leftLb];
        
        _leftRadio = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"add_cell_ratio"]];
        [_leftRadio setCenterY:25];
        [_leftRadio setLeft:CGRectGetMinX(leftLb.frame) - 10 - CGRectGetWidth(_rightRadio.frame)];
        [self.contentView addSubview:_leftRadio];
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, Screen_Width, 1)];
        _lineView.backgroundColor = [UIColor borderColor];
        [self.contentView addSubview:_lineView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
