//
//  ISSJobInputCell.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobInputCell : UITableViewCell
{
    UIView *_lineView;
}

@property (nonatomic,strong) UILabel *leftLb;
@property (nonatomic,strong) UILabel *rightLb;

@end

@interface ISSJobRadioCell : UITableViewCell
{
    UIView *_lineView;
}

@property (nonatomic,strong) UILabel *leftLb;
@property (nonatomic,strong) UIImageView *leftRadio;
@property (nonatomic,strong) UIImageView *rightRadio;

@end
