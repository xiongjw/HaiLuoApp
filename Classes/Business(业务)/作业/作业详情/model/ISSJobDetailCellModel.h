//
//  ISSJobDetailCellModel.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/13.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobDetailCellModel : JSONModel

@property (nonatomic, copy) NSString *title;
//@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *showValue;
@property (nonatomic,strong) UIColor *textColor;

@end
