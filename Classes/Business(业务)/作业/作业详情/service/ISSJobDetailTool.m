//
//  ISSJobDetailTool.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobDetailTool.h"

#import "ISSInfoMainVC.h"

@implementation ISSJobDetailTool

+ (NSString *)getJobRole:(NSDictionary *)jobData
{
    NSDictionary *work = jobData[@"work"];
    NSString *userId = [ISSUserData sharedInstance].userId;
    if ([userId isEqualToString:work[@"leading"]])
        return @"leading";
    else if ([userId isEqualToString:work[@"guardian"]])
        return @"guardian";
    else if ([userId isEqualToString:work[@"auditor"]])
        return @"auditor";
    else if ([userId isEqualToString:work[@"approver"]])
        return @"approver";
    else if ([userId isEqualToString:[NSString noNullString:work[@"gas"]]])
        return @"gas";
    return @"";
}

+ (NSInteger)doJobProgress:(NSDictionary *)jobData jobRole:(NSString *)jobRole
{
    NSArray *list = jobData[@"list"];
    NSDictionary *work = jobData[@"work"];
    
    if (list.count == 0) {
        return 0;
    }
    NSInteger progress = 0;
    for (NSDictionary *data in list)
    {
        if ([data[@"userId"] isEqualToString:work[jobRole]]) {
            progress += 1;
        }
    }
    return progress;
}

+ (NSArray *)getJobDetailTableData:(NSDictionary *)jobData jobTypeList:(NSArray *)jobTypeList
{
    NSDictionary *work = jobData[@"work"];
    NSString *gas = [NSString noNullString:work[@"gas"]];
    
    NSArray *list0 = @[
                       @{@"title":LoadLanguagesKey(@"job_base_startTime"),
                         @"showValue":[ISSDataDeal getDateStrFromResponseData:[work[@"startTime"] longLongValue]]}
                       ];
    NSMutableArray *mutList0 = [[NSMutableArray alloc] initWithArray:list0];
    if ([work[@"endTime"] longLongValue] > 0) {
        [mutList0 addObject:@{@"title":LoadLanguagesKey(@"job_base_endTime"),
                              @"showValue":[ISSDataDeal getDateStrFromResponseData:[work[@"endTime"] longLongValue]]}];
    }
    NSArray *list1 = @[
                       @{@"title":LoadLanguagesKey(@"job_base_charge"),
                         @"showValue":[NSString noNullString:work[@"leadingName"]]},
                       @{@"title":LoadLanguagesKey(@"job_base_guardian"),
                         @"showValue":[NSString noNullString:work[@"guardianName"]]},
                       @{@"title":LoadLanguagesKey(@"job_base_auditor"),
                         @"showValue":[NSString noNullString:work[@"auditorName"]]},
                       @{@"title":LoadLanguagesKey(@"job_base_approver"),
                         @"showValue":[NSString noNullString:work[@"approverName"]]}
                       ];
    NSMutableArray *mutList1 = [[NSMutableArray alloc] initWithArray:list1];
    if (gas.length > 0)
    {
        // 有限空闲作业 展现气体负责人
        [mutList1 insertObject:@{@"title":LoadLanguagesKey(@"job_base_GasDetector"),
                                 @"showValue":[NSString noNullString:work[@"gasName"]]}
                       atIndex:0];
    }

    NSArray *list2 = @[
                       @{@"title":LoadLanguagesKey(@"job_base_equipmentType"),
                         @"showValue":[NSString noNullString:work[@"equipmentTypeName"]]},
                       @{@"title":LoadLanguagesKey(@"job_base_equipmentName"),
                         @"showValue":work[@"equipmentName"]},
                       @{@"title":LoadLanguagesKey(@"job_base_equipmentCode"),
                         @"showValue":work[@"equipmentCode"]}
                       ];
    NSArray *list3 = @[
                       @{@"title":LoadLanguagesKey(@"job_base_equipmentArea"),
                         @"showValue":[self getJobArea:work jobTypeList:jobTypeList]},
                       @{@"title":LoadLanguagesKey(@"job_base_point"),
                         @"showValue":[NSString noNullString:work[@"part"]]},
                       @{@"title":LoadLanguagesKey(@"job_base_Organization"),
                         @"showValue":[NSString noNullString:work[@"companyName"]]},
                       @{@"title":LoadLanguagesKey(@"job_base_num"),
                         @"showValue":FormatString(@"%lld",[work[@"numberPeople"] longLongValue])}
                       ];
    NSArray *list4 = @[
                       @{@"title":LoadLanguagesKey(@"job_base_content"),
                         @"showValue":[NSString noNullString:work[@"content"]]},
                       ];
    
    NSArray *list5 = @[];
    if ([work[@"isDanger"] integerValue] == 1) {
        // 危险作业类别
        list5 = @[@{@"title":LoadLanguagesKey(@"job_base_dangerJobType"),
                    @"showValue":[NSString noNullString:work[@"typeName"]]}];
    }
    if (list5.count > 0) return @[mutList0,mutList1,list2,list3,list4,list5];
    return @[mutList0,mutList1,list2,list3,list4];
    
}

+ (NSString *)getJobArea:(NSDictionary *)work jobTypeList:(NSArray *)jobTypeList
{
    NSString *str = @"";
    for (NSDictionary *data in jobTypeList) {
        if ([data[@"id"] longLongValue] == [work[@"area"] longLongValue]) {
            str = data[@"name"];
            break;
        }
    }
    return str;
}

+ (NSString *)getSubmitAddress:(id)delegate
{
    UIViewController *aSelf = delegate;
    NSString *str = @"未知地点";
    UINavigationController *nav = aSelf.tabBarController.viewControllers[0];
    for (UIViewController *vc in nav.viewControllers)
    {
        if ([vc isKindOfClass:[ISSInfoMainVC class]]) {
            ISSInfoMainVC *infoVC = (ISSInfoMainVC *)vc;
            str = infoVC.addressInfo;
            break;
        }
    }
#if TARGET_IPHONE_SIMULATOR
    str = @"湖北省武汉市江夏区高新大道999号";
#endif
    return str;
}

@end
