//
//  ISSJobDetailTool.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/5/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSJobDetailTool : NSObject

// 角色
+ (NSString *)getJobRole:(NSDictionary *)jobData;

// 第一、二轮扫码拍照是否提交
+ (NSInteger)doJobProgress:(NSDictionary *)jobData jobRole:(NSString *)jobRole;

+ (NSArray *)getJobDetailTableData:(NSDictionary *)jobData jobTypeList:(NSArray *)jobTypeList;

// 扫码拍照地址
+ (NSString *)getSubmitAddress:(id)delegate;

@end
