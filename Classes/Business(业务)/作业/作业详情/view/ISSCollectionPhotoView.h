//
//  ISSCollectionPhotoView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSCollectionPhotoView : UICollectionView

- (instancetype)initWithFrame:(CGRect)frame imageList:(NSArray *)imageList;

@end
