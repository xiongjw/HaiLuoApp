//
//  ISSBasePhotoView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSBasePhotoView : UIView

- (instancetype)initWithFrame:(CGRect)frame imageList:(NSArray *)imageList;

@end
