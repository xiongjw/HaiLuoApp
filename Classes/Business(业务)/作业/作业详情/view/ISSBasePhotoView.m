//
//  ISSBasePhotoView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBasePhotoView.h"

#import "UIImageView+Create.h"

#import "LBPhotoBrowserManager.h"

#import "TZImageManager.h"

#import "SDPhotoBrowser.h"

@interface ISSBasePhotoView() <SDPhotoBrowserDelegate>
{
    LBPhotoBrowserManager *_lbPhotoBrowserManager;
    NSArray *_imageList;
}
@end

@implementation ISSBasePhotoView

- (void)handleSingleTap:(UITapGestureRecognizer *)tap
{
    UIImageView *imageView = (UIImageView *)tap.view;
    NSInteger index = imageView.tag - 1000;
    
    bool useLBPhoto = NO;
    if (useLBPhoto)
    {
        // 这种方式适用于cell，view上有点小问题，
        NSMutableArray *items = @[].mutableCopy;
        for (int i = 0; i < _imageList.count; i++ )
        {
            LBPhotoWebItem *item = [[LBPhotoWebItem alloc]initWithURLString:_imageList[i] frame:imageView.frame];
            [items addObject:item];
        }
        [_lbPhotoBrowserManager showImageWithWebItems:items selectedIndex:index fromImageViewSuperView:self];
        //_lbPhotoBrowserManager.lowGifMemory = YES;
        [[_lbPhotoBrowserManager addLongPressShowTitles:@[@"保存",@"取消"]] addTitleClickCallbackBlock:^(UIImage *image, NSIndexPath *indexPath, NSString *title) {
            LBPhotoBrowserLog(@"%@",title);
            if ([@"保存" isEqualToString:title] && image) {
                [[TZImageManager manager] savePhotoWithImage:image completion:^(NSError *error) {
                    if (!error) {
                        [[UIApplication sharedApplication].keyWindow makeToast:@"保存成功"];
                    }
                }];
            }
        }];
    }
    else
    {
        SDPhotoBrowser *photoBrowser = [SDPhotoBrowser new];
        photoBrowser.delegate = self;
        photoBrowser.currentImageIndex = index;
        photoBrowser.imageCount = _imageList.count;
        photoBrowser.sourceImagesContainerView = self;
        
        [photoBrowser show];
    }
}

- (instancetype)initWithFrame:(CGRect)frame imageList:(NSArray *)imageList
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _lbPhotoBrowserManager = [LBPhotoBrowserManager defaultManager];
        _imageList = imageList;
        
        self.backgroundColor = [UIColor clearColor];
        
        CGFloat x = 25;
        CGFloat width = 60;
        
        UIImageView *imageView = nil;
        for (int i = 0; i < imageList.count; i++)
        {
            imageView = [UIImageView imageView:CGRectMake(x, 0, width, width)
                                         scale:YES];
            imageView.tag = 1000 + i;
            [self addSubview:imageView];
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:imageList[i]] placeholderImage:nil];
            
            imageView.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
            [imageView addGestureRecognizer:singleTap];
            
            x = CGRectGetMaxX(imageView.frame) + 30;
        }
        
    }
    return self;
}

#pragma mark  SDPhotoBrowserDelegate

// 返回临时占位图片（即原来的小图）
-(UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    UIImageView *imageView = (UIImageView *)self.subviews[index];
    return imageView.image;
}
// 返回高质量图片的url
- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:_imageList[index]];
}

@end
