//
//  ISSJobDetailChangeView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobDetailChangeView.h"

#import "ISSJobDetailTool.h"

@implementation ISSJobDetailChangeView

- (instancetype)initWithDelegate:(id)Delegate
                      detailData:(NSDictionary *)detailData
                      clickBlock:(JobDetailChangeClickBlock)clickBlock
{
    self = [super initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    if (self) {
        
        self.detailData = detailData;
        _clickBlock = clickBlock;
        
        NSDictionary *work = self.detailData[@"work"];
        NSInteger status = [work[@"status"] integerValue];
        NSString *jobRole = [ISSJobDetailTool getJobRole:detailData];
        NSString *gas = [NSString noNullString:work[@"gas"]];
        
        UIView *changeView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, Screen_Width - 30, 0)];
        changeView.backgroundColor = [UIColor whiteColor];
        changeView.layer.borderWidth = 1;
        changeView.layer.borderColor = [UIColor borderColor].CGColor;
        changeView.layer.cornerRadius = 8;
        changeView.layer.masksToBounds = YES;
        [self addSubview:changeView];
        
        CGFloat contentWidth = CGRectGetWidth(changeView.frame);
        
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 39, 39)];
        [changeView addSubview:iconView];
        if ([work[@"isDanger"] integerValue] == 1) iconView.image = [UIImage imageNamed:@"job_list_danger"];
        else iconView.image = [UIImage imageNamed:@"job_list_normal"];
        
        NSString *dateText = [ISSDataDeal getDateStrFromResponseData:[work[@"createTime"] longLongValue]];
        CGSize dateSize = [dateText sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]}];
        UILabel *dateLb = [UILabel oneLineLbWithX:contentWidth - 10 - (dateSize.width + 5) y:0 width:dateSize.width + 5 fontSize:12 color:[UIColor describeColor_61] text:dateText];
        [dateLb setCenterY:iconView.centerY];
        dateLb.textAlignment = NSTextAlignmentRight;
        [changeView addSubview:dateLb];
        
        UILabel *titleLb = [UILabel mutLineLbWithX:45 y:10 width:CGRectGetMinX(dateLb.frame) - 45 fontSize:14 color:[UIColor titleColor_26] text:work[@"name"]];
        [changeView addSubview:titleLb];
        
        [titleLb setWidth:CGRectGetMinX(dateLb.frame) - CGRectGetMinX(titleLb.frame)];
        
        /* ----------------------------------------进度条----------------------------------------*/
        self.progressView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLb.frame) + 20, contentWidth, 0)];
        [changeView addSubview:self.progressView];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(45, 0, contentWidth - 90, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdddddd);
        [self.progressView addSubview:lineView];
        
        NSArray *roleList = @[
                              gas.length > 0 ? LoadLanguagesKey(@"job_base_GasDetector") : @"",
                              LoadLanguagesKey(@"job_base_charge"),
                              LoadLanguagesKey(@"job_base_guardian"),
                              LoadLanguagesKey(@"job_base_auditor"),
                              LoadLanguagesKey(@"job_base_approver")
                              ];
        NSArray *personList = @[
                                gas.length > 0 ? [NSString noNullString:work[@"gasName"]] : @"",
                                [NSString noNullString:work[@"leadingName"]],
                                [NSString noNullString:work[@"guardianName"]],
                                [NSString noNullString:work[@"auditorName"]],
                                [NSString noNullString:work[@"approverName"]]
                                ];
        
        int start = gas.length > 0 ? 0 : 1;
        
        UIView *pointView = nil;
        UILabel *roleLb = nil;
        UILabel *personLb = nil;
        for (int i = start; i < personList.count; i++)
        {
            pointView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
            pointView.backgroundColor = [UIColor describeColor_9a];
            pointView.layer.cornerRadius = CGRectGetHeight(pointView.frame)/2;
            pointView.layer.masksToBounds = YES;
            pointView.centerY = lineView.centerY;
            pointView.centerX = CGRectGetMinX(lineView.frame) + CGRectGetWidth(lineView.frame)*(i-start)/(personList.count - 1 - start);
            pointView.tag = 20 + i;
            [self.progressView addSubview:pointView];
            
            roleLb = [UILabel oneLineLbWithX:pointView.centerX - 35 y:CGRectGetMaxY(pointView.frame) + 10 width:70 fontSize:12 color:[UIColor describeColor_9a] text:roleList[i]];
            roleLb.textAlignment = NSTextAlignmentCenter;
            [self.progressView addSubview:roleLb];
            
            personLb = [UILabel oneLineLbWithX:pointView.centerX - 30 y:CGRectGetMaxY(roleLb.frame) + 10 width:60 fontSize:12 color:[UIColor titleColor_26] text:personList[i]];
            personLb.textAlignment = NSTextAlignmentCenter;
            [self.progressView addSubview:personLb];
            
            if (status >= 3)
            {
                pointView.backgroundColor = UIColorFromRGB(0x00a733);
                roleLb.textColor = UIColorFromRGB(0x00a733);
                personLb.textColor = UIColorFromRGB(0x00a733);
                if ((i == 0 && [self doJobProgress:@"gas"] == 2) ||
                    (i == 1 && [self doJobProgress:@"leading"] == 2) ||
                    (i == 2 && [self doJobProgress:@"guardian"] == 2) ||
                    (i == 3 && [self doJobProgress:@"auditor"] == 2) ||
                    (i == 4 && [self doJobProgress:@"approver"] == 2))
                {
                    pointView.backgroundColor = UIColorFromRGB(0x42b5f8);
                    roleLb.textColor = UIColorFromRGB(0x42b5f8);
                    personLb.textColor = UIColorFromRGB(0x42b5f8);
                }
            }
            else
            {
                if ((i == 0 && [self doJobProgress:@"gas"] > 0) ||
                    (i == 1 && [self doJobProgress:@"leading"] > 0) ||
                    (i == 2 && [self doJobProgress:@"guardian"] > 0) ||
                    (i == 3 && [self doJobProgress:@"auditor"] > 0) ||
                    (i == 4 && [self doJobProgress:@"approver"] > 0))
                {
                    pointView.backgroundColor = UIColorFromRGB(0x00a733);
                    roleLb.textColor = UIColorFromRGB(0x00a733);
                    personLb.textColor = UIColorFromRGB(0x00a733);
                }
            }
        }
        [self.progressView setHeight:CGRectGetMaxY(personLb.frame) + 15];
        
        /* ----------------------------------------扫描拍照按钮----------------------------------------*/
        
        NSArray *imageList = self.detailData[@"list"];
        UIView *endPoint = nil;
        CGFloat posY = CGRectGetMaxY(self.progressView.frame) + 15;
        pointView = nil;
        UIView *vLine = nil;
        titleLb = nil;
        X_Button *lookPhotoBtn = nil;
        UIImageView *addressIcon = nil;
        UILabel *addressLb = nil;
        if (imageList.count > 0)
        {
            for (int i = 0; i < imageList.count; i++)
            {
                pointView = [[UIView alloc] initWithFrame:CGRectMake(20, posY, 8, 8)];
                pointView.backgroundColor = UIColorFromRGB(0x00a733);
                //pointView.backgroundColor = status >= 3 ? UIColorFromRGB(0x42b5f8) : UIColorFromRGB(0x18da38);
                pointView.layer.cornerRadius = CGRectGetHeight(pointView.frame)/2;
                pointView.layer.masksToBounds = YES;
                [changeView addSubview:pointView];
                if (i == 0) {
                    vLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(pointView.frame), 1, 0)];
                    vLine.backgroundColor = UIColorFromRGB(0xdddddd);
                    [vLine setCenterX:pointView.centerX];
                    [changeView addSubview:vLine];
                }
                
                lookPhotoBtn = [X_Button buttonWithType:UIButtonTypeCustom];
                lookPhotoBtn.frame = CGRectMake(contentWidth - 12 - 75, 0, 75, 16);
                [lookPhotoBtn setCenterY:pointView.centerY];
                lookPhotoBtn.layer.borderWidth = 0.5;
                lookPhotoBtn.layer.borderColor = UIColorFromRGB(0x00a733).CGColor;
                [lookPhotoBtn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xc5edd4)] forState:UIControlStateNormal];
                lookPhotoBtn.titleLabel.font = [UIFont systemFontOfSize:12];
                [lookPhotoBtn setTitleColor:UIColorFromRGB(0x00a733) forState:UIControlStateNormal];
                [lookPhotoBtn setTitle:LoadLanguagesKey(@"job_detail_lookPhoto") forState:UIControlStateNormal];
                lookPhotoBtn.tag = 666 + i;
                [lookPhotoBtn addTarget:self action:@selector(lookPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
                [changeView addSubview:lookPhotoBtn];
                
                //NSString *text = FormatString(@"提交时间 %@",[ISSDataDeal getDateStrFromResponseData:[imageList[i][@"createTime"] longLongValue] format:@"MM-dd HH:mm"]);
                NSString *text = FormatString(@"%@  %@",[self getShowRole:imageList[i] work:work],[ISSDataDeal getDateStrFromResponseData:[imageList[i][@"createTime"] longLongValue] format:@"MM-dd HH:mm"]);
                titleLb = [UILabel oneLineLbWithX:CGRectGetMaxX(pointView.frame) + 15 y:0 width:CGRectGetMinX(lookPhotoBtn.frame) - 10 - (CGRectGetMaxX(pointView.frame) + 15) fontSize:14 color:[UIColor titleColor_26] text:text];
                titleLb.font = [UIFont boldSystemFontOfSize:14];
                [titleLb setCenterY:pointView.centerY];
                [changeView addSubview:titleLb];
                
                addressIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"info_location"]];
                [addressIcon setLeft:CGRectGetMinX(titleLb.frame)];
                [addressIcon setTop:CGRectGetMaxY(titleLb.frame) + 10];
                [changeView addSubview:addressIcon];
                
                addressLb = [UILabel oneLineLbWithX:CGRectGetMaxX(addressIcon.frame) + 5 y:0 width:contentWidth - 10 - (CGRectGetMaxX(addressIcon.frame) + 5) fontSize:12 color:UIColorFromRGB(0xb5b5b5) text:[NSString noNullString:imageList[i][@"location"]]];
                [addressLb setCenterY:addressIcon.centerY];
                [changeView addSubview:addressLb];
                // 暂时显示人吧
                /*
                 addressLb = [UILabel oneLineLbWithX:CGRectGetMinX(titleLb.frame) y:CGRectGetMaxX(titleLb.frame) + 10 width:contentWidth - 10 - CGRectGetMaxX(titleLb.frame) fontSize:12 color:UIColorFromRGB(0xb5b5b5) text:[NSString noNullString:work[FormatString(@"%@Name",[self getRole:work])]]];
                 */
                
                posY = CGRectGetMaxY(addressIcon.frame) + 15;
            }
            
            if (status == 5)
            {
                [vLine setHeight:posY - CGRectGetMinY(vLine.frame)];
            }
            else
            {
                
                [vLine setHeight:posY + 15 + 25 - CGRectGetMinY(vLine.frame)];
                
                endPoint = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(pointView.frame), CGRectGetMaxY(vLine.frame) - 4, 8, 8)];
                endPoint.backgroundColor = status >= 3 ? UIColorFromRGB(0x42b5f8) : UIColorFromRGB(0x18da38);
                endPoint.layer.cornerRadius = CGRectGetHeight(pointView.frame)/2;
                endPoint.layer.masksToBounds = YES;
                [changeView addSubview:endPoint];
                
            }
        }
        
        if (status >= 3)
        {
            // 有角色，并且作业不处于已关闭的状态
            if (jobRole.length > 0 && status != 5)
            {
                CGFloat width = (contentWidth - CGRectGetMaxX(endPoint.frame) - 15*3)/2;
                [self addSubview:[self createBtn:CGRectMake(15+CGRectGetMaxX(endPoint.frame) + 15, posY + 15 + 45/2, width, 45) bgColor:UIColorFromRGB(0x42b5f8) iconName:@"job_detail_scan" btnTitle:LoadLanguagesKey(@"job_detail_scan") tag:10]];
                [self addSubview:[self createBtn:CGRectMake(15+CGRectGetMaxX(endPoint.frame)+15 + width + 15, posY + 15 + 45/2, width, 45) bgColor:UIColorFromRGB(0x42b5f8) iconName:@"job_detail_camera" btnTitle:LoadLanguagesKey(@"job_detail_camera") tag:11]];
                
                [changeView setHeight:posY + 15 + 45 + 20];
                [self setHeight:CGRectGetMaxY(changeView.frame)];
            }
            else
            {
                [changeView setHeight:posY];
                [self setHeight:CGRectGetMaxY(changeView.frame)];
            }
        }
        else
        {
            // 有角色，并且作业不处于已撤销的状态
            if (jobRole.length > 0 && status != 1)
            {
                /*
                [changeView setHeight:CGRectGetMaxY(endPoint.frame) + 15 + 45/2];
                [self setHeight:CGRectGetMaxY(changeView.frame) + 45/2 + 10];
                
                CGFloat width = (contentWidth - 60)/2;
                [self addSubview:[self createBtn:CGRectMake(15+15, CGRectGetMaxY(changeView.frame) - 45/2, width, 45) bgColor:UIColorFromRGB(0x18da38) iconName:@"job_detail_scan" btnTitle:LoadLanguagesKey(@"job_detail_scan") tag:10]];
                [self addSubview:[self createBtn:CGRectMake(15+15+width+30, CGRectGetMaxY(changeView.frame) - 45/2, width, 45) bgColor:UIColorFromRGB(0x18da38) iconName:@"job_detail_camera" btnTitle:LoadLanguagesKey(@"job_detail_camera") tag:11]];
                 */
                CGFloat width = (contentWidth - CGRectGetMaxX(endPoint.frame) - 15*3)/2;
                [self addSubview:[self createBtn:CGRectMake(15+CGRectGetMaxX(endPoint.frame) + 15, posY + 15 + 45/2, width, 45) bgColor:UIColorFromRGB(0x18da38) iconName:@"job_detail_scan" btnTitle:LoadLanguagesKey(@"job_detail_scan") tag:10]];
                [self addSubview:[self createBtn:CGRectMake(15+CGRectGetMaxX(endPoint.frame)+15 + width + 15, posY + 15 + 45/2, width, 45) bgColor:UIColorFromRGB(0x18da38) iconName:@"job_detail_camera" btnTitle:LoadLanguagesKey(@"job_detail_camera") tag:11]];
                
                [changeView setHeight:posY + 15 + 45 + 20];
                [self setHeight:CGRectGetMaxY(changeView.frame)];
            }
            else
            {
                [changeView setHeight:posY];
                [self setHeight:CGRectGetMaxY(changeView.frame)];
            }
        }
        
    }
    return self;
}

- (X_Button *)createBtn:(CGRect)frame
                bgColor:(UIColor *)bgColor
               iconName:(NSString *)iconName
               btnTitle:(NSString *)btnTitle
                    tag:(NSInteger)tag
{
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    btn.backgroundColor = bgColor;
    //[btn setBackgroundImage:[UIImage imageWithColor:bgColor] forState:UIControlStateNormal];
    btn.layer.cornerRadius = CGRectGetHeight(frame)/2;
    //btn.layer.masksToBounds = YES;
    
    btn.layer.shadowOffset =  CGSizeMake(0, 8);
    btn.layer.shadowOpacity = 0.3;
    btn.layer.shadowColor =  bgColor.CGColor;
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
    iconView.centerY = frame.size.height/2;
    iconView.centerX = frame.size.width/3 - 10;
    [btn addSubview:iconView];
    
    UILabel *lb = [UILabel oneLineLbWithX:0 y:0 fontSize:15 color:[UIColor whiteColor] text:btnTitle];
    lb.centerY = frame.size.height/2;
    lb.centerX = frame.size.width*2/3 - 10;
    if (btnTitle.length >= 6) {
        lb.centerX = frame.size.width*2/3;
    }
    [btn addSubview:lb];
    
    NSString *jobRole = [ISSJobDetailTool getJobRole:self.detailData];
    if (jobRole.length > 0)
    {
        // 添加一个完成的勾
        UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"job_detail_scansuccess_white"]];
        [icon setCenterY:frame.size.height/2];
        [icon setLeft:CGRectGetMaxX(lb.frame) + 10];
        icon.tag = 7777;
        [btn addSubview:icon];
        
        // 如果提交了，打勾
        NSInteger status = [self.detailData[@"work"][@"status"] integerValue];
        if ((status >= 3 && [self doJobProgress:jobRole] == 2) ||
            (status < 3 && [self doJobProgress:jobRole] > 0)) {
            // 如果做了，就隐藏，（之前的做法，是点击的时候判断），modify by xjw 2018.6.8
            btn.hidden = YES;
        }
        else {
            icon.hidden = YES;
        }
    }
    btn.tag = tag;
    [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (void)clickAction:(X_Button *)btn
{
    if (_clickBlock)
    {
        if (btn.tag == 10) _clickBlock(@"btn_scan");
        else  _clickBlock(@"btn_camera");
    }
}

// 执行打勾操作,此操作是临时，只有提交至服务器才生效
- (void)afterScanOrCamera:(NSInteger)tag
{
    X_Button *btn = [self viewWithTag:tag];
    UIImageView *icon = [btn viewWithTag:7777];
    icon.hidden = NO;
}

- (void)lookPhotoAction:(X_Button *)btn
{
    if (_lookPhotoBlock) {
        _lookPhotoBlock(btn.tag - 666);
    }
}

- (NSInteger)doJobProgress:(NSString *)jobRole
{
    return [ISSJobDetailTool doJobProgress:self.detailData jobRole:jobRole];
}

// 暂时没用到
- (NSString *)getShowRole:(NSDictionary *)item work:(NSDictionary *)work
{
    NSString *userId = item[@"userId"];
    NSString *roleNameKey = @"";
    if ([userId isEqualToString:work[@"leading"]])
        roleNameKey = @"leadingName";
    else if ([userId isEqualToString:work[@"guardian"]])
        roleNameKey = @"guardianName";
    else if ([userId isEqualToString:work[@"auditor"]])
        roleNameKey = @"auditorName";
    else if ([userId isEqualToString:work[@"approver"]])
        roleNameKey =  @"approverName";
    else if ([userId isEqualToString:work[@"gas"]])
        roleNameKey =  @"gasName";
    return [NSString noNullString:work[roleNameKey]];
    /*
    NSDictionary *data = @{
                           @"leadingName":@"负责人",
                           @"guardianName":@"监护人",
                           @"auditorName":@"审核人",
                           @"approverName":@"批准人",
                           @"gasName":@"气体检测人"
                           
                           };
    return FormatString(@"%@(%@)",[NSString noNullString:work[roleNameKey]],data[roleNameKey]);
     */
}

@end
