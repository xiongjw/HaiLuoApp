//
//  ISSJobDetailChangeView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^JobDetailChangeClickBlock)(NSString *btnTitle);

typedef void (^JobDetailLookPhotoBlock)(NSInteger tag);

@interface ISSJobDetailChangeView : UIView

@property (nonatomic,strong) UIView *progressView;

@property (nonatomic,  copy) JobDetailChangeClickBlock clickBlock;

@property (nonatomic,  copy) JobDetailLookPhotoBlock lookPhotoBlock;

@property (nonatomic,strong) NSDictionary *detailData;

- (instancetype)initWithDelegate:(id)Delegate
                     detailData:(NSDictionary *)detailData
                      clickBlock:(JobDetailChangeClickBlock)clickBlock;

// 执行打勾操作,此操作是临时，只有提交至服务器才生效
- (void)afterScanOrCamera:(NSInteger)tag;

@end
