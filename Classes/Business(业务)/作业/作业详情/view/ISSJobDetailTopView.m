//
//  ISSJobDetailTopView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobDetailTopView.h"

#import "ISSJobDetailTool.h"

@implementation ISSJobDetailTopView

- (void)clickAction:(X_Button *)btn
{
    NSArray *btnArray = @[@"btn_revoke",@"btn_commit",@"btn_edit"];
    if (_clickBlock) {
        _clickBlock(btnArray[btn.tag - 1]);
    }
}

- (X_Button *)createSubmitBtn:(CGRect)frame btnTitle:(NSString *)btnTitle tag:(NSInteger)tag
{
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    [btn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x223388)] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 8;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    btn.tag = tag;
    [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // 如果做了提交，就隐藏，（之前的做法，是点击的时候判断），modify by xjw 2018.6.8
    if (tag == 2)
    {
        NSString *jobRole = [ISSJobDetailTool getJobRole:self.detailData];
        NSInteger status = [self.detailData[@"work"][@"status"] integerValue];
        if ((status >= 3 && [self doJobProgress:jobRole] == 2) ||
            (status < 3 && [self doJobProgress:jobRole] > 0)) {
            btn.hidden = YES;
        }
    }
    
    return btn;
}

- (NSInteger)doJobProgress:(NSString *)jobRole
{
    return [ISSJobDetailTool doJobProgress:self.detailData jobRole:jobRole];
}

- (instancetype)initWithFrame:(CGRect)frame
                   detailData:(NSDictionary *)detailData
                   clickBlock:(JobDetailClickBlock)clickBlock
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.detailData = detailData;
        _clickBlock = clickBlock;
        self.backgroundColor = [UIColor themeColor];
        
        CGFloat btnWidth = (Screen_Width - 15*4)/3;
        NSString *jobRole = [ISSJobDetailTool getJobRole:detailData];
        
        NSArray *btnArray = @[LoadLanguagesKey(@"btn_revoke"),LoadLanguagesKey(@"btn_commit"),LoadLanguagesKey(@"btn_edit")];
        NSString *gas = [NSString noNullString:detailData[@"work"][@"gas"]];
        // 只有是负责人 并且没有做操作，才可以撤销可以修改
        if ([@"leading" isEqualToString:jobRole])
        {
            if (gas.length > 0) {
                if ([ISSJobDetailTool doJobProgress:detailData jobRole:@"gas"] == 0) {
                    for (int i = 0; i < 3; i++) {
                        [self addSubview:[self createSubmitBtn:CGRectMake(15*(i+1)+btnWidth*i, 10, btnWidth, 30) btnTitle:btnArray[i] tag:i+1]];
                    }
                }
                else {
                    [self addSubview:[self createSubmitBtn:CGRectMake((Screen_Width - btnWidth)/2, 10, btnWidth, 30) btnTitle:btnArray[1] tag:2]];
                }
            }
            else {
                if ([ISSJobDetailTool doJobProgress:detailData jobRole:@"leading"] == 0) {
                    for (int i = 0; i < 3; i++) {
                        [self addSubview:[self createSubmitBtn:CGRectMake(15*(i+1)+btnWidth*i, 10, btnWidth, 30) btnTitle:btnArray[i] tag:i+1]];
                    }
                }
                else {
                    [self addSubview:[self createSubmitBtn:CGRectMake((Screen_Width - btnWidth)/2, 10, btnWidth, 30) btnTitle:btnArray[1] tag:2]];
                }
            }
        }
        else {
            [self addSubview:[self createSubmitBtn:CGRectMake((Screen_Width - btnWidth)/2, 10, btnWidth, 30) btnTitle:btnArray[1] tag:2]];
        }
    }
    return self;
}

@end
