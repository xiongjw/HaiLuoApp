//
//  ISSCollectionPhotoView.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/17.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCollectionPhotoView.h"

#import "UIImageView+Create.h"

#import "LBPhotoBrowserManager.h"

#import "TZImageManager.h"
#import "TZImagePickerController.h"

@interface ISSWebImageCollectionViewCell :UICollectionViewCell
@property (nonatomic , strong) UIImageView *mImageView;
@end

@implementation ISSWebImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _mImageView = [UIImageView imageView:CGRectMake(0, 0, 60, 60) scale:YES];
        [self.contentView addSubview:_mImageView];
    }
    return self;
}

@end


@interface ISSCollectionPhotoView ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>
{
    LBPhotoBrowserManager *_lbPhotoBrowserManager;
}

@property (nonatomic , strong)  NSArray *imageList;

@end

static NSString * const reuseIdentifier = @"ISSWebImageCell";

@implementation ISSCollectionPhotoView

- (instancetype)initWithFrame:(CGRect)frame imageList:(NSArray *)imageList;
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.itemSize = CGSizeMake(60, 60);
    flowLayout.minimumLineSpacing = 30;
    
    self = [super initWithFrame:frame collectionViewLayout:flowLayout];
    if (self) {
        
        _lbPhotoBrowserManager = [LBPhotoBrowserManager defaultManager];
        self.imageList = imageList;
        
        self.backgroundColor = [UIColor clearColor];
        self.dataSource = self;
        self.delegate = self;
        if (iOS11Later) {
            self.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self registerClass:[ISSWebImageCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    }
    return self;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.imageList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ISSWebImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell.mImageView sd_setImageWithURL:[NSURL URLWithString:self.imageList[indexPath.row]] placeholderImage:[UIImage imageNamed:@"placeHolder_image"]];
    return cell;
}
#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *items = @[].mutableCopy;
    for (int i = 0; i < self.imageList.count; i++ )
    {
        ISSWebImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        LBPhotoWebItem *item = [[LBPhotoWebItem alloc] initWithURLString:self.imageList[i] frame:cell.frame];
        [items addObject:item];
    }
    
    [_lbPhotoBrowserManager showImageWithWebItems:items selectedIndex:indexPath.row fromImageViewSuperView:self];
    //_lbPhotoBrowserManager.lowGifMemory = YES;
    [[_lbPhotoBrowserManager addLongPressShowTitles:@[LoadLanguagesKey(@"btn_save"),LoadLanguagesKey(@"btn_cancel")]] addTitleClickCallbackBlock:^(UIImage *image, NSIndexPath *indexPath, NSString *title) {
        LBPhotoBrowserLog(@"%@",title);
        if ([LoadLanguagesKey(@"btn_save") isEqualToString:title] && image) {
            [[TZImageManager manager] savePhotoWithImage:image completion:^(NSError *error) {
                if (!error) {
                    [[UIApplication sharedApplication].keyWindow makeToast:LoadLanguagesKey(@"photo_saveSuccess")];
                }
            }];
        }
    }];
    [_lbPhotoBrowserManager addCollectionViewLinkageStyle:UICollectionViewScrollPositionCenteredHorizontally cellReuseIdentifier:reuseIdentifier];
    //_lbPhotoBrowserManager.needPreloading = NO;
}

//调节item边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 20, 0, 30);
}

@end
