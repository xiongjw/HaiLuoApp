//
//  ISSJobDetailTopView.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/12.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^JobDetailClickBlock)(NSString *btnTitle);

@interface ISSJobDetailTopView : UIView

@property (nonatomic, copy) JobDetailClickBlock clickBlock;

@property (nonatomic,strong) NSDictionary *detailData;

- (instancetype)initWithFrame:(CGRect)frame
                   detailData:(NSDictionary *)detailData
                   clickBlock:(JobDetailClickBlock)clickBlock;

@end
