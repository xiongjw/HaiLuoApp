//
//  ISSJobDetailTakeCameraVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TakeCameraSuccessResultBlock)(NSString *imageStr);

@interface ISSJobDetailTakeCameraVC : ISSBaseVC

@property (nonatomic, copy) TakeCameraSuccessResultBlock resultBlock;

@end
