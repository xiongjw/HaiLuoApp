//
//  ISSJobDetailVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobDetailVC : ISSBaseTableVC

@property (nonatomic, assign) long jobId;

@property (nonatomic,strong) NSArray *jobList;

@end
