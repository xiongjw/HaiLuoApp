//
//  ISSJobDetailVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/11.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobDetailVC.h"

#import "ISSCheckMaintainDetailCell.h"

#import "ISSJobDetailTopView.h"
#import "ISSJobDetailChangeView.h"

#import "ISSJobDetailCellModel.h"

#import "ISSEditJobVC.h"
#import "ISSJobDetailTakeCameraVC.h"

#import "ISSBaseScanVC.h"
#import "ISSJobLookPhotoDetailVC.h"

#import "ISSInfoMainVC.h"

#import "ISSJobDetailTool.h"

@interface ISSJobDetailVC ()
{
    ISSJobDetailTopView *_topView;
    ISSJobDetailChangeView *_headerView;
    
    BOOL _isRefresh;
    
    // 扫码或拍照成功之后的label提示
    UILabel *_flashLb;
    
    
    // 临时变量
    BOOL _doScanTemp;
    BOOL _doCameraTemp;
    //NSString *_codeStr;
    NSString *_imageStr;//逗号拼接
}

@property (nonatomic,strong) NSDictionary *detailData;
@property (nonatomic,strong) NSDictionary *workData;
@property (nonatomic,  copy) NSString *jobRole;
@property (nonatomic,assign) NSInteger jobStatus;

@end

@implementation ISSJobDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:LoadLanguagesKey(@"job_detail_navTitle") showBackBtn:YES];
    
    // 右上角增加刷新
    self.navigationItem.rightBarButtonItem = [ISSNavUtil getBarItem:self btnType:5 clickAction:@selector(jobDetailRequest)];
    
    _isRefresh = NO;
    [self jobDetailRequest];
    
    self.mTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 20)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.resultDataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultDataList[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ISSCheckMaintainDetailCell *cell = (ISSCheckMaintainDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ISSCheckMaintainDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier type:0];
    }
    
    [cell layoutJobDetailCell:self.resultDataList[indexPath.section][indexPath.row]];
    
    CGFloat h = [ISSPubfun getLabelHeight:CGRectGetWidth(cell.rightLb.frame) font:cell.rightLb.font text:cell.rightLb.text];
    [cell dealRoundCornerAndLine:indexPath.row
                    numberOfRows:[self.resultDataList[indexPath.section] count]
                       rowHeight:MAX(50, CGRectGetMinY(cell.rightLb.frame)*2 + h)];
    
    return cell;
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - 详情接口
- (void)jobDetailRequest
{
    if (_isRefresh) {
        return;
    }
    _isRefresh = YES;
    
    WeakSelf(self)
    NSDictionary *data = @{@"jobId":@(self.jobId)};
    [X_HttpUtil apiRequest:@"job/getJob" param:data view:self.view successBlock:^(NSDictionary *resultData) {
        NSLog(@"resultData:%@",resultData);
        _isRefresh = NO;
        [weakself dealResultData:resultData[@"data"]];
    } failureBlock:^(NSDictionary *resultData) {
        _isRefresh = NO;
    }];
}

/*
 *  这里要注意，
 *  1、不是这几个人进来的界面情况
 *  2、此作业已撤销 或者已经结束的情况，有些按钮就不用出现了
 */

- (void)dealResultData:(NSDictionary *)data
{
    self.detailData = data;
    self.workData = data[@"work"];
    self.jobRole = [ISSJobDetailTool getJobRole:data];
    self.jobStatus = [self.workData[@"status"] integerValue];
    
    WeakSelf(self)
    if (_topView)
    {
        [_topView removeFromSuperview];
        _topView = nil;
    }
    
    CGFloat posY = 0;
    
    // 这四种人进来才可能有情况出现按钮
    if (self.jobRole.length > 0)
    {
        // 判断作业状态，已撤销、已关闭，不出现按钮
        if (self.jobStatus == 1 || self.jobStatus == 5) {
            
        }
        else
        {
            _topView = [[ISSJobDetailTopView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 50) detailData:data clickBlock:^(NSString *btnTitle) {
                [weakself topClickAction:btnTitle];
            }];
            [self.view addSubview:_topView];
            posY = CGRectGetMaxY(_topView.frame);
        }
    }
    
    self.mTableView.frame = CGRectMake(0, posY, Screen_Width, Screen_Height - 64 - CGRectGetMaxY(_topView.frame));
    
    if (_headerView)
    {
        [_headerView removeFromSuperview];
        _headerView = nil;
    }
    _headerView = [[ISSJobDetailChangeView alloc] initWithDelegate:self detailData:data clickBlock:^(NSString *btnTitle) {
        
        [weakself changeClickAction:btnTitle];
    }];
    _headerView.lookPhotoBlock = ^(NSInteger tag) {
        
        NSArray *list = weakself.detailData[@"list"];
        ISSJobLookPhotoDetailVC *vc = [[ISSJobLookPhotoDetailVC alloc] init];
        vc.itemData = list[tag];
        vc.projectName = weakself.detailData[@"work"][@"name"];
        [weakself.navigationController pushViewController:vc animated:YES];
        
    };
    self.mTableView.tableHeaderView = _headerView;
    
    if (self.resultDataList.count > 0)
    {
        [self.resultDataList removeAllObjects];
    }
    
    NSArray *tableList = [ISSJobDetailTool getJobDetailTableData:data jobTypeList:self.jobList];
    
    NSString *gas = [NSString noNullString:self.workData[@"gas"]];
    for (int i = 0; i < tableList.count; i++)
    {
        NSArray *section = tableList[i];
        NSMutableArray *list = [[NSMutableArray alloc] init];
        for (int j = 0; j < section.count; j++)
        {
            ISSJobDetailCellModel *model = [[ISSJobDetailCellModel alloc] init];
            model.title = section[j][@"title"];
            model.showValue = section[j][@"showValue"];
            model.textColor = [UIColor describeColor_66];
            if (i == 1)
            {
                if (self.jobStatus >= 3)
                {
                    model.textColor = UIColorFromRGB(0x00a733);
                    if (gas.length > 0)
                    {
                        // 第二轮
                        if ((j == 0 && [self doJobProgress:@"gas"] == 2) ||
                            (j == 1 && [self doJobProgress:@"leading"] == 2) ||
                            (j == 2 && [self doJobProgress:@"guardian"] == 2) ||
                            (j == 3 && [self doJobProgress:@"auditor"] == 2) ||
                            (j == 4 && [self doJobProgress:@"approver"] == 2))
                        {
                            model.textColor = UIColorFromRGB(0x42b5f8);
                        }
                    }
                    else
                    {
                        // 第二轮
                        if ((j == 0 && [self doJobProgress:@"leading"] == 2) ||
                            (j == 1 && [self doJobProgress:@"guardian"] == 2) ||
                            (j == 2 && [self doJobProgress:@"auditor"] == 2) ||
                            (j == 3 && [self doJobProgress:@"approver"] == 2))
                        {
                            model.textColor = UIColorFromRGB(0x42b5f8);
                        }
                    }
                }
                else
                {
                    if (gas.length > 0)
                    {
                        // 第一轮
                        if ((j == 0 && [self doJobProgress:@"gas"] > 0) ||
                            (j == 1 && [self doJobProgress:@"leading"] > 0) ||
                            (j == 2 && [self doJobProgress:@"guardian"] > 0) ||
                            (j == 3 && [self doJobProgress:@"auditor"] > 0) ||
                            (j == 4 && [self doJobProgress:@"approver"] > 0))
                        {
                            model.textColor = UIColorFromRGB(0x00a733);
                        }
                    }
                    else
                    {
                        // 第一轮
                        if ((j == 0 && [self doJobProgress:@"leading"] > 0) ||
                            (j == 1 && [self doJobProgress:@"guardian"] > 0) ||
                            (j == 2 && [self doJobProgress:@"auditor"] > 0) ||
                            (j == 3 && [self doJobProgress:@"approver"] > 0))
                        {
                            model.textColor = UIColorFromRGB(0x00a733);
                        }
                    }
                }
            }
            [list addObject:model];
        }
        [self.resultDataList addObject:list];
    }
    [self.mTableView reloadData];
}

#pragma mark - top点击事件
- (void)topClickAction:(NSString *)btnTitle
{
    WeakSelf(self)
    if ([@"btn_revoke" isEqualToString:btnTitle])
    {
        [X_HttpUtil apiRequest:@"job/cancelJob" param:@{@"jobId":@(self.jobId)} view:self.view successBlock:^(NSDictionary *resultData) {
            
            [self.view makeToast:LoadLanguagesKey(@"job_detail_undoSuccess")];
            [weakself jobDetailRequest];
            
            // 提交通知，列表界面刷新
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshJobListNote" object:nil];
        }];
    }
    else if ([@"btn_commit" isEqualToString:btnTitle])
    {
        if (_doScanTemp && _doCameraTemp)
        {
            NSDictionary *data = @{
                                   @"workId":@(self.jobId),
                                   @"code":self.detailData[@"work"][@"equipmentCode"],
                                   @"imgs":_imageStr,
                                   @"location":[ISSJobDetailTool getSubmitAddress:self]
                                   };
            [X_HttpUtil apiRequest:@"job/submitJob" param:data view:self.view successBlock:^(NSDictionary *resultData) {
                
                [self.view makeToast:LoadLanguagesKey(@"job_detail_submitSuccess")];
                [weakself jobDetailRequest];
                _doScanTemp = NO;
                _doCameraTemp = NO;
                //_imageStr = @"";
            }];
        }
        else
        {
            [self.view makeToast:LoadLanguagesKey(@"job_detail_doScanAndCamera")];
        }
    }
    else if ([@"btn_edit" isEqualToString:btnTitle])
    {
        ISSEditJobVC *vc = [[ISSEditJobVC alloc] init];
        vc.detailData = self.detailData;
        vc.successBlock = ^{
            [weakself jobDetailRequest];
        };
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [ISSPubfun modifyNavigationController:nav navColor:[UIColor whiteColor]];
        [self presentViewController:nav animated:YES completion:NULL];
    }
}

#pragma mark - 扫描、拍照点击事件
- (void)changeClickAction:(NSString *)btnTitle
{
    NSString *gas = [NSString noNullString:self.workData[@"gas"]];
    
    // 气体检测人存在
    if (gas.length > 0)
    {
        if (self.jobStatus >= 3)
        {
            // 直接扫码或者拍照
            if ([@"gas" isEqualToString:self.jobRole]) [self doClickAction:btnTitle time:2];
            else
            {
                if ([@"leading" isEqualToString:self.jobRole])
                {
                    // 只有气体检测人提交了才可以
                    if ([self doJobProgress:@"gas"] == 2) [self doClickAction:btnTitle time:2];
                    else [self.view makeToast:LoadLanguagesKey(@"job_detail_gasNoSubmit")];
                }
                else
                {
                    // 只有负责人提交了才可以
                    if ([self doJobProgress:@"leading"] == 2) [self doClickAction:btnTitle time:2];
                    else [self.view makeToast:LoadLanguagesKey(@"job_detail_leadingNoSubmit")];
                }
            }
        }
        else
        {
            // 直接扫码或者拍照
            if ([@"gas" isEqualToString:self.jobRole]) [self doClickAction:btnTitle time:1];
            else
            {
                if ([@"leading" isEqualToString:self.jobRole])
                {
                    // 只有气体检测人提交了才可以
                    if ([self doJobProgress:@"gas"] > 0) [self doClickAction:btnTitle time:1];
                    else [self.view makeToast:LoadLanguagesKey(@"job_detail_gasNoSubmit")];
                }
                else
                {
                    // 只有负责人提交了才可以
                    if ([self doJobProgress:@"leading"] > 0) [self doClickAction:btnTitle time:1];
                    else [self.view makeToast:LoadLanguagesKey(@"job_detail_leadingNoSubmit")];
                }
            }
        }
    }
    else
    {
        // 判断能不能扫码或者拍照
        if (self.jobStatus >= 3)
        {
            // 直接扫码或者拍照
            if ([@"leading" isEqualToString:self.jobRole]) [self doClickAction:btnTitle time:2];
            else
            {
                // 只有负责人提交了才可以
                if ([self doJobProgress:@"leading"] == 2) [self doClickAction:btnTitle time:2];
                else [self.view makeToast:LoadLanguagesKey(@"job_detail_leadingNoSubmit")];
            }
        }
        else
        {
            // 直接扫码或者拍照
            if ([@"leading" isEqualToString:self.jobRole]) [self doClickAction:btnTitle time:1];
            else
            {
                // 只有负责人提交了才可以
                if ([self doJobProgress:@"leading"] > 0) [self doClickAction:btnTitle time:1];
                else [self.view makeToast:LoadLanguagesKey(@"job_detail_leadingNoSubmit")];
            }
        }
    }
    
}

- (void)doClickAction:(NSString *)btnTitle time:(int)time
{
    if (time == 1)
    {
        if ([self doJobProgress:self.jobRole] > 0) [self.view makeToast:LoadLanguagesKey(@"job_detail_noReSubmit")];
        else [self goScanOrCamera:btnTitle];
    }
    else
    {
        if ([self doJobProgress:self.jobRole] == 2) [self.view makeToast:LoadLanguagesKey(@"job_detail_noReSubmit")];
        else [self goScanOrCamera:btnTitle];
    }
}

- (void)goScanOrCamera:(NSString *)btnTitle
{
    if ([@"btn_scan" isEqualToString:btnTitle]) [self goScanVC];
    else [self goCamera];
}

- (void)goScanVC
{
#if TARGET_IPHONE_SIMULATOR
    // 扫码完毕，准备打勾,scanInfo其实是nil，是为了与检维公用block
    [self showFlashLb:LoadLanguagesKey(@"job_detail_scanSuccess")];
    [_headerView afterScanOrCamera:10];
    _doScanTemp = YES;
    return;
#endif
    
    WeakSelf(self)
    ISSBaseScanVC *vc = [[ISSBaseScanVC alloc] init];
    vc.fromType = 0;
    vc.originalCode = self.detailData[@"work"][@"equipmentCode"];
    vc.resultBlock = ^(NSDictionary *scanInfo) {
        // 扫码完毕，准备打勾,scanInfo其实是nil，是为了与检维公用block
        [weakself showFlashLb:LoadLanguagesKey(@"job_detail_scanSuccess")];
        [_headerView afterScanOrCamera:10];
        _doScanTemp = YES;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)goCamera
{    
    WeakSelf(self)
    ISSJobDetailTakeCameraVC *vc = [[ISSJobDetailTakeCameraVC alloc] init];
    vc.resultBlock = ^(NSString *imageStr) {
        //拍照提交完毕，准备打勾
        [weakself showFlashLb:LoadLanguagesKey(@"job_detail_cameraSuccess")];
        [_headerView afterScanOrCamera:11];
        _doCameraTemp = YES;
        _imageStr = imageStr;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showFlashLb:(NSString *)msg
{
    if (!_flashLb)
    {
        //_flashLb = [UILabel mutLineLbWithX:15 y:CGRectGetMaxY(_topView.frame) width:Screen_Width - 30 fontSize:14 color:[UIColor describeColor_9a] text:msg];
        _flashLb = [UILabel oneLineLbWithX:0 y:CGRectGetMaxY(_topView.frame) width:Screen_Width fontSize:14 color:[UIColor describeColor_9a] text:msg];
        _flashLb.textAlignment = NSTextAlignmentCenter;
        _flashLb.backgroundColor = [UIColor whiteColor];
        _flashLb.alpha = 0;
        [self.view addSubview:_flashLb];
    }
    _flashLb.text = msg;
    
    NSString *language = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    bool isEn = [language hasPrefix:@"en"];
    if (!isEn) {
        _flashLb.attributedText = [ISSPubfun setLabelAttributed:_flashLb.text range:NSMakeRange(0, 5) color:UIColorFromRGB(0x1c63ff) font:_flashLb.font];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        _flashLb.alpha = 0;
        [UIView animateWithDuration:.3 animations:^{
            _flashLb.alpha = 1;
        }];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:.3 animations:^{
            _flashLb.alpha = 0;
        }];
    });
}

- (void)reloadTableWithRow:(NSInteger)row textColor:(UIColor *)textColor
{
    ISSJobDetailCellModel *model = self.resultDataList[1][row];
    model.textColor = textColor;
    [self.mTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:1]]
                           withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSInteger)doJobProgress:(NSString *)jobRole
{
    return [ISSJobDetailTool doJobProgress:self.detailData jobRole:jobRole];
}

@end
