//
//  ISSJobLookPhotoDetailVC.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSJobLookPhotoDetailVC : ISSBaseVC

@property (nonatomic,  copy) NSString *projectName;
@property (nonatomic,strong) NSDictionary *itemData;

@end
