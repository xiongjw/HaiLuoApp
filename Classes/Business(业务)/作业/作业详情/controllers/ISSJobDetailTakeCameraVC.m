//
//  ISSJobDetailTakeCameraVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/15.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobDetailTakeCameraVC.h"

#import "UIImageView+Create.h"

#import "AXChoosePhotoHelper.h"

#import "LBPhotoBrowserManager.h"

#import "TZImageManager.h"

#import "ISSUploadDeal.h"

@interface ISSJobDetailTakeCameraVC ()
{
    UIView *_photoView;
    
    NSMutableArray *_imagelist;
    
    NSInteger _uploadIndex;
}

@end

@implementation ISSJobDetailTakeCameraVC

- (void)submitAction
{
    if (_imagelist.count == 0) {
        return;
    }
    if (_imagelist.count == 1) {
        [[ISSUploadDeal sharedInstance] uploadImage:_imagelist[0] view:self.view.window successBlock:^(NSString *photoUrls) {
            if (_resultBlock) {
                _resultBlock(photoUrls);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    else
    {
        [[ISSUploadDeal sharedInstance] uploadImages:_imagelist view:self.view.window successBlock:^(NSString *photoUrls) {
            if (_resultBlock) {
                _resultBlock(photoUrls);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
}

- (void)takeCamera:(X_Button *)btn
{
    WeakSelf(self)
#if TARGET_IPHONE_SIMULATOR
    [[AXChoosePhotoHelper sharedInstance] chooseMorePhoto:self maxCount:3-_imagelist.count block:^(NSArray<UIImage *> *photos) {
        [_imagelist addObjectsFromArray:photos];
        [weakself addImageToPhotoView];
    }];
#elif TARGET_OS_IPHONE
    [[AXChoosePhotoHelper sharedInstance] pushImagePickerController:self block:^(UIImage *photo) {
        
        [_imagelist addObject:photo];
        [weakself addImageToPhotoView];
    }];
#endif
}

- (void)handleSingleTap:(UITapGestureRecognizer *)tap
{
    NSMutableArray *items = @[].mutableCopy;
    
    for (UIView *view in _photoView.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]])
        {
            UIImageView *imageView = (UIImageView *)view;
            LBPhotoLocalItem *item = [[LBPhotoLocalItem alloc]initWithImage:imageView.image frame:imageView.frame];
            [items addObject:item];
        }
    }
    // 这里只要你开心 可以无限addBlock
    [[LBPhotoBrowserManager defaultManager] showImageWithLocalItems:items selectedIndex:tap.view.tag - 100 fromImageViewSuperView:_photoView];
    /*
    [[[[LBPhotoBrowserManager defaultManager] showImageWithLocalItems:items selectedIndex:tap.view.tag - 100 fromImageViewSuperView:_photoView] addLongPressShowTitles:@[@"保存图片",@"取消"]] addTitleClickCallbackBlock:^(UIImage *image, NSIndexPath *indexPath, NSString *title) {
        LBPhotoBrowserLog(@"%@",title);
        //UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        //图片存入相册
        if ([@"保存" isEqualToString:title]) {
            [[TZImageManager manager] savePhotoWithImage:image completion:nil];
        }
    }];
     */
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress
{
    UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:LoadLanguagesKey(@"btn_alert") message:LoadLanguagesKey(@"job_detail_deletePhoto") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"点击了取消按钮");
    }];
    // 响应方法-相册
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:LoadLanguagesKey(@"btn_confirm") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        NSInteger tag = longPress.view.tag - 100;
        [_imagelist removeObjectAtIndex:tag];
        [self addImageToPhotoView];
    }];
    // 添加响应方式
    [actionSheetController addAction:cancelAction];
    [actionSheetController addAction:confirmAction];
    // 显示
    [self presentViewController:actionSheetController animated:YES completion:nil];
}

#pragma mark -选择图片之后展示
- (void)addImageToPhotoView
{
    [[_photoView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat x = 25;
    CGFloat width = 60;
    
    UIImageView *imageView = nil;
    X_Button *addBtn = nil;
    for (int i = 0; i < MIN(_imagelist.count + 1, 3); i++)
    {
        if (i == _imagelist.count && _imagelist.count < 3)
        {
            addBtn = [X_Button buttonWithType:UIButtonTypeCustom];
            addBtn.frame = CGRectMake(x, 0, 60, 60);
            addBtn.layer.borderWidth = 1;
            addBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [addBtn setImage:[UIImage imageNamed:@"job_detail_takeCamera"] forState:UIControlStateNormal];
            addBtn.tag = 100 + i;
            [addBtn addTarget:self action:@selector(takeCamera:) forControlEvents:UIControlEventTouchUpInside];
            [_photoView addSubview:addBtn];
        }
        else
        {
            imageView = [UIImageView imageView:CGRectMake(x, 0, width, width)
                                         image:_imagelist[i]
                                         scale:YES];
            imageView.tag = 100 + i;
            [_photoView addSubview:imageView];
            
            imageView.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
            [imageView addGestureRecognizer:singleTap];
            
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
            longPress.minimumPressDuration = 0.5;
            [imageView addGestureRecognizer:longPress];
        }
        
        x = CGRectGetMaxX(imageView.frame) + 30;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 130+322/2)];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    
    WeakSelf(self)
    X_Button *btn = [ISSNavUtil getImageBtn:6 clickBlock:^{
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
    [btn setLeft:15];
    [btn setTop:20];
    [topView addSubview:btn];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, Screen_Width-120, 44)];
    titleLb.text = LoadLanguagesKey(@"job_detail_camera_navTitle");
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    titleLb.textColor = [UIColor describeColor_61];
    [topView addSubview:titleLb];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 130, Screen_Width, .5)];
    lineView.backgroundColor = [UIColor sepLineColor];
    [topView addSubview:lineView];
    
    UILabel *alertLb = [UILabel mutLineLbWithX:25 y:CGRectGetMaxY(lineView.frame) + 20 width:Screen_Width - 50 fontSize:13 color:[UIColor describeColor_9a] text:LoadLanguagesKey(@"job_detail_camera_alert")];
    [topView addSubview:alertLb];
    
    _photoView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lineView.frame) + 60, Screen_Width, 60)];
    [topView addSubview:_photoView];
    
    _imagelist = [[NSMutableArray alloc] init];
    
    [self addImageToPhotoView];
    
    X_Button *submitBtn = [ISSCreateUIHelper createInputSubmitBtnWithTitle:LoadLanguagesKey(@"btn_confirm") posY:CGRectGetMaxY(topView.frame) + 100];
    [submitBtn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
