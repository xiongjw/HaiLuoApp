//
//  ISSJobLookPhotoDetailVC.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/16.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSJobLookPhotoDetailVC.h"

#import "ISSCollectionPhotoView.h"

@interface ISSJobLookPhotoDetailVC ()
{
    NSArray *_resultList;
}

@end

@implementation ISSJobLookPhotoDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 130)];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    
    WeakSelf(self)
    X_Button *btn = [ISSNavUtil getImageBtn:6 clickBlock:^{
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
    [btn setLeft:15];
    [btn setTop:20];
    [topView addSubview:btn];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(30, 90, Screen_Width-120, 44)];
    //titleLb.text = LoadLanguagesKey(@"job_base_projectName");
    titleLb.text = self.projectName;
    titleLb.font = [UIFont boldSystemFontOfSize:15];
    titleLb.textColor = [UIColor titleColor_26];
    [topView addSubview:titleLb];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 130, Screen_Width, .5)];
    lineView.backgroundColor = [UIColor sepLineColor];
    [topView addSubview:lineView];
    
    
    _resultList = @[
                      @{
                          @"title":FormatString(@"提交时间：%@",[ISSDataDeal getDateStrFromResponseData:[self.itemData[@"createTime"] longLongValue]]),
                          @"imageList":[ISSDataDeal getNewImageList:[[NSString noNullString:self.itemData[@"imgs"]] componentsSeparatedByString:@","]]
                          }
                      ];
    
    CGFloat posY = CGRectGetMaxY(lineView.frame);
    UILabel *alertLb = nil;
    ISSCollectionPhotoView *photoView = nil;
    UIView *line = nil;
    for (int i = 0; i < _resultList.count; i++)
    {
        alertLb = [UILabel oneLineLbWithX:25 y:posY + 20 fontSize:13 color:[UIColor describeColor_9a] text:_resultList[i][@"title"]];
        [topView addSubview:alertLb];
        
        photoView = [[ISSCollectionPhotoView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(alertLb.frame) + 20, Screen_Width, 60) imageList:_resultList[i][@"imageList"]];
        [topView addSubview:photoView];
        
        if (i < _resultList.count - 1) {
            line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(photoView.frame) + 20, Screen_Width, 1)];
            line.backgroundColor = [UIColor sepLineColor];
            [topView addSubview:line];
        }
        posY = CGRectGetMaxY(photoView.frame) + 20;
    }
    [topView setHeight:CGRectGetMaxY(photoView.frame) + 20];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
