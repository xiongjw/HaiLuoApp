//
//  ISSUserData.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSUserData : NSObject

// 登录返回
//@property (nonatomic, copy) NSString *token;

// 查询用户信息
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *realName;
@property (nonatomic, copy) NSString *userRole;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *orgName;
@property (nonatomic, copy) NSString *orgId;

//用户权限
@property (nonatomic,assign) BOOL menus_privilege_yh;//有无隐患菜单权限
@property (nonatomic,assign) BOOL menus_privilege_zy;//有无作业菜单权限
@property (nonatomic,assign) BOOL menus_privilege_jx;//有无检维菜单权限

@property (nonatomic,assign) BOOL operation_privilege_yh_add;//有无隐患新增权限（包括违章）
@property (nonatomic,assign) BOOL operation_privilege_yh_detailView;//有无隐患详情查看权限（包括违章），安全待定
@property (nonatomic,assign) BOOL operation_privilege_zy_add;//有无作业新增权限
//@property (nonatomic,assign) BOOL operation_privilege_zy_revoke;//有无作业撤销权限
//@property (nonatomic,assign) BOOL operation_privilege_zy_edit;//有无作业修改权限

+ (ISSUserData *)sharedInstance;

- (void)saveLoginData:(NSDictionary *)data;

- (void)setCacheLoginData:(NSDictionary *)data;

- (void)clearUserData;

- (void)queryUserInfo;
- (void)queryUserInfo:(UIView *)aView successBlock:(void (^)(void))successBlock;

@end
