//
//  ISSUserData.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSUserData.h"

#define ISSUserTokenKey @"ISSUserTokenKey"
#define ISSLoginDataKey @"ISSLoginDataKey"
//#define ISSUserDataKey @"ISSUserDataKey"

@implementation ISSUserData

+ (ISSUserData *)sharedInstance
{
    static ISSUserData *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[ISSUserData alloc] init];
    });
    return instance;
}

- (NSDictionary *)getNewData:(NSDictionary *)data
{
    if (data[@"menus_privilege"] && data[@"operation_privilege"])
        return @{@"menus_privilege":data[@"menus_privilege"],@"operation_privilege":data[@"operation_privilege"]};
    return @{
             @"menus_privilege":@{@"yh":@(1),@"zy":@(1),@"jx":@(1)},
             @"operation_privilege":@{@"yh_add":@(1),@"yh_detailView":@(1),@"zy_add":@(1)}
             };
}

- (void)saveLoginData:(NSDictionary *)data
{
    [ISSDataStorage saveObject:data[@"Access-Token"] key:ISSUserTokenKey];
    
    data = [self getNewData:data];
    //[ISSDataStorage saveObject:data key:ISSLoginDataKey];
    [ISSDataStorage saveObject:data key:ISSLoginDataKey];
    
    NSDictionary *menus_privilege = data[@"menus_privilege"];
    NSDictionary *operation_privilege = data[@"operation_privilege"];
    self.menus_privilege_yh = [menus_privilege[@"yh"] boolValue];
    self.menus_privilege_zy = [menus_privilege[@"zy"] boolValue];
    self.menus_privilege_jx = [menus_privilege[@"jx"] boolValue];
    
    self.operation_privilege_yh_add = [operation_privilege[@"yh_add"] boolValue];
    self.operation_privilege_yh_detailView = [operation_privilege[@"yh_detailView"] boolValue];
    self.operation_privilege_zy_add = [operation_privilege[@"zy_add"] boolValue];
    //self.operation_privilege_zy_revoke = [operation_privilege[@"zy_repeal"] boolValue];
    //self.operation_privilege_zy_edit = [operation_privilege[@"zy_xiuGai"] boolValue];
}

- (void)setCacheLoginData:(NSDictionary *)data
{
    NSDictionary *menus_privilege = data[@"menus_privilege"];
    NSDictionary *operation_privilege = data[@"operation_privilege"];
    self.menus_privilege_yh = [menus_privilege[@"yh"] boolValue];
    self.menus_privilege_zy = [menus_privilege[@"zy"] boolValue];
    self.menus_privilege_jx = [menus_privilege[@"jx"] boolValue];
    
    self.operation_privilege_yh_add = [operation_privilege[@"yh_add"] boolValue];
    self.operation_privilege_yh_detailView = [operation_privilege[@"yh_detailView"] boolValue];
    self.operation_privilege_zy_add = [operation_privilege[@"zy_add"] boolValue];
    //self.operation_privilege_zy_revoke = [operation_privilege[@"zy_repeal"] boolValue];
    //self.operation_privilege_zy_edit = [operation_privilege[@"zy_xiuGai"] boolValue];
}

- (void)clearUserData
{
    [ISSDataStorage removeObject:ISSUserTokenKey];
    [ISSDataStorage removeObject:ISSLoginDataKey];
    
    [self setValue:@"" forKey:@"userId"];
    [self setValue:@"" forKey:@"realName"];
    [self setValue:@"" forKey:@"userRole"];
    [self setValue:@"" forKey:@"sex"];
    [self setValue:@"" forKey:@"userName"];
    [self setValue:@"" forKey:@"phoneNum"];
    [self setValue:@"" forKey:@"companyId"];
    [self setValue:@"" forKey:@"companyName"];
    [self setValue:@"" forKey:@"orgName"];
    [self setValue:@"" forKey:@"orgId"];
    
    self.menus_privilege_yh = NO;
    self.menus_privilege_zy = NO;
    self.menus_privilege_jx = NO;
    
    self.operation_privilege_yh_add = NO;
    self.operation_privilege_yh_detailView = NO;
    self.operation_privilege_zy_add = NO;
    //self.operation_privilege_zy_revoke = NO;
    //self.operation_privilege_zy_edit = NO;
    
    //[ISSDataStorage removeObject:ISSUSerDataKey];
}

- (void)queryUserInfo
{
    @synchronized(self) {
        dispatch_group_t group = dispatch_group_create();
        dispatch_queue_t queue = dispatch_queue_create([@"get.configList" UTF8String], NULL);
        dispatch_group_async(group, queue, ^{
            // 并行执行的线程
            [self queryUserInfo:nil successBlock:nil];
        });
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            
        });
    }
    
}

- (void)queryUserInfo:(UIView *)aView successBlock:(void (^)(void))successBlock
{
    WeakSelf(self)
    if (aView)
    {
        [X_HttpUtil apiRequest:@"info/getMyInfo" param:@{} view:aView successBlock:^(NSDictionary *resultData) {
            NSLog(@"resultData:%@",resultData);
            [weakself dealUserResultData:resultData];
            if (successBlock) {
                successBlock();
            }
        } failureBlock:^(NSDictionary *resultData) {
            
        }];
    }
    else
    {
        [X_HttpUtil apiRequest:@"info/getMyInfo" param:@{} view:nil showErrorMessage:NO successBlock:^(NSDictionary *resultData) {
            NSLog(@"resultData:%@",resultData);
            
            [weakself dealUserResultData:resultData];
            if (successBlock) {
                successBlock();
            }
            
        } failureBlock:^(NSDictionary *resultData) {
            
        }];
    }
}

- (void)dealUserResultData:(NSDictionary *)resultData
{
    NSDictionary *data = resultData[@"data"];
    /*
    NSDictionary *cacheData = @{
                                @"userId":[NSString noNullString:data[@"id"]],
                                @"realName":[NSString noNullString:data[@"realName"]],
                                @"userRole":[NSString noNullString:data[@"userRole"]],
                                @"sex":[NSString noNullString:data[@"sex"]],
                                @"userName":[NSString noNullString:data[@"userName"]],
                                @"phoneNum":[NSString noNullString:data[@"phoneNum"]],
                                @"companyName":[NSString noNullString:data[@"companyName"]],
                                @"orgName":[NSString noNullString:data[@"orgName"]],
                                @"orgId":[NSString noNullString:data[@"orgId"]],
                                };
     */
    //保存相关数据
    [self setValue:[NSString noNullString:data[@"id"]] forKey:@"userId"];
    [self setValue:[NSString noNullString:data[@"realName"]] forKey:@"realName"];
    [self setValue:[NSString noNullString:data[@"userRole"]] forKey:@"userRole"];
    [self setValue:[NSString noNullString:data[@"sex"]] forKey:@"sex"];
    [self setValue:[NSString noNullString:data[@"userName"]] forKey:@"userName"];
    [self setValue:[NSString noNullString:data[@"phoneNum"]] forKey:@"phoneNum"];
    [self setValue:[NSString noNullString:data[@"factoryId"]] forKey:@"companyId"];
    [self setValue:[NSString noNullString:data[@"companyName"]] forKey:@"companyName"];
    [self setValue:[NSString noNullString:data[@"orgName"]] forKey:@"orgName"];
    [self setValue:[NSString noNullString:data[@"orgId"]] forKey:@"orgId"];
    
//    [ISSDataStorage saveObject:data key:ISSUSerDataKey];
}

@end
