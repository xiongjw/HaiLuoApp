//
//  ISSDataDeal.m
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSDataDeal.h"

@implementation ISSDataDeal

//时间戳转换成字符串

+ (NSString *)getDateStr:(NSTimeInterval)secs format:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:secs]];
}

+ (NSString *)getDateStrFromResponseData:(long long)secs
{
    if (secs == 0) {
        return @"";
    }
    return [self getDateStrFromResponseData:secs format:AXDateMinuteFormat];
}

+ (NSString *)getDateStrFromResponseData:(long long)secs format:(NSString *)format
{
    return [self getDateStr:secs / 1000.0 format:format];
}

//
+ (long long)getTimeStamp:(NSString *)timeStr
{
    return [self getTimeStamp:timeStr format:AXDateMinuteFormat];
}

+ (long long)getTimeStamp:(NSString *)timeStr format:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString:timeStr];
    long long timestamp = [date timeIntervalSince1970] * 1000;
    return timestamp;
}

+ (long long)getCurrentTimeStamp
{
    long long timestamp = [[NSDate date] timeIntervalSince1970] * 1000;
    return timestamp;
}

+ (NSString *)getCurrentTimeStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSArray *)getNewImageList:(NSArray *)imageList
{
    NSMutableArray *urlStrings = [NSMutableArray new];
    for (int i = 0; i < imageList.count; i++)
    {
        NSString *urlString = imageList[i];
        if (urlString.length > 0)
        {
            // 怕安卓不转
            if ([urlString containsString:@"\\"]) {
                urlString = [urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            }
            [urlStrings addObject:LoadImageUrl(urlString)];
        }
    }
    return urlStrings.copy;
}

+ (NSString *)getFirstImageUrl:(NSArray *)imageList
{
    NSString *urlString = @"";
    if (imageList.count > 0) {
        urlString = imageList[0];
        if ([urlString containsString:@"\\"]) {
            urlString = [urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        }
        urlString = LoadImageUrl(urlString);
    }
    return urlString;
}

@end
