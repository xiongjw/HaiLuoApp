//
//  ISSDataDeal.h
//  HaiLuoApp
//
//  Created by xiongjw on 2018/4/19.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSDataDeal : NSObject

+ (NSString *)getDateStrFromResponseData:(long long)secs;
+ (NSString *)getDateStrFromResponseData:(long long)secs format:(NSString *)format;

+ (long long)getTimeStamp:(NSString *)timeStr;
+ (long long)getTimeStamp:(NSString *)timeStr format:(NSString *)format;
+ (long long)getCurrentTimeStamp;
+ (NSString *)getCurrentTimeStr;

+ (NSArray *)getNewImageList:(NSArray *)imageList;
+ (NSString *)getFirstImageUrl:(NSArray *)imageList;

@end
