//
//  ISSCreateUIHelper.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSCreateUIHelper : NSObject

+ (X_Button *)createSubmitBtnWithTitle:(NSString *)btnTitle posY:(CGFloat)posY;

+ (X_Button *)createInputSubmitBtnWithTitle:(NSString *)btnTitle posY:(CGFloat)posY;
//********************创建TextField***************************/

+ (UITextField *)makeTextField:(id)delegate
                     withFrame:(CGRect)frame
                     maxLength:(NSInteger)maxLength;

+ (UITextField *)makeTextField:(id)delegate
                     withFrame:(CGRect)frame
                andPlaceholder:(NSString *)holder
                     maxLength:(NSInteger)maxLength;

+ (UITextField *)makeTextField:(id)delegate
                     withFrame:(CGRect)frame
                        andTag:(NSInteger)tag
                andPlaceholder:(NSString *)holder
                     maxLength:(NSInteger)maxLength;

@end
