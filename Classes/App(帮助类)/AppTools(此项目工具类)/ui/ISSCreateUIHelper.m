//
//  ISSCreateUIHelper.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCreateUIHelper.h"

@implementation ISSCreateUIHelper

+ (X_Button *)createSubmitBtnWithTitle:(NSString *)btnTitle posY:(CGFloat)posY
{
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeSystem];
    btn.frame = CGRectMake(35, posY, Screen_Width - 70, 50);
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    btn.backgroundColor = UIColorFromRGB(0x1c63ff);
    //[btn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x1c63ff)] forState:UIControlStateNormal];
    //[btn setBackgroundImage:[UIImage imageWithColor:RGB(36, 104, 246)] forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:17];
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.numberOfLines = 0;
    return btn;
}

+ (X_Button *)createInputSubmitBtnWithTitle:(NSString *)btnTitle posY:(CGFloat)posY
{
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeSystem];
    btn.frame = CGRectMake(60, posY, Screen_Width - 120, 40);
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    btn.backgroundColor = UIColorFromRGB(0x1c63ff);
    //[btn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x1c63ff)] forState:UIControlStateNormal];
    //[btn setBackgroundImage:[UIImage imageWithColor:RGB(36, 104, 246)] forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[btn setTitleColor:RGB(36, 104, 246) forState:UIControlStateHighlighted];
    btn.titleLabel.font = [UIFont systemFontOfSize:17];
    btn.layer.cornerRadius = 20;
    //btn.layer.masksToBounds = YES;
    
    btn.layer.shadowOffset =  CGSizeMake(0, 6);
    btn.layer.shadowOpacity = 0.3;
    btn.layer.shadowColor =  UIColorFromRGB(0x1c63ff).CGColor;
    return btn;
}

//********************创建TextField***************************/

+ (UITextField *)makeTextField:(id)delegate
                     withFrame:(CGRect)frame
                     maxLength:(NSInteger)maxLength
{
    return [self makeTextField:delegate withFrame:frame andPlaceholder:@"" maxLength:maxLength];
}

+ (UITextField *)makeTextField:(id)delegate
                     withFrame:(CGRect)frame
                andPlaceholder:(NSString *)holder
                     maxLength:(NSInteger)maxLength
{
    return [self makeTextField:delegate withFrame:frame andTag:0 andPlaceholder:holder maxLength:maxLength];
}

+ (UITextField *) makeTextField:(id)delegate
                      withFrame:(CGRect)frame
                         andTag:(NSInteger)tag
                 andPlaceholder:(NSString *)holder
                      maxLength:(NSInteger)maxLength
{
    
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.tintColor = [UIColor describeColor_61];
    textField.placeholder = holder;
    
    //[textField setAttributedPlaceholder:<#(NSAttributedString * _Nullable)#>];ios10
    [textField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [UIColor describeColor_61];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;  //垂直居中
    textField.font = [UIFont systemFontOfSize:14];
    
    textField.backgroundColor = [UIColor clearColor];
    textField.autocorrectionType = UITextAutocorrectionTypeNo;    // no auto correction support
    textField.keyboardType = UIKeyboardTypeDefault;
    
    textField.returnKeyType = UIReturnKeyDone;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;    // has a clear 'x' button to the right
    
    textField.tag = tag;
    
    [textField setAccessibilityLabel:@"textField"];
    
    //textField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    textField.text = @"";
    if (delegate) textField.delegate = delegate;    // let us be the delegate so we know when the keyboard's "Done" button is pressed
    
    if (maxLength > 0) {
        [textField limitTextLength:(int)maxLength];
    }
    
    return textField;
}

@end
