//
//  ISSAppEternal.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#ifndef ISSAppEternal_h
#define ISSAppEternal_h


#define DefaultCenter                   [NSNotificationCenter defaultCenter]

#define KLogout                         @"logout"

//占位图
#define PlaceholderImage_HeadPortrait               [UIImage imageNamed:@"placeHolder_headportrait"]

#endif /* ISSAppEternal_h */
