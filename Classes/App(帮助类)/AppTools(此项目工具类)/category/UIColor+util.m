//
//  UIColor+util.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/2.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "UIColor+util.h"

@implementation UIColor (util)

+ (UIColor *)themeColor
{
    return UIColorFromRGB(0x18287a);
}

+ (UIColor *)titleColor_26
{
    return UIColorFromRGB(0x262626);
}

+ (UIColor *)describeColor_9a
{
    return UIColorFromRGB(0x9a9a9a);
}

+ (UIColor *)describeColor_61
{
    return UIColorFromRGB(0x616161);
}

+ (UIColor *)describeColor_66
{
    return UIColorFromRGB(0x666666);
}

+ (UIColor *)borderColor
{
    return UIColorFromRGB(0xeeeeee);
}

+ (UIColor *)sepLineColor
{
    return UIColorFromRGB(0xd9d9d9);
}

@end
