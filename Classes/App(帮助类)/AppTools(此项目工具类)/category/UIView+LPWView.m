//
//  UIView+LPWView.m
//  ModuleProject
//
//
//  Created by issuser on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//


#import "UIView+LPWView.h"

@implementation UIView (LPWView)


/*
 渐变色
 
 beginPoint     渐变起始点
 endPoint       渐变起结束点
 beginColor     渐变开始颜色
 endColor       渐变结束颜色
 size           渐变色的大小，就是视图的size

*/

-(void)gradientBgColor:(CGPoint)beginPoint endPoint:(CGPoint)endPoint beginColor:(UIColor *)beginColor endColor:(UIColor *)endColor size:(CGSize)size cornerRadius:(NSInteger)cornerRadius{
    
    CAGradientLayer *colorLayer = [CAGradientLayer layer];
    
    colorLayer.frame = CGRectMake(0, 0, size.width,  size.height);
    
    colorLayer.colors = @[(id)beginColor.CGColor,(id)endColor.CGColor];
    
    colorLayer.startPoint = CGPointMake(beginPoint.x/size.width, beginPoint.y/size.height) ;
    
    colorLayer.endPoint = CGPointMake(endPoint.x/size.width, endPoint.y/size.height);
    
    colorLayer.cornerRadius = cornerRadius;
    
    [self.layer insertSublayer:colorLayer atIndex:0];
    
    
}



/*
 切圆角带边框
 
 radious        圆角尺寸
 borWidth       边框宽度
 borColor       边框颜色
 
 */

- (void)cutRadiousWithBoreder:(CGFloat)radious borWidth:(CGFloat)borWidth borColor:(UIColor *)borColor{
    
    [self cutRadiousAction:radious];
    
    self.layer.borderColor = borColor.CGColor;
    
    self.layer.borderWidth = borWidth;
    
}



/*
 切圆角不带边框
 
 radious        圆角尺寸
 
 */
- (void)cutRadiousAction:(CGFloat)radious {
    
    self.layer.cornerRadius = radious;
    
    self.layer.masksToBounds = true;
}

@end
