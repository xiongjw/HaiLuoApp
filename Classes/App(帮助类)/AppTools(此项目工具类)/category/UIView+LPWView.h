//
//  UIView+LPWView.h
//  ModuleProject
//
//
//  Created by issuser on 2018/4/10.
//  Copyright © 2018年 xiongjw. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UIView (LPWView)

- (void)cutRadiousWithBoreder:(CGFloat)radious borWidth:(CGFloat)borWidth borColor:(UIColor *)borColor;

- (void)cutRadiousAction:(CGFloat)radious;

-(void)gradientBgColor:(CGPoint)beginPoint endPoint:(CGPoint)endPoint beginColor:(UIColor *)beginColor endColor:(UIColor *)endColor size:(CGSize)size cornerRadius:(NSInteger)cornerRadius;

@end
