//
//  UIColor+util.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/2.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (util)

+ (UIColor *)themeColor;

+ (UIColor *)titleColor_26;

+ (UIColor *)describeColor_9a;

+ (UIColor *)describeColor_61;
+ (UIColor *)describeColor_66;

+ (UIColor *)borderColor;

+ (UIColor *)sepLineColor;

@end
